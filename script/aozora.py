#!/usr/bin/python

# Copyright (c) 2022 Azel
#
# quantimg is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# quantimg is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------
# 青空文庫テキストを txtnovel のフォーマットに変換。
# 通常は Shift-JIS として扱う。
# aozora.py [-c ENCODING] [-o DIR] <txtfile...>
#  "txtnovel-<filename>" で出力される。

import sys
import re
import os

if len(sys.argv) < 2:
	print('aozora.py [-c ENCODING] [-o DIR] <txtfile...>')
	sys.exit(0)

opt_enc = 'cp932'
opt_dir = ''

args = []
pos = 1
arglen = len(sys.argv)

while pos < arglen:
	o = sys.argv[pos]
	
	if o == "-c":
		opt_enc = sys.argv[pos + 1]
		pos += 1
	elif o == "-o":
		opt_dir = sys.argv[pos + 1]
		pos += 1
	else:
		args.append(o)

	pos += 1

#----

num_map = str.maketrans({chr(ord('０') + i): chr(ord('0') + i) for i in range(10)})

def convert_num(s):
	global num_map
	return s.translate(num_map)

# N字下げ
def rep_line_jisage(m):
	return "@ihead[" + convert_num(m.group(1)) + ",line]"

# ここからN字下げ
def rep_jisage(m):
	return "@ihead[" + convert_num(m.group(1)) + "];"

# ここからN字下げ、折り返し
def rep_jisage2(m):
	return "@ihead[" + convert_num(m.group(1)) + "," + convert_num(m.group(2)) + "];"

# ここから天付き、折り返し
def rep_jisage3(m):
	return "@ihead[0," + convert_num(m.group(1)) + "];"

# 地からN字上げ
def rep_jiage(m):
	return "@foot[" + convert_num(m.group(1)) + ",line]"

# ここから地からN字上げ
def rep_jiage2(m):
	return "@foot[" + convert_num(m.group(1)) + "];"

# 縦中横
def rep_ty(m):
	if len(m.group(1)) < 4:
		return '@ty[' + m.group(1) + ']'
	else:
		return m.group(1)

# JIS コード
def rep_jis(m):
	d = b'\x1b$('

	if m.group(1) == '1':
		d += b'O'
	else:
		d += b'P'

	d += bytes([0x20 + int(m.group(2))])
	d += bytes([0x20 + int(m.group(3))])

	return d.decode("iso2022_jp_3")

# Unicode
def rep_unicode(m):
	return chr(int(m.group(1),base=16))

# 画像
def rep_image(m):
	fname = m.group(1)

	pos = fname.find('、')
	if pos != -1:
		fname = fname[:pos]
	
	return "@img[file=" + fname + "];"

#---------

class Convert:
	REPDAT = (
	('［＃本文終わり］', ''),
	('［＃改ページ］', '@np;'),
	('［＃改丁］', '@nt;'),
	('［＃改見開き］', '@nm;'),
	('［＃改段］', '@nd;'),
	('［＃ここで字下げ終わり］', '@ihead;'),
	('［＃地付き］','@foot[0,line]'),
	('［＃ここから地付き］', '@foot[0];'),
	('［＃ここで地付き終わり］', '@foot;'),
	('［＃ここで字上げ終わり］', '@foot;'),
	('［＃ページの左右中央］', '@body-align[center,page];'),
	('［＃大見出し］','@cap1;'),
	('［＃中見出し］','@cap2;'),
	('［＃小見出し］','@cap3;'),
	('［＃大見出し終わり］',''),
	('［＃中見出し終わり］',''),
	('［＃小見出し終わり］',''),
	('［＃傍点終わり］','@kt[]'),
	('［＃白ゴマ傍点終わり］','@kt[]'),
	('［＃丸傍点終わり］','@kt[]'),
	('［＃白丸傍点終わり］','@kt[]'),
	('［＃傍線］','@bs[line]'),
	('［＃傍線終わり］','@bs[]'),
	('［＃二重傍線］','@bs[double]'),
	('［＃二重傍線終わり］','@bs[]'),
	('［＃横組み］',''),
	('［＃横組み終わり］',''),
	('［＃ここから横組み］',''),
	('［＃ここで横組み終わり］','')
	)

	def __init__(self):
		self.caption_map = str.maketrans({'大':'1', '中':'2', '小':'3'})

	def readfile(self,fname,enc):
		global opt_dir

		f = open(fname, 'rb')
		dat = f.read()
		f.close()

		try:
			self.dat = dat.decode(enc)
		except UnicodeDecodeError:
			self.dat = dat.decode('utf_8')

		if enc == 'cp932':
			self.dat = self.dat.replace('～', '〜')

		self.flags = {}

		self.outname = "txtnovel-" + os.path.basename(fname)
		self.outname = os.path.join(opt_dir, self.outname)

	def output(self):
		dat = '@font[base]{ file:/*フォントファイル*/; }\n@layout[base]{}\n\n'

		if 'b' in self.flags:
			dat += '@kenten[b]{ char:﹅ }\n'

		if 'sb' in self.flags:
			dat += '@kenten[sb]{ char:﹆ }\n'

		if 'm' in self.flags:
			dat += '@kenten[m]{ char:・ }\n'

		if 'sm' in self.flags:
			dat += '@kenten[sm]{ char:○ }\n'

		self.dat = dat + '\n@start-body;\n' + self.dat
	
		f = open(self.outname, 'wt')
		f.write(self.dat)
		f.close()

	def run(self):
		dat = self.dat
	
		for s,d in self.REPDAT:
			dat = dat.replace(s, d)

		dat = re.sub(r'｜([^《]+?)《([^》]+?)》', r'@gr[\1|\2]', dat)
		dat = re.sub(r'［＃([０-９]+?)字下げ］', rep_line_jisage, dat)
		dat = re.sub(r'［＃ここから([０-９]+?)字下げ］', rep_jisage, dat)
		dat = re.sub(r'［＃ここから([０-９]+?)字下げ、折り返して([０-９]+?)字下げ］', rep_jisage2, dat)
		dat = re.sub(r'［＃ここから改行天付き、折り返して([０-９]+?)字下げ］', rep_jisage3, dat)
		dat = re.sub(r'［＃地から([０-９]+?)字上げ］', rep_jiage, dat)
		dat = re.sub(r'［＃ここから地から([０-９]+?)字上げ］', rep_jiage2, dat)
		dat = re.sub(r'［＃[^（］]*?（([^）］]+?)）入る］', rep_image, dat)
		dat = re.sub(r'［＃縦中横］([^［］]+?)［＃縦中横終わり］', r'@ty[\1]', dat)
		dat = re.sub(r'［＃「[^」］]+?」は横組み］', '', dat)
		dat = re.sub(r'([!?]{2,3})', r'@typ[\1]', dat)
		dat = re.sub(r'([^゛])゛', r'@dk[\1]', dat) 

		if dat.find('［＃傍点］') != -1:
			self.flags['b'] = True
			dat = dat.replace('［＃傍点］', '@kt[b]')

		if dat.find('［＃白ゴマ傍点］') != -1:
			self.flags['sb'] = True
			dat = dat.replace('［＃白ゴマ傍点］', '@kt[sb]')

		if dat.find('［＃丸傍点］') != -1:
			self.flags['m'] = True
			dat = dat.replace('［＃丸傍点］', '@kt[m]')

		if dat.find('［＃白丸傍点］') != -1:
			self.flags['sm'] = True
			dat = dat.replace('［＃白丸傍点］', '@kt[sm]')

		self.dat = dat

		self._ruby()
		self._ruby_combine()
		self._caption()
		self._back_str('に傍点', '@kt[b]', '@kt[]', 'b')
		self._back_str('に白ゴマ傍点', '@kt[sb]', '@kt[]', 'sb')
		self._back_str('に丸傍点', '@kt[m]', '@kt[]', 'm')
		self._back_str('に白丸傍点', '@kt[sm]', '@kt[]', 'sm')
		self._back_str('に傍線', '@bs[line]', '@bs[]', None)
		self._back_str('に二重傍線', '@bs[double]', '@bs[]', None)
		self._back_str('は縦中横', '@ty[', ']', None)

		self.dat = re.sub(r'※［＃[^、］]+?、第\d水準(\d)-(\d+)-(\d+)］', rep_jis, self.dat)
		self.dat = re.sub(r'※［＃[^、］]+?、(\d)-(\d+)-(\d+)］', rep_jis, self.dat)
		self.dat = re.sub(r'※［＃[^、］]+?、U\+([0-9a-fA-F]+)[^］]*?］', rep_unicode, self.dat)

		self.dat = re.sub(r'［＃([^］]+?)］', r'/*\1*/', self.dat)
		self.dat = re.sub(r'^[ 　]+?@cap', r'@cap', self.dat, flags=re.M)

	# ルビ
	def _ruby(self):
		pos = 0
		dat = ""
		mo = re.compile(r'《([^》]+?)》')
		mo_kanji = re.compile(r'[\u2E80-\u2FDF\u3005-\u3007\u3400-\u4DBF\u4E00-\u9FFF\uF900-\uFAFF\U00020000-\U0002EBEF々仝〆〇ヶ〻]+$')

		while 1:
			m = mo.search(self.dat[pos:])
			if not m:
				if pos == 0: return
				break

			sr = m.group(1)
			mpos = pos + m.start(0)

			if ord(self.dat[mpos - 1]) < 255:
				# ASCII文字
				top = mpos - 1
				while top >= 0 and self.dat[top] != ' ' and self.dat[top] != '\n':
					top -= 1

				top += 1
			else:
				# 漢字
				top = mpos - 1
				while top >= 0 and self.dat[top] != '\n':
					m2 = mo_kanji.match(self.dat[top])
					if not m2: break
					top -= 1

				top += 1

			dat += self.dat[pos:top] + '@gr[' + self.dat[top:mpos] + '|' + sr + ']'

			pos += m.end(0)

		self.dat = dat + self.dat[pos:]

	# ルビ結合
	def _ruby_combine(self):
		pos = 0
		dat = ""
		mo = re.compile(r'@gr\[([^]]+?)\]')

		while 1:
			m = mo.search(self.dat[pos:])
			if not m:
				if pos == 0: return
				break

			dat += self.dat[pos:pos + m.start(0)]

			instr = m.group(1)
			npos = pos + m.end(0)

			while 1:
				m = mo.match(self.dat[npos:])
				if not m: break

				instr += '|' + m.group(1)
				npos += m.end(0)

			dat += '@gr[' + instr + ']'
			pos = npos

		self.dat = dat + self.dat[pos:]

	# 見出し
	def _caption(self):
		pos = 0
		dat = ""
		mo = re.compile(r'［＃「([^」］]+?)」は([大中小])見出し］')

		while 1:
			m = mo.search(self.dat[pos:])
			if not m:
				if pos == 0: return
				break

			s = m.group(1)
			tp = m.group(2).translate(self.caption_map)
			toppos = pos + m.start(0)

			top = toppos - len(s)
			dat += self.dat[pos:top] + '@cap' + tp + ';' + self.dat[top:toppos]

			pos += m.end(0)

		self.dat = dat + self.dat[pos:]

	# "「..」.."
	def _back_str(self,after,repb,repa,flagstr):
		pos = 0
		dat = ""
		mo = re.compile(r'［＃「([^」］]+?)」' + after + '］')

		while 1:
			m = mo.search(self.dat[pos:])
			if not m:
				if pos == 0: return
				break

			s = m.group(1)
			toppos = pos + m.start(0)
			top = toppos - len(s)

			dat += self.dat[pos:top] + repb + self.dat[top:toppos] + repa

			pos += m.end(0)

		self.dat = dat + self.dat[pos:]

		if pos != 0 and flagstr:
			self.flags[flagstr] = True
	
#-------

cv = Convert()

for fname in args:
	cv.readfile(fname, opt_enc)
	cv.run()
	cv.output()
