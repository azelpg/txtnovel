/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

extern const uint8_t g_cmpstring[];

#define CMPSTR_POS__ 0x0000
#define CMPSTR_POS_DEF 0x0004
#define CMPSTR_POS_FONT 0x0008
#define CMPSTR_POS_FULLWIDTH_SET 0x0010
#define CMPSTR_POS_KENTEN 0x0020
#define CMPSTR_POS_INCLUDE 0x0028
#define CMPSTR_POS_IMAGE_TYPE 0x0030
#define CMPSTR_POS_LAYOUT 0x003C
#define CMPSTR_POS_OUTPUT 0x0044
#define CMPSTR_POS_PAPER 0x004C
#define CMPSTR_POS_PDF 0x0054
#define CMPSTR_POS_PAGE_TITLE 0x0058
#define CMPSTR_POS_PAGE_NUMBER 0x0064
#define CMPSTR_POS_START_BODY 0x0070
#define CMPSTR_POS_ALIGN 0x007C
#define CMPSTR_POS_BACKGROUND 0x0084
#define CMPSTR_POS_BODY_ALIGN 0x0090
#define CMPSTR_POS_CAPTION1 0x009C
#define CMPSTR_POS_CAPTION2 0x00A8
#define CMPSTR_POS_CAPTION3 0x00B4
#define CMPSTR_POS_DASH_WIDTH 0x00C0
#define CMPSTR_POS_GSUB 0x00CC
#define CMPSTR_POS_INDENT 0x00D4
#define CMPSTR_POS_INDEX 0x00DC
#define CMPSTR_POS_LINE 0x00E4
#define CMPSTR_POS_MODE 0x00EC
#define CMPSTR_POS_PROCESS 0x00F4
#define CMPSTR_POS_RUBY 0x00FC
#define CMPSTR_POS_SPACE 0x0104
#define CMPSTR_POS_TEXT_COLOR 0x010C
#define CMPSTR_POS_CAP1 0x0118
#define CMPSTR_POS_CAP2 0x0120
#define CMPSTR_POS_CAP3 0x0128
#define CMPSTR_POS_D 0x0130
#define CMPSTR_POS_DK 0x0134
#define CMPSTR_POS_END_BODY 0x0138
#define CMPSTR_POS_F 0x0144
#define CMPSTR_POS_GR 0x0148
#define CMPSTR_POS_GR2 0x014C
#define CMPSTR_POS_IHEAD 0x0150
#define CMPSTR_POS_IFOOT 0x0158
#define CMPSTR_POS_IMG 0x0160
#define CMPSTR_POS_INDEX_PAGE 0x0164
#define CMPSTR_POS_JR 0x0170
#define CMPSTR_POS_KT 0x0174
#define CMPSTR_POS_LR 0x0178
#define CMPSTR_POS_M 0x017C
#define CMPSTR_POS_MR 0x0180
#define CMPSTR_POS_NP 0x0184
#define CMPSTR_POS_ND 0x0188
#define CMPSTR_POS_NM 0x018C
#define CMPSTR_POS_NT 0x0190
#define CMPSTR_POS_PAGECNT 0x0194
#define CMPSTR_POS_PAGENUM 0x019C
#define CMPSTR_POS_PDFSEP 0x01A4
#define CMPSTR_POS_R 0x01AC
#define CMPSTR_POS_TY 0x01B0
#define CMPSTR_POS_TYP 0x01B4
#define CMPSTR_POS_TITLE 0x01B8
#define CMPSTR_POS_FOOT 0x01C0
#define CMPSTR_POS_BS 0x01C8
#define CMPSTR_POS_CR 0x01CC
#define CMPSTR_POS_C 0x01D0
#define CMPSTR_POS_PREFIX 0x01D4
#define CMPSTR_POS_FORMAT 0x01DC
#define CMPSTR_POS_DPI 0x01E4
#define CMPSTR_POS_WIDTH 0x01E8
#define CMPSTR_POS_HEIGHT 0x01F0
#define CMPSTR_POS_AA 0x01F8
#define CMPSTR_POS_HINTING 0x01FC
#define CMPSTR_POS_GRID 0x0204
#define CMPSTR_POS_GRID_COLOR 0x020C
#define CMPSTR_POS_PDF_FIXED 0x0218
#define CMPSTR_POS_JPEG 0x0224
#define CMPSTR_POS_SIZE 0x022C
#define CMPSTR_POS_BLEED 0x0234
#define CMPSTR_POS_PAGE 0x023C
#define CMPSTR_POS_AUTHOR 0x0244
#define CMPSTR_POS_SUBJECT 0x024C
#define CMPSTR_POS_NO_UNICODE 0x0254
#define CMPSTR_POS_OUTLINE 0x0260
#define CMPSTR_POS_FONTSIZE 0x0268
#define CMPSTR_POS_POS 0x0274
#define CMPSTR_POS_VSPACE 0x0278
#define CMPSTR_POS_HSPACE 0x0280
#define CMPSTR_POS_NO_PAGE 0x0288
#define CMPSTR_POS_BEFORE 0x0290
#define CMPSTR_POS_AFTER 0x0298
#define CMPSTR_POS_SEP 0x02A0
