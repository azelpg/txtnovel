/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * 出力
 *********************************/

#include <stdio.h>
#include <math.h>

#include <mlk.h>
#include <mlk_str.h>
#include <mlk_list.h>
#include <mlk_stdio.h>

#include "app.h"
#include "setting.h"
#include "output.h"
#include "pdf.h"
#include "font/font.h"
#include "image.h"


//--------------------

static const char *g_format_ext[] = {
	"pdf", "bmp", "png", "jpg", "psd"
};

//--------------------


/* dpi,width,height 値を取得
 *
 * return: どの値を使うか */

static int _get_image_value(int *dpi,int *width,int *height)
{
	int d,w,h;

	//コマンドラインオプションが優先

	d = g_app->dpi;
	w = g_app->width;
	h = g_app->height;

	//次に、テキスト設定

	if(!d && !w && !h)
	{
		d = g_set->output.dpi;
		w = g_set->output.width;
		h = g_set->output.height;
	}

	//

	*dpi = d;
	*width = w;
	*height = h;

	if(d)
		return 0;
	else if(w)
		return 1;
	else if(h)
		return 2;
	else
	{
		//すべて指定がなければ、600 dpi
		*dpi = 600;
		return 0;
	}
}

/* 出力設定の取得
 *
 * コマンドラインオプション > テキストでの設定 > デフォルト の順で優先。 */

static void _set_output(OutputData *p)
{
	int ret,dpi,width,height;
	double pw,ph,d_dpi;

	//ファイル名先頭

	if(mStrIsnotEmpty(&g_app->str_output))
		p->prefix = g_app->str_output.buf;
	else if(mStrIsnotEmpty(&g_set->output.str_prefix))
		p->prefix = g_set->output.str_prefix.buf;
	else
		p->prefix = "out";

	//フォーマット

	if(g_app->format)
		p->format = g_app->format;
	else if(g_set->output.format)
		p->format = g_set->output.format;
	else
		p->format = OUT_FORMAT_PDF;

	//用紙サイズ (mm -> inch)
	// :裁ち切りを含む

	pw = (g_set->paper_w + g_set->bleed * 2) / 25.4;
	ph = (g_set->paper_h + g_set->bleed * 2) / 25.4;

	//dpi,width,height

	p->dpi_output = 0;

	ret = _get_image_value(&dpi, &width, &height);

	switch(ret)
	{
		//dpi
		case 0:
			d_dpi = dpi;
			width  = (int)(pw * dpi + 0.5);
			height = (int)(ph * dpi + 0.5);

			//画像出力時の DPI
			p->dpi_output = dpi;
			break;
		//width
		case 1:
			d_dpi = width / pw;
			dpi = (int)(d_dpi + 0.5);
			height = (int)(width * (ph / pw) + 0.5);
			break;
		//height
		default:
			d_dpi = height / ph;
			dpi = (int)(d_dpi + 0.5);
			width = (int)(height * (pw / ph) + 0.5);
			break;
	}

	if(dpi <= 0) dpi = 1;
	if(width <= 0) width = 1;
	if(height <= 0) height = 1;

	p->dpi = dpi;
	p->width = p->width_output = width;
	p->height = height;
	p->dpi_image = d_dpi;

	//見開き2P分

	if(g_set->page_type == PAGETYPE_TWO_1P)
		p->width_output = p->width * 2;

	//アンチエイリアス
	// :default = 実際の DPI 値が 300 未満なら ON

	if(g_app->antialias)
		p->antialias = g_app->antialias - 1;
	else if(g_set->output.antialias)
		p->antialias = g_set->output.antialias - 1;
	else
		p->antialias = (dpi < 300);

	//グリッド描画

	if(g_app->flags & APP_FLAGS_DRAW_GRID)
		g_set->output.grid = 1;

	//用紙サイズ (pt 単位)

	p->paper_w = convert_mm_to_pt(g_set->paper_w);
	p->paper_h = convert_mm_to_pt(g_set->paper_h);

	p->paper_fullw = (int)(pw * 72 * 1000 + 0.5);
	p->paper_fullh = (int)(ph * 72 * 1000 + 0.5);

	p->bleed_w = convert_mm_to_pt(g_set->bleed);
}

/* ページ番号の桁数取得 */

static int _get_pageno_dig(void)
{
	int max = g_app->last_pageno;

	if(max < 10)
		return 1;
	else if(max < 100)
		return 2;
	else if(max < 1000)
		return 3;
	else if(max < 10000)
		return 4;
	else
		return 5;
}


//======================


/* OutputData 解放 */

static void _free_output(void *ptr,int err)
{
	OutputData *p = (OutputData *)ptr;

	pdf_free(p->pdf);

	mListDeleteAll(&p->list_char);
	mListDeleteAll(&p->list_bousen);

	mStrFree(&p->str_tmp);

	image_free(p->outimg);

	if(p->fpin) fclose(p->fpin);

	mFree(p);
}

/* OutputData 初期化 */

static OutputData *_init_output(void)
{
	OutputData *p;
	char *fname;

	p = (OutputData *)mMalloc0(sizeof(OutputData));
	if(!p) app_enderr_alloc();

	g_app->proc_ptr = p;
	g_app->free_proc = _free_output;

	//レイアウトファイル

	fname = app_set_tmpfile(TMPFILENAME_LAYOUT);

	p->fpin = mFILEopen(fname, "rb");
	if(!p->fpin) app_enderrno(MLKERR_OPEN, fname);

	//出力情報

	_set_output(p);

	p->is_output_img = (p->format != OUT_FORMAT_PDF);

	//拡張子

	p->fname_ext = g_format_ext[p->format - 1];

	//ページ番号の桁数

	p->pageno_dig = _get_pageno_dig();

	//目次用のグリフを登録

	output_set_index_glyph(p);

	//初期レイアウト

	p->layout = g_set->layout_base;

	output_change_layout(p);

	p->pt_baselayout[0] = p->pt_layout[0];
	p->pt_baselayout[1] = p->pt_layout[1];

	//アウトラインの区切り文字を 0 にする

	mStrReplaceChar_null(&g_set->pdf.str_outline, ',');

	//

	if(p->is_output_img)
		output_image_init(p);
	else
		output_pdf_init(p);
	
	return p;
}

/** レイアウトファイルから読み込んで、出力 */

void app_output_file(void)
{
	OutputData *p;

	p = _init_output();

	output_proc_main(p);

	app_end_proc();
}


//================================
// sub
//================================


/* 指定ページ番号が奇数位置か */

static int _is_page_odd(int no)
{
	if(g_set->page_type == PAGETYPE_ONE)
		//単ページは常に奇数位置
		return 1;
	else
		return (no & 1);
}

/* 偶数/奇数ページの基本版面位置を取得 */

static void _get_layout_pos(OutputData *p,mPoint *dst,int no)
{
	LayoutItem *pl = p->layout;

	//単ページは常に奇数位置

	if(g_set->page_type == PAGETYPE_ONE) no = 1;

	//x

	if(pl->sp_gutter.val)
		//のどの余白あり
		dst->x = (no)? p->paper_w - pl->sp_gutter.val - p->layout_width: pl->sp_gutter.val;
	else
		//中央
		dst->x = (p->paper_w - p->layout_width) / 2;

	dst->x += p->bleed_w;

	//y

	if(pl->sp_head.val)
		//天の余白あり
		dst->y = pl->sp_head.val;
	else if(pl->sp_foot.val)
		//地の余白あり
		dst->y = p->paper_h - pl->sp_foot.val - p->layout_height;
	else
		//中央
		dst->y = (p->paper_h - p->layout_height) / 2;

	dst->y += p->bleed_w;
}

/** レイアウト変更時 */

void output_change_layout(OutputData *p)
{
	LayoutItem *pl = p->layout;
	int32_t w;

	p->is_horz   = (pl->mode == LAYOUT_MODE_HORZ);
	p->is_column = (pl->mode == LAYOUT_MODE_VERT2);

	p->basefont = pl->basefont.item->font;
	p->basefont_size = pl->basefont.size;
	p->basefont_height = (p->is_horz)? p->layout->basefont.height: p->basefont_size;

	p->rubyfont = pl->ruby_font.item->font;
	p->rubyfont_size = pl->ruby_font.size;

	p->line_length = pl->line_len.val;

	//基本版面のサイズ

	w = p->layout->line_feed.val * (pl->line_num - 1) + p->basefont_height;

	if(p->is_horz)
	{
		p->layout_width = p->line_length;
		p->layout_height = w;
	}
	else
	{
		p->layout_width = w;
		p->layout_height = p->line_length;

		if(p->is_column)
			p->layout_height += pl->sp_column.val + p->line_length;
	}

	//基本版面の位置

	_get_layout_pos(p, p->pt_layout, 0);
	_get_layout_pos(p, p->pt_layout + 1, 1);

	//各出力ごと

	if(p->is_output_img)
		output_image_change_layout(p);
	else
		output_pdf_change_layout(p);

	//現在の背景画像
	// :上記の各出力ごとの後に実行すること。

	p->img_bkgnd = p->layout->bkgndimg.item;
	p->img_bkgnd_left = p->layout->bkgnd_img_left;
	p->img_bkgnd_right = p->layout->bkgnd_img_right;
}

/** 現在ページの背景画像を取得
 *
 * return: 表示しないページでは NULL になる */

ImageItem *output_get_page_bkgndimg(OutputData *p)
{
	ImageItem *pi;
	int no_page,is_odd;

	//画像

	is_odd = _is_page_odd(p->curpage);

	if(p->img_bkgnd_left && is_odd)
		pi = p->img_bkgnd_left;
	else if(p->img_bkgnd_right && !is_odd)
		pi = p->img_bkgnd_right;
	else if(p->img_bkgnd)
		pi = p->img_bkgnd;
	else
		return NULL;

	//--- 表示するか

	//PDF 出力時、タイル描画は不可

	if(!p->is_output_img && p->layout->bkgndimg.base == IMAGEINFO_BASE_TILE)
		return NULL;

	//表示しないページ

	no_page = p->layout->bkgndimg_no_page;

	if((no_page & LAYOUT_NOPAGE_BLANK) && !(p->page_flags & PAGE_F_HAVE_BODY))
		//空白ページ (本文文字がない)
		pi = NULL;
	else if((no_page & LAYOUT_NOPAGE_IMAGE) && p->img_page.item)
		//画像ページ
		pi = NULL;

	return pi;
}

/** ImageInfo: 用紙上のサイズをセット */

void output_set_imagesize(OutputData *p,ImageInfo *info)
{
	//width = 0 で省略。
	//PDF 出力時、100% 指定の場合は用紙サイズ

	if(!info->width
		|| (!p->is_output_img && info->width < 0))
	{
		info->width = p->paper_fullw;
		info->height = p->paper_fullh;
	}
}

/** 画像の表示位置を取得
 *
 * 現在のページ位置が対象。
 * PDF 出力時、"tile" の場合は paper/center
 *
 * dst: 画像出力なら px 位置。
 *   PDF なら出力ページの用紙位置。(両方とも見開き2P適用済み) */

void output_get_image_pos(OutputData *p,ImageItem *piimg,ImageInfo *info,mPoint *dst)
{
	mPoint *ppt;
	int x,y,w,h,imgw,imgh,fright;

	//基準位置

	switch(info->base)
	{
		//仕上がり用紙
		case IMAGEINFO_BASE_PAPER:
		case IMAGEINFO_BASE_TILE:
			x = y = p->bleed_w;
			w = p->paper_w;
			h = p->paper_h;
			break;
		//裁ち切り含む
		case IMAGEINFO_BASE_BLEED:
			x = y = 0;
			w = p->paper_fullw;
			h = p->paper_fullh;
			break;
		//基本版面
		default:
			ppt = p->pt_layout + (p->curpage & 1);
			x = ppt->x;
			y = ppt->y;
			w = p->layout_width;
			h = p->layout_height;
			break;
	}

	//画像のサイズ
	// :画像出力時で "100%" 指定時は、画像サイズから計算 (画像は読み込み済みであること)

	imgw = info->width;
	imgh = info->height;

	if(p->is_output_img && imgw < 0)
	{
		imgw = lround(piimg->img->width  / p->dpi_image * 72000);
		imgh = lround(piimg->img->height / p->dpi_image * 72000);
	}

	//配置

	w = w - imgw;
	h = h - imgh;

	fright = _is_page_odd(p->curpage); //のど側で右寄せか

	switch(info->align)
	{
		//中央寄せ
		case IMAGEINFO_ALIGN_CENTER:
			x += w / 2;
			y += h / 2;
			break;
		//のど側上
		case IMAGEINFO_ALIGN_IN_TOP:
			if(fright) x += w;
			break;
		//のど側下
		case IMAGEINFO_ALIGN_IN_BOTTOM:
			if(fright) x += w;
			y += h;
			break;
		//小口側上
		case IMAGEINFO_ALIGN_OUT_TOP:
			if(!fright) x += w;
			break;
		//小口側下
		case IMAGEINFO_ALIGN_OUT_BOTTOM:
			if(!fright) x += w;
			y += h;
			break;
	}

	//位置調整

	if(fright)
		x -= info->x;
	else
		x += info->x;

	y += info->y;

	//見開き2P

	if(g_set->page_type == PAGETYPE_TWO_1P && !(p->curpage & 1))
		x += p->paper_fullw;

	//画像 px 位置
	// :小数点以下切り上げにしないと、端寄せでずれる

	if(p->is_output_img)
	{
		x = lround(x * p->img_mulw);
		y = lround(y * p->img_mulh);
	}

	//

	dst->x = x;
	dst->y = y;
}



//========================
// 目次用のグリフ登録
//========================
/* PDF 出力の場合、出力の開始時点で、使用するグリフはすべて登録されている必要があるので、
 * 余白文字と数字のグリフは、出力の初期化時にすべて登録しておく。 */


//漢数字
static const uint16_t *g_kansuuji_char = u"〇一二三四五六七八九";


/* 余白の文字の GID と幅を取得
 *
 * dst_gid: 0 で空白 */

static int _get_index_space_glyph(LayoutItem *layout,Font *font,uint16_t *dst_gid)
{
	uint16_t gid;
	uint32_t code;

	code = layout->index_sp_char;

	//空白

	if(!code)
	{
		if(dst_gid) *dst_gid = 0;
		return 0;
	}

	//------

	if(code & (1<<31))
	{
		gid = font_get_cid_to_gid(font, code & 0xffff);
		code = 0;
	}
	else
		gid = font_get_unicode_gid(font, code);

	//縦書き

	if(layout->mode != LAYOUT_MODE_HORZ)
		gid = font_get_gsub(font, FONT_DATA_GSUB_VERT, gid);

	//

	if(dst_gid) *dst_gid = gid;

	return font_regist_glyph(font, gid, code, layout->basefont.size, (layout->mode != LAYOUT_MODE_HORZ));
}

/* 目次の数字グリフの GID と幅を取得 */

static int _get_index_number_glyph(LayoutItem *layout,Font *font,int no,uint16_t *dst_gid)
{
	uint32_t code;
	uint16_t gid,srcgid;
	int type,fvert;

	type = layout->index_number;
	fvert = (layout->mode != LAYOUT_MODE_HORZ);

	if(type == LAYOUT_INDEX_NUM_KANJI)
		code = g_kansuuji_char[no];
	else
		code = no + '0';

	gid = font_get_unicode_gid(font, code);

	//GSUB

	if(type == LAYOUT_INDEX_NUM_KANJI)
	{
		//漢数字、縦書き

		 if(fvert)
			gid = font_get_gsub(font, FONT_DATA_GSUB_VERT, gid);
	}
	else
	{
		srcgid = gid;

		//1/3幅

		if(type == LAYOUT_INDEX_NUM_NUM3)
			gid = font_get_gsub(font, FONT_DATA_GSUB_TWID, gid);

		//半角幅

		if(type == LAYOUT_INDEX_NUM_NUM2
			|| (type == LAYOUT_INDEX_NUM_NUM3 && gid == srcgid))
			gid = font_get_gsub(font, FONT_DATA_GSUB_HWID, gid);
	}

	//

	if(dst_gid) *dst_gid = gid;

	return font_regist_glyph(font, gid, code, layout->basefont.size, (fvert && type == LAYOUT_INDEX_NUM_KANJI));
}

/** 出力開始時、目次用のグリフを登録 */

void output_set_index_glyph(OutputData *p)
{
	IndexItem *pi;
	LayoutItem *layout;
	Font *font;
	int pno;

	MLK_LIST_FOR(g_app->list_index, pi, IndexItem)
	{
		layout = pi->layout;
	
		//文字はすべて基本フォント

		font = layout->basefont.item->font;

		//空白文字

		_get_index_space_glyph(layout, font, NULL);

		//数字

		for(pno = pi->save_pageno; pno; pno /= 10)
			_get_index_number_glyph(layout, font, pno % 10, NULL);
	}
}


//============================
// 目次用のグリフ文字を追加
//============================


/* 目次用の数字文字を追加 */

static CharItem *_add_index_char(OutputData *p,int no)
{
	CharItem *pch;

	pch = (CharItem *)mListAppendNew(&p->list_char, sizeof(CharItem));
	if(!pch) app_enderr_alloc();

	pch->font = p->basefont;
	pch->fontsize = p->basefont_size;

	pch->width = _get_index_number_glyph(p->layout, p->basefont, no, &pch->gid);

	return pch;
}

/* 目次のページ数の文字を追加
 *
 * return: 幅 */

static int _add_index_char_pageno(OutputData *p,IndexItem *pi)
{
	CharItem *pch,*pchtop;
	int x,y,w,n,pageno,ftateyoko;

	x = pi->line_len + pi->line_remain;
	y = pi->ypos;
	w = 0;
	pchtop = NULL;

	//縦中横
	ftateyoko = (!p->is_horz && p->layout->index_number != LAYOUT_INDEX_NUM_KANJI);

	//後ろの文字から順に
	// :ページ位置が記録されていない場合、0 となる。
	// :その場合、数字は表示されない。

	for(pageno = pi->save_pageno; pageno; pageno /= 10)
	{
		pch = _add_index_char(p, pageno % 10);

		if(!pchtop) pchtop = pch;

		if(ftateyoko)
		{
			//縦中横
			// :とりあえず、行の右端位置に

			y += pch->width;

			pch->x = x - p->basefont_size;
			pch->y = y;
			pch->flags |= CHAR_F_TATEYOKO;
		}
		else
		{
			//横書き or 縦書きで漢数字

			x -= pch->width;
			
			pch->x = x;
			pch->y = pi->ypos;
		}

		w += pch->width;
	}

	//縦中横時、y 位置を揃える

	if(ftateyoko)
	{
		n = (p->basefont_size - w) / 2;
	
		for(pch = pchtop; pch; pch = (CharItem *)pch->i.next)
			pch->y += n;
	
		w = p->basefont_size;
	}

	return w;
}

/** 目次のページで、追加文字をセット */

void output_add_index_char(OutputData *p)
{
	IndexItem *pi;
	CharItem *pch;
	int w,charw,x,fdash;
	uint16_t gid;

	//余白文字

	charw = _get_index_space_glyph(p->layout, p->basefont, &gid);

	//余白の全角ダッシュを線で描画するか

	fdash = (p->layout->dash_width
		&& (p->layout->index_sp_char == 0x2014 || p->layout->index_sp_char == 0x2015));

	//目次アイテムごと

	MLK_LIST_FOR(g_app->list_index, pi, IndexItem)
	{
		if(pi->pageno != p->curpage) continue;

		//ページ数の文字を終端に追加

		w = _add_index_char_pageno(p, pi);

		//残り幅に、余白の文字を追加

		if(gid)
		{
			x = pi->line_len;
			w = pi->line_remain - w;

			for(; w >= charw; w -= charw)
			{
				pch = (CharItem *)mListAppendNew(&p->list_char, sizeof(CharItem));
				if(!pch) app_enderr_alloc();

				pch->font = p->basefont;
				pch->fontsize = p->basefont_size;
				pch->gid = gid;
				pch->width = charw;
				pch->x = x;
				pch->y = pi->ypos;

				if(fdash)
					pch->flags |= CHAR_F_DRAW_DASH;

				x += charw;
			}
		}
	}
}

