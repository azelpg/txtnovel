/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

typedef struct _FontItem FontItem;
typedef struct _KentenItem KentenItem;
typedef struct _LayoutItem LayoutItem;

//レイアウト位置。1000 = 1pt = 1/72 inch
typedef int32_t layoutpos;

//------- ルビ

/* ルビ文字の1文字分 */

typedef struct
{
	mListItem i;

	uint16_t gid,
		flags;
	uint32_t code;
	layoutpos width,	//字幅 (常にグリフの元の字幅と同じ)
		sp_before,		//ルビ関連の前後の空き量
		sp_after,
		x,y;
}RubyChar;

enum
{
	RUBYCHAR_FLAG_VROTATE = 1<<0	//縦書き90度回転
};

/* ルビデータ */

typedef struct
{
	mListItem i;

	uint16_t type,		//ルビタイプ
		flags;
	int32_t	len;		//ルビ文字数
	layoutpos width,	//親文字列の長さ (ベタ組)
		ruby_width,		//ルビ文字列の長さ (ベタ組)
		ruby_shift,		//ルビ文字を前後に掛ける場合のずらし量 (負の値で前、正の値で後ろ)
		ruby_over_after;	//ルビが後ろの文字に掛かっている幅
	RubyChar *chtop;		//先頭文字
	struct _StrData *parent;	//親文字列
}RubyData;

enum
{
	RUBY_TYPE_MONO_TOP,			//モノルビ (肩付き)
	RUBY_TYPE_MONO_CENTER,		//モノルビ (中付き)
	RUBY_TYPE_JYUKUGO_TOP,		//熟語ルビ (肩付き)
	RUBY_TYPE_JYUKUGO_CENTER,	//熟語ルビ (中付き)
	RUBY_TYPE_GROUP,			//グループルビ:先頭と終端に空きを付ける
	RUBY_TYPE_GROUP2,			//グループルビ:先頭と終端を揃える
	RUBY_TYPE_LONG,

	RUBY_TYPE_SET_MONO = 100,	//データのセット時
	RUBY_TYPE_SET_JYUKUGO
};

enum
{
	RUBY_FLAG_CENTER = 1<<0,	//(モノルビ/熟語ルビ) 中付き
	RUBY_FLAG_PREV = 1<<1,		//前に熟語ルビがある
	RUBY_FLAG_NEXT = 1<<2,		//後ろに熟語ルビがある
	RUBY_FLAG_LAST = 1<<3,		//最後の熟語ルビ
	RUBY_FLAG_ADJUST_SP = 1<<4,		//肩付きで、行頭/行末時に親字間を調整
	RUBY_FLAG_ADJUST_CENTER = 1<<5	//中付き時、行頭/行末時の調整対象
};

//------- 通常文字

/* 通常文字の1文字分 */

typedef struct
{
	mListItem i;

	uint16_t gid,		//元フォントのGID
		flags;
	uint32_t code;		//Unicode 文字からセットされた場合、文字コード (0 でなし)
	layoutpos width,	//実際の字幅
		raw_width,		//フォントの元の送り幅
		sp_before,		//ルビ関連の前後の空き量
		sp_after,
		x,y;
	KentenItem *kenten;
}StrChar;

enum
{
	STRCHAR_FLAG_DAKUTEN = 1<<0,	//濁点合成
	STRCHAR_FLAG_VROTATE = 1<<1,	//縦書き欧文時、90度回転させる
	STRCHAR_FLAG_TATEYOKO = 1<<2,	//縦中横
	STRCHAR_FLAG_KENTEN = 1<<3		//圏点文字
};

/* 通常文字列、分割不可なものを一体とする */

typedef struct _StrData
{
	mListItem i;

	uint8_t classno,	//文字クラス
		class_flags,	//クラスのフラグ
		flags,
		bousen;
	int32_t len,		//文字数
		lineno;			//行位置
	layoutpos x,y,		//基本版面からの位置
		width,			//分割不可な文字列の長さ
		fontsize,		//この文字のフォントの全角サイズ
		fix_before,		//前後の固定の空き量 (規定の空き量の最小値も含む)
		fix_after,
		sp_before,		//詰め可能な前後の空き量
		sp_after,
		width_before;	//fix_before の中で、文字の幅に含める分の幅 (行調整の空き)
	StrChar *chtop;		//先頭文字
	RubyData *ruby;		//ルビデータ (NULL でなし)
	FontItem *font;		//文字のフォント
}StrData;

//フラグ
enum
{
	STR_FLAG_LINE_TOP = 1<<0,		//行頭
	STR_FLAG_LINE_BOTTOM = 1<<1,	//行末
	STR_FLAG_LINE_WRAP_TOP = 1<<2,	//折返しの行頭か
	STR_FLAG_HW = 1<<3,				//半角幅扱いするもの
	STR_FLAG_HW_POSUP = 1<<4,		//半角幅:縦組時、位置を上に1/2ずらす [始め括弧]
	STR_FLAG_NEXT_PAGE = 1<<5,		//折り返しによる次のページの文字
	STR_FLAG_HANGED = 1<<6			//行の調整時、ぶら下げになった
};

//文字クラス
//[!] 変更時、layout_class.c も変更
enum
{
	STRCLASS_NONE,
	STRCLASS_START_KAKKO,	//始め括弧
	STRCLASS_END_KAKKO,		//終わり括弧
	STRCLASS_HAIFUN,		//ハイフン
	STRCLASS_KUGIRI,		//区切り約物
	STRCLASS_TYUUTEN,		//中点
	STRCLASS_KUTEN,			//句点
	STRCLASS_DOKUTEN,		//読点
	STRCLASS_REPEAT,		//繰り返し記号
	STRCLASS_TYOUON,		//長音
	STRCLASS_JASPACE,		//和字間隔
	STRCLASS_KANA,			//かな
	STRCLASS_SMALL_KANA,	//小書きのかな
	/* ここまでは、マップ等で判定 */
	STRCLASS_NOSEP,			//分離禁止文字
	/* ここまで、フラグ値あり*/
	STRCLASS_ENSPACE,		//欧文間隔
	STRCLASS_EURO,			//欧文
	STRCLASS_TATEYOKO,		//縦中横
	STRCLASS_WITH_RUBY		//ルビ付きの親文字
};

//文字クラスのフラグ
enum
{
	STRCLASS_F_HALF_WIDTH = 1<<0,		//約物の半角幅対象
	STRCLASS_F_ENDKAKKO_NEXT_BETA = 1<<1,	//終わり括弧の次に来た時、ベタ組にする
	STRCLASS_F_BREAK_HEAD = 1<<2,		//行頭禁則対象
	STRCLASS_F_HANG = 1<<3,				//ぶら下げ対象
	STRCLASS_F_BREAK_FOOT = 1<<4,		//行末禁則対象
	STRCLASS_F_NO_ADD_SPACE = 1<<5,		//前後に字間を空けられない
	STRCLASS_F_RUBY_OVER_AFTER = 1<<6,	//ルビ文字を後ろに掛けられる対象
	STRCLASS_F_RUBY_OVER_BEFORE = 1<<7	//ルビ文字を前に掛けられる対象
};

//------ 柱・ノンブルの文字

typedef struct
{
	mListItem i;

	uint16_t gid;
	layoutpos width,
		x,y;
}NormalChar;

