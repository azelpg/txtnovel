/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * app 関数
 *********************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <mlk.h>
#include <mlk_str.h>
#include <mlk_list.h>
#include <mlk_file.h>
#include <mlk_charset.h>

#include "app.h"
#include "image.h"
#include "setting.h"

#include "cmpstring_data.h"


/** アプリ終了 */

void app_exit(int ret)
{
	app_free(ret);

	exit(ret);
}

/** 処理データを成功として解放し、初期化 */

void app_end_proc(void)
{
	if(g_app->free_proc)
		(g_app->free_proc)(g_app->proc_ptr, FALSE);

	g_app->proc_ptr = NULL;
	g_app->free_proc = NULL;
	g_app->puterr_proc = NULL;
}

/** シグナル受信による終了 */

void app_end_signal(void)
{
	printf("\n* stop\n");
	
	app_exit(1);
}


//===========================
// エラー
//===========================


/** エラーメッセージの表示
 *
 * UTF-8。最後に改行を出力する
 * 
 * mes2: 追加の文字列。NULL でなし */

void app_puterr(const char *mes,const char *mes2)
{
	if(mes == APP_ERRMES_ALREADY) return;
	
	//経過

	if(g_app->prog_curno)
		putchar('\n');

	//処理固有の情報

	if(g_app->puterr_proc)
		(g_app->puterr_proc)(g_app->proc_ptr);

	//
	
	mPutUTF8_stderr(mes);

	if(mes2)
	{
		fputs("\n ", stderr);
		mPutUTF8_stderr(mes2);
	}

	fputc('\n', stderr);
}

/** エラーメッセージを表示して終了 */

void app_enderr(const char *mes)
{
	app_puterr(mes, NULL);

	app_exit(1);
}

/** エラーメッセージを表示して終了 */

void app_enderr2(const char *mes,const char *mes2)
{
	app_puterr(mes, mes2);

	app_exit(1);
}

/** エラー番号のメッセージを表示して終了 */

void app_enderrno(mlkerr err,const char *mes2)
{
	app_enderr2(app_get_errmes(err), mes2);
}

/** メモリ確保エラーによる終了 */

void app_enderr_alloc(void)
{
	app_enderrno(MLKERR_ALLOC, NULL);
}

/** エラー番号のエラーメッセージを表示
 *
 * err: -1 ですでに表示済み */

void app_puterrno(mlkerr err,const char *mes2)
{
	if(err != APPERR_PUT_ERR)
		app_puterr(app_get_errmes(err), mes2);
}

/** エラーの文字列を取得
 *
 * return: NULL ですでに表示済み */

const char *app_get_errmes(mlkerr err)
{
	switch(err)
	{
		//表示済み
		case APPERR_PUT_ERR:
			return NULL;
		case MLKERR_ALLOC:
			return "メモリが足りません";
		case MLKERR_OPEN:
			return "ファイルが開けません";
		case MLKERR_IO:
			return "ファイルの読み書きエラー";
		case MLKERR_INVALID_VALUE:
			return "値が正しくありません";
		case MLKERR_UNSUPPORTED:
			return "非対応のフォーマットが含まれています";
		case MLKERR_FORMAT_HEADER:
			return "フォーマットのヘッダが正しくありません";
		case MLKERR_DAMAGED:
			return "ファイルが壊れているか、正しくありません";
	}

	return "エラー";
}


//===========================
// 作業ファイル
//===========================


/** 作業ファイル名をセットして、返す
 *
 * fname: パスを含まないファイル名 */

char *app_set_tmpfile(const char *fname)
{
	mStrSetLen(&g_app->str_tmpdir, g_app->tmpdir_pos);
	mStrAppendText(&g_app->str_tmpdir, fname);

	return g_app->str_tmpdir.buf;
}

/** 作業ファイルを削除する */

void app_delete_tmpfile(const char *fname)
{
	app_set_tmpfile(fname);

	mDeleteFile(g_app->str_tmpdir.buf);
}


//===========================
// 経過
//===========================


/** 経過の初期化
 *
 * max: 0 でも問題なし */

void app_progress_init(uint32_t max)
{
	int i;
	uint32_t *pd;

	g_app->prog_curno = 1;

	pd = g_app->prog_pos;

	for(i = 1; i < 20; i++)
		*(pd++) = (uint32_t)(i / 20.0 * max + 0.5);

	*pd = max;
}

/** 経過の位置をセット */

void app_progress_setpos(uint32_t pos)
{
	int i;

	if(!g_app->prog_curno
		|| pos < g_app->prog_pos[g_app->prog_curno - 1])
		return;

	//次の位置

	for(i = g_app->prog_curno + 1; i <= 20; i++)
	{
		putchar('.');

		if(!(i & 3))
			printf("%d%%", i * 5);

		if(pos < g_app->prog_pos[i - 1])
			break;
	}

	//
	
	if(i == 21)
		putchar('\n');

	g_app->prog_curno = (i == 21)? 0: i;

	fflush(stdout);
}


//===========================
// 目次アイテム
//===========================


/** 目次アイテムから名前で検索 */

IndexItem *app_search_index_item(const char *name)
{
	IndexItem *pi;

	MLK_LIST_FOR(g_app->list_index, pi, IndexItem)
	{
		if(strcmp(name, pi->name) == 0)
			return pi;
	}

	return NULL;
}

/** 目次アイテムの追加
 *
 * title: NULL で指定なし */

IndexItem *app_add_index_item(const char *name,const char *title)
{
	IndexItem *pi;
	int len;

	if(!name || !(*name)) return NULL;

	pi = app_search_index_item(name);

	//新規追加

	if(!pi)
	{
		len = strlen(name);

		pi = (IndexItem *)mListAppendNew(&g_app->list_index, sizeof(IndexItem) + len + 1);
		if(!pi) app_enderr_alloc();

		memcpy(pi->name, name, len);
	}

	//タイトル

	if(title)
		mStrdup_free(&pi->title, title);

	return pi;
}

/** 指定ページ番号の目次アイテムに、PDF ページオブジェクト番号をセット */

void app_index_set_pdf_objno(int page,int objno)
{
	IndexItem *pi;

	MLK_LIST_FOR(g_app->list_index, pi, IndexItem)
	{
		if(pi->save_pageno == page)
		{
			pi->pdf_objno = objno;
			break;
		}
	}
}


//=======================
// 画像
//=======================


/** ファイル名から画像を検索 */

ImageItem *app_search_image(const char *filename)
{
	ImageItem *pi;

	MLK_LIST_FOR(g_app->list_image, pi, ImageItem)
	{
		if(strcmp(filename, pi->filename) == 0)
			return pi;
	}

	return NULL;
}

/** 画像の追加 (ファイルは読み込まない)
 *
 * filename: 空文字列でなし
 * return: NULL でなし */

ImageItem *app_add_image(const char *filename)
{
	ImageItem *pi;
	int len;

	if(!(*filename)) return NULL;

	if(!mIsExistFile(filename))
		app_enderr2("画像ファイルが存在しません", filename);

	//同じファイルがすでにある場合

	pi = app_search_image(filename);
	if(pi) return pi;

	//追加

	len = strlen(filename);

	pi = (ImageItem *)mListAppendNew(&g_app->list_image, sizeof(ImageItem) + len + 1);
	if(!pi) app_enderr_alloc();

	memcpy(pi->filename, filename, len);

	return pi;
}

/** 画像アイテムの画像を読み込み
 *
 * すでに読み込まれている場合、何もしない */

void app_image_load(ImageItem *pi)
{
	if(!pi || pi->img) return;

	pi->img = image_load(pi->filename, g_set->image_bits);
}

/** 画像アイテムの画像を解放 */

void app_image_free(ImageItem *pi)
{
	if(pi)
	{
		image_free(pi->img);
		pi->img = NULL;
	}
}

/** すべての PDF 情報をリセット (PDF ファイル開始時) */

void app_image_reset_pdf(void)
{
	ImageItem *pi;
	int resno = 1;

	MLK_LIST_FOR(g_app->list_image, pi, ImageItem)
	{
		pi->pdf_objno = 0;
		pi->pdf_resno = resno++;
	}
}
