/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * 出力
 *********************************/

#include <stdio.h>
#include <math.h>

#include <mlk.h>
#include <mlk_list.h>
#include <mlk_rectbox.h>

#include "app.h"
#include "setting.h"
#include "output.h"
#include "layout_file.h"
#include "font/font.h"


/* 文字データ読み込み
 *
 * font: NULL で、フォント指定データあり
 * finfo: TRUE で、ノンブル・柱
 * return: フォント指定ありの場合、次があるか */

static int _read_chars(OutputData *p,FILE *fp,Font *font,int32_t fontsize,int finfo)
{
	mList *list = &p->list_char;
	CharItem *pi;
	uint8_t flags,fdrawdash;
	uint16_t gid;
	int16_t rx,ry;
	int32_t x,y,fvert,vert;

	//フォント

	if(!font)
	{
		fread(&fontsize, 1, 4, fp);

		if(!fontsize) return 0;

		fread(&font, 1, sizeof(void *), fp);
	}

	//

	x = y = 0;

	fvert = (!p->is_horz && !finfo);
	fdrawdash = (p->layout->dash_width != 0);

	while(1)
	{
		//フラグ

		fread(&flags, 1, 1, fp);

		if(flags == LFILE_CHARF_END) break;

		//GID

		fread(&gid, 1, 2, fp);

		//x 位置

		if(flags & LFILE_CHARF_HAVE_X_REL)
		{
			fread(&rx, 1, 2, fp);
			x += rx;
		}
		else if(flags & LFILE_CHARF_HAVE_X_ABS)
			fread(&x, 1, 4, fp);

		//y 位置

		if(flags & LFILE_CHARF_HAVE_Y_REL)
		{
			fread(&ry, 1, 2, fp);
			y += ry;
		}
		else if(flags & LFILE_CHARF_HAVE_Y_ABS)
			fread(&y, 1, 4, fp);

		//---------

		//文字追加

		pi = (CharItem *)mListAppendNew(list, sizeof(CharItem));
		if(!pi) app_enderr_alloc();

		pi->gid = gid;
		pi->x = x;
		pi->y = y;
		pi->font = font;
		pi->fontsize = fontsize;

		if(flags & LFILE_CHARF_TATEYOKO)
			pi->flags |= CHAR_F_TATEYOKO;

		if(flags & LFILE_CHARF_VROTATE)
			pi->flags |= CHAR_F_VROTATE;

		if(finfo)
			pi->flags |= CHAR_F_INFO;

		if(flags & LFILE_CHARF_PAPER_ABS)
			pi->flags |= CHAR_F_PAPER_ABS;

		if((flags & LFILE_CHARF_DASH_CHAR) && fdrawdash)
			pi->flags |= CHAR_F_DRAW_DASH;

		//幅取得

		vert = fvert;

		if(flags & (LFILE_CHARF_TATEYOKO | LFILE_CHARF_VROTATE))
			vert = 0;
		
		pi->width = lround((double)font_get_advance_width(font, vert, gid) / font->emsize * fontsize);

		//

		if(flags & LFILE_CHARF_TATEYOKO)
			//縦中横
			y -= pi->width;
		else
			x += pi->width;
	}

	return 1;
}

/* 傍線データ読み込み */

static void _read_bousen(OutputData *p,FILE *fp)
{
	BousenItem *pi;
	uint8_t type;
	int32_t fs,x,y,xx,yy,len,topx,topy,fhorz;

	fhorz = p->is_horz;
	topx = p->pt_layout_top.x;
	topy = p->pt_layout_top.y;

	while(1)
	{
		fread(&type, 1, 1, fp);
		if(!type) break;
		
		fread(&x, 1, 4, fp);
		fread(&y, 1, 4, fp);
		fread(&fs, 1, 4, fp);
		fread(&len, 1, 4, fp);

		//用紙上の位置に変換

		if(fhorz)
		{
			xx = topx + x;
			yy = topy + y;
		}
		else
		{
			xx = topx - y;
			yy = topy + x;
		}

		//

		pi = (BousenItem *)mListAppendNew(&p->list_bousen, sizeof(BousenItem));
		if(!pi) app_enderr_alloc();

		pi->type = type;
		pi->x = xx;
		pi->y = yy;
		pi->fontsize = fs;
		pi->len = len;
	}
}

/* 全角ダッシュを線として描画する場合、連続する文字を1つにまとめる */

static void _set_dashchar(OutputData *p)
{
	CharItem *pi,*next,*next2;
	int32_t bottom,cnt;

	if(!p->layout->dash_width) return;

	for(pi = (CharItem *)p->list_char.top; pi; pi = next)
	{
		next = (CharItem *)pi->i.next;

		if(!(pi->flags & CHAR_F_DRAW_DASH)) continue;

		//y 位置・フォントが同じで、連続する全角ダッシュをまとめる
		// :１文字だけの場合は、文字として扱う。
		// :間にアキがあっても繋がる。

		bottom = pi->x + pi->width;
		cnt = 1;

		for(; next; next = next2)
		{
			next2 = (CharItem *)next->i.next;
			
			if(!(next->flags & CHAR_F_DRAW_DASH)
				|| pi->y != next->y
				|| pi->font != next->font
				|| pi->fontsize != next->fontsize)
				break;

			bottom = next->x + next->width;

			mListDelete(&p->list_char, MLISTITEM(next));

			cnt++;
		}

		if(cnt > 1)
			pi->width = bottom - pi->x;
		else
			//線で描画しない
			pi->flags &= ~CHAR_F_DRAW_DASH;
	}
}

/* 現在ページの、レイアウト上の文字開始位置をセット */

static void _set_layout_top(OutputData *p)
{
	mPoint *pt;
	int topx,topy;

	pt = p->pt_layout + (p->curpage & 1);

	topx = pt->x;
	topy = pt->y;

	if(!p->is_horz)
	{
		topx += p->layout_width;

		if(p->page_flags & PAGE_F_COLUMN2)
			topy += p->layout->sp_column.val + p->line_length;
	}

	p->pt_layout_top.x = topx;
	p->pt_layout_top.y = topy;
}


//=============================
// 読み込み
//=============================


/* コマンド処理 */

static void _proc_command(OutputData *p,FILE *fp,int cmd)
{
	switch(cmd)
	{
		//レイアウト変更
		case LFILE_CMD_LAYOUT:
			fread(&p->layout, 1, sizeof(void *), fp);

			output_change_layout(p);
			break;
	}
}

/* 次のページ/段を読み込み
 *
 * return: 1 で終了 */

static int _read_page(OutputData *p)
{
	FILE *fp = p->fpin;
	int32_t no;
	uint8_t flags;

	p->page_flags = 0;
	p->img_page.item = NULL;

	//ページが来るまで読み込み

	while(1)
	{
		if(fread(&no, 1, 4, fp) != 4)
			return 1;

		if(no > 0) break;

		//コマンド

		_proc_command(p, fp, no);
	}

	p->curpage = no;

	//フラグ

	fread(&flags, 1, 1, fp);

	if(flags & LFILE_PAGEF_COLUMN2)
		p->page_flags |= PAGE_F_COLUMN2;

	if(flags & LFILE_PAGEF_HAVE_BODY)
		p->page_flags |= PAGE_F_HAVE_BODY;

	if(flags & LFILE_PAGEF_HAVE_INDEX)
		p->page_flags |= PAGE_F_HAVE_INDEX;

	if(flags & LFILE_PAGEF_PDF_SEP)
		p->page_flags |= PAGE_F_PDFSEP;

	//レイアウト上の先頭位置

	_set_layout_top(p);

	//本文

	if(flags & LFILE_PAGEF_HAVE_BODY)
	{
		//基本フォント
		
		_read_chars(p, fp, p->basefont, p->basefont_size, 0);

		//目次の文字を追加する
		// :追加文字は、基本フォントでの文字なので、ここで追加

		if(flags & LFILE_PAGEF_HAVE_INDEX)
			output_add_index_char(p);

		//ルビフォント

		_read_chars(p, fp, p->rubyfont, p->rubyfont_size, 0);

		//基本フォント以外

		while(_read_chars(p, fp, NULL, 0, 0));

		//傍線

		_read_bousen(p, fp);

		//描画する全角ダッシュ文字 (ルビ、目次空白を含む)

		_set_dashchar(p);
	}

	//ノンブル

	if(flags & LFILE_PAGEF_HAVE_NUMBER)
		_read_chars(p, fp, g_set->number.font.item->font, g_set->number.font.size, 1);
	
	//柱

	if(flags & LFILE_PAGEF_HAVE_TITLE)
		_read_chars(p, fp, g_set->title.font.item->font, g_set->title.font.size, 1);

	//画像
	
	if(flags & LFILE_PAGEF_HAVE_IMAGE)
		fread(&p->img_page, 1, sizeof(ImageInfo), fp);

	return 0;
}

/* 文字の位置を、用紙上の位置としてセット
 *
 * 用紙の左上を (0,0) として、グリフの左上位置。
 * PDF & 縦組の場合、縦中横と90度回転は横書き扱いなので、後ろにまとめる。
 * また、PDF & ダッシュ線描画時は、線での描画になるので、一番最後に移動。
 *
 * [通常文字(ノンブル・柱含む)]-[縦中横]-[90度回転]-[ダッシュ線] */

static void _set_char_pos(OutputData *p)
{
	CharItem *pi,*bottom,*pinext;
	mListItem *pidash,*pirot;
	mPoint *pt_info;
	int32_t fontsize,x,y,w,topx,topy,fhorz,flags;
	double dashw;
	mBox box;

	mRectEmpty(&p->rc_number);

	if(!p->list_char.top) return;

	fhorz = p->is_horz;

	pt_info = p->pt_baselayout + (p->curpage & 1);

	topx = p->pt_layout_top.x;
	topy = p->pt_layout_top.y;

	dashw = p->layout->dash_width / 1000.0;

	//----- 文字

	bottom = (CharItem *)p->list_char.bottom;
	pidash = NULL;
	pirot = NULL;

	for(pi = (CharItem *)p->list_char.top; 1; pi = pinext)
	{
		pinext = (CharItem *)pi->i.next;
	
		fontsize = pi->fontsize;
		flags = pi->flags;

		//隠しノンブルの背景色の範囲を追加

		if(pi->flags & CHAR_F_PAPER_ABS)
		{
			box.x = pi->x;
			box.y = pi->y;
			box.w = pi->width;
			box.h = g_set->number.font.height;

			mRectUnion_box(&p->rc_number, &box);
		}

		//-------

		if(flags & CHAR_F_DRAW_DASH)
		{
			//---- 全角ダッシュを線で描画

			w = lround(fontsize * dashw);
			if(w < 1) w = 1;

			if(fhorz)
			{
				x = topx + pi->x;
				y = topy + pi->y + fontsize / 2 - w / 2;
			}
			else
			{
				x = topx - pi->y - fontsize / 2 - w / 2;
				y = topy + pi->x;
			}
		}
		else if(fhorz || (flags & CHAR_F_INFO))
		{
			//---- 横組、ノンブル・柱
			// 基本版面の左上を 0,0 として、グリフの左上位置 or 用紙上の絶対位置

			x = pi->x;
			y = pi->y;

			if(!(flags & CHAR_F_PAPER_ABS))
			{
				if(flags & CHAR_F_INFO)
				{
					x += pt_info->x;
					y += pt_info->y;
				}
				else
				{
					x += topx;
					y += topy;
				}
			}
		}
		else
		{
			//----- 縦組
			// :通常時は、基本版面の右上を 0,0 とした、グリフの右上位置。
			// :縦中横時、y はグリフの左上の位置。
			
			y = topy + pi->x;

			if(flags & CHAR_F_TATEYOKO)
				//縦中横
				x = topx - pi->y;
			else
				//縦書き or 90度回転
				x = topx - fontsize - pi->y;
		}

		pi->x = x;
		pi->y = y;

		//移動

		if(flags & CHAR_F_DRAW_DASH)
		{
			//ダッシュ描画: 終端

			mListMove(&p->list_char, MLISTITEM(pi), NULL);

			if(!pidash) pidash = MLISTITEM(pi); //ダッシュの先頭
		}
		else if(flags & CHAR_F_TATEYOKO)
		{
			//縦中横: 90度回転、またはダッシュの前

			mListMove(&p->list_char, MLISTITEM(pi), (pirot)? pirot: pidash);
		}
		else if(flags & CHAR_F_VROTATE)
		{
			//90度回転: ダッシュの前

			mListMove(&p->list_char, MLISTITEM(pi), pidash);

			if(!pirot) pirot = MLISTITEM(pi);
		}

		//元々の終端

		if(pi == bottom) break;
	}
}


//==========================
// main
//==========================


/* 出力ページ単位のクリア */

static void _clear_output_page(OutputData *p)
{
	if(p->is_output_img)
		output_image_clear_output_page(p);
	else
		output_pdf_clear_output_page(p);
}

/* ページを出力
 *
 * no: ページ番号 */

static void _output_page(OutputData *p,int no,int flast)
{
	if(p->is_output_img)
		output_image_put_page(p, no, flast);
	else
		output_pdf_put_page(p, no, flast);
}

/* 情報の表示 */

static void _put_info(OutputData *p)
{
	mPoint pt;

	pt = p->pt_layout[1];
	pt.x -= p->bleed_w;
	pt.y -= p->bleed_w;

	printf("format: %s\nprefix: %s\n", p->fname_ext, p->prefix);

	printf("base layout, odd horz: %.2fmm | %.2fmm | %.2fmm\n",
		pt.x / 1000.0 * (25.4 / 72),
		p->layout_width / 1000.0 * (25.4 / 72),
		(p->paper_w - pt.x - p->layout_width) / 1000.0 * (25.4 / 72));

	printf("base layout, odd vert: %.2fmm | %.2fmm | %.2fmm\n",
		pt.y / 1000.0 * (25.4 / 72),
		p->layout_height / 1000.0 * (25.4 / 72),
		(p->paper_h - pt.y - p->layout_height) / 1000.0 * (25.4 / 72));

	printf("[output] ");
}

/** 出力処理 */

void output_proc_main(OutputData *p)
{
	int prevno = -1,pagecnt = 0,is_double2p;

	_put_info(p);

	is_double2p = (g_set->page_type == PAGETYPE_TWO_1P);

	while(1)
	{
		app_progress_setpos(pagecnt);

		if(g_app->signal_flag)
			app_end_signal();
	
		if(_read_page(p)) break;

		//出力単位ごとのページが変わった時
		// :2段組で、前とページ位置が同じなら対象外。
		// :見開き2P分なら、現在ページが偶数ページで切り替え。

		if(prevno == -1)
		{
			//最初のページは常にクリア
			
			_clear_output_page(p);
		}
		else if(p->curpage != prevno
			&& (!is_double2p || !(p->curpage & 1)) )
		{
			//出力ページの出力
			
			if(prevno != -1)
				_output_page(p, prevno, FALSE);

			//出力ページ単位のクリア

			_clear_output_page(p);
		}

		//現在のページのクリア
		// :2段組の下段時以外

		if(p->curpage != prevno)
		{
			if(p->is_output_img)
				output_image_clear_cur_page(p);
			else
				output_pdf_clear_cur_page(p);
		}

		//ページ/段を書き込み

		_set_char_pos(p);

		if(p->is_output_img)
			output_image_draw_page(p);
		else
			output_pdf_write_page(p);

		//

		mListDeleteAll(&p->list_char);
		mListDeleteAll(&p->list_bousen);

		prevno = p->curpage;
		pagecnt++;
	}

	//最後のページを出力

	if(prevno != -1)
		_output_page(p, prevno, TRUE);
}
