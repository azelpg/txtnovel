/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * テキスト読み込み処理
 *********************************/

#include <stdio.h>

#include <mlk.h>
#include <mlk_list.h>
#include <mlk_buf.h>
#include <mlk_str.h>

#include "app.h"
#include "setting.h"
#include "text.h"


//==============================
// 本文前の処理
//==============================


/* @ 以降のコマンド名取得。"[ { ;" の位置まで進む。
 *
 * @ の前/コマンド名の後に空白は使用可。
 *
 * return: 次の文字 */

static int32_t _get_setting_command_name(TextData *p,char *name)
{
	int32_t c;
	int len = 0;

	while(1)
	{
		c = text_setting_getchar(p);

		if(c == '{' || c == '[' || c == ';' || c == ' ')
		{
			if(!len) app_enderr("コマンド名の長さが 0 です");
		
			*name = 0;
			break;
		}
		else if((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || c == '-')
		{
			if(len > 31) app_enderr("コマンド名が長すぎます");

			*(name++) = c;
			len++;
		}
		else
			app_enderr("無効な文字です");
	}

	//空白スキップ

	if(c == ' ')
	{
		c = text_setting_skip_getchar(p);

		if(c != '{' && c != '[' && c != ';')
			app_enderr("コマンド名の後に { [ ; がありません");
	}

	return c;
}

/* @command[...] の定義名を取得。'{ ;' の位置まで進む
 *
 * [] 内に空白は入れないこと。
 *
 * return: 次の文字 */

static int32_t _get_setting_command_defname(TextData *p,char *name)
{
	int32_t c;
	int len = 0;

	while(1)
	{
		c = text_setting_getchar(p);

		if(c == ']')
		{
			if(!len)
				app_enderr("定義名の長さが 0 です");

			*name = 0;
			break;
		}
		else if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')
			|| (c >= '0' && c <= '9') || c == '-' || c == '_' || c == ':')
		{
			if(len > 31) app_enderr("定義名が長すぎます");

			*(name++) = c;
			len++;
		}
		else
			app_enderr("無効な文字です");
	}

	//'{ ;' までの空白をスキップ

	c = text_setting_skip_getchar(p);

	if(c != '{' && c != ';')
		app_enderr("コマンド名の後に { ; がありません");

	return c;
}

/* コマンド名と [] 内の名前を取得し、'{ ;' の位置まで進める
 *
 * 名前は 31 文字まで。
 * 
 * defname: [] がなければ空文字列
 * return: -1 で終端。次の文字 */

static int _get_setting_command(TextData *p,char *cmdname,char *defname)
{
	int32_t c;

	*defname = 0;

	while(1)
	{
		//空白をスキップ
		
		c = text_setting_skip_getchar(p);
		if(c == -1) return -1;

		//グループの終端の場合

		if(p->group_func && c == '}')
		{
			(p->group_func)(p);
			
			p->group_func = NULL;

			continue;
		}

		break;
	}

	//

	if(c != '@')
		app_enderr("無効な文字です");

	//コマンド名

	c = _get_setting_command_name(p, cmdname);

	//'[' の場合、定義名を取得

	if(c == '[')
		c = _get_setting_command_defname(p, defname);

	return c;
}

/* 本文前の設定部分の処理 */

static void _proc_settings(TextData *p)
{
	int32_t c;
	char cmdname[32],defname[32];

	while(1)
	{
		//コマンド名 + 定義名取得
		
		c = _get_setting_command(p, cmdname, defname);
		if(c == -1) break;

		//コマンド、引数の処理
		// :通常コマンドは、"} ;" まで読み込まれた状態になる。
		// :{} グループは、"{" の位置のまま

		if(text_proc_setting_command(p, c, cmdname, defname))
		{
			//1 で @start-body。次の行へ

			text_skip_to_enter(p);
			return;
		}
	}
}


//=======================
// 本文部分
//=======================


/* @ 以降のコマンド名を取得。'[ ;' の位置まで進む
 *
 * return: 次の文字 */

static int32_t _get_body_command(TextData *p,char *name)
{
	int32_t c;
	int len = 0;

	while(1)
	{
		c = text_body_getchar(p);

		if(c == '[' || c == ';')
		{
			if(!len) app_enderr("コマンド名の長さが 0 です");
		
			*name = 0;
			break;
		}
		else if((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || c == '-')
		{
			if(len > 31) app_enderr("コマンド名が長すぎます");

			*(name++) = c;
			len++;
		}
		else
			app_enderr("無効な文字です");
	}

	return c;
}

/* 本文部分の読み込み */

static void _proc_body(TextData *p)
{
	int32_t c,last_cmd,last_char;
	uint32_t last_pos;
	char cmdname[32];

	last_cmd = last_char = 0;
	last_pos = 0;

	while(1)
	{
		c = text_body_getchar(p);
		if(c == -1) break;

		if(c == '@')
		{
			//コマンド

			c = _get_body_command(p, cmdname);

			last_cmd = text_proc_body_command(p, c, cmdname);

			if(last_cmd == -1) break; //@end-body

			last_char = 0;
		}
		else
		{
			//--- 通常1文字

			if(last_char &&
				((c >= 0xFE00 && c <= 0xFE0F)|| (c >= 0xE0100 && c <= 0xE01EF)))
			{
				//UVS のセレクタの場合、前の文字を削除して追加

				text_add_body_char_uvs(p, c, last_char, last_pos);
			}
			else
			{
				//前がコマンドで、';' で終了している場合は、改行を無視

				if((c == '\n' && last_cmd == ';') || c == 0xFE00)
					last_char = 0;
				else
				{
					last_char = c;
					last_pos = p->buf_body.cursize;
					
					text_add_body_char(p, c);
				}
			}

			last_cmd = 0;
		}
	}

	text_add_body_end(p);

#if 0
	//データ確認
	uint8_t *ps;
	uint32_t size;
	int pcnt;

	ps = p->buf_body.buf;
	size = p->buf_body.cursize;

	for(pcnt = 0; size; ps++, size--)
	{
		printf("%02X ", *ps);

		pcnt++;
		if(pcnt == 16)
		{
			printf("\n");
			pcnt = 0;
		}
	}

	printf("\n");
#endif
}


//=======================
// main
//=======================


/* FileItem 破棄 */

static void _destroy_fileitem(mList *list,mListItem *item)
{
	if(((FileItem *)item)->fp)
		fclose(((FileItem *)item)->fp);
}

/* TextData 初期化 */

static TextData *_init_textdata(void)
{
	TextData *p;

	p = (TextData *)mMalloc0(sizeof(TextData));
	if(!p) app_enderr_alloc();

	g_app->proc_ptr = p;
	g_app->free_proc = textdata_free;

	//

	p->list.item_destroy = _destroy_fileitem;

	mStrAlloc(&p->str, 32);
	mStrAlloc(&p->str2, 64);

	if(!mBufAlloc(&p->buf_body, 128 * 1024, 128 * 1024))
		app_enderr_alloc();

	return p;
}

/* 値をチェック */

static void _check_settings(TextData *p)
{
	const char *err;

	g_app->puterr_proc = NULL; //エラー時、位置を表示しない

	err = setting_check_value();
	if(err) app_enderr(err);
}

/** テキストを読み込んで、本文データを作成 */

void app_read_text(void)
{
	TextData *p;

	p = _init_textdata();

	text_open_input(p, g_app->str_input.buf, NULL);

	//

	g_app->puterr_proc = text_put_error;

	//設定部分を読み込み

	_proc_settings(p);

	//本文部分

	_proc_body(p);

	//値をチェック

	_check_settings(p);

	//

	app_end_proc();
}
