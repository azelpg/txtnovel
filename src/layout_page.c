/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * レイアウト処理
 *********************************/

#include <stdio.h>

#include <mlk.h>

#include "app.h"
#include "setting.h"
#include "layout.h"



/* 通常文字を追加 */

static void _add_str(LayoutData *p,BodyChar *pc)
{
	int type;

	//一体として扱う範囲をまとめてセット

	if(pc->type == BODYCHAR_TYPE_UNICODE)
	{
		type = bodych_get_type(pc);

		if(type)
		{
			layout_str_addset_area(p, type);
			return;
		}
	}

	//1文字

	layout_str_addset_one(p, pc);
}

/* コマンドの戻り値を処理 */

static void _set_command_ret(LayoutData *p,int ret)
{
	switch(ret)
	{
		//ページ終了して次のデータへ
		case LAYOUT_CMDRET_END_PAGE:
			p->linedat.endtype = ENDTYPE_END_PAGE;
			break;
		//前の文字をページ出力して、次のデータへ
		case LAYOUT_CMDRET_OUTPUT_NEXT:
			p->linedat.endtype = ENDTYPE_CMD_AFTER;
			break;
		//前の文字をページ出力して、同じコマンドから処理
		case LAYOUT_CMDRET_OUTPUT_CUR:
			p->bchar.next = 0;
			p->linedat.endtype = ENDTYPE_CMD_AFTER;
			break;
	}
}

/* 段落(1行)分の文字データを読み込む
 *
 * 改行/終端/ページ切り替え系コマンドまで。
 * コマンドしかない場合など、文字データがない場合もあり。 */

static void _read_line(LayoutData *p)
{
	BodyChar *pc = &p->bchar;
	mListItem *laststr,*lastruby;
	int ret;

	//クリア

	mMemset0(&p->linedat, sizeof(LayoutLineDat));

	layout_set_linedat_cur(p);

	//最後の位置

	laststr = p->list_str.bottom;
	lastruby = p->list_ruby.bottom;

	//追加中、行に文字があるか判断するため、最後の位置をセット

	p->pagedat.pstr_top = (StrData *)laststr;

	//本文データから読み込み

	while(1)
	{
		ret = body_getchar_line(&p->buf, pc);

		if(ret)
		{
			//段落終了
			p->linedat.endtype = ret;
			break;
		}
		else if(pc->type == BODYCHAR_TYPE_COMMAND)
		{
			//コマンド
			// :ret = 0 以外で、ページとして強制終了

			ret = layout_proc_command(p, pc->code);

			if(ret)
			{
				_set_command_ret(p, ret);
				break;
			}
		}
		else
			//文字
			_add_str(p, pc);
	}

	//目次と見出しは同時に使えない

	if(p->linedat.caption && p->linedat.index)
		app_enderr("目次と見出しは同時に指定できません");

	//

	p->pagedat.pstr_top = layout_str_getdat_lasttop(p, laststr);
	p->pagedat.pr_top = layout_ruby_getdat_lasttop(p, lastruby);

	//規定の空き量をセット

	layout_str_setline_rule_space(p);

	//ルビの長さと字間をセット

	layout_ruby_set_line(p);
}

/* 行の折返しと、行の長さ調整 */

static void _set_line(LayoutData *p)
{
	StrData *pstr;

	//データがなく、改行のみの場合

	if(!p->pagedat.pstr_top && p->linedat.endtype == ENDTYPE_ENTER)
	{
		p->pagedat.linenum++;
		return;
	}

	//字上げ幅
	p->linedat.indent_foot_width = p->linedat.indent_foot * p->base_fontsize;

	//字上げ幅を除く行長
	p->linedat.line_length_max = p->line_length - p->linedat.indent_foot_width;

	//各行の処理

	for(pstr = p->pagedat.pstr_top; pstr; )
	{
		//行頭の処理
		// :区切り約物の後、和字間隔のみで終わっている場合 NULL
		
		p->linedat.pstr_top = pstr = layout_proc_line_top(p, pstr);

		if(!pstr) break;

		//行末位置を決めて、処理

		layout_proc_line_bottom(p);

		//行の長さを調整

		layout_set_line_length(p);

		//次行へ

		pstr = p->linedat.pstr_next;

		p->pagedat.linenum++;
	}
}

/* 次のページ/段へ */

static void _next_page(LayoutData *p)
{
	int cmd;

	cmd = p->linedat.endtype;

	if(p->pagedat.linenum != p->pagedat.linenum_page
		|| cmd == ENDTYPE_ENTER)
	{
		//折り返しなどで次ページにデータがある場合
		// or 丁度ページの終端で改行終わり
		// => 改ページ or 改段

		cmd = (p->is_column)? ENDTYPE_NEWCOLUMN: ENDTYPE_NEWPAGE;
	}
	else if(cmd == ENDTYPE_CMD_AFTER || cmd == ENDTYPE_FINISH)
	{
		//[終端][レイアウト変更][画像 cur(前の文字出力)]
		// => 改ページ

		cmd = ENDTYPE_NEWPAGE;
	}
	
	//

	if(p->is_column && cmd == ENDTYPE_NEWCOLUMN)
	{
		//2段組: 改段
		
		p->cur_column ^= 1;

		if(!p->cur_column)
			layout_next_page(p);
	}
	else
	{
		//改ページ

		layout_next_page(p);

		if(g_set->page_type != PAGETYPE_ONE)
		{
			//見開き
			
			if(cmd == ENDTYPE_NEWPAGE_ODD)
			{
				//改丁 (偶数ページなら、空ページ追加)

				if(!(p->pageno & 1))
				{
					layout_write_page(p, WRITEPAGE_F_EMPTY_BODY);
					layout_next_page(p);
				}
			}
			else if(cmd == ENDTYPE_NEWPAGE_EVEN)
			{
				//改見開き (奇数ページなら、空ページ追加)
				
				if(p->pageno & 1)
				{
					layout_write_page(p, WRITEPAGE_F_EMPTY_BODY);
					layout_next_page(p);
				}
			}
		}
	}
}

/* 画像ページの出力
 *
 * return: 1 で、出力した */

static int _write_image(LayoutData *p)
{
	LayoutImageItem *pi;
	int f;

	pi = (LayoutImageItem *)p->list_image.top;
	if(!pi) return 0;

	//2段組で下段位置の場合は除く

	if(p->is_column && p->cur_column) return 0;

	//

	f = 0;

	switch(pi->page)
	{
		//ページの切り替え時
		case BODY_IMG_PAGE_CUR:
		case BODY_IMG_PAGE_INS:
			f = 1;
			break;
		//奇数ページ
		case BODY_IMG_PAGE_INS_LEFT:
			f = (p->pageno & 1);
			break;
		//偶数ページ
		case BODY_IMG_PAGE_INS_RIGHT:
			f = !(p->pageno & 1);
			break;
	}

	//出力

	if(f)
	{
		layout_write_image_page(p);
		return 1;
	}

	return 0;
}

/* linenum から、linenum_page をセット */

static void _set_linenum_page(LayoutData *p)
{
	int n;

	n = p->pagedat.linenum;

	if(n > p->layout->line_num)
		n = p->layout->line_num;

	p->pagedat.linenum_page = n;
}

/* 1ページ/段分のデータを取得 */

static void _get_page_data(LayoutData *p)
{
	int fnext;

	while(1)
	{
		p->pagedat.lineno = p->pagedat.linenum;
	
		//1行分の文字を取得
		// :ここで、最初にレイアウト変更が行われる可能性があるので、
		// :行数などの設定値は、ここより前にローカル変数にセットしたりせずに、直接参照すること。
		
		_read_line(p);

		//行の折返しと長さ調整
		// :折返しで、次ページを超える範囲がある場合あり

		_set_line(p);

		//linenum_page セット

		_set_linenum_page(p);

		//--- ここから先は、linenum_page を必要に応じて調整

		//目次と見出しの設定
		// :目次と見出しは折り返し禁止なので、この時点で、次ページを超えないことは保証されている

		fnext = layout_setline_caption_index(p);

		//描画位置、行位置をセット

		layout_setline_drawpos(p);

		//強制的に終了 or 行数を超えた or 終端が改行以外で終了

		if(fnext
			|| p->pagedat.linenum >= p->layout->line_num //[!]レイアウトの変更時、1行分取得時に値が変わるので、直接参照
			|| p->linedat.endtype != ENDTYPE_ENTER)
			break;
	}
}

/* 1ページ/1段分の本文の処理 */

static void _proc_page(LayoutData *p)
{
	//ページの初期値

	layout_set_pagedat_cur(p);

	//前ページからの残りがある場合、終端タイプセット

	if(p->pagedat.endtype)
		p->linedat.endtype = p->pagedat.endtype;

	//ページデータ
	// :linenum,linenum_page をセットする

	if(p->pagedat.linenum >= p->layout->line_num
		|| (p->pagedat.endtype && p->pagedat.endtype != ENDTYPE_ENTER))
	{
		//前ページからの折り返しなどで、すでに行数を超えている場合
		// or 前ページからの残りがあって、行数は超えていないが、終端が改ページ扱いの場合

		_set_linenum_page(p);
	}
	else
	{
		//[行数を超える/終端/ページ切り替え] まで取得
		// :文字がない場合もあり。

		_get_page_data(p);

		if(p->linedat.endtype == ENDTYPE_END_PAGE) return;
	}

	//行位置から、次のページの文字のフラグを ON

	layout_set_nextpage_flag(p);

	//------ 文字のページ出力

	//終端で文字がない場合は何もしない
	// :ただし、画像が残っている場合は、タイミングが来るまで空白ページを追加

	if(!p->list_image.top
		&& p->linedat.endtype == ENDTYPE_FINISH && !layout_has_page_char(p))
		return;

	//ページ確定後の処理
	// :list_str, list_str2 に分割される

	layout_proc_page(p);

	//出力

	layout_write_page(p, 0);

	//残っている次ページの行を除いて削除

	layout_delete_curpage(p);

	//次のページ/段へ

	_next_page(p);
}

/* ページデータのクリア */

static void _next_pagedat(LayoutData *p)
{
	LayoutPageDat *pd = &p->pagedat;
	int lnum,endtype,pdfsep;

	endtype = 0;

	pdfsep = (pd->pdfsep == 2);

	//次のページの行数 (未処理の行数)

	lnum = pd->linenum - pd->linenum_page;

	if(lnum) endtype = p->linedat.endtype;

	//クリア

	mMemset0(pd, sizeof(LayoutPageDat));

	pd->linenum = lnum;
	pd->endtype = endtype;
	pd->pdfsep = pdfsep;
}

/* 情報の表示 */

static void _put_info(void)
{
	LayoutItem *pl = g_set->layout_base;

	printf("%d x %d mm\n",
		g_set->paper_w + g_set->bleed * 2,
		g_set->paper_h + g_set->bleed * 2);

	printf("base layout: %d x %d\n",
		pl->line_len.val / pl->basefont.size,
		pl->line_num);
}

/** 本文データを全て処理 */

void layout_proc_all(LayoutData *p)
{
	_put_info();
	
	printf("[layout] ");

	//先頭が終端で、残り行/画像がなくなるまで処理

	p->bchar.next = 1;

	while(1)
	{
		app_progress_setpos(p->buf - g_app->buf_body);
	
		if(body_is_finish(p->buf)
			&& !p->pagedat.linenum
			&& !p->list_image.top)
			break;

		//画像ページが出力されなかった場合、通常の処理
		// :画像が複数ある時、連続で挿入される場合がある。

		if(!_write_image(p))
			_proc_page(p);

		//ページデータをクリア

		_next_pagedat(p);
	}

	//debug

#if DEBUG_PUT_LAYOUT
	layout_debug_put_layout(p);
#endif
}
