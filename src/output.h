/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

typedef struct _Font Font;
typedef struct _ImageItem ImageItem;
typedef struct _Image Image;
typedef struct _PDFData PDFData;

/* 文字アイテム */

typedef struct
{
	mListItem i;

	uint16_t gid,
		flags;
	int32_t x,y,
		width,		//全角ダッシュ時、線の長さ
		fontsize;
	Font *font;
}CharItem;

enum
{
	CHAR_F_TATEYOKO = 1<<0,		//縦中横
	CHAR_F_VROTATE = 1<<1,		//90度回転
	CHAR_F_INFO = 1<<2,			//ノンブル・柱 (位置は左上を 0,0 とする)
	CHAR_F_PAPER_ABS = 1<<3,	//裁ち切り含む、用紙上の絶対位置
	CHAR_F_DRAW_DASH = 1<<4		//全角ダッシュを線として描画 (width が長さ)
};

/* 傍線 */

typedef struct
{
	mListItem i;

	uint8_t type;
	int32_t x,y,fontsize,len;
}BousenItem;

#define BOUSEN_WIDTH   0.05
#define BOUSEN_DOUBLE  0.15

/* 出力データ */

typedef struct
{
	uint8_t is_horz,	//横組か
		is_column,		//2段組みか
		is_output_img,	//画像出力か
		outimg_bits,	//出力画像の初期ビット数
		pageno_dig,		//ページの桁数
		pdfsep_flag;	//PDF分割フラグ

	int format,
		dpi,			//画像の出力 DPI (int)
		dpi_output,		//画像出力時の DPI (dpi 指定ならあり。width,height 指定なら 0)
		width,			//画像の出力サイズ (1P分)
		height,
		width_output,	//出力のサイズ (複数ページ分)
		antialias;		//0 or 1

	int32_t curpage,	//現在読み込まれたページ
		start_page,		//先頭のページ (0 でなし)
		page_flags,		//読み込んだページのフラグ
		paper_w,		//裁ち切りを含まない用紙サイズ (pt 単位)
		paper_h,
		paper_fullw,	//裁ち切りを含む用紙サイズ
		paper_fullh,
		bleed_w,		//裁ち切りの幅
		line_length,	//行長 (1段の高さ)
		layout_width,	//基本版面のサイズ
		layout_height,
		basefont_size,		//base フォントサイズ
		basefont_height,	//base フォントの横書き高さ
		rubyfont_size,
		bkgndcol;		//出力ページ単位の背景色 (-1 でタイル背景)

	double dpi_image,
		img_mulw,			//画像出力時、用紙位置 -> イメージ位置用の掛ける値
		img_mulh;

	mPoint pt_layout[2],	//偶数/奇数ページの基本版面位置
		pt_baselayout[2],	//ベースレイアウトでの基本版面位置
		pt_layout_top;		//現在ページの、先頭文字の開始位置

	mList list_char,	//文字データ
		list_bousen;	//傍線データ

	mStr str_tmp;		//作業用文字列
	mRect rc_number;	//隠しノンブルの背景の範囲

	ImageInfo img_page;	//ページ画像 (item == NULL でなし)

	FILE *fpin;			//レイアウトデータ
	PDFData *pdf;		//PDF 出力用データ
	const char *prefix,	//出力ファイル名先頭
		*fname_ext;		//出力ファイルの拡張子

	LayoutItem *layout;		//現在のレイアウト
	Font *basefont,
		*rubyfont;
	ImageItem *img_bkgnd,	//現在のレイアウトの背景画像
		*img_bkgnd_left,
		*img_bkgnd_right;
	Image *outimg;			//(画像出力時) 出力用画像
}OutputData;

enum
{
	PAGE_F_COLUMN2 = 1<<0,		//2段組の下段
	PAGE_F_HAVE_BODY = 1<<1,	//本文文字がある
	PAGE_F_HAVE_INDEX = 1<<2,	//目次タイトルを含む
	PAGE_F_PDFSEP = 1<<3		//PDF 分割
};

//---------------

void output_proc_main(OutputData *p);

/* sub */

void output_change_layout(OutputData *p);
void output_get_image_pos(OutputData *p,ImageItem *piimg,ImageInfo *info,mPoint *dst);
void output_set_imagesize(OutputData *p,ImageInfo *info);
ImageItem *output_get_page_bkgndimg(OutputData *p);

void output_set_index_glyph(OutputData *p);
void output_add_index_char(OutputData *p);

/* image */

void output_image_init(OutputData *p);
void output_image_change_layout(OutputData *p);
void output_image_clear_output_page(OutputData *p);
void output_image_clear_cur_page(OutputData *p);
void output_image_put_page(OutputData *p,int no,int flast);
void output_image_draw_page(OutputData *p);

/* pdf */

void output_pdf_init(OutputData *p);
void output_pdf_clear_output_page(OutputData *p);
void output_pdf_clear_cur_page(OutputData *p);
void output_pdf_write_page(OutputData *p);
void output_pdf_put_page(OutputData *p,int no,int flast);
void output_pdf_change_layout(OutputData *p);
