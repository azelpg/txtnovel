/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

typedef struct _LayoutItem LayoutItem;
typedef struct _PaperPos PaperPos;
typedef struct _FontSpec FontSpec;
typedef struct _ImageInfo ImageInfo;

//1mm = N pt
#define UNIT_1MM_PT  (72.0/25.4)

/* 読み込みファイルアイテム */

typedef struct
{
	mListItem i;
	
	FILE *fp;
	uint8_t *buf;	//NULL 以外で、バッファの位置
	int line,cpos;	//戻った時の次の位置
	char filename[0];	//ファイルのパス (エラー出力用)
}FileItem;

/* @def アイテム */

typedef struct
{
	mListItem i;
	char name[32];	//定義名
	uint32_t hash;	//名前のハッシュ値
	uint8_t str[0];	//文字列
}DefItem;

/* 処理データ */

typedef struct _TextData
{
	mBuf buf_body;	//本文データ
	mList list,		//読み込みファイルのネストリスト
		list_def;	//@def 定義リスト
	mStr str,
		str2;		//作業用
	
	void *data;		//作業用。エラー時は解放される
	void (*free_data)(void *data);	//data の解放関数

	void (*group_func)(struct _TextData *p); //設定時、{} のグループ中

	LayoutItem *curlayout;	//現在設定中のレイアウト

	FileItem *cur;
	int line,cpos,	//次の文字位置 (line = 1-。cpos = 0〜。0 で行頭)
		cur_line,	//1文字読み込み後、その文字の位置
		cur_cpos;
}TextData;

/* 1文字 */

#define CHAR_F_BSLASH  (1<<30) //'\'+char の通常文字
#define CHAR_F_TOP     (1<<29) //行頭文字
#define CHAR_F_CID     (1<<28) //CID
#define CHAR_MASK      (~CHAR_F_TOP)	//TOP 以外のマスク
#define CHAR_MASK_CODE 0xffffff	//文字コードのマスク
#define CHAR_MASK_CID  (CHAR_F_CID | CHAR_MASK_CODE)	//文字コード+CID


/*---- func ---*/

#define text_cmp(s1,s2)     (!strcmp(s1, s2))
#define text_cmpi(s1,s2)    (!strcasecmp(s1, s2))
#define text_cmpin(s1,s2,n) (!strncasecmp(s1, s2, n))

int text_proc_setting_command(TextData *p,int32_t c,char *cmdname,char *defname);
int32_t text_proc_body_command(TextData *p,int32_t c,char *cmdname);

/* sub */

void textdata_free(void *ptr,int err);
void text_put_error(void *ptr);

void *text_alloc_data(TextData *p,int size,void (*freefunc)(void *));
void text_free_data(TextData *p);

void text_close_input(TextData *p);
void text_open_input(TextData *p,const char *filename,DefItem *item);

char *text_get_textbase_filename(const char *filename);
DefItem *text_search_defitem(TextData *p,const char *name);
ImageItem *text_add_imagefile(char *fname);

int32_t text_get_char(TextData *p);
int text_readchar_next(TextData *p,int ch);
void text_skip_to_enter(TextData *p);
int32_t text_setting_getchar(TextData *p);
int32_t text_setting_skip_getchar(TextData *p);
int32_t text_body_getchar(TextData *p);

void text_add_body_end(TextData *p);
void text_add_body_cmdno(TextData *p,int no);
void text_add_body_byte(TextData *p,uint8_t v);
void text_add_body_ptr(TextData *p,void *ptr);
void text_add_body_char(TextData *p,int32_t c);
void text_add_body_char_uvs(TextData *p,int32_t c,int32_t last_c,uint32_t lastpos);

int text_search_item_name(const char *name,const uint16_t *posarr);

int text_get_split_next(char *str,char ch,char **ppnext);

int text_get_string_yesno(TextData *p,const char *str);
int text_get_string_int(TextData *p,const char *str,int min,int max);
int text_get_string_int_comma(TextData *p,char *str,int maxnum,int min,int max,int *dst);
int text_get_string_comma_double_to_int(TextData *p,char *str,int maxnum,int *dst);
int text_get_string_index(TextData *p,const char *val,const char *strs);
uint32_t text_get_string_char(TextData *p,const char *str);
void text_get_string_paperpos(TextData *p,char *str,PaperPos *dst);
uint32_t text_get_string_flags(TextData *p,char *str,const char *flagstr);
uint32_t text_get_string_rgb(TextData *p,const char *str);
void text_get_string_fontname(TextData *p,const char *str,FontSpec *dst);
double text_get_string_fontsize(TextData *p,char *str,FontSpec *dst);
void text_get_string_imgsize(TextData *p,char *str,ImageInfo *dst);
int text_get_string_imgpos(TextData *p,char *str,ImageInfo *dst);
void text_get_string_body_indent(TextData *p,uint8_t *dst);
void text_get_string_body_align(TextData *p,uint8_t *dst,mlkbool fpage);
void text_get_string_comma_item(TextData *p,mStr *str,void (*func)(TextData *p,const char *name,const char *value));
