/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * レイアウト処理: 文字クラスなど
 *********************************/

#include <mlk.h>

#include "layout_dat.h"


//--------------

//2010 - 2049
#define MAP1_ST 0x2010
#define MAP1_ED 0x2049

static const uint8_t g_map1[58] = {
 3, 0, 0, 3, 0, 0, 0, 0, 1, 2, 0, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4 };

//3000 - 30FE
#define MAP2_ST 0x3000
#define MAP2_ED 0x30FE

static const uint8_t g_map2[255] = {
10, 7, 6, 0, 0, 8, 0, 0, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 0, 0, 1, 2, 1, 2, 1, 2, 0, 0, 3, 1, 0, 2,
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0,
 0,12,11,12,11,12,11,12,11,12,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
11,11,11,12,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
11,11,11,12,11,12,11,12,11,11,11,11,11,11,12,11,11,11,11,11,11,12,12, 0, 0, 0, 0, 0, 0, 8, 8, 0,
 3,12,11,12,11,12,11,12,11,12,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
11,11,11,12,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
11,11,11,12,11,12,11,12,11,11,11,11,11,11,12,11,11,11,11,11,11,12,12,11,11,11,11, 5, 9, 8, 8 };

//FF01 - FF60
#define MAP3_ST 0xFF01
#define MAP3_ED 0xFF60

static const uint8_t g_map3[96] = {
 4, 0, 0, 0, 0, 0, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 0, 0, 0, 4, 0,
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0,
 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 0, 1, 2 };

//小書きかな 31F0-31FF 対応
#define SMALLKANA_ST 0x31F0
#define SMALLKANA_ED 0x31FF
static const uint16_t *g_big_kana = u"クシストヌハヒフヘホムラリルレロ";

//クラスフラグ

static const uint8_t g_class_flags[] = {
	//NONE
	0,
	//始め括弧
	STRCLASS_F_HALF_WIDTH | STRCLASS_F_ENDKAKKO_NEXT_BETA | STRCLASS_F_BREAK_FOOT | STRCLASS_F_NO_ADD_SPACE | STRCLASS_F_RUBY_OVER_BEFORE,
	//終わり括弧
	STRCLASS_F_HALF_WIDTH | STRCLASS_F_ENDKAKKO_NEXT_BETA | STRCLASS_F_BREAK_HEAD | STRCLASS_F_NO_ADD_SPACE | STRCLASS_F_RUBY_OVER_AFTER,
	//ハイフン
	STRCLASS_F_BREAK_HEAD | STRCLASS_F_NO_ADD_SPACE,
	//区切り約物
	STRCLASS_F_BREAK_HEAD | STRCLASS_F_NO_ADD_SPACE,
	//中点
	STRCLASS_F_BREAK_HEAD | STRCLASS_F_NO_ADD_SPACE | STRCLASS_F_RUBY_OVER_AFTER | STRCLASS_F_RUBY_OVER_BEFORE,
	//句点
	STRCLASS_F_HALF_WIDTH | STRCLASS_F_ENDKAKKO_NEXT_BETA | STRCLASS_F_BREAK_HEAD | STRCLASS_F_HANG | STRCLASS_F_NO_ADD_SPACE | STRCLASS_F_RUBY_OVER_AFTER,
	//読点
	STRCLASS_F_HALF_WIDTH | STRCLASS_F_ENDKAKKO_NEXT_BETA | STRCLASS_F_BREAK_HEAD | STRCLASS_F_HANG | STRCLASS_F_NO_ADD_SPACE | STRCLASS_F_RUBY_OVER_AFTER,
	//繰り返し記号
	STRCLASS_F_BREAK_HEAD,
	//長音
	STRCLASS_F_BREAK_HEAD | STRCLASS_F_RUBY_OVER_AFTER | STRCLASS_F_RUBY_OVER_BEFORE,
	//和字間隔
	STRCLASS_F_NO_ADD_SPACE /*| STRCLASS_F_RUBY_OVER_AFTER | STRCLASS_F_RUBY_OVER_BEFORE*/,
	//かな
	STRCLASS_F_RUBY_OVER_AFTER | STRCLASS_F_RUBY_OVER_BEFORE,
	//小書きのかな
	STRCLASS_F_BREAK_HEAD | STRCLASS_F_RUBY_OVER_AFTER | STRCLASS_F_RUBY_OVER_BEFORE,
	//分離禁止文字
	STRCLASS_F_RUBY_OVER_AFTER | STRCLASS_F_RUBY_OVER_BEFORE
};

//--------------


/** 文字クラス取得 (欧文は除く)
 *
 * rule_book: 行頭禁則で対象外にするものは文字クラスを変更する */

int layout_get_char_class(uint32_t c,int rule_book)
{
	int no;

	if(c == ' ')
		return STRCLASS_ENSPACE;
	else if(c >= SMALLKANA_ST && c <= SMALLKANA_ED)
		//小書き仮名の一部
		no = STRCLASS_SMALL_KANA;
	else if(c >= MAP1_ST && c <= MAP1_ED)
	{
		no = g_map1[c - MAP1_ST];
	}
	else if(c >= MAP2_ST && c <= MAP2_ED)
	{
		no = g_map2[c - MAP2_ST];
	}	
	else if(c >= MAP3_ST && c <= MAP3_ED)
	{
		no = g_map3[c - MAP3_ST];
	}
	else
		no = 0;

	if(!no) return 0;

	//禁則処理関連の調整

	if(rule_book)
	{
		if(c == u'々')
			return 0;
		else if(no == STRCLASS_TYOUON)
			return STRCLASS_KANA;
		else if(no == STRCLASS_SMALL_KANA)
			return STRCLASS_KANA;
	}

	return no;
}

/** クラスのフラグを取得 */

uint8_t layout_get_class_flags(int cno)
{
	if(cno <= STRCLASS_NOSEP)
		return g_class_flags[cno];
	else
		return 0;
}

/** 小書き仮名を通常の仮名に変換
 *
 * それ以外は、そのまま返す */

uint32_t layout_ruby_kana_to_big(uint32_t c)
{
	//小書き仮名以外

	if(layout_get_char_class(c, FALSE) != STRCLASS_SMALL_KANA)
		return c;

	//

	if((c >= u'ぁ' && c <= u'ぉ')
		|| (c >= u'ァ' && c <= u'ォ')
		|| c == u'っ' || c == u'ッ'
		|| (c >= u'ゃ' && c <= u'ょ')
		|| (c >= u'ャ' && c <= u'ョ'))
	{
		if(c & 1) c++;
	}
	else if(c >= SMALLKANA_ST && c <= SMALLKANA_ED)
		return g_big_kana[c - SMALLKANA_ST];
	else if(c >= 0x308E && c <= 0x30F6)
	{
		switch(c)
		{
			case u'ゎ': c = u'わ'; break;
			case u'ヮ': c = u'ワ'; break;
			case u'ゕ': c = u'か'; break;
			case u'ゖ': c = u'け'; break;
			case u'ヵ': c = u'カ'; break;
			case u'ヶ': c = u'ケ'; break;
		}
	}

	return c;
}

/** 平仮名/片仮名かどうか */

mlkbool is_unicode_kana(uint32_t c)
{
	int n;

	if(c >= SMALLKANA_ST && c <= SMALLKANA_ED)
		//小書き仮名の一部
		return TRUE;
	else if(c >= MAP2_ST && c <= MAP2_ED)
	{
		//かな、小書きかな
		
		n = g_map2[c - MAP2_ST];

		if(n == STRCLASS_KANA || n == STRCLASS_SMALL_KANA)
			return TRUE;
	}

	return FALSE;
}
