/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

//===========================
// 設定用コマンド定義
//===========================

/* 関数時のパラメータ */

typedef struct
{
	uint8_t multi_proc,	//複数項目 + 定義名あり時、0=最初、1=項目、2=終了
		def_param;		//CommandDef の param
	char *defname;		//[] 内の定義名
	void *ptr;			//自由に使って良い
}CommandParam;

/* コマンド定義 */

typedef struct
{
	uint8_t val,
		flags,
		param;
	uint16_t name_pos;
	int (*func)(TextData *p,CommandParam *);
}CommandDef;

//値のタイプ
enum
{
	CMDVAL_END,
	CMDVAL_STR,		//{} 内に1つの文字列
	CMDVAL_MULTI,	//name:value; の複数項目
	CMDVAL_RAW,		//{} 内はすべて対象
	CMDVAL_NONE,	//なし (; で終わる)
	CMDVAL_GROUP	//{} で囲まれたグループ
};

//フラグ
enum
{
	CMDFLAG_HAVE_DEFNAME = 1<<0,	//@command[] の定義名が必要
	CMDFLAG_MULTI_PROC = 1<<1,		//CMDVAL_MULTI で、開始と終了の処理が必要
	CMDFLAG_START_BODY = 1<<2		//@start-body
};

enum
{
	MULTI_PROC_FIRST,
	MULTI_PROC_ITEM,
	MULTI_PROC_END
};

enum
{
	CMDFUNC_RET_OK,
	CMDFUNC_RET_STR_INVALID,	//1つの文字列で、値が正しくない
	CMDFUNC_RET_ITEM_UNDEF,		//未定義の項目名
	CMDFUNC_RET_ITEM_INVALID	//項目の値が正しくない
};

/*---- func ----*/

static int _cmdval_output(TextData *p,CommandParam *param);
static int _cmdval_paper(TextData *p,CommandParam *param);
static int _cmdval_pdf(TextData *p,CommandParam *param);
static int _cmdval_include(TextData *p,CommandParam *param);
static int _cmdval_def(TextData *p,CommandParam *param);
static int _cmdval_font(TextData *p,CommandParam *param);
static int _cmdval_kenten(TextData *p,CommandParam *param);
static int _cmdval_fullwidth_set(TextData *p,CommandParam *param);
static int _cmdval_image_type(TextData *p,CommandParam *param);
static int _cmdval_page_title(TextData *p,CommandParam *param);
static int _cmdval_page_number(TextData *p,CommandParam *param);
static int _cmdval_layout(TextData *p,CommandParam *param);

static int _cmdval_mode(TextData *p,CommandParam *param);
static int _cmdval_layout_space(TextData *p,CommandParam *param);
static int _cmdval_layout_font(TextData *p,CommandParam *param);
static int _cmdval_layout_align(TextData *p,CommandParam *param);
static int _cmdval_layout_bodyalign(TextData *p,CommandParam *param);
static int _cmdval_line(TextData *p,CommandParam *param);
static int _cmdval_indent(TextData *p,CommandParam *param);
static int _cmdval_index(TextData *p,CommandParam *param);
static int _cmdval_dash_width(TextData *p,CommandParam *param);
static int _cmdval_text_color(TextData *p,CommandParam *param);
static int _cmdval_background(TextData *p,CommandParam *param);
static int _cmdval_caption(TextData *p,CommandParam *param);
static int _cmdval_ruby(TextData *p,CommandParam *param);
static int _cmdval_process(TextData *p,CommandParam *param);
static int _cmdval_gsub(TextData *p,CommandParam *param);

/* レイアウト外のコマンド */

static const CommandDef g_command_def[] = {
 {CMDVAL_RAW, CMDFLAG_HAVE_DEFNAME, 0, CMPSTR_POS_DEF, _cmdval_def}, //def
 {CMDVAL_MULTI, CMDFLAG_HAVE_DEFNAME | CMDFLAG_MULTI_PROC, 0, CMPSTR_POS_FONT, _cmdval_font}, //font
 {CMDVAL_STR, CMDFLAG_HAVE_DEFNAME, 0, CMPSTR_POS_FULLWIDTH_SET, _cmdval_fullwidth_set}, //fullwidth-set
 {CMDVAL_MULTI, CMDFLAG_HAVE_DEFNAME | CMDFLAG_MULTI_PROC, 0, CMPSTR_POS_KENTEN, _cmdval_kenten}, //kenten
 {CMDVAL_STR, 0, 0, CMPSTR_POS_INCLUDE, _cmdval_include}, //include
 {CMDVAL_STR, 0, 0, CMPSTR_POS_IMAGE_TYPE, _cmdval_image_type}, //image-type
 {CMDVAL_GROUP, CMDFLAG_HAVE_DEFNAME, 0, CMPSTR_POS_LAYOUT, _cmdval_layout}, //layout
 {CMDVAL_MULTI, 0, 0, CMPSTR_POS_OUTPUT, _cmdval_output}, //output
 {CMDVAL_MULTI, 0, 0, CMPSTR_POS_PAPER, _cmdval_paper}, //paper
 {CMDVAL_MULTI, 0, 0, CMPSTR_POS_PDF, _cmdval_pdf}, //pdf
 {CMDVAL_MULTI, 0, 0, CMPSTR_POS_PAGE_TITLE, _cmdval_page_title}, //page-title
 {CMDVAL_MULTI, 0, 0, CMPSTR_POS_PAGE_NUMBER, _cmdval_page_number}, //page-number
 {CMDVAL_NONE, CMDFLAG_START_BODY, 0, CMPSTR_POS_START_BODY, NULL}, //start-body
 {CMDVAL_END, 0,0,0,0}
};

/* レイアウト内のコマンド */

static const CommandDef g_command_def_layout[] = {
 {CMDVAL_STR, 0, 0, CMPSTR_POS_ALIGN, _cmdval_layout_align}, //align
 {CMDVAL_MULTI, 0, 0, CMPSTR_POS_BACKGROUND, _cmdval_background}, //background
 {CMDVAL_STR, 0, 0, CMPSTR_POS_BODY_ALIGN, _cmdval_layout_bodyalign}, //body-align
 {CMDVAL_MULTI, 0, 0, CMPSTR_POS_CAPTION1 ,_cmdval_caption}, //caption1
 {CMDVAL_MULTI, 0, 1, CMPSTR_POS_CAPTION2, _cmdval_caption}, //caption2
 {CMDVAL_MULTI, 0, 2, CMPSTR_POS_CAPTION3, _cmdval_caption}, //caption3
 {CMDVAL_STR, 0, 0, CMPSTR_POS_DASH_WIDTH, _cmdval_dash_width}, //dash-width
 {CMDVAL_MULTI, 0, 0, CMPSTR_POS_FONT, _cmdval_layout_font}, //font
 {CMDVAL_STR, 0, 0, CMPSTR_POS_GSUB, _cmdval_gsub}, //gsub
 {CMDVAL_MULTI, 0, 0, CMPSTR_POS_INDENT, _cmdval_indent}, //indent
 {CMDVAL_MULTI, 0, 0, CMPSTR_POS_INDEX, _cmdval_index}, //index
 {CMDVAL_MULTI, 0, 0, CMPSTR_POS_LINE, _cmdval_line}, //line
 {CMDVAL_STR, 0, 0, CMPSTR_POS_MODE, _cmdval_mode}, //mode
 {CMDVAL_MULTI, 0, 0, CMPSTR_POS_PROCESS, _cmdval_process}, //process
 {CMDVAL_MULTI, 0, 0, CMPSTR_POS_RUBY, _cmdval_ruby}, //ruby
 {CMDVAL_MULTI, 0, 0, CMPSTR_POS_SPACE, _cmdval_layout_space}, //space
 {CMDVAL_STR, 0, 0, CMPSTR_POS_TEXT_COLOR, _cmdval_text_color}, //text-color
 {CMDVAL_END, 0,0,0,0}
};

//---------------

//output

static const uint16_t g_comitem_output[] = {
 CMPSTR_POS_PREFIX, //prefix
 CMPSTR_POS_FORMAT, //format
 CMPSTR_POS_DPI, //dpi
 CMPSTR_POS_WIDTH, //width
 CMPSTR_POS_HEIGHT, //height
 CMPSTR_POS_AA, //aa
 CMPSTR_POS_HINTING, //hinting
 CMPSTR_POS_GRID, //grid
 CMPSTR_POS_GRID_COLOR, //grid-color
 CMPSTR_POS_PDF_FIXED, //pdf-fixed
 CMPSTR_POS_JPEG, //jpeg
 0xffff
};

//paper

static const uint16_t g_comitem_paper[] = {
 CMPSTR_POS_SIZE, //size
 CMPSTR_POS_BLEED, //bleed
 CMPSTR_POS_PAGE, //page
 0xffff
};

//pdf

static const uint16_t g_comitem_pdf[] = {
 CMPSTR_POS_TITLE, //title
 CMPSTR_POS_AUTHOR, //author
 CMPSTR_POS_SUBJECT, //subject
 CMPSTR_POS_NO_UNICODE, //no-unicode
 CMPSTR_POS_OUTLINE, //outline
 0xffff
};

//page-title,page-number

static const uint16_t g_comitem_page_title_number[] = {
 CMPSTR_POS_FONT, //font
 CMPSTR_POS_FONTSIZE, //fontsize
 CMPSTR_POS_POS, //pos
 CMPSTR_POS_VSPACE, //vspace
 CMPSTR_POS_HSPACE, //hspace
 CMPSTR_POS_NO_PAGE, //no-page
 0xffff
};

//caption

static const uint16_t g_comitem_caption[] = {
 CMPSTR_POS_FONT, //font
 CMPSTR_POS_FONTSIZE, //fontsize
 CMPSTR_POS_INDENT, //indent
 CMPSTR_POS_WIDTH, //width
 CMPSTR_POS_BEFORE, //before
 CMPSTR_POS_AFTER, //after
 CMPSTR_POS_ALIGN, //align
 CMPSTR_POS_SEP, //sep
 0xffff
};

