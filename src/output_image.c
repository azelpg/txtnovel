/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * 出力: 画像出力
 *********************************/

#include <stdio.h>
#include <math.h>

#include <mlk.h>
#include <mlk_list.h>
#include <mlk_str.h>
#include <mlk_rectbox.h>

#include "app.h"
#include "setting.h"
#include "output.h"
#include "font/font.h"
#include "image.h"


/* 出力画像は、内容に合わせてビット数を自動で判断する。
 * そのため、outimg->imgbits を、描画時の内容に合わせてセット。 */


/* 画像を、用紙サイズに合わせてリサイズ */

static void _resize_image(OutputData *p,Image *img,ImageInfo *info)
{
	int w,h;

	if(info->width < 0) return;

	output_set_imagesize(p, info);

	w = lround(info->width  / 72000.0 * p->dpi_image);
	h = lround(info->height / 72000.0 * p->dpi_image);

	if(!image_resize(img, w, h))
		app_enderr("画像のリサイズに失敗");
}

/* RGB 色のビット数を適用 */

static int _apply_color_bits(int bits,uint32_t c)
{
	int r,g,b,add;

	if(bits == 24) return 24;

	r = MLK_RGB_R(c);
	g = MLK_RGB_G(c);
	b = MLK_RGB_B(c);
	add = r + g + b;

	if(r != g || g != b)
		//1,8 -> 24
		return 24;
	else if(bits == 1 && add != 0 && add != 255 * 3)
		//1 -> 8
		return 8;
	else
		return bits;
}

/* 背景画像の読み込み */

static void _load_bkgnd_image(OutputData *p,ImageItem *curimg,ImageItem *img)
{
	//同じ背景なら、そのまま

	if(curimg == img) return;

	//以前の画像を解放
	
	app_image_free(curimg);

	//読み込み

	if(img)
	{
		app_image_load(img);

		//用紙サイズに合わせてリサイズ
		// :tile 時は常に等倍

		if(p->layout->bkgndimg.base != IMAGEINFO_BASE_TILE)
			_resize_image(p, img->img, &p->layout->bkgndimg);
	}
}

/** 初期化 */

void output_image_init(OutputData *p)
{
	//FreeType 初期化

	if(freetype_init())
		app_enderr("FreeType の初期化に失敗");
	
	//FreeType フォント読み込み

	setting_load_ftfont(p->antialias);

	//出力用画像作成

	p->outimg = image_new(p->width_output, p->height);

	//

	p->img_mulw = (double)p->width / p->paper_fullw;
	p->img_mulh = (double)p->height / p->paper_fullh;
}

/** レイアウト変更時 */

void output_image_change_layout(OutputData *p)
{
	int bits;

	//背景画像読み込み

	_load_bkgnd_image(p, p->img_bkgnd, p->layout->bkgndimg.item);
	_load_bkgnd_image(p, p->img_bkgnd_left, p->layout->bkgnd_img_left);
	_load_bkgnd_image(p, p->img_bkgnd_right, p->layout->bkgnd_img_right);

	//出力画像の初期ビット数

	bits = (p->antialias)? 8: 1;

	bits = _apply_color_bits(bits, p->layout->textcol);
	bits = _apply_color_bits(bits, p->layout->bkgndcol);

	p->outimg_bits = bits;
}

/** 出力ページのクリア (現在のページを先頭とする) */

void output_image_clear_output_page(OutputData *p)
{
	ImageItem *piimg;

	//出力画像の初期ビット数
	// :最初のページのアンチエイリアス・テキスト色・背景色から

	p->outimg->imgbits = p->outimg_bits;

	//見開き2Pの場合の先頭ページ位置
	// :基本的に偶数位置。
	// :先頭ページの場合のみ、奇数になることがあるので、その場合は -1

	p->start_page = p->curpage;

	if(p->curpage & 1) p->start_page--;

	//背景
	// :タイル画像と背景色は、出力ページ単位で塗りつぶす。
	// :見開き2Pの場合、先頭と終端で空のページがあれば、最初と最後の背景と同じになる。
	// :見開き2Pの場合、2P目でタイル画像に変わった場合は、適用されない。

	piimg = output_get_page_bkgndimg(p);

	if(piimg && p->layout->bkgndimg.base == IMAGEINFO_BASE_TILE)
	{
		image_fill_tile(p->outimg, piimg->img);

		p->bkgndcol = -1;
	}
	else
	{
		image_clear(p->outimg, p->layout->bkgndcol);

		p->bkgndcol = p->layout->bkgndcol;
	}
}

/** 現在のページのクリア */

void output_image_clear_cur_page(OutputData *p)
{
	ImageItem *piimg;
	mPoint pt;

	//出力画像のビット数
	// :見開き2Pの2P目でレイアウトが変わった時

	if(p->outimg->imgbits < p->outimg_bits)
		p->outimg->imgbits = p->outimg_bits;

	//背景色
	// :出力ページ単位で塗りつぶされているので、見開き2Pでレイアウトが変わった時だけ塗る。
	// :タイル画像で塗られている場合は除く。

	if(p->bkgndcol != -1 && p->layout->bkgndcol != p->bkgndcol)
	{
		image_fillbox(p->outimg, (p->curpage & 1)? 0: p->width, 0,
			p->width, p->height, p->layout->bkgndcol);
	}

	//背景画像を描画

	piimg = output_get_page_bkgndimg(p);

	if(piimg && p->layout->bkgndimg.base != IMAGEINFO_BASE_TILE)
	{
		output_get_image_pos(p, piimg, &p->layout->bkgndimg, &pt);
	
		image_blt(p->outimg, pt.x, pt.y, piimg->img);
	}
}

/** 画像の出力 */

void output_image_put_page(OutputData *p,int no,int flast)
{
	if(g_set->page_type == PAGETYPE_TWO_1P)
	{
		//(<page_left>-<page_right>)
		// :1P目と2P目でページ番号が連続していない場合あり
	
		mStrSetFormat(&p->str_tmp, "%s%0*d-%0*d.%s",
			p->prefix, p->pageno_dig, no, p->pageno_dig, p->start_page,
			p->fname_ext);
	}
	else
		mStrSetFormat(&p->str_tmp, "%s%0*d.%s", p->prefix, p->pageno_dig, no, p->fname_ext);

	//ビット数は描画内容で判断

	image_savefile(p->outimg, p->str_tmp.buf, p->format, p->outimg->imgbits, p->dpi_output);
}


//==========================
// 描画
//==========================


/* 水平線を描画 */

static void _draw_line_horz(OutputData *p,Image *img,double x,double y,double w,double h,uint32_t col)
{
	int nw,nh;

	nw = lround(w);
	
	if(p->antialias)
		image_drawline_horz_aa(img, x, y, nw, h, col);
	else
	{
		nh = lround(h);
		if(nh < 1) nh = 1;

		image_fillbox(img, floor(x), floor(y), nw, nh, col);
	}
}

/* 垂直線を描画 */

static void _draw_line_vert(OutputData *p,Image *img,double x,double y,double w,double h,uint32_t col)
{
	int nw,nh;

	nh = lround(h);
	
	if(p->antialias)
		image_drawline_vert_aa(img, x, y, w, nh, col);
	else
	{
		nw = lround(w);
		if(nw < 1) nw = 1;

		image_fillbox(img, floor(x), floor(y), nw, nh, col);
	}
}

/** ページの描画 */

void output_image_draw_page(OutputData *p)
{
	CharItem *pi;
	BousenItem *pibs;
	Image *img;
	Font *font;
	ImageItem *piimg;
	mPoint pt;
	int32_t fontsize,fhorz,pagex;
	uint32_t col;
	double mulw,mulh,dashw,dx,dy,dw;
	mBox box;

	img = p->outimg;
	col = p->layout->textcol;
	fhorz = p->is_horz;

	mulw = p->img_mulw;
	mulh = p->img_mulh;

	if(g_set->page_type == PAGETYPE_TWO_1P && !(p->curpage & 1))
		pagex = p->width;
	else
		pagex = 0;

	dashw = p->layout->dash_width / 1000.0;

	//---- ページの画像・グリッド

	piimg = p->img_page.item;

	if(piimg && piimg != IMAGE_ITEM_NOFILE)
	{
		app_image_load(piimg);

		//tile 時は、デフォルトのサイズ

		_resize_image(p, piimg->img, &p->img_page);

		output_get_image_pos(p, piimg, &p->img_page, &pt);

		image_blt(img, pt.x, pt.y, piimg->img);
		
		app_image_free(piimg);
	}
	else if(g_set->output.grid)
	{
		//グリッド (画像がある時はなし)
	
		image_draw_grid(img,
			p->pt_layout_top.x, p->pt_layout_top.y,
			p->basefont_size, p->basefont_height,
			p->line_length, p->layout->line_feed.val, p->layout->line_num,
			fhorz, mulw, mulh, pagex, g_set->output.grid_col);
	}

	//---- 隠しノンブルの背景色

	if(!mRectIsEmpty(&p->rc_number))
	{
		mBoxSetRect(&box, &p->rc_number);

		image_fillbox(img,
			(int)floor(box.x * mulw) + pagex, floor(box.y * mulh),
			lround(box.w * mulw), lround(box.h * mulh), g_set->number_hide_col);
	}

	//----- 文字

	font = NULL;
	fontsize = 0;

	MLK_LIST_FOR(p->list_char, pi, CharItem)
	{
		//フォントサイズ
		
		if(font != pi->font || fontsize != pi->fontsize)
		{
			font = pi->font;
			fontsize = pi->fontsize;

			fontft_set_size(font, fontsize, p->dpi_image);
		}

		//描画

		if(pi->flags & CHAR_F_DRAW_DASH)
		{
			//全角ダッシュを線で描画

			dx = pi->x * mulw + pagex;
			dy = pi->y * mulh;

			if(fhorz)
			{
				//水平

				_draw_line_horz(p, img, dx, dy,
					pi->width * mulw, fontsize * dashw * mulh, col);
			}
			else
			{
				//垂直

				_draw_line_vert(p, img, dx, dy,
					fontsize * dashw * mulw, pi->width * mulh, col);
			}
		}
		else if(fhorz || (pi->flags & (CHAR_F_INFO | CHAR_F_TATEYOKO)))
		{
			//横書き
			// :座標は [26:6]

			fontft_draw_glyph_horz(font, img,
				(int)floor(pi->x * mulw * 64) + pagex * 64, floor(pi->y * mulh * 64), pi->gid, col);
		}
		else
		{
			//縦書き or 90度回転

			fontft_draw_glyph_vert(font, img,
				(int)floor(pi->x * mulw * 64) + pagex * 64, floor(pi->y * mulh * 64),
				pi->gid, col, pi->flags & CHAR_F_VROTATE);
		}
	}

	//---- 傍線

	MLK_LIST_FOR(p->list_bousen, pibs, BousenItem)
	{
		dx = pibs->x * mulw + pagex;
		dy = pibs->y * mulh;
		fontsize = pibs->fontsize;
		dw = fontsize * BOUSEN_WIDTH;
	
		if(fhorz)
		{
			_draw_line_horz(p, img, dx, dy, pibs->len * mulw, dw * mulh, col);

			if(pibs->type == 2)
			{
				_draw_line_horz(p, img,
					dx, dy + fontsize * BOUSEN_DOUBLE * mulh,
					pibs->len * mulw, dw * mulh, col);
			}
		}
		else
		{
			_draw_line_vert(p, img, dx, dy, dw * mulw, pibs->len * mulh, col);

			if(pibs->type == 2)
			{
				_draw_line_vert(p, img,
					dx + fontsize * BOUSEN_DOUBLE * mulw, dy,
					dw * mulw, pibs->len * mulh, col);
			}
		}
	}
}


