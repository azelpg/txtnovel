/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

typedef struct _PDFData PDFData;
typedef struct _FontSubsetInfo FontSubsetInfo;
typedef struct _FontItem FontItem;
typedef struct _ImageItem ImageItem;
typedef struct _Font Font;

enum
{
	PDF_DRAWGLYPH_F_HORZ = 1<<0,	//横書き
	PDF_DRAWGLYPH_F_VROTATE = 1<<1,	//横書きを90度回転
	PDF_DRAWGLYPH_F_NO_COORD = 1<<2	//前の文字から連続した位置
};


void pdf_new(PDFData **dst,int paperw,int paperh);
void pdf_free(PDFData *p);

void pdf_end_init(PDFData *p);

void pdf_openfile(PDFData *p,const char *filename);
void pdf_endfile(PDFData *p,int page_start,int page_end);

void pdf_add_font(PDFData *p,FontItem *fontitem,FontSubsetInfo *info);

void pdf_clear_page(PDFData *p);
void pdf_set_pageno(PDFData *p,int pageno);
void pdf_add_page(PDFData *p);

void pdf_draw_image(PDFData *p,ImageItem *piimg,int x,int y,int w,int h);
void pdf_draw_fillbox(PDFData *p,int x,int y,int w,int h);
void pdf_draw_fillbox_col(PDFData *p,int x,int y,int w,int h,uint32_t col);
void pdf_set_color(PDFData *p,uint32_t col);
void pdf_draw_change_font(PDFData *p,Font *font,int fontsize,mlkbool horz);
void pdf_draw_glyph(PDFData *p,Font *font,int x,int y,uint16_t gid,uint8_t flags);
void pdf_end_text(PDFData *p);

