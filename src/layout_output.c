/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * レイアウト: 出力
 *********************************/

#include <stdio.h>
#include <math.h>

#include <mlk.h>
#include <mlk_list.h>
#include <mlk_stdio.h>

#include "app.h"
#include "setting.h"
#include "layout.h"
#include "layout_file.h"
#include "font/font.h"


//--------------------

typedef struct
{
	layoutpos lastx,lasty;
}_posdat;

//--------------------


/* 文字データを出力
 *
 * lastx は関数後、各自でセット */

static void _write_char(FILE *fp,layoutpos x,layoutpos y,uint16_t gid,uint8_t flags,_posdat *dat)
{
	int32_t n,nx,ny;
	int16_t wx,wy;

	//---- フラグ

	//x 位置の変更
	// :lastx = 前回の文字に、元グリフの送り幅を足した位置

	if(x != dat->lastx)
	{
		n = x - dat->lastx;

		if(n < INT16_MIN || n > INT16_MAX)
		{
			nx = x;
			flags |= LFILE_CHARF_HAVE_X_ABS;
		}
		else
		{
			wx = n;
			flags |= LFILE_CHARF_HAVE_X_REL;
		}

		dat->lastx = x;
	}

	//y 位置の変更
	// :lasty = 文字の左上の位置

	if(y != dat->lasty)
	{
		n = y - dat->lasty;

		if(n < INT16_MIN || n > INT16_MAX)
		{
			ny = y;
			flags |= LFILE_CHARF_HAVE_Y_ABS;
		}
		else
		{
			wy = n;
			flags |= LFILE_CHARF_HAVE_Y_REL;
		}

		dat->lasty = y;
	}

	//-----

	//フラグ

	fwrite(&flags, 1, 1, fp);

	//GID

	fwrite(&gid, 1, 2, fp);

	//x 位置

	if(flags & LFILE_CHARF_HAVE_X_REL)
		fwrite(&wx, 1, 2, fp);
	else if(flags & LFILE_CHARF_HAVE_X_ABS)
		fwrite(&nx, 1, 4, fp);

	//y 位置

	if(flags & LFILE_CHARF_HAVE_Y_REL)
		fwrite(&wy, 1, 2, fp);
	else if(flags & LFILE_CHARF_HAVE_Y_ABS)
		fwrite(&ny, 1, 4, fp);
}

/* 本文文字を出力
 *
 * type: [0]基本フォント [1]基本フォント以外 */

static void _write_strchar(LayoutData *p,FILE *fp,int type)
{
	StrData *pstr;
	StrChar *pch;
	FontItem *lastfont;
	int32_t lastsize;
	int i;
	uint8_t flags;
	_posdat dat;

	dat.lastx = dat.lasty = 0;

	pstr = (type)? (StrData *)p->list_str2.top: (StrData *)p->list_str.top;

	lastfont = NULL;
	lastsize = 0;

	for(; pstr; pstr = (StrData *)pstr->i.next)
	{
		if(pstr->flags & STR_FLAG_NEXT_PAGE) break;

		//欧文/和字間隔はアキとして扱うため、除外

		if(pstr->classno == STRCLASS_ENSPACE
			|| pstr->classno == STRCLASS_JASPACE)
		{
			continue;
		}

		//フォント

		if(type && (lastfont != pstr->font || lastsize != pstr->fontsize))
		{
			if(lastfont)
				mFILEwriteByte(fp, LFILE_CHARF_END);
		
			fwrite(&pstr->fontsize, 1, 4, fp);
			fwrite(&pstr->font->font, 1, sizeof(void *), fp);

			lastfont = pstr->font;
			lastsize = pstr->fontsize;

			dat.lastx = dat.lasty = 0;
		}

		//

		pch = pstr->chtop;
	
		for(i = pstr->len; i; i--, pch = (StrChar *)pch->i.next)
		{
			flags = 0;

			//欧文90度回転

			if(pch->flags & STRCHAR_FLAG_VROTATE)
				flags |= LFILE_CHARF_VROTATE;

			//縦中横

			if(pch->flags & STRCHAR_FLAG_TATEYOKO)
				flags |= LFILE_CHARF_TATEYOKO;

			//全角ダッシュ文字

			if(pch->code == 0x2014 || pch->code == 0x2015)
				flags |= LFILE_CHARF_DASH_CHAR;

			//

			_write_char(fp, pch->x, pch->y, pch->gid, flags, &dat);

			if(pch->flags & STRCHAR_FLAG_TATEYOKO)
				dat.lasty = pch->y - pch->raw_width;
			else
				dat.lastx = pch->x + pch->raw_width;
		}
	}

	mFILEwriteByte(fp, LFILE_CHARF_END);
}

/* ルビ文字出力 */

static void _write_rubychar(LayoutData *p,FILE *fp)
{
	RubyData *pr;
	RubyChar *pch;
	StrData *pstr;
	int i;
	uint8_t flags;
	_posdat dat;

	dat.lastx = dat.lasty = 0;

	MLK_LIST_FOR(p->list_ruby, pr, RubyData)
	{
		pstr = pr->parent;

		if(pstr->flags & STR_FLAG_NEXT_PAGE) break;

		//

		pch = pr->chtop;

		for(i = pr->len; i; i--, pch = (RubyChar *)pch->i.next)
		{
			flags = 0;
			
			//欧文90度回転

			if(pch->flags & RUBYCHAR_FLAG_VROTATE)
				flags |= LFILE_CHARF_VROTATE;

			//全角ダッシュ文字

			if(pch->code == 0x2014 || pch->code == 0x2015)
				flags |= LFILE_CHARF_DASH_CHAR;

			//

			_write_char(fp, pch->x, pch->y, pch->gid, flags, &dat);

			dat.lastx = pch->x + pch->width;
		}
	}

	mFILEwriteByte(fp, LFILE_CHARF_END);
}

/* 傍線データを出力 (各リストごと) */

static void _write_bousen(LayoutData *p,FILE *fp,mList *list)
{
	StrData *pstr;
	Font *font;
	int32_t x,y,w,curx,cury,curw,cursize,fontsize,fhorz,ffirst,posh,posv,height;
	uint8_t type,curtype;

	fhorz = p->is_horz;
	fontsize = -1;
	posh = posv = 0;
	ffirst = 1;

	MLK_LIST_FOR(*list, pstr, StrData)
	{
		if(pstr->flags & STR_FLAG_NEXT_PAGE) break;

		if(!pstr->bousen) continue;

		if(fontsize != pstr->fontsize)
		{
			fontsize = pstr->fontsize;
			font = pstr->font->font;
			posv = fontsize * 8 / 100;
			posh = fontsize * 10 / 100;

			if(fhorz) height = lround((double)(font->ascent - font->descent) / font->emsize * fontsize);
		}

		//行調整で追加されたアキは、幅として追加する

		type = pstr->bousen;
		x = pstr->x - pstr->sp_before - pstr->width_before;
		y = pstr->y;
		w = pstr->width + pstr->sp_after + pstr->sp_before + pstr->width_before;

		if(fhorz)
			y += height + posh;
		else
			y -= posv;

		//

		if(ffirst)
		{
			ffirst = 0;
			curtype = type;
			curx = x;
			cury = y;
			curw = w;
			cursize = fontsize;
		}
		else
		{
			if(type == curtype && x == curx + curw && y == cury && fontsize == cursize)
				//同じタイプが繋がっている
				curw += w;
			else
			{
				//前を出力し、新しい位置に

				fwrite(&curtype, 1, 1, fp);
				fwrite(&curx, 1, 4, fp);
				fwrite(&cury, 1, 4, fp);
				fwrite(&fontsize, 1, 4, fp);
				fwrite(&curw, 1, 4, fp);

				curtype = type, curx = x, cury = y, curw = w, cursize = fontsize;
			}
		}
	}

	//最後のデータ

	if(!ffirst)
	{
		fwrite(&curtype, 1, 1, fp);
		fwrite(&curx, 1, 4, fp);
		fwrite(&cury, 1, 4, fp);
		fwrite(&cursize, 1, 4, fp);
		fwrite(&curw, 1, 4, fp);
	}
}

/* ノンブル出力
 *
 * 基本版面の左上を (0,0) として、グリフの左上位置 or 用紙上の絶対位置
 *
 * hide: 隠しノンブルか
 * return: ノンブル全体の幅 */

static int _write_number(LayoutData *p,FILE *fp,int hide)
{
	mList list = MLIST_INIT;
	NormalChar *pch;
	_posdat dat;
	int w;
	uint8_t flags;

	w = layout_set_number_char(p, &list, hide);

	dat.lastx = dat.lasty = 0;

	MLK_LIST_FOR(list, pch, NormalChar)
	{
		flags = (hide)? LFILE_CHARF_PAPER_ABS: 0;
	
		_write_char(fp, pch->x, pch->y, pch->gid, flags, &dat);

		dat.lastx = pch->x + pch->width;
	}

	mFILEwriteByte(fp, LFILE_CHARF_END);

	mListDeleteAll(&list);

	return w;
}

/* 柱の文字列を出力
 *
 * 基本版面の左上を (0,0) として、グリフの左上位置 */

static void _write_title(LayoutData *p,FILE *fp,int num_w)
{
	NormalChar *pch;
	_posdat dat;

	layout_set_title_drawpos(p, num_w);

	dat.lastx = dat.lasty = 0;

	MLK_LIST_FOR(p->list_title, pch, NormalChar)
	{
		_write_char(fp, pch->x, pch->y, pch->gid, 0, &dat);

		dat.lastx = pch->x + pch->width;
	}

	mFILEwriteByte(fp, LFILE_CHARF_END);
}

/** ページ/段データをファイルに出力
 *
 * ※折り返しで次ページに行く文字は除く。 */

void layout_write_page(LayoutData *p,uint32_t wflags)
{
	FILE *fp = p->fpout;
	int32_t n,finfo,num_w = 0;
	uint8_t flags;

	p->write_pagenum++;
	p->last_write_pageno = p->pageno;

	//ノンブル・柱を出力するか

	finfo = layout_get_title_number_show(p, wflags & WRITEPAGE_F_IMAGE);

	//---------- 書き込み

	//ページ番号

	n = p->pageno;
	fwrite(&n, 1, 4, fp);

	//----- フラグ

	flags = 0;

	//2段組の下段

	if(p->is_column && p->cur_column)
		flags |= LFILE_PAGEF_COLUMN2;

	//本文あり

	if(!(wflags & WRITEPAGE_F_EMPTY_BODY) && layout_has_page_char(p))
		flags |= LFILE_PAGEF_HAVE_BODY;

	//画像ページ

	if(wflags & WRITEPAGE_F_IMAGE)
		flags |= LFILE_PAGEF_HAVE_IMAGE;

	//ノンブルあり

	if(finfo & 1)
		flags |= LFILE_PAGEF_HAVE_NUMBER;

	//柱あり

	if((finfo & 2) && p->list_title.top)
		flags |= LFILE_PAGEF_HAVE_TITLE;

	//目次タイトルを含む

	if(p->pagedat.has_index)
		flags |= LFILE_PAGEF_HAVE_INDEX;

	//PDF分割
	// :2段組で、下段にセットされた場合は、動作しない

	if(p->pagedat.pdfsep)
		flags |= LFILE_PAGEF_PDF_SEP;

	fwrite(&flags, 1, 1, fp);

	//---- データ

	//本文

	if(flags & LFILE_PAGEF_HAVE_BODY)
	{
		//基本フォントでの本文文字

		_write_strchar(p, fp, 0);

		//ルビ文字

		_write_rubychar(p, fp);

		//基本フォント以外での本文文字

		if(p->list_str2.top)
			_write_strchar(p, fp, 1);

		n = 0;
		fwrite(&n, 1, 4, fp);

		//傍線

		_write_bousen(p, fp, &p->list_str);
		_write_bousen(p, fp, &p->list_str2);

		mFILEwriteByte(fp, 0);
	}

	//ノンブル

	if(flags & LFILE_PAGEF_HAVE_NUMBER)
		num_w = _write_number(p, fp, finfo & 4);

	//柱

	if(flags & LFILE_PAGEF_HAVE_TITLE)
		_write_title(p, fp, num_w);

	//画像

	if(flags & LFILE_PAGEF_HAVE_IMAGE)
		fwrite(&((LayoutImageItem *)p->list_image.top)->info, 1, sizeof(ImageInfo), fp);
}

/** レイアウト変更を出力 */

void layout_write_change_layout(LayoutData *p)
{
	int32_t cmd = LFILE_CMD_LAYOUT;

	fwrite(&cmd, 1, 4, p->fpout);
	fwrite(&p->layout, 1, sizeof(void *), p->fpout);
}

/** 画像ページを出力 */

void layout_write_image_page(LayoutData *p)
{
	LayoutImageItem *pi;

	pi = (LayoutImageItem *)p->list_image.top;

	//PDF 分割
	// :次のページもフラグを ON にする

	if(pi->flags & BODY_IMG_FLAGS_PDFSEP)
		p->pagedat.pdfsep = 2;

	//ノンブル表示

	p->pagedat.number_type = pi->pagenum;

	//出力

	layout_write_page(p, WRITEPAGE_F_EMPTY_BODY | WRITEPAGE_F_IMAGE);

	//削除

	mListDelete(&p->list_image, MLISTITEM(pi));

	//次ページへ

	layout_next_page(p);
}


//=================================
// debug
//=================================


#if DEBUG_PUT_LAYOUT

/* 文字データ
 *
 * return: type=1 で終了 */

static int _putdebug_chars(FILE *fp,int type)
{
	uint8_t fb;
	uint16_t gid;
	int32_t nval;
	int16_t wval;
	void *ptr;

	//フォント

	if(type)
	{
		mFILEreadOK(fp, &nval, 4);
		if(!nval) return 1;
		
		mFILEreadOK(fp, &ptr, sizeof(void *));

		printf("<font:%p, %d>\n", ptr, nval);
	}

	//

	while(1)
	{
		//フラグ
	
		mFILEreadOK(fp, &fb, 1);

		if(fb == LFILE_CHARF_END) break;

		printf("flags:0x%02X", fb);

		//GID

		mFILEreadOK(fp, &gid, 2);
		printf(", GID:%d", gid);

		//x

		if(fb & LFILE_CHARF_HAVE_X_REL)
		{
			mFILEreadOK(fp, &wval, 2);
			printf(", rx:%d", wval);
		}
		else if(fb & LFILE_CHARF_HAVE_X_ABS)
		{
			mFILEreadOK(fp, &nval, 4);
			printf(", x:%d", nval);
		}

		//y

		if(fb & LFILE_CHARF_HAVE_Y_REL)
		{
			mFILEreadOK(fp, &wval, 2);
			printf(", ry:%d", wval);
		}
		else if(fb & LFILE_CHARF_HAVE_Y_ABS)
		{
			mFILEreadOK(fp, &nval, 4);
			printf(", y:%d", nval);
		}

		putchar('\n');
	}

	return 0;
}

/* 傍線データ */

static void _putdebug_bousen(FILE *fp)
{
	int32_t x,y,w,fs;
	uint8_t type;

	while(1)
	{
		mFILEreadOK(fp, &type, 1);
		if(!type) break;

		mFILEreadOK(fp, &x, 4);
		mFILEreadOK(fp, &y, 4);
		mFILEreadOK(fp, &fs, 4);
		mFILEreadOK(fp, &w, 4);

		printf("%d,%d,%d,%d,%d\n", type, x, y, fs, w);
	}
}

/* ページデータを出力 */

static void _putdebug_page(FILE *fp)
{
	uint8_t flags;

	//flags

	mFILEreadOK(fp, &flags, 1);

	printf("pageflag: 0x%02X\n", flags);

	//本文

	if(flags & LFILE_PAGEF_HAVE_BODY)
	{
		//基本フォントの文字

		printf("<body>\n");
		_putdebug_chars(fp, 0);

		//ルビ文字

		printf("<ruby>\n");
		_putdebug_chars(fp, 0);

		//基本フォント以外の文字

		while(!_putdebug_chars(fp, 1));

		//傍線

		printf("<bousen>\n");
		_putdebug_bousen(fp);
	}

	//ノンブル

	if(flags & LFILE_PAGEF_HAVE_NUMBER)
	{
		printf("<number>\n");
		_putdebug_chars(fp, 0);
	}
	
	//柱

	if(flags & LFILE_PAGEF_HAVE_TITLE)
	{
		printf("<title>\n");
		_putdebug_chars(fp, 0);
	}

	//画像
	
	if(flags & LFILE_PAGEF_HAVE_IMAGE)
	{
		fseek(fp, sizeof(ImageInfo), SEEK_CUR);
	}
}

/* 各コマンド */

static void _putdebug_cmd(FILE *fp,int no)
{
	switch(no)
	{
		case LFILE_CMD_LAYOUT:
			printf("<layout>\n");
			fseek(fp, sizeof(void *), SEEK_CUR);
			break;
	}
}

/** レイアウトデータを stdout に出力 */

void layout_debug_put_layout(LayoutData *p)
{
	FILE *fp = p->fpout;
	int32_t cmd;

	rewind(fp);

	while(1)
	{
		if(mFILEreadOK(fp, &cmd, 4)) break;

		if(cmd > 0)
		{
			printf("--- page:%d ---\n", cmd);
			
			_putdebug_page(fp);

			printf("----------\n");
		}
		else
			_putdebug_cmd(fp, cmd);
	}
}

#endif
