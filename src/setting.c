/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/********************************
 * 設定データ
 ********************************/

#include <string.h>
#include <math.h>

#include <mlk.h>
#include <mlk_str.h>
#include <mlk_list.h>
#include <mlk_unicode.h>

#include "app.h"
#include "setting.h"
#include "font/font.h"


//-------------------

SettingData *g_set = NULL;

//-------------------


/** mm -> pt (x 1000) 変換 */

int convert_mm_to_pt(int mm)
{
	return lround(mm / 25.4 * 72 * 1000);
}


//========================


/* フォントアイテム破棄 */

static void _destroy_fontitem(mList *list,mListItem *item)
{
	font_free(((FontItem *)item)->font);
}

/** 解放 */

void setting_free(void)
{
	SettingData *p = g_set;

	if(p)
	{
		mStrFree(&p->pdf.str_title);
		mStrFree(&p->pdf.str_author);
		mStrFree(&p->pdf.str_subject);
		mStrFree(&p->pdf.str_outline);

		mStrFree(&p->output.str_prefix);

		mListDeleteAll(&p->list_font);
		mListDeleteAll(&p->list_layout);
		mListDeleteAll(&p->list_kenten);
		mListDeleteAll(&p->list_fullwidth);

		mFree(p);
	}
}

/** 確保 */

void setting_new(void)
{
	SettingData *p;

	//確保

	p = g_set = (SettingData *)mMalloc0(sizeof(SettingData));
	if(!p)
		app_enderr_alloc();

	p->list_font.item_destroy = _destroy_fontitem;

	//デフォルト値

	p->paper_w = 105; //A6
	p->paper_h = 148;
	p->number_hide_x = 5;
	p->number_hide_y = 10;
	p->number_hide_col = 0xffffff;
	
	p->title.no_page = TITLENUMBER_PAGE_EVEN | TITLENUMBER_PAGE_BLANK | TITLENUMBER_PAGE_IMAGE;
	p->number.no_page = TITLENUMBER_PAGE_BLANK;

	p->output.grid_col = 0x80d0ff;
}


//=======================
// レイアウト
//=======================


/* レイアウトデータをコピー */

static void _layout_copy(LayoutItem *dst,LayoutItem *src)
{
	mListItem i;

	i = dst->i;

	memcpy(dst, src, sizeof(LayoutItem));

	dst->i = i;
}

/* 目次のデフォルト値をセット */

static void _set_default_caption(LayoutItem *pi)
{
	CaptionLayout *p;
	int i;

	for(i = 0; i < 3; i++)
	{
		pi->caption[i].before = 1;
		pi->caption[i].after = 1;
		pi->caption[i].width = 1;
	}

	//caption1

	p = pi->caption;
	p->font.size = 1300;
	p->font.unit = UNIT_TYPE_X;
	p->indent = 3;
	p->after = 2;

	//caption2

	p = pi->caption + 1;
	
	p->font.size = 1100;
	p->font.unit = UNIT_TYPE_X;
	p->indent = 5;

	//caption3

	p = pi->caption + 2;

	p->indent = 7;
	p->sep = 1;
}

/** レイアウトを追加
 *
 * すでにレイアウトがある場合は、そのポインタを返す。
 *
 * name_copy: NULL または空文字列以外で、指定レイアウトを継承 */

const char *setting_add_layout(const char *name,const char *name_copy,LayoutItem **dst)
{
	LayoutItem *pi,*picopy = NULL;

	//同名のものがすでにある場合

	pi = setting_search_layout(name);
	if(pi)
	{
		*dst = pi;
		return NULL;
	}

	//コピーするレイアウト

	if(name_copy && *name_copy)
	{
		picopy = setting_search_layout(name_copy);
		if(!picopy) return "継承するレイアウトが存在しません";
	}

	//追加

	pi = (LayoutItem *)mListAppendNew(&g_set->list_layout, sizeof(LayoutItem));
	if(!pi) return "メモリが足りません";

	//

	if(picopy)
		//コピー
		_layout_copy(pi, picopy);
	else
	{
		//デフォルト値
		
		pi->hang_flag = 1;
		pi->reduce_flag = 1;
		pi->bkgndcol = 0xffffff;
		pi->dash_width = 50; //0.05
		pi->gsub = LAYOUT_GSUB_RUBY | LAYOUT_GSUB_NLCK | LAYOUT_GSUB_HKNA | LAYOUT_GSUB_VKNA;

		_set_default_caption(pi);
	}

	strcpy(pi->name, name);

	*dst = pi;

	//base レイアウト

	if(strcmp(name, "base") == 0)
		g_set->layout_base = pi;

	return NULL;
}

/** レイアウト名から検索 */

LayoutItem *setting_search_layout(const char *name)
{
	LayoutItem *pi;

	MLK_LIST_FOR(g_set->list_layout, pi, LayoutItem)
	{
		if(strcmp(name, pi->name) == 0)
			return pi;
	}

	return NULL;
}


//=======================
// フォント
//=======================


/* フォント読み込み */

static const char *_load_font(Font **dst,const char *filename,int index)
{
	FontItem *pi;

	//同じファイル名、index のフォントがあるか

	MLK_LIST_FOR(g_set->list_font, pi, FontItem)
	{
		if(strcmp(filename, pi->filename) == 0 && index == pi->index)
		{
			font_addref(pi->font);

			*dst = pi->font;

			return NULL;
		}
	}

	//Font 新規読み込み

	if(font_loadfile(dst, filename, index))
		return APP_ERRMES_ALREADY;

	return NULL;
}

/** フォントを追加
 *
 * すでに同名のものがある場合、追加できない。
 *
 * filename: NULL で 'base' と同じフォントを使う
 * size: pt 単位
 * return: エラー文字列。NULL で成功 (APP_ERRMES_ALREADY でエラー表示済み) */

const char *setting_add_font(const char *name,
	const char *filename,int index,double size,int *dakuten,int flags)
{
	FontItem *pi;
	Font *font;
	const char *err;
	int len;

	//同名のものがある

	pi = setting_search_font(name);
	if(pi) return "すでに同じ名前のフォントがあります";

	//フォント

	if(!filename)
	{
		//'base' と同じフォントを使う

		pi = setting_search_font("base");
		if(!pi) return "'base' のフォントがありません";

		font = pi->font;

		font_addref(font);
	}
	else
	{
		//フォント読み込む

		err = _load_font(&font, filename, index);
		if(err) return err;
	}

	//フォント追加

	len = strlen(filename) + 1;

	pi = (FontItem *)mListAppendNew(&g_set->list_font, sizeof(FontItem) + len);
	if(!pi)
	{
		font_free(font);
		return "メモリが足りません";
	}

	strcpy(pi->name, name);
	memcpy(pi->filename, filename, len);

	pi->size = lround(size * 1000);
	pi->index = index;
	pi->font = font;
	pi->flags = flags;
	pi->dakuten_horz_x = dakuten[0];
	pi->dakuten_horz_y = dakuten[1];
	pi->dakuten_vert_x = dakuten[2];
	pi->dakuten_vert_y = dakuten[3];

	//base の場合、アイテムを記録

	if(strcmp(name, "base") == 0)
		g_set->fontitem_base = pi;

	return NULL;
}

/** フォントを名前から検索 */

FontItem *setting_search_font(const char *name)
{
	FontItem *pi;

	MLK_LIST_FOR(g_set->list_font, pi, FontItem)
	{
		if(strcmp(name, pi->name) == 0)
			return pi;
	}

	return NULL;
}

/** FreeType フォントを読み込み (画像出力時) */

void setting_load_ftfont(int antialias)
{
	FontItem *pi;

	//Font は、同じフォントファイル+index なら、同じポインタになっているので、
	//読み込まれていない場合のみ読み込む。

	MLK_LIST_FOR(g_set->list_font, pi, FontItem)
	{
		if(fontft_load(pi->font, pi->filename, pi->index, antialias, g_set->output.hinting))
			app_enderr2("FreeType でフォントの読み込みに失敗", pi->filename);
	}
}


//=======================
// 圏点
//=======================


/** 圏点を名前から検索
 *
 * dst_no: NULL 以外で、番号をセット (1〜) */

KentenItem *setting_search_kenten(const char *name,int *dst_no)
{
	KentenItem *pi;
	int no = 1;

	MLK_LIST_FOR(g_set->list_kenten, pi, KentenItem)
	{
		if(strcmp(name, pi->name) == 0)
		{
			if(dst_no) *dst_no = no;
		
			return pi;
		}

		no++;
	}

	return NULL;
}

/** 圏点の追加 */

KentenItem *setting_add_kenten(const char *name)
{
	KentenItem *pi;
	int len;

	//同名のものがあるか

	pi = setting_search_kenten(name, NULL);
	if(pi) return pi;

	//追加

	if(g_set->list_kenten.num >= 255)
		app_enderr("圏点は 255 個までしか作れません");

	len = strlen(name) + 1;
	
	pi = (KentenItem *)mListAppendNew(&g_set->list_kenten, sizeof(KentenItem) + len);
	if(!pi) app_enderr_alloc();

	memcpy(pi->name, name, len);

	return pi;
}


//=======================
// 全角幅セット
//=======================


/** 全角幅セットを名前から検索 */

FullWidthItem *setting_search_fullwidth(const char *name)
{
	FullWidthItem *pi;

	MLK_LIST_FOR(g_set->list_fullwidth, pi, FullWidthItem)
	{
		if(strcmp(name, pi->name) == 0)
			return pi;
	}

	return NULL;
}

/** 全角幅セットの追加 */

const char *setting_add_fullwidth(const char *name,const char *text)
{
	FullWidthItem *pi;
	uint32_t *buf;
	int len;

	//同名のものがある場合、エラー

	pi = setting_search_fullwidth(name);
	if(pi) return "すでに同じ名前のセットがあります";

	//UTF-8 -> UTF-32

	buf = mUTF8toUTF32_alloc(text, -1, &len);
	if(!buf) return "メモリが足りません";

	//追加

	pi = (FullWidthItem *)mListAppendNew(&g_set->list_fullwidth, sizeof(FullWidthItem) + len * 4);
	if(!pi)
	{
		mFree(buf);
		return "メモリが足りません";
	}

	strcpy(pi->name, name);

	memcpy(pi->code, buf, len * 4);

	mFree(buf);

	return NULL;
}


//=======================
// 値
//=======================


/** FontSpec の値を適用
 *
 * font: デフォルトフォント (NULL で base)
 * defsize: 単位がデフォルト時、フォントサイズに対する倍率。
 *
 * [デフォルト/x 単位時]
 *   font == NULL: pi のフォントのデフォルトサイズに対する倍率。
 *   font != NULL: font の実際のサイズに対する倍率 */

void setting_set_fontspec(FontSpec *pi,FontSpec *font,double defsize)
{
	Font *pf;
	int size;

	//FontItem

	if(!pi->item)
		pi->item = (font)? font->item: g_set->fontitem_base;

	//フォントサイズ (pt)
	// :FontItem::size は 1000 = 1pt

	size = (font)? font->size: pi->item->size;

	switch(pi->unit)
	{
		//デフォルト
		case UNIT_TYPE_DEFAULT:
			pi->size = lround(defsize * size);
			break;
		//x (倍率)
		case UNIT_TYPE_X:
			pi->size = lround(pi->size / 1000.0 * size);
			break;
	}

	//横書きの高さ

	pf = pi->item->font;

	pi->height = lround((double)(pf->ascent - pf->descent) / pf->emsize * pi->size);
}


//=======================
// 値の決定
//=======================


/* 用紙上の位置や幅をセット (1000 = 1pt)
 *
 * font: 基準のフォント (NULL で base)
 * defw: デフォルト時、font の実際のサイズに対する倍率 */

static void _set_paperpos(PaperPos *p,FontSpec *font,double defw)
{
	double size;

	size = (font)? font->size: g_set->fontitem_base->size;

	switch(p->unit)
	{
		case UNIT_TYPE_DEFAULT:
			p->val = lround(size * defw);
			break;
		//'w' フォントサイズに対する倍率。1.0 = 1000
		case UNIT_TYPE_W:
			p->val = lround(p->val / 1000.0 * size);
			break;
	}
}

/** 設定データの決定後、値をチェック＆デフォルト値をセット
 *
 * return: エラー文字列。NULL で成功 */

const char *setting_check_value(void)
{
	SettingData *p = g_set;
	LayoutItem *pl;
	KentenItem *kt;
	FontSpec *font;
	int i,n;

	//'base' フォントがない

	if(!p->fontitem_base)
		return "'base' フォントが定義されていません";

	//base レイアウトがない

	if(!p->layout_base)
		return "'base' レイアウトが定義されていません";

	//FontSpec

	setting_set_fontspec(&p->title.font, NULL, 0.7);
	setting_set_fontspec(&p->number.font, NULL, 0.7);

	//PaperPos

	_set_paperpos(&p->title_number_space, &p->title.font, 1.5); //柱のフォントサイズが基準

	//圏点

	MLK_LIST_FOR(p->list_kenten, kt, KentenItem)
	{
		setting_set_fontspec(&kt->font, NULL, 0.5);
	}

	//レイアウト

	MLK_LIST_FOR(p->list_layout, pl, LayoutItem)
	{
		setting_set_fontspec(&pl->basefont, NULL, 1);

		font = &pl->basefont;

		setting_set_fontspec(&pl->ruby_font, font, 0.5);

		//PaperPos
		// :sp_head,sp_foot,sp_gutter はデフォルトで 0
		
		_set_paperpos(&pl->sp_column, font, 3);
		_set_paperpos(&pl->line_feed, font, 1.7);

		//行長

		if(pl->line_len.unit)
			_set_paperpos(&pl->line_len, font, 0);
		else
		{
			n = (pl->mode == LAYOUT_MODE_HORZ)? p->paper_w: p->paper_h;
			n = convert_mm_to_pt(n - 20);

			if(pl->mode == LAYOUT_MODE_VERT2)
				n = (n - pl->sp_column.val) / 2;

			n = n / pl->basefont.size * pl->basefont.size;
			if(n < 1) n = pl->basefont.size * 5;

			pl->line_len.val = n;
		}

		//行数

		if(!pl->line_num)
		{
			n = (pl->mode == LAYOUT_MODE_HORZ)? p->paper_h: p->paper_w;
			n = convert_mm_to_pt(n - 30);
			n = (n - pl->basefont.size) / pl->line_feed.val + 1;
			if(n < 1) n = 1;

			pl->line_num = n;
		}

		//見出し

		for(i = 0; i < 3; i++)
		{
			setting_set_fontspec(&pl->caption[i].font, font, 1);
		}
	}

	//柱・ノンブルの余白
	// :base レイアウトのフォントサイズが基準。レイアウトの設定後に行う

	font = &p->layout_base->basefont;

	_set_paperpos(&p->title.vspace, font, 1);
	_set_paperpos(&p->title.hspace, font, 0);

	_set_paperpos(&p->number.vspace, font, 1);
	_set_paperpos(&p->number.hspace, font, 0);

	return NULL;
}
