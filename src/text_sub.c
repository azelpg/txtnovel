/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * テキスト読み込みサブ関数
 *********************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <mlk.h>
#include <mlk_str.h>
#include <mlk_buf.h>
#include <mlk_list.h>
#include <mlk_stdio.h>
#include <mlk_charset.h>
#include <mlk_unicode.h>
#include <mlk_string.h>
#include <mlk_util.h>

#include "app.h"
#include "setting.h"
#include "bodydata.h"
#include "text.h"
#include "cmpstring.h"


#define UNICODE_MAX 0x10ffff


/** TextData 解放 */

void textdata_free(void *ptr,int err)
{
	TextData *p = (TextData *)ptr;

	if(err)
		mBufFree(&p->buf_body);
	else
	{
		//成功時はバッファを使う

		mBufCutCurrent(&p->buf_body);

		g_app->buf_body = p->buf_body.buf;

		app_progress_init(p->buf_body.cursize - 2);
	}

	mListDeleteAll(&p->list);
	mListDeleteAll(&p->list_def);

	mStrFree(&p->str);
	mStrFree(&p->str2);

	text_free_data(p);

	mFree(p);
}

/** 追加のエラー表示 */

void text_put_error(void *ptr)
{
	TextData *p = (TextData *)ptr;

	fputs("> ", stderr);
	mPutUTF8_stderr(p->cur->filename);
	fprintf(stderr, "\n> [line:%d, column:%d]\n", p->cur_line, p->cur_cpos);
}

/** 作業用データを確保 */

void *text_alloc_data(TextData *p,int size,void (*freefunc)(void *))
{
	p->data = mMalloc0(size);
	if(!p->data) app_enderr_alloc();

	p->free_data = freefunc;

	return p->data;
}

/** 作業用データを解放 */

void text_free_data(TextData *p)
{
	if(p->data)
	{
		if(p->free_data)
			(p->free_data)(p->data);

		mFree(p->data);

		p->data = NULL;
	}

	p->free_data = NULL;
}


//=======================
// 入力テキスト
//=======================


/** テキストを閉じる */

void text_close_input(TextData *p)
{
	FileItem *pi;

	//閉じて削除

	mListDelete(&p->list, MLISTITEM(p->cur));

	//前のファイルに戻る

	pi = p->cur = (FileItem *)p->list.bottom;

	if(pi)
	{
		p->line = pi->line;
		p->cpos = pi->cpos;
	}
}

/** 入力テキストを開く
 *
 * filename: テキストで指定された文字列のまま
 * item: NULL 以外で、定義文をセット。
 *   その場合、filename は定義文の情報 "@def[name]" */

void text_open_input(TextData *p,const char *filename,DefItem *item)
{
	FileItem *pi;
	char *path;
	int len;

	//現在位置を記録

	if(p->cur)
	{
		pi = p->cur;
		pi->line = p->line;
		pi->cpos = p->cpos;
	}

	//新規追加

	len = strlen(filename) + 1;

	pi = (FileItem *)mListAppendNew(&p->list, sizeof(FileItem) + len);
	if(!pi) app_enderr_alloc();

	memcpy(pi->filename, filename, len);

	//開く

	if(item)
	{
		//定義文

		pi->buf = item->str;
	}
	else
	{
		//ファイル

		if(p->list.num == 1)
			//最初のファイルはそのまま指定
			path = NULL;
		else
			path = text_get_textbase_filename(filename);

		pi->fp = mFILEopen((path)? path: filename, "rb");

		mFree(path);
		
		if(!pi->fp)
		{
			mListDelete(&p->list, MLISTITEM(pi));
			app_enderr2("ファイルが開けません", filename);
		}
	}

	//

	p->cur = pi;
	p->line = 1;
	p->cpos = 0;
}


//==========================
// ほか
//==========================


/** 相対パス時、テキストファイル位置を基準としたファイルパスを取得
 *
 * 絶対パス時はそのままコピー。 */

char *text_get_textbase_filename(const char *filename)
{
	mStr str = MSTR_INIT;

	if(*filename == '/')
		mStrSetText(&str, filename);
	else
	{
		mStrPathGetDir(&str, g_app->str_input.buf);
		mStrPathJoin(&str, filename);
	}

	return str.buf;
}

/** 定義文の名前から検索 */

DefItem *text_search_defitem(TextData *p,const char *name)
{
	DefItem *pi;
	uint32_t hash;

	hash = mCalcStringHash(name);

	MLK_LIST_FOR(p->list_def, pi, DefItem)
	{
		if(hash == pi->hash && strcmp(name, pi->name) == 0)
			return pi;
	}

	return NULL;
}

/** 画像を登録して、アイテムポインタ取得 */

ImageItem *text_add_imagefile(char *fname)
{
	char *path;
	ImageItem *pi;

	path = text_get_textbase_filename(fname);

	pi = app_add_image(path);

	mFree(path);

	return pi;
}


//==========================
// 文字取得
//==========================


/* 1文字取得 */

static int _read_char(FileItem *pi)
{
	int c;

	if(pi->fp)
		c = fgetc(pi->fp);
	else
	{
		c = *(pi->buf);

		if(c)
			pi->buf++;
		else
			c = EOF;
	}

	return c;
}

/* 次の1文字を読み込んで、返す。
 * 指定文字でなければ、位置を戻す。
 * ※終端時、前のファイルの次の文字にはつながらない。 */

static int _read_char_next(FileItem *pi,int ch)
{
	int c;

	if(pi->fp)
	{
		c = fgetc(pi->fp);

		if(c != ch && c != EOF)
			ungetc(c, pi->fp);
	}
	else
	{
		c = *(pi->buf);

		if(!c)
			c = EOF;
		else if(c == ch)
			pi->buf++;
	}

	return c;
}

/* UTF-8 1文字取得
 *
 * 改行は '\n' に統一して返す。
 *
 * return: 文字。-1 で終端 */

static int32_t _get_char_utf8(TextData *p)
{
	FileItem *pi = p->cur;
	int32_t i,len,c,min,b;

	while(1)
	{
		c = _read_char(pi);
		if(c != EOF) break;

		if(!p->cur->i.prev) return -1;

		//終端時、前の入力がある時は戻る

		text_close_input(p);

		pi = p->cur;
	}

	//改行

	if(c == '\r' || c == '\n')
	{
		if(c == '\r')
			_read_char_next(pi, '\n');

		return '\n';
	}

	//

	if(c < 0x80)
	{
		len = 1;
		min = 0;
	}
	else if(c < 0xc0)
		len = -1;
	else if(c < 0xe0)
	{
		len = 2;
		min = 0x80;
		c &= 0x1f;
	}
	else if(c < 0xf0)
	{
		len = 3;
		min = 1<<11;
		c &= 0x0f;
	}
	else if(c < 0xf8)
	{
		len = 4;
		min = 1<<16;
		c &= 0x07;
	}
	else
		len = -1;

	//エラー

	if(len == -1) goto ERR;

	//2byte目以降を処理

	if(len > 1)
	{
		for(i = 1; i < len; i++)
		{
			b = _read_char(pi);

			if(b == EOF || (b & 0xc0) != 0x80)
				goto ERR;

			c = (c << 6) | (b & 0x3f);
		}

		//最小バイトで表現されていない
		if(c < min) goto ERR;
	}

	return c;

ERR:
	app_enderr("UTF-8 コードエラー");
	return -1;
}

/** 処理対象の1文字を取得
 *
 * \+Enter は、無視される。
 * 改行はそのまま返す。
 * コメントは除外。
 *
 * return: -1 で終端。フラグもセットされる。 */

int32_t text_get_char(TextData *p)
{
	int32_t c,c2;
	int fcomment = 0,is_top;

	p->cur_line = p->line;
	p->cur_cpos = p->cpos;

	//fcomment: 1=行コメント、2=複数行コメント

	while(1)
	{
		c = _get_char_utf8(p);
		if(c == -1) break;

		//無効な文字 (文字位置に含めず、スキップ)

		if(c == '\t' || c > UNICODE_MAX || c == 0xFEFF /*BOM*/
			|| c == 0xFFFF || c == 0xFFFE || (c >= 0xD800 && c <= 0xDFFF))
			continue;

		//文字位置

		is_top = (p->cpos == 0);

		p->cur_line = p->line;
		p->cur_cpos = p->cpos;
		p->cpos++;

		//

		if(c == '\n')
		{
			//--- 改行

			p->line++;
			p->cpos = 0;

			//コメント中

			if(fcomment)
			{
				p->cur_line = p->line;

				if(fcomment == 1)
					fcomment = 0;

				continue;
			}
		}
		else if(fcomment)
		{
			//コメント中

			if(fcomment == 2 && c == '*')
			{
				c2 = _read_char_next(p->cur, '/');
				if(c2 == '/')
					fcomment = 0;
			}
			
			continue;
		}
		else if(c == '#' && is_top)
		{
			//行頭が '#' の場合、改行の次までスキップ

			fcomment = 1;
			continue;
		}
		else if(c == '\\')
		{
			//'\' の次は、通常の文字として扱う (改行も同様)
			// : \+Enter の改行は無視

			c = _get_char_utf8(p);

			if(c == '\n')
			{
				p->line++;
				p->cpos = 0;
				continue;
			}
			else if(c == -1)
				c = '\\';

			c |= CHAR_F_BSLASH;
		}
		else if(c == '/')
		{
			//複数行コメント
			
			c2 = _read_char_next(p->cur, '*');

			if(c2 == '*')
			{
				fcomment = 2;
				continue;
			}
		}

		//1文字返す

		if(is_top) c |= CHAR_F_TOP;

		return c;
	}

	return -1;
}

/** 次の1文字を読み込む。指定文字ならそのまま。
 *  指定文字でなければ、戻す。
 *
 * return: 1 で ch の文字だった */

int text_readchar_next(TextData *p,int ch)
{
	int c;

	c = _read_char_next(p->cur, ch);

	return (c == ch);
}

/** 改行までをスキップ (next = 次の行頭) */

void text_skip_to_enter(TextData *p)
{
	int32_t c;

	do
	{
		c = text_get_char(p);
	}
	while(c != -1 && (c & CHAR_MASK_CODE) != '\n');
}

/** [設定部分用] 改行を空白扱いにして、1文字取得 */

int32_t text_setting_getchar(TextData *p)
{
	int32_t c;

	c = text_get_char(p);

	if(c != -1)
	{
		if((c & CHAR_MASK_CODE) == '\n') c = ' ';

		c &= CHAR_MASK;
	}

	return c;
}

/** [設定部分用] 空白 (改行含む) をスキップして、次の文字取得
 *
 * \+space は除く。
 * 行頭フラグは OFF になる。 */

int32_t text_setting_skip_getchar(TextData *p)
{
	int32_t c;

	do
	{
		c = text_get_char(p);
	}
	while((c & CHAR_MASK) == ' ' || (c & CHAR_MASK_CODE) == '\n');

	if(c != -1) c &= CHAR_MASK;

	return c;
}

/* ';' までの文字列取得
 *
 * hex: TRUE で16進数、FALSE で数字のみ */

static void _get_body_char_string(TextData *p,mStr *str,int hex)
{
	int32_t c,f;

	mStrEmpty(str);

	while(1)
	{
		c = text_get_char(p);
		if(c == -1) break;

		c &= CHAR_MASK;
		
		if(c == ';') return;

		if(hex)
			f = ((c >= '0' && c <='9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'));
		else
			f = (c >= '0' && c <='9');

		if(!f) break;

		mStrAppendUnichar(str, c & CHAR_MASK_CODE);
	}

	app_enderr("$...; の無効な文字です");
}

/** [本文部分] Unicode/CID の1文字を取得
 *
 * $u/$c/$n は処理される。
 * 行頭フラグは OFF になる。
 * CID フラグが ON の場合あり。 */

int32_t text_body_getchar(TextData *p)
{
	int32_t c;

	c = text_get_char(p);
	if(c == -1) return -1;

	c &= CHAR_MASK;

	if(c != '$') return c;

	//---- "$...;"

	c = text_get_char(p) & CHAR_MASK;

	if(c != 'u' && c != 'c' && c != 'n')
		app_enderr("$ の次が無効な文字です");

	//; まで取得

	_get_body_char_string(p, &p->str, (c == 'u'));

	//コード値

	if(c == 'u')
	{
		//(Unicode) $uXXXX;

		c = strtoul(p->str.buf, NULL, 16);

		if(c <= 0 || c > UNICODE_MAX || c == 0xFFFF || c == 0xFFFE
			|| (c >= 0xD800 && c <= 0xDFFF))
		{
			app_enderr("$u の Unicode 値が無効です");
		}
	}
	else if(c == 'c')
	{
		//(CID) $cN;

		c = strtoul(p->str.buf, NULL, 10);

		if(c <= 0 || c > 0xffff)
			app_enderr("$c の CID 値が無効です");

		c |= CHAR_F_CID;
	}
	else
	{
		//(CID 2,3桁数字) $nXX;

		c = strtoul(p->str.buf, NULL, 10);

		if(c < 0 || c > 149)
			app_enderr("$n の数値が範囲外です (0-149)");

		c = CHAR_F_CID | (c + 20749);
	}

	return c;
}


//==========================
// 本文データ追加
//==========================


/** 本文に終端値を追加 */

void text_add_body_end(TextData *p)
{
	uint16_t v = BODYCHAR_FINISH;

	if(!mBufAppend(&p->buf_body, &v, 2))
		app_enderr_alloc();
}

/** 本文にコマンド番号を追加 */

void text_add_body_cmdno(TextData *p,int no)
{
	uint16_t v = no + BODYCHAR_CMD_FIRST;

	if(!mBufAppend(&p->buf_body, &v, 2))
		app_enderr_alloc();
}

/** 本文に1バイト値を追加 */

void text_add_body_byte(TextData *p,uint8_t v)
{
	if(!mBufAppend(&p->buf_body, &v, 1))
		app_enderr_alloc();
}

/** 本文にポインタ値を追加 */

void text_add_body_ptr(TextData *p,void *ptr)
{
	if(!mBufAppend(&p->buf_body, (void *)&ptr, sizeof(void*)))
		app_enderr_alloc();
}

/** 本文の1文字を追加 */

void text_add_body_char(TextData *p,int32_t c)
{
	mBuf *buf = &p->buf_body;
	uint16_t u16[2];
	int size;

	c &= CHAR_MASK_CID;

	if(c & CHAR_F_CID)
	{
		//CID

		u16[0] = BODYCHAR_CID;
		u16[1] = c & CHAR_MASK_CODE;
		size = 4;
	}
	else if(c <= 0xffff)
	{
		//UTF-16

		u16[0] = c;
		size = 2;
	}
	else
	{
		//UTF-16 (hi,low)

		c -= 0x10000;

		u16[0] = (c >> 10) + BODYCHAR_HI_FIRST;
		u16[1] = c & 0x3ff;
		size = 4;
	}

	if(!mBufAppend(buf, u16, size))
		app_enderr_alloc();
}

/** UVS 文字を追加
 *
 * 一つ前の文字を削除して、追加する */

void text_add_body_char_uvs(TextData *p,int32_t c,int32_t last_c,uint32_t lastpos)
{
	mBuf *buf = &p->buf_body;
	uint8_t dat[8];

	mBufBack(buf, buf->cursize - lastpos);

	*((uint16_t *)dat) = BODYCHAR_UVS;
	dat[2] = (uint8_t)(c >> 16);	//セレクタ
	dat[3] = (uint8_t)(c >> 8);
	dat[4] = (uint8_t)c;
	dat[5] = (uint8_t)(last_c >> 16);	//1文字目
	dat[6] = (uint8_t)(last_c >> 8);
	dat[7] = (uint8_t)last_c;

	if(!mBufAppend(buf, dat, 8))
		app_enderr_alloc();
}


//==========================
// 項目から値を取得
//==========================


/** 項目名を検索する
 *
 * posarr: 0xffff で終了
 * return: posarr の値 (-1 で見つからなかった) */

int text_search_item_name(const char *name,const uint16_t *posarr)
{
	int len,num,i,index;
	uint8_t dat[32];
	const uint32_t *ps,*pcmp;

	len = strlen(name);
	num = (len + 4) / 4 - 1;

	memset(dat, 0, 32);
	dat[0] = len;
	memcpy(dat + 1, name, len);

	//名前を検索

	for(index = 0; *posarr != 0xffff; posarr++, index++)
	{
		ps = (const uint32_t *)dat;
		pcmp = (const uint32_t *)(g_cmpstring + *posarr);

		if(*ps == *pcmp)
		{
			ps++;
			pcmp++;

			for(i = num; i && *ps == *pcmp; i--, ps++, pcmp++);

			if(!i) return *posarr;
		}
	}

	return -1;
}


//==========================
// 文字列から値を取得
//==========================
/* [!] 空文字列の場合がある */


/** 指定文字で区切った文字長さと、次の位置を取得
 *
 * 区切り文字は 0 に置き換える。
 *
 * return: 文字列の長さ */

int text_get_split_next(char *str,char ch,char **ppnext)
{
	char *sep;

	sep = strchr(str, ch);

	if(sep)
	{
		*ppnext = sep + 1;
		*sep = 0;
	}
	else
		*ppnext = sep = strchr(str, 0);

	return sep - str;
}

/** 文字列から yes/no の値を取得 */

int text_get_string_yesno(TextData *p,const char *str)
{
	if(text_cmpi(str, "yes") || text_cmpi(str, "y") || text_cmp(str, "1"))
		return 1;
	else if(text_cmpi(str, "no") || text_cmpi(str, "n") || text_cmp(str, "0"))
		return 0;
	else
	{
		app_enderr2("値が正しくありません", str);
		return 0;
	}
}

/** 文字列から数値を取得
 *
 * str: NULL で p->str.buf */

int text_get_string_int(TextData *p,const char *str,int min,int max)
{
	int n;

	if(!str) str = p->str.buf;

	n = atoi(str);

	if(n < min || n > max)
		app_enderr2("値が範囲外です", str);

	return n;
}

/** 文字列から、カンマで区切られた数値を取得
 *
 * str: NULL で p->str.buf
 * return: 取得した数 */

int text_get_string_int_comma(TextData *p,char *str,int maxnum,int min,int max,int *dst)
{
	int n,cnt = 0;

	if(!str) str = p->str.buf;

	for(; maxnum; maxnum--)
	{
		n = strtol(str, &str, 10);

		if(n < min || n > max)
			app_enderr2("値が範囲外です", str);

		*(dst++) = n;
		cnt++;

		//次へ

		if(!(*str)) break;

		if(*str != ',')
			app_enderr2("無効な文字です", str);

		str++;
	}

	return cnt;
}

/** 文字列から、カンマで区切られた数値 (double -> 1.0=1000) を取得
 *
 * return: 取得した数 */

int text_get_string_comma_double_to_int(TextData *p,char *str,int maxnum,int *dst)
{
	char *next;
	double d;
	int cnt = 0;

	for(; *str && maxnum; str = next, maxnum--)
	{
		text_get_split_next(str, ',', &next);

		d = strtod(str, NULL);

		*(dst++) = lround(d * 1000);

		cnt++;
	}

	return cnt;
}

/** 列挙文字列から検索し、インデックス値を返す
 *
 * val: NULL で p->str.buf
 * strs: ';' で区切る */

int text_get_string_index(TextData *p,const char *val,const char *strs)
{
	int n;

	if(!val) val = p->str.buf;

	n = mStringGetSplitTextIndex(val, -1, strs, ';', TRUE);
	if(n == -1)
		app_enderr2("値が正しくありません", p->str.buf);

	return n;
}

/** 文字列から、１文字を取得
 *
 * str: NULL で p->str.buf */

uint32_t text_get_string_char(TextData *p,const char *str)
{
	uint32_t c = 0;

	if(!str) str = p->str.buf;

	if(*str == 'U' && str[1] == '+')
	{
		//Unicode

		c = strtoul(str + 2, NULL, 16);
		if(!c || c > UNICODE_MAX)
			app_enderr2("無効な Unicode 値です", str);
	}
	else if(strncmp(str, "CID+", 4) == 0)
	{
		//CID

		c = strtoul(str + 4, NULL, 10);
		if(!c || c > 0xffff)
			app_enderr2("無効な CID 値です", str);

		c |= 1<<31;
	}
	else if(!(*str))
		app_enderr("文字が指定されていません");
	else
	{
		//先頭の1文字

		mUTF8GetChar(&c, str, -1, NULL);
	}

	return c;
}

/** "N<mm/pt/w>" から、用紙上の位置や幅を取得 (1000 = 1pt)
 *
 * 単位は、0 以外は常に指定。負の値も可能。
 * w の場合、後で計算する。
 *
 * str: NULL で p->str.buf */

void text_get_string_paperpos(TextData *p,char *str,PaperPos *dst)
{
	double d;

	if(!str) str = p->str.buf;

	d = strtod(str, &str);

	if(d == 0)
	{
		dst->val = 0;
		dst->unit = UNIT_TYPE_SET;
	}
	else if(text_cmp(str, "w"))
	{
		dst->val = lround(d * 1000);
		dst->unit = UNIT_TYPE_W;
	}
	else if(text_cmp(str, "mm"))
	{
		//mm -> pt
		
		dst->val = lround(d * UNIT_1MM_PT * 1000);
		dst->unit = UNIT_TYPE_SET;
	}
	else if(text_cmp(str, "pt"))
	{
		//pt

		dst->val = lround(d * 1000);
		dst->unit = UNIT_TYPE_SET;
	}
	else if(!(*str))
		app_enderr("単位を指定してください");
	else
		app_enderr2("単位が正しくありません", str);
}

/** 文字列から、',' で区切られたフラグを取得
 *
 * 'none' は常に 0。
 *
 * str: NULL で p->str.buf
 * flagstr: 下位ビットから順に ';' で区切る */

uint32_t text_get_string_flags(TextData *p,char *str,const char *flagstr)
{
	char *end;
	int n,fend;
	uint8_t f = 0;

	if(!str) str = p->str.buf;

	while(*str)
	{
		//終端位置 (end = , or 終端)

		fend = 0;

		for(end = str; *end && *end != ','; end++);

		fend = !(*end);

		*end = 0;

		//検索 ('none' 以外の場合)

		if(strcasecmp(str, "none"))
		{
			n = mStringGetSplitTextIndex(str, end - str, flagstr, ';', TRUE);
			if(n == -1)
				app_enderr2("未定義の名前です", str);

			f |= 1 << n;
		}

		//次へ

		if(fend) break;

		str = end + 1;
	}

	return f;
}

/** 文字列から、RGB色を取得
 *
 * rgb(R,G,B) or #XXXXXX
 *
 * str: NULL = p->str.buf */

uint32_t text_get_string_rgb(TextData *p,const char *str)
{
	uint32_t c = 0;
	int i,n,val[3] = {0,0,0};

	if(!str) str = p->str.buf;

	if(*str == '#')
	{
		c = strtoul(str + 1, NULL, 16);

		if(c > 0xffffff) c = 0xffffff;
	}
	else if(strncmp(str, "rgb(", 4) == 0)
	{
		mStringGetSplitInt(str + 4, ',', val, 3);

		for(i = 0; i < 3; i++)
		{
			n = val[i];
			if(n < 0 || n > 255)
				app_enderr2("値が正しくありません", str);
		}

		c = (val[0] << 16) | (val[1] << 8) | val[2];
	}
	else
		app_enderr2("値が正しくありません", str);

	return c;
}

/** フォント名から、FontSpec:item にセット */

void text_get_string_fontname(TextData *p,const char *str,FontSpec *dst)
{
	dst->item = setting_search_font(str);

	if(!dst->item)
		app_enderr2("指定名のフォントがありません", str);
}

/** 文字列から、フォントサイズ取得
 *
 * 単位なしなら pt
 *
 * dst: NULL の場合、戻り値で値を返す (単位 x は使用不可) */

double text_get_string_fontsize(TextData *p,char *str,FontSpec *dst)
{
	double d;
	int unit;

	unit = UNIT_TYPE_SET;

	d = strtod(str, &str);

	if(strcmp(str, "Q") == 0)
	{
		//1Q = 0.25mm, 1mm = 72/25.4 pt
		
		d = d * 0.25 * UNIT_1MM_PT;
	}
	else if(strcmp(str, "pt") == 0 || !(*str))
	{
		//pt
	}
	else if(dst && strcmp(str, "x") == 0)
	{
		//フォントサイズの倍率

		unit = UNIT_TYPE_X;
	}
	else
		app_enderr2("単位が正しくありません", str);

	//

	if(dst)
	{
		dst->size = lround(d * 1000);
		dst->unit = unit;
	}

	return d;
}

/** テキストから、画像の用紙上のサイズを取得 */

void text_get_string_imgsize(TextData *p,char *str,ImageInfo *dst)
{
	char *pc;
	double w,h;

	if(text_cmp(str, "100%"))
		dst->width = dst->height = -1;
	else
	{
		w = strtod(str, &pc);

		if(*pc != 'x')
			app_enderr2("NxN で指定してください", str);

		h = strtod(pc + 1, &pc);

		//単位

		if(text_cmp(pc, "mm") || !(*pc))
		{
			w *= UNIT_1MM_PT * 1000;
			h *= UNIT_1MM_PT * 1000;
		}
		else if(text_cmp(pc, "pt"))
		{
			w *= 1000;
			h *= 1000;
		}
		else
			app_enderr2("単位が正しくありません", str);

		//

		dst->width  = lround(w);
		dst->height = lround(h);

		if(dst->width <= 0 || dst->height <= 0)
			app_enderr2("値が正しくありません", str);
	}
}

/** テキストから、画像の位置を取得
 *
 * return: 0 で成功 */

int text_get_string_imgpos(TextData *p,char *str,ImageInfo *dst)
{
	char *next;
	int n,len,no;
	double d;

	if(text_cmp(str, "tile"))
	{
		dst->base = IMAGEINFO_BASE_TILE;
		dst->align = IMAGEINFO_ALIGN_CENTER;
	}
	else
	{
		for(no = 0; *str; str = next, no++)
		{
			len = text_get_split_next(str, '/', &next);

			switch(no)
			{
				//基準位置
				case 0:
					n = mStringGetSplitTextIndex(str, len, "paper;bleed;body", ';', TRUE);
					if(n == -1) return 1;

					dst->base = n;
					break;
				//配置
				case 1:
					n = mStringGetSplitTextIndex(str, len, "center;in-top;in-bottom;out-top;out-bottom", ';', TRUE);
					if(n == -1) return 1;

					dst->align = n;
					break;
				//x
				case 2:
					d = strtod(str, NULL);
					dst->x = lround(d * UNIT_1MM_PT * 1000);
					break;
				//y
				case 3:
					d = strtod(str, NULL);
					dst->y = lround(d * UNIT_1MM_PT * 1000);
					break;
			}
		}
	}

	return 0;
}

/** 文字列から、ihead,ifoot の値を取得
 *
 * "N[,N][,line]" or 省略
 * 2番目の値を省略した時は、1番目の値と同じ。
 *
 * dst_num: 2個。'line' があると、最初の値で 0x80 が ON になる。 */

void text_get_string_body_indent(TextData *p,uint8_t *dst)
{
	char *pc,*next;
	int n,len,num_cnt = 0,fline = 0;

	dst[0] = dst[1] = 0;

	if(!p->str.len) return;

	for(pc = p->str.buf; *pc; pc = next)
	{
		len = text_get_split_next(pc, ',', &next);

		if(text_cmpin(pc, "line", len))
		{
			fline = 1;
		}
		else if(num_cnt < 2)
		{
			n = strtol(pc, NULL, 10);

			if(n < 0) n = 0;
			else if(n > 127) n = 127;

			dst[num_cnt] = n;
			num_cnt++;
		}
	}

	if(num_cnt == 1)
		dst[1] = dst[0];

	if(fline)
		dst[0] |= 0x80;
}

/** 文字列から、本文の align/body-align の値を取得
 *
 * "NAME[,line or page]" or 省略
 *
 * dst: line がある場合、0x80 が ON
 * fpage: TRUE で page、FALSE で line */

void text_get_string_body_align(TextData *p,uint8_t *dst,mlkbool fpage)
{
	char *pc,*next;
	int n,len,fline = 0;

	*dst = 0;

	if(!p->str.len) return;

	for(pc = p->str.buf; *pc; pc = next)
	{
		len = text_get_split_next(pc, ',', &next);

		if(text_cmpin(pc, (fpage)? "page": "line", len))
			fline = 1;
		else
		{
			n = mStringGetSplitTextIndex(pc, len, "top;center;bottom", ';', TRUE);
			if(n < 0) app_enderr2("値が正しくありません", pc);

			*dst = n;
		}
	}

	if(fline)
		*dst |= 0x80;
}

/** 'name=value,...' の文字列から、各項目を取得
 *
 * func: value は、"=" がなければ空文字列 */

void text_get_string_comma_item(TextData *p,mStr *str,void (*func)(TextData *p,const char *name,const char *value))
{
	char *pc,*val,*next;

	mStrReplaceChar_null(str, ',');

	for(pc = str->buf; *pc; pc = next)
	{
		next = pc + strlen(pc) + 1;
	
		val = strchr(pc, '=');
		if(!val)
			val = next - 1;
		else
		{
			*val = 0;
			val++;
		}

		(func)(p, pc, val);
	}
}

