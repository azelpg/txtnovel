/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * 出力: PDF 出力
 *********************************/

#include <stdio.h>
#include <math.h>

#include <mlk.h>
#include <mlk_list.h>
#include <mlk_str.h>
#include <mlk_file.h>
#include <mlk_rectbox.h>

#include "app.h"
#include "setting.h"
#include "output.h"
#include "pdf.h"
#include "font/font.h"


/* サブセットフォントと PDF フォント情報を作成
 *
 * 実際には使われていないフォントは除外とする */

static int _create_subset_font(OutputData *p,FontItem *pi,int resno)
{
	Font *font;
	FontSubsetInfo info;

	font = pi->font;

	font->pdf_resno_horz = -1;
	font->pdf_resno_vert = -1;

	//使用されているグリフから、情報を取得
	// :一つも使用されていない場合は、除外

	font_get_subset_info(font, &info);

	if(!info.has_horz && !info.has_vert)
		return resno;

	//PDF のリソース番号

	if(info.has_horz)
		font->pdf_resno_horz = resno++;

	if(info.has_vert)
		font->pdf_resno_vert = resno++;

	//Font: srcGID -> outGID マップ確保

	font->gidmap = (uint16_t *)mMalloc(font->glyph_num * 2);
	if(!font->gidmap) app_enderr_alloc();

	//

	mStrPathGetBasename(&p->str_tmp, pi->filename);

	printf(" %s [%d]", p->str_tmp.buf, pi->index);

	if(info.has_horz) printf(" H");
	if(info.has_vert) printf(" V");

	putchar('\n');

	//フォント追加
	
	pdf_add_font(p->pdf, pi, &info);

	return resno;
}

/* すべてのサブセットフォントの作成 */

static void _create_subset_font_all(OutputData *p)
{
	FontItem *pi;
	int resno = 1;

	printf("create subset font:\n");

	MLK_LIST_FOR(g_set->list_font, pi, FontItem)
	{
		//未処理の場合のみ

		if(!pi->font->pdf_resno_horz)
			resno = _create_subset_font(p, pi, resno);
	}
}

/** 初期化 */

void output_pdf_init(OutputData *p)
{
	int w;

	w = p->paper_fullw;
	if(g_set->page_type == PAGETYPE_TWO_1P) w += p->paper_fullw;

	pdf_new(&p->pdf, w, p->paper_fullh);

	//サブセットフォントとフォント情報作成
	// :PDF 分割時は共通のデータとなるので、最初に作成しておく

	_create_subset_font_all(p);

	//共通のデータ出力後

	pdf_end_init(p->pdf);
}

/** レイアウト変更時 */

void output_pdf_change_layout(OutputData *p)
{
	//背景画像の実際のサイズをセット

	output_set_imagesize(p, &p->layout->bkgndimg);
}

/** 出力ページ単位のクリア */

void output_pdf_clear_output_page(OutputData *p)
{
	p->pdfsep_flag = FALSE;

	//新規 PDF ファイル
	// :PDF 分割時は、ファイル名に使うページ番号の範囲が確定していない。
	// :また、途中でエラーが発生する可能性もあるので、成功時にコピーする。

	if(!p->start_page)
	{
		p->start_page = p->curpage;

		pdf_openfile(p->pdf, app_set_tmpfile(TMPFILENAME_OUTPUT_PDF));
	}

	//ページ内容クリア

	pdf_clear_page(p->pdf);
}

/** 現在のページのクリア */

void output_pdf_clear_cur_page(OutputData *p)
{
	ImageItem *piimg;
	mPoint pt;

	//目次用にページ番号セット

	pdf_set_pageno(p->pdf, p->curpage);

	//PDF 分割

	if(p->page_flags & PAGE_F_PDFSEP)
		p->pdfsep_flag = TRUE;

	//背景色

	if(p->layout->bkgndcol != 0xffffff)
	{
		pt.x = 0;

		if(g_set->page_type == PAGETYPE_TWO_1P && !(p->curpage & 1))
			pt.x = p->paper_fullw;
	
		pdf_draw_fillbox_col(p->pdf, pt.x, 0,
			p->paper_fullw, p->paper_fullh, p->layout->bkgndcol);
	}

	//背景画像
	// :タイル描画は不可

	piimg = output_get_page_bkgndimg(p);

	if(piimg)
	{
		output_get_image_pos(p, piimg, &p->layout->bkgndimg, &pt);

		pdf_draw_image(p->pdf, piimg, pt.x, pt.y,
			p->layout->bkgndimg.width, p->layout->bkgndimg.height);
	}
}

/* PDF ファイルの終了 */

static void _end_file(OutputData *p,int lastno)
{
	//PDF ファイルを完成
	
	pdf_endfile(p->pdf, p->start_page, lastno);

	//出力先にコピーする

	if(g_set->output.pdf_fixed)
		mStrSetText(&p->str_tmp, p->prefix);
	else
		mStrSetFormat(&p->str_tmp, "%s%d-%d.pdf", p->prefix, p->start_page, lastno);

	mCopyFile(app_set_tmpfile(TMPFILENAME_OUTPUT_PDF), p->str_tmp.buf);
}

/** ページの書き込み */

void output_pdf_put_page(OutputData *p,int no,int flast)
{
	//前のデータのページを追加

	pdf_add_page(p->pdf);

	//

	if(flast)
	{
		//最後のページ
		
		_end_file(p, no);
	}
	else if(p->pdfsep_flag && !(g_app->flags & APP_FLAGS_NO_PDFSEP))
	{
		//PDF 分割時、ファイルを閉じて新規作成
		// :見開き2Pの場合、いずれかで ON なら分割する

		_end_file(p, no);

		pdf_openfile(p->pdf, app_set_tmpfile(TMPFILENAME_OUTPUT_PDF));

		p->start_page = p->curpage;
	}
}

/** ページ/段の出力 */

void output_pdf_write_page(OutputData *p)
{
	PDFData *pdf = p->pdf;
	CharItem *pi;
	BousenItem *pibs;
	ImageItem *piimg;
	Font *font;
	mPoint pt;
	int32_t x,y,w,nextx,nexty,fontsize,pagex;
	uint8_t fhorz,ffonthorz,last_fonthorz,drawflags;
	double dashw;
	mBox box;

	fhorz = p->is_horz;

	if(g_set->page_type == PAGETYPE_TWO_1P && !(p->curpage & 1))
		pagex = p->paper_fullw;
	else
		pagex = 0;

	//---- ページの画像

	piimg = p->img_page.item;

	if(piimg && piimg != IMAGE_ITEM_NOFILE)
	{
		output_set_imagesize(p, &p->img_page);
	
		output_get_image_pos(p, piimg, &p->img_page, &pt);

		pdf_draw_image(pdf, piimg, pt.x, pt.y, p->img_page.width, p->img_page.height);
	}

	//---- 隠しノンブルの背景色

	if(!mRectIsEmpty(&p->rc_number))
	{
		mBoxSetRect(&box, &p->rc_number);
	
		pdf_draw_fillbox_col(pdf, box.x + pagex, box.y, box.w, box.h, g_set->number_hide_col);
	}

	//----- 文字
	// :2段組で続きがあったとしても、テキストオブジェクトは一度終了する。

	pdf_set_color(pdf, p->layout->textcol);

	font = NULL;
	fontsize = 0;
	last_fonthorz = -1;
	nextx = nexty = -1;

	MLK_LIST_FOR(p->list_char, pi, CharItem)
	{
		//ダッシュは別描画
	
		if(pi->flags & CHAR_F_DRAW_DASH) break;
	
		//横書きのフォントを使うか

		ffonthorz = (fhorz || (pi->flags & (CHAR_F_INFO | CHAR_F_TATEYOKO | CHAR_F_VROTATE)));

		//フォント変更
		// :PDF 内では、縦書き/横書きで別のフォントを指定する。
		
		if(font != pi->font || fontsize != pi->fontsize || ffonthorz != last_fonthorz)
		{
			font = pi->font;
			fontsize = pi->fontsize;
			last_fonthorz = ffonthorz;

			pdf_draw_change_font(pdf, font, fontsize, ffonthorz);
		}

		//--- 描画

		x = pi->x + pagex;
		y = pi->y;

		//グリフ描画フラグ

		drawflags = 0;

		if(ffonthorz)
			drawflags |= PDF_DRAWGLYPH_F_HORZ;

		if(pi->flags & CHAR_F_VROTATE)
			drawflags |= PDF_DRAWGLYPH_F_VROTATE;

		if(x == nextx && y == nexty)
			drawflags |= PDF_DRAWGLYPH_F_NO_COORD;

		pdf_draw_glyph(pdf, font, x, y, pi->gid, drawflags);

		//送り幅を足した次の位置

		nextx = x;
		nexty = y;

		if(fhorz || (pi->flags & (CHAR_F_INFO | CHAR_F_TATEYOKO)))
			nextx += pi->width;
		else
			nexty += pi->width;
	}

	pdf_end_text(pdf);

	//---- 全角ダッシュを線で描画

	dashw = p->layout->dash_width / 1000.0;

	for(; pi; pi = (CharItem *)pi->i.next)
	{
		w = lround(fontsize * dashw);
		if(w < 1) w = 1;

		x = pi->x + pagex;
		y = pi->y;

		if(fhorz)
			pdf_draw_fillbox(pdf, x, y, pi->width, w);
		else
			pdf_draw_fillbox(pdf, x, y, w, pi->width);
	}

	//---- 傍線

	MLK_LIST_FOR(p->list_bousen, pibs, BousenItem)
	{
		x = pibs->x + pagex;
		y = pibs->y;
		fontsize = pibs->fontsize;

		w = lround(fontsize * BOUSEN_WIDTH);
		if(w < 1) w = 1;
	
		if(fhorz)
		{
			pdf_draw_fillbox(pdf, x, y, pibs->len, w);

			if(pibs->type == 2)
				pdf_draw_fillbox(pdf, x, y + lround(fontsize * BOUSEN_DOUBLE), pibs->len, w);
		}
		else
		{
			pdf_draw_fillbox(pdf, x, y, w, pibs->len);

			if(pibs->type == 2)
				pdf_draw_fillbox(pdf, x + lround(fontsize * BOUSEN_DOUBLE), y, w, pibs->len);
		}
	}
}

