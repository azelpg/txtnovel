/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

//===========================
// 本文用コマンド定義
//===========================

/* 関数時のパラメータ */

typedef struct
{
	int proc,
		cmdno;
}BodyCmdParam;

/* コマンド定義 */

typedef struct
{
	uint8_t val,
		flags,
		cmdno;	//コマンド番号
	uint16_t name_pos;
	int (*func)(TextData *p,BodyCmdParam *);
}BodyCmdDef;

//値のタイプ
enum
{
	CMDVAL_END,
	CMDVAL_STR,		//1つの文字列 or なし ([] or ; の空状態を含む。CID 文字は使えない)
	CMDVAL_RUBY,	//@r のルビ
	CMDVAL_RUBY2,	//"...|..." のルビ
	CMDVAL_CHARS,	//文字列挙
	CMDVAL_MULTI	//"name=val..." の複数項目。proc で各処理段階がある
					//[!] 同じ項目が複数回来る可能性もあるので、注意。
};

//フラグ
enum
{
	CMDFLAG_ARG = 1<<0,			//引数が必須 (@name; の場合エラー。[] があって空の場合は判定しない)
	CMDFLAG_NO_ADD_CMDNO = 1<<1,	//コマンド番号をデータに追加しない
	CMDFLAG_END_BODY = 1<<2		//@end-body
};

//関数処理段階

enum
{
	CMDPROC_FIRST,
	CMDPROC_ITEM,
	CMDPROC_END
};

//関数戻り値
enum
{
	CMDFUNC_RET_OK,
	CMDFUNC_RET_ALLOC,	//メモリエラー
	CMDFUNC_RET_STR_UNFOUND_NAME,	//指定名が存在しない
	CMDFUNC_RET_STR_INVALID,	//1つの文字列で、値が正しくない
	CMDFUNC_RET_ITEM_UNDEF		//未定義の項目名
};

/*-------------------*/

static int _bodycmd_align(TextData *p,BodyCmdParam *param);
static int _bodycmd_kenten(TextData *p,BodyCmdParam *param);
static int _bodycmd_bousen(TextData *p,BodyCmdParam *param);
static int _bodycmd_def(TextData *p,BodyCmdParam *param);
static int _bodycmd_include(TextData *p,BodyCmdParam *param);
static int _bodycmd_layout(TextData *p,BodyCmdParam *param);
static int _bodycmd_font(TextData *p,BodyCmdParam *param);
static int _bodycmd_pagecnt(TextData *p,BodyCmdParam *param);
static int _bodycmd_pagenumber(TextData *p,BodyCmdParam *param);
static int _bodycmd_indent_head(TextData *p,BodyCmdParam *param);
static int _bodycmd_indent_foot(TextData *p,BodyCmdParam *param);
static int _bodycmd_image(TextData *p,BodyCmdParam *param);
static int _bodycmd_index(TextData *p,BodyCmdParam *param);
static int _bodycmd_foot(TextData *p,BodyCmdParam *param);

//先頭文字のコード順

static const BodyCmdDef g_bodycmd_def[] = {
 {CMDVAL_STR, 0, BODYCMD_NOOP, CMPSTR_POS__, NULL}, //-
 //a
 {CMDVAL_STR, 0, BODYCMD_ALIGN, CMPSTR_POS_ALIGN, _bodycmd_align}, //align
 //b
 {CMDVAL_STR, 0, BODYCMD_BODYALIGN, CMPSTR_POS_BODY_ALIGN, _bodycmd_align}, //body-align
 {CMDVAL_STR, 0, BODYCMD_BOUSEN, CMPSTR_POS_BS, _bodycmd_bousen}, //bs
 //c
 {CMDVAL_RUBY, CMDFLAG_ARG, BODYCMD_CRUBY, CMPSTR_POS_C, NULL}, //c
 {CMDVAL_RUBY2, CMDFLAG_ARG, BODYCMD_CRUBY, CMPSTR_POS_CR, NULL}, //cr
 {CMDVAL_STR, 0, BODYCMD_CAPTION1, CMPSTR_POS_CAP1, NULL}, //cap1
 {CMDVAL_STR, 0, BODYCMD_CAPTION2, CMPSTR_POS_CAP2, NULL}, //cap2
 {CMDVAL_STR, 0, BODYCMD_CAPTION3, CMPSTR_POS_CAP3, NULL}, //cap3
 //d
 {CMDVAL_STR, CMDFLAG_ARG | CMDFLAG_NO_ADD_CMDNO, 0, CMPSTR_POS_D, _bodycmd_def}, //d
 {CMDVAL_CHARS, CMDFLAG_ARG, BODYCMD_DAKUTEN, CMPSTR_POS_DK, NULL}, //dk
 //e
 {CMDVAL_STR, CMDFLAG_NO_ADD_CMDNO | CMDFLAG_END_BODY, 0, CMPSTR_POS_END_BODY, NULL}, //end-body
 //f
 {CMDVAL_STR, 0, BODYCMD_FONT, CMPSTR_POS_F, _bodycmd_font}, //f
 {CMDVAL_STR, 0, BODYCMD_FONT, CMPSTR_POS_FONT, _bodycmd_font}, //font
 {CMDVAL_STR, CMDFLAG_NO_ADD_CMDNO, 0, CMPSTR_POS_FOOT, _bodycmd_foot}, //foot
 //g
 {CMDVAL_RUBY2, CMDFLAG_ARG, BODYCMD_GRUBY, CMPSTR_POS_GR, NULL}, //gr
 {CMDVAL_RUBY2, CMDFLAG_ARG, BODYCMD_GRUBY2, CMPSTR_POS_GR2, NULL}, //gr2
 //i
 {CMDVAL_STR, 0, BODYCMD_INDENT_HEAD, CMPSTR_POS_IHEAD, _bodycmd_indent_head}, //ihead
 {CMDVAL_STR, 0, BODYCMD_INDENT_FOOT, CMPSTR_POS_IFOOT, _bodycmd_indent_foot}, //ifoot
 {CMDVAL_MULTI, CMDFLAG_ARG, BODYCMD_IMAGE, CMPSTR_POS_IMG, _bodycmd_image}, //img
 {CMDVAL_STR, CMDFLAG_ARG | CMDFLAG_NO_ADD_CMDNO, 0, CMPSTR_POS_INCLUDE, _bodycmd_include}, //include
 {CMDVAL_MULTI, CMDFLAG_ARG, BODYCMD_INDEX, CMPSTR_POS_INDEX, _bodycmd_index}, //index
 {CMDVAL_MULTI, CMDFLAG_ARG, BODYCMD_INDEX_PAGE, CMPSTR_POS_INDEX_PAGE, _bodycmd_index}, //index-page
 //j
 {CMDVAL_RUBY2, CMDFLAG_ARG, BODYCMD_JRUBY, CMPSTR_POS_JR, NULL}, //jr
 //k
 {CMDVAL_STR, 0, BODYCMD_KENTEN, CMPSTR_POS_KT, _bodycmd_kenten}, //kt
 //l
 {CMDVAL_RUBY2, CMDFLAG_ARG, BODYCMD_LRUBY, CMPSTR_POS_LR, NULL}, //lr
 {CMDVAL_STR, 0, BODYCMD_LAYOUT, CMPSTR_POS_LAYOUT, _bodycmd_layout}, //layout
 //m
 {CMDVAL_RUBY, CMDFLAG_ARG, BODYCMD_MRUBY, CMPSTR_POS_M, NULL}, //m
 {CMDVAL_RUBY2, CMDFLAG_ARG, BODYCMD_MRUBY, CMPSTR_POS_MR, NULL}, //mr
 //n
 {CMDVAL_STR, 0, BODYCMD_NEWPAGE, CMPSTR_POS_NP, NULL}, //np
 {CMDVAL_STR, 0, BODYCMD_NEWCOLUMN, CMPSTR_POS_ND, NULL}, //nd
 {CMDVAL_STR, 0, BODYCMD_NEWPAGE_ODD, CMPSTR_POS_NT, NULL}, //nt
 {CMDVAL_STR, 0, BODYCMD_NEWPAGE_EVEN, CMPSTR_POS_NM, NULL}, //nm
 //p
 {CMDVAL_STR, CMDFLAG_ARG, BODYCMD_PAGE_COUNT, CMPSTR_POS_PAGECNT, _bodycmd_pagecnt}, //pagecnt
 {CMDVAL_STR, 0, BODYCMD_PAGE_NUMBER, CMPSTR_POS_PAGENUM, _bodycmd_pagenumber}, //pagenum
 {CMDVAL_STR, 0, BODYCMD_PDFSEP, CMPSTR_POS_PDFSEP, NULL}, //pdfsep
 //r
 {CMDVAL_RUBY, CMDFLAG_ARG, BODYCMD_JRUBY, CMPSTR_POS_R, NULL}, //r
 //t
 {CMDVAL_CHARS, CMDFLAG_ARG, BODYCMD_TY, CMPSTR_POS_TY, NULL}, //ty
 {CMDVAL_CHARS, CMDFLAG_ARG, BODYCMD_TY_PROP, CMPSTR_POS_TYP, NULL}, //typ
 {CMDVAL_CHARS, 0, BODYCMD_TITLE, CMPSTR_POS_TITLE, NULL}, //title
 {CMDVAL_END,0,0,0,0}
};

