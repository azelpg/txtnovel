/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * PDF データ
 *********************************/

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include <time.h>

#include <mlk.h>
#include <mlk_str.h>
#include <mlk_list.h>
#include <mlk_unicode.h>
#include <mlk_zlib.h>
#include <mlk_stdio.h>
#include <mlk_string.h>

#include "app.h"
#include "setting.h"
#include "pdf.h"
#include "font/font.h"
#include "image.h"

#include "pdf_pv.h"



//============================
// オブジェクト
//============================


/** オブジェクトの追加
 *
 * 実際にデータを書き込む前に、オフセット値をセットすること。
 *
 * obj: 予約した番号を使う。NULL で新規
 *   objno == 0 の場合は追加しない。 */

PDFObjItem *pdf_add_object(PDFData *p,PDFReservedObject *obj)
{
	PDFObjItem *pi;

	if(obj && !obj->objno) return NULL;

	//

	pi = (PDFObjItem *)mListAppendNew(&p->list_obj, sizeof(PDFObjItem));
	if(!pi) app_enderr_alloc();

	if(obj)
	{
		pi->objno = obj->objno;
		pi->offset = obj->offset;
	}
	else
		pi->objno = p->cur_objno++;

	return pi;
}

/** 圧縮オブジェクト内のオブジェクトとして追加
 *
 * データは後から書き込んでも良い。
 * 実際にデータを書き込む前に、pdf_start_write_objstrm() を実行すること。
 * (オフセットは昇順のため、書き込み時にソートされる)
 *
 * ただし、常にすべてのデータが書き込まれた状態で、最終的なオブジェクトを作成する必要がある。
 *
 * obj: NULL で新規。予約した番号を使う場合
 * fwrite: 直後にデータを書き込む */

PDFObjstrmItem *pdf_add_object_in_stream(PDFData *p,PDFReservedObject *obj,mlkbool fwrite)
{
	PDFObjItem *pi,*parent;
	PDFObjstrmItem *pist;

	if(obj && !obj->objno) return NULL;

	//圧縮オブジェクト

	if(p->cur_objstrm)
		parent = p->cur_objstrm;
	else
	{
		parent = pdf_add_object(p, NULL);

		p->cur_objstrm = parent;
	}

	//オブジェクト (PDFObjItem)
	// :index は出力時にセット

	pi = pdf_add_object(p, obj);

	pi->objno_strm = parent->objno;
	pi->offset = 0;

	//PDFObjstrmItem

	pist = (PDFObjstrmItem *)mListAppendNew(&p->list_objstrm, sizeof(PDFObjstrmItem));
	if(!pist) app_enderr_alloc();

	pist->objno = pi->objno;
	pist->piobj = pi;
	
	if(obj) pist->offset = obj->offset;

	//書き込み開始

	if(fwrite)
		pdf_start_write_objstrm(p, pist);

	return pist;
}

/** 通常オブジェクトのデータ書き込み開始 */

void pdf_start_write_object(PDFData *p,PDFObjItem *pi)
{
	pi->offset = ftell(p->fp);
}

/** 圧縮オブジェクトのデータ書き込み開始 */

void pdf_start_write_objstrm(PDFData *p,PDFObjstrmItem *pi)
{
	pi->offset = ftell(p->fpstrm);
}


//============================
// 入出力
//============================


/** 座標数値の出力
 *
 * PDF での座標系に変換してあること。
 * 小数点以下3桁まで。終端の '0' は除外する。
 *
 * fspace: 後に空白を追加 */

void pdf_put_coord(FILE *fp,int val,mlkbool fspace)
{
	char m[16],*pc;
	int len;

	if(val == 0)
		fputc('0', fp);
	else
	{
		len = mIntToStr_float(m, val, 3);

		for(pc = m + len - 1; *pc == '0'; pc--);

		if(*pc == '.') pc--;

		pc[1] = 0;

		fputs(m, fp);
	}

	if(fspace) fputc(' ', fp);
}

/** double 値の出力 */

void pdf_put_double(FILE *fp,double val,mlkbool fspace)
{
	pdf_put_coord(fp, lround(val * 1000), fspace);
}

/** fp の先頭から指定サイズを、バッファに読み込み
 *
 * size: -1 で、現在位置までのサイズ
 * return: データサイズ */

uint32_t pdf_read_file_buf(FILE *fp,int size,uint8_t **ppdst)
{
	uint8_t *buf;

	if(size < 0)
		size = ftell(fp);

	buf = (uint8_t *)mMalloc(size);
	if(!buf) app_enderr_alloc();

	rewind(fp);

	if(fread(buf, 1, size, fp) != size)
	{
		mFree(buf);
		app_enderrno(MLKERR_IO, NULL);
	}

	rewind(fp);

	*ppdst = buf;

	return size;
}

/** fpin 先頭から指定サイズを読み込み、fpout に出力 */

void pdf_copy_file(PDFData *p,FILE *fpin,FILE *fpout,int size)
{
	uint8_t *buf;
	int n;

	buf = p->workbuf;

	rewind(fpin);

	while(size)
	{
		n = (size < PDF_WORKBUF_SIZE)? size: PDF_WORKBUF_SIZE;

		if(fread(buf, 1, n, fpin) != n)
			app_enderrno(MLKERR_IO, NULL);

		fwrite(buf, 1, n, fpout);

		size -= n;
	}
}

/** fp の先頭から指定サイズを、圧縮して fpcomp に書き込む
 *
 * size: -1 で、現在位置までのデータ
 * return: 圧縮後のサイズ */

int pdf_compress_file(PDFData *p,FILE *fp,int size)
{
	uint8_t *buf;
	mZlib *zlib;
	int ret,n;

	if(size < 0) size = ftell(fp);

	buf = p->workbuf;
	zlib = p->zlib;

	rewind(fp);
	rewind(p->fpcomp);

	mZlibEncReset(zlib);

	//読み込んで、圧縮

	while(size)
	{
		n = (size < PDF_WORKBUF_SIZE)? size: PDF_WORKBUF_SIZE;

		if(fread(buf, 1, n, fp) != n)
			app_enderrno(MLKERR_IO, NULL);
	
		ret = mZlibEncSend(zlib, buf, n);
		if(ret)
			app_enderrno(ret, NULL);

		size -= n;
	}

	//終了

	ret = mZlibEncFinish(zlib);
	if(ret)
		app_enderrno(ret, NULL);

	return mZlibEncGetSize(zlib);
}

/** ストリームの出力 (無圧縮 or zlib)
 *
 * /Length から、endobj まで出力する。
 * それより前のデータはあらかじめ出力しておくこと。
 *
 * fpin: 先頭から、ストリームの無圧縮データをセットしておく
 * fpout: 出力先
 * size: 無圧縮のサイズ。-1 で、fpin の先頭から現在位置まで */

void pdf_put_stream(PDFData *p,FILE *fpin,FILE *fpout,int size)
{
	int outsize;

	if(size < 0)
		size = ftell(fpin);

	//出力サイズ

#if DEBUG_PDF_UNCOMP
	outsize = size;
#else
	//圧縮

	outsize = pdf_compress_file(p, fpin, size);

	if(outsize > size) outsize = size;
#endif

	//

	fprintf(fpout, "/Length %d", outsize);

	if(outsize < size)
		fputs("/Filter/FlateDecode", fpout);

	fputs(">>stream\n", fpout);

	//データ

	pdf_copy_file(p, (outsize == size)? fpin: p->fpcomp, fpout, outsize);

	fputs("\nendstream\nendobj\n", fpout);
}

/** ストリームの出力 (常に zlib 圧縮) */

void pdf_put_stream_zlib(PDFData *p,FILE *fpin,FILE *fpout,int size)
{
	int outsize;

	if(size < 0)
		size = ftell(fpin);

	//圧縮

	outsize = pdf_compress_file(p, fpin, size);

	//

	fprintf(fpout, "/Length %d/Filter/FlateDecode>>stream\n", outsize);

	//データ

	pdf_copy_file(p, p->fpcomp, fpout, outsize);

	fputs("\nendstream\nendobj\n", fpout);
}

/** ストリームの出力 (fpcomp に圧縮済み) */

void pdf_put_stream_compressed(PDFData *p)
{
	FILE *fp = p->fp;
	int size;

	size = ftell(p->fpcomp);

	fprintf(fp, "/Length %d/Filter/FlateDecode>>stream\n", size);

	pdf_copy_file(p, p->fpcomp, fp, size);

	fputs("\nendstream\nendobj\n", fp);
}

/** ストリームの出力 (ファイルの内容を無圧縮で)
 *
 * JPEG ファイル時 */

void pdf_put_stream_file(PDFData *p,const char *filename)
{
	FILE *fpin;
	int size;

	fpin = mFILEopen(filename, "rb");
	if(!fpin) app_enderrno(MLKERR_OPEN, filename);

	fseek(fpin, 0, SEEK_END);
	size = ftell(fpin);

	//

	fprintf(p->fp, "/Length %d>>stream\n", size);

	pdf_copy_file(p, fpin, p->fp, size);

	fputs("\nendstream\nendobj\n", p->fp);

	fclose(fpin);
}


//============================
// 出力 (PDF)
//============================


/** (PDF) オブジェクトの出力を開始 & 先頭の文字列を出力
 *
 * type: /Type の名前。NULL でなし */

void pdf_put_object_top(PDFData *p,PDFObjItem *pi,const char *type)
{
	pdf_start_write_object(p, pi);

	fprintf(p->fp, "%d 0 obj<<", pi->objno);

	if(type)
		fprintf(p->fp, "/Type/%s", type);
}

/* 圧縮オブジェクト: オフセット順にソート */

static int _sortfunc_objstrm(mListItem *item1,mListItem *item2,void *param)
{
	int32_t off1,off2;

	off1 = ((PDFObjstrmItem *)item1)->offset;
	off2 = ((PDFObjstrmItem *)item2)->offset;

	return (off1 < off2)? -1: 1;
}

/** (PDF) 圧縮オブジェクトのオブジェクトを出力
 *
 * すべてのオブジェクトのデータが実際に書き込まれた状態であること。
 * オブジェクトの作成後、書き込まれていないデータが残っていないようにする。 */

void pdf_put_objectstrem(PDFData *p)
{
	PDFObjItem *piobj;
	PDFObjstrmItem *pi;
	FILE *fpstrm;
	uint8_t *buf;
	int datsize,first_pos,index;

	if(!p->cur_objstrm) return;

	fpstrm = p->fpstrm;
	piobj = p->cur_objstrm;

	//オフセット位置の昇順にソート

	mListSort(&p->list_objstrm, _sortfunc_objstrm, 0);

	//インデックスのセット

	index = 0;

	MLK_LIST_FOR(p->list_objstrm, pi, PDFObjstrmItem)
	{
		pi->piobj->index = index++;
	}

	//----- データ (fpstrm)

	//データ部分を読み込み

	datsize = pdf_read_file_buf(fpstrm, -1, &buf);

	//オブジェクト番号とオフセットを書き込み

	MLK_LIST_FOR(p->list_objstrm, pi, PDFObjstrmItem)
	{
		fprintf(fpstrm, "%d %d%c",
			pi->objno, pi->offset, (pi->i.next)? ' ': '\n');
	}

	first_pos = ftell(fpstrm);

	//データを書き込み

	fwrite(buf, 1, datsize, fpstrm);

	mFree(buf);

	datsize += first_pos;

	//----- オブジェクトを PDF に出力

	pdf_put_object_top(p, piobj, "ObjStm");

	fprintf(p->fp, "/N %d/First %d", p->list_objstrm.num, first_pos);

	pdf_put_stream(p, fpstrm, p->fp, datsize);

	//----- クリア

	mListDeleteAll(&p->list_objstrm);

	p->cur_objstrm = NULL;

	rewind(p->fpstrm);
}

/** 圧縮オブジェクトが指定数以上なら出力
 *
 * ※すべてのデータが実際に書き込まれていることが前提なので、
 *   各タイミングで手動で実行する。 */

void pdf_put_objectstrem_max(PDFData *p)
{
	if(p->list_objstrm.num >= 95)
		pdf_put_objectstrem(p);
}


//=========================================
// 出力 (PDF:クロスリファレンスストリーム)
//=========================================


/* 値からバイト数を計算して、bytes と比較し、大きい方を返す */

static int _get_value_bytes(uint32_t v,int bytes)
{
	int n;

	if(v < 256)
		n = 1;
	else if(v < (1<<16))
		n = 2;
	else if(v < (1<<24))
		n = 3;
	else
		n = 4;

	return (n < bytes)? bytes: n;
}

/* 指定バイト数で値を出力 (BE) */

static void _put_value_bytes(FILE *fp,uint32_t v,int bytes)
{
	uint8_t b[4];

	switch(bytes)
	{
		case 1:
			b[0] = (uint8_t)v;
			break;
		case 2:
			b[0] = (uint8_t)(v >> 8);
			b[1] = (uint8_t)v;
			break;
		case 3:
			b[0] = (uint8_t)(v >> 16);
			b[1] = (uint8_t)(v >> 8);
			b[2] = (uint8_t)v;
			break;
		default:
			b[0] = (uint8_t)(v >> 24);
			b[1] = (uint8_t)(v >> 16);
			b[2] = (uint8_t)(v >> 8);
			b[3] = (uint8_t)v;
			break;
	}

	fwrite(b, 1, bytes, fp);
}

/* フィールドのバイト数取得 */

static void _get_field_bytes(PDFData *p,int *psize2,int *psize3)
{
	PDFObjItem *pi;
	int no,index,size2,size3,max_no,max_index;
	uint32_t offset,max_offset;

	//2番目 = 一番値が大きいオフセット値とオブジェクト番号が収まるバイト数
	//3番目 = インデックス値が収まるバイト数

	max_offset = 0;
	max_no = 0;
	max_index = 0;

	MLK_LIST_FOR(p->list_obj, pi, PDFObjItem)
	{
		no = pi->objno_strm;
		index = pi->index;
		offset = pi->offset;
	
		if(max_no < no) max_no = no;
		if(max_index < index) max_index = index;
		if(max_offset < offset) max_offset = offset;
	}

	//

	size2 = _get_value_bytes(no, 0);
	size2 = _get_value_bytes(offset, size2);

	size3 = _get_value_bytes(index, 0);

	*psize2 = size2;
	*psize3 = size3;
}

/* オブジェクト番号順にソート */

static int _sortfunc_objno(mListItem *item1,mListItem *item2,void *param)
{
	if( ((PDFObjItem *)item1)->objno < ((PDFObjItem *)item2)->objno )
		return -1;
	else
		return 1;
}

/** クロスリファレンスストリームの出力
 *
 * ※クロスリファレンスストリーム自身も含める。 */

void pdf_put_crossref_stream(PDFData *p)
{
	FILE *fpstrm;
	PDFObjItem *pi,*picross;
	int no,size2,size3;

	//オブジェクト番号順にソート
	// :圧縮オブジェクトで番号予約する場合、データが書き込める段階でオブジェクトを追加しないと、
	// :圧縮オブジェクトの親が複数になった場合、追加時の親と実際の親が異なってしまう場合がある。
	// :そのため、オブジェクト番号順でアイテムを追加することはできない。

	mListSort(&p->list_obj, _sortfunc_objno, NULL);

	//

	picross = pdf_add_object(p, NULL);

	pdf_put_object_top(p, picross, "XRef");

	p->offset_crossref = picross->offset;

	//フィールドバイト数

	_get_field_bytes(p, &size2, &size3);

	//---- データ (fpstrm)

	fpstrm = p->fpstrm;

	rewind(fpstrm);

	//空

	_put_value_bytes(fpstrm, 0, 1);
	_put_value_bytes(fpstrm, 0, size2);
	_put_value_bytes(fpstrm, 0xffffffff, size3);

	//各オブジェクト

	MLK_LIST_FOR(p->list_obj, pi, PDFObjItem)
	{
		if(pi->objno_strm)
		{
			//圧縮オブジェクト
			_put_value_bytes(fpstrm, 2, 1);
			_put_value_bytes(fpstrm, pi->objno_strm, size2);
			_put_value_bytes(fpstrm, pi->index, size3);
		}
		else
		{
			_put_value_bytes(fpstrm, 1, 1);
			_put_value_bytes(fpstrm, pi->offset, size2);
			_put_value_bytes(fpstrm, 0, size3);
		}
	}

	//---- PDF に出力

	no = picross->objno + 1;

	fprintf(p->fp, "/Size %d/Index[0 %d]", no, no);

	fprintf(p->fp, "/W[1 %d %d]/Root %d 0 R/Info %d 0 R",
		size2, size3, p->objno_root, p->objno_info);

	pdf_put_stream(p, fpstrm, p->fp, -1);
}
	

//============================
// 出力 (圧縮オブジェクト)
//============================


/** (fpstrm) UTF-16BE 文字列を出力
 *
 * str: NULL または空文字列で出力しない */

void pdf_put_string_utf16(PDFData *p,const char *name,const char *str)
{
	FILE *fp = p->fpstrm;
	mlkuchar uc;
	mlkuchar16 c[2];
	int len,i;

	if(!str || !(*str)) return;

	fprintf(fp, "/%s<FEFF", name);

	while(*str)
	{
		if(mUTF8GetChar(&uc, str, -1, &str) == -1) break;

		len = mUnicharToUTF16(uc, c, 2);
		if(len == -1) break;

		for(i = 0; i < len; i++)
			fprintf(fp, "%04X", c[i]); 
	}

	fputc('>', fp);
}

/* 作成・最終更新日時を出力 */

static void _put_date(PDFData *p)
{
	struct tm *tm;
	time_t tmval;
	char m[32],*ps,*pd;
	int len;

	tmval = time(NULL);

	tm = localtime(&tmval);

	len = strftime(m, 32, "%Y%m%d%H%M%S%z", tm);

	ps = m + len - 1;
	pd = m + len + 2;

	*(pd--) = 0;
	*(pd--) = '\'';
	*(pd--) = *(ps--);
	*(pd--) = *(ps--);
	*(pd--) = '\'';

	fprintf(p->fpstrm, "/CreationDate(D:%s)", m);
	fprintf(p->fpstrm, "/ModDate(D:%s)", m);
}

/** 共通のグラフィック状態を追加 & 出力 */

void pdf_putadd_graphic_state(PDFData *p)
{
	PDFObjstrmItem *pi;

	pi = pdf_add_object_in_stream(p, NULL, TRUE);

	p->objno_gs = pi->objno;

	//重なったオブジェクトの背景を消去しない。
	//グリフは個別に合成する。

	fputs("<</Type/ExtGState/OP true/TK false>>\n", p->fpstrm);
}

/** Info 辞書を追加 & 出力 */

void pdf_putadd_info(PDFData *p)
{
	FILE *fp = p->fpstrm;
	PDFObjstrmItem *pi;

	pi = pdf_add_object_in_stream(p, NULL, TRUE);

	p->objno_info = pi->objno;

	//

	fputs("<<", fp);

	pdf_put_string_utf16(p, "Author", g_set->pdf.str_author.buf);
	pdf_put_string_utf16(p, "Title", g_set->pdf.str_title.buf);
	pdf_put_string_utf16(p, "Subject", g_set->pdf.str_subject.buf);

	fprintf(fp, "/Producer(%s)", APPNAME_CREATOR);

	_put_date(p);

	fputs("/Trapped/False>>\n", fp);
}

/** カタログ辞書を追加 & 出力 */

void pdf_putadd_catalog(PDFData *p)
{
	FILE *fp = p->fpstrm;
	PDFObjstrmItem *pi;

	pi = pdf_add_object_in_stream(p, NULL, TRUE);

	p->objno_root = pi->objno;

	//

	fprintf(fp, "<</Type/Catalog/ViewerPreferences<</Direction/R2L>>/Pages %d 0 R",
		p->obj_rootpage.objno);

	if(p->objno_outline)
		fprintf(fp, "/Outlines %d 0 R/PageMode/UseOutlines", p->objno_outline);

	fputs(">>\n", fp);
}

/** ルートのページツリーノードを追加 & 出力 */

void pdf_putadd_root_pagenode(PDFData *p)
{
	PDFObjItem *pi;
	FILE *fpstrm;
	int flag;

	//開始時に番号を予約しているので、ここで追加

	pdf_add_object_in_stream(p, &p->obj_rootpage, TRUE);

	//--------

	fpstrm = p->fpstrm;

	fprintf(fpstrm, "<</Type/Pages/Count %d", p->pageobj_num);

	//用紙サイズ

	fputs("/MediaBox[0 0 ", fpstrm);

	pdf_put_coord(fpstrm, p->paper_fullw, TRUE);
	pdf_put_coord(fpstrm, p->paper_fullh, FALSE);

	//子のページオブジェクト

	fputs("]/Kids[", fpstrm);

	flag = 0;

	MLK_LIST_FOR(p->list_obj, pi, PDFObjItem)
	{
		if(pi->flags & PDFOBJITEM_F_PAGEOBJECT)
		{
			if(flag)
				fputc(' ', fpstrm);
			else
				flag = 1;

			fprintf(fpstrm, "%d 0 R", pi->objno);
		}
	}


	fputs("]>>\n", fpstrm);
}

/** アウトラインの出力 */

typedef struct
{
	mListItem i;
	PDFReservedObject obj;
	IndexItem *item;
}_outline_item;

void pdf_putadd_outline(PDFData *p,int start,int end)
{
	FILE *fp;
	char *pc;
	IndexItem *pi;
	PDFObjstrmItem *parent;
	_outline_item *piol;
	mList list = MLIST_INIT;

	if(mStrIsEmpty(&g_set->pdf.str_outline)) return;

	//セットするアウトラインを抽出
	// :PDF 分割時は、ファイル内に含まれるページのみ

	for(pc = g_set->pdf.str_outline.buf; *pc; pc += strlen(pc) + 1)
	{
		pi = app_search_index_item(pc);

		if(pi && start <= pi->save_pageno && pi->save_pageno <= end)
		{
			piol = (_outline_item *)mListAppendNew(&list, sizeof(_outline_item));
			if(!piol) app_enderr_alloc();

			piol->item = pi;

			piol->obj.objno = p->cur_objno++;
		}
	}

	if(!list.top) return;

	//アウトラインの親

	fp = p->fpstrm;

	parent = pdf_add_object_in_stream(p, NULL, TRUE);

	p->objno_outline = parent->objno;

	fprintf(fp, "<</Type/Outlines/Count %d/First %d 0 R/Last %d 0 R>>\n",
		list.num, ((_outline_item *)list.top)->obj.objno,
		((_outline_item *)list.bottom)->obj.objno);

	//アウトラインアイテム

	MLK_LIST_FOR(list, piol, _outline_item)
	{
		pdf_put_objectstrem_max(p);
	
		pdf_add_object_in_stream(p, &piol->obj, TRUE);

		//

		fprintf(fp, "<</Parent %d 0 R/Dest[%d 0 R /XYZ null null null]",
			parent->objno, piol->item->pdf_objno);

		pc = piol->item->title;
		if(!pc) pc = piol->item->name;

		pdf_put_string_utf16(p, "Title", pc);

		if(piol->i.prev)
			fprintf(fp, "/Prev %d 0 R", ((_outline_item *)piol->i.prev)->obj.objno);

		if(piol->i.next)
			fprintf(fp, "/Next %d 0 R", ((_outline_item *)piol->i.next)->obj.objno);

		fputs(">>\n", fp);
	}

	mListDeleteAll(&list);
}


//==============================
// ページ関連
//==============================


/** グラフィック命令の出力
 *
 * '%d' : 整数値 (int)
 * '%p' : 用紙位置・幅 (int) 左上位置
 * '%y' : 用紙位置 y (int) 左上位置
 * '%c' : 塗りつぶしの色 (uint32:RGB)
 * 最後に空白が出力される */

void pdf_put_draw(PDFData *p,FILE *fp,const char *format,...)
{
	va_list ap;
	char c;
	uint32_t col;
	int n,r,g,b;
	char m[16];

	va_start(ap, format);

	while(1)
	{
		c = *(format++);
		if(!c) break;

		if(c != '%')
		{
			fputc(c, fp);
			continue;
		}

		//--- '%'+char
		
		c = *(format++);

		switch(c)
		{
			//整数値
			case 'd':
				n = mIntToStr(m, va_arg(ap, int));
				fwrite(m, 1, n, fp);
				break;
			//用紙 x 位置、幅
			case 'p':
				pdf_put_coord(fp, va_arg(ap, int), FALSE);
				break;
			//用紙 y 位置
			case 'y':
				n = p->paper_fullh - va_arg(ap, int);
				
				pdf_put_coord(fp, n, FALSE);
				break;
			//塗りつぶしの色
			case 'c':
				col = va_arg(ap, uint32_t);
				r = MLK_RGB_R(col);
				g = MLK_RGB_G(col);
				b = MLK_RGB_B(col);

				if(r == g && g == b)
				{
					pdf_put_double(fp, r / 255.0, TRUE);

					fputc('g', fp);
				}
				else
				{
					pdf_put_double(fp, r / 255.0, TRUE);
					pdf_put_double(fp, g / 255.0, TRUE);
					pdf_put_double(fp, b / 255.0, TRUE);

					fputs("rg", fp);
				}
				break;
		}
	}

	va_end(ap);

	fputc(' ', fp);
}

/* 使用画像として追加 */

static void _add_image_used(PDFData *p,ImageItem *img)
{
	ImageItem **ppar;
	int i,num;

	ppar = p->pagest.img;
	num = p->pagest.imgnum;

	//すでにあれば、何もしない

	for(i = 0; i < num; i++)
	{
		if(ppar[i] == img) return;
	}

	//新規追加

	ppar[num] = img;

	p->pagest.imgnum++;
}

/** 画像の追加
 *
 * 現在の PDF ファイルで画像がまだ追加されていなければ、追加 */

void pdf_add_image(PDFData *p,ImageItem *piimg)
{
	PDFObjItem *pi;
	FILE *fp;
	Image *img;
	int fjpeg;

	//ページで使用されている画像として登録

	_add_image_used(p, piimg);

	//現在の PDF ファイルに追加済み

	if(piimg->pdf_objno) return;

	//------ 新規追加

	//オブジェクト追加
	
	pi = pdf_add_object(p, NULL);

	piimg->pdf_objno = pi->objno;

	//画像を読み込み
	// :JPEG は常にそのまま埋め込む (カラータイプの変換はできない)。

	app_image_load(piimg);

	img = piimg->img;

	fjpeg = (img->filetype == IMAGE_FILETYPE_JPEG);

	//圧縮して出力 (JPEG 以外)

	if(!fjpeg)
	{
		rewind(p->fpcomp);

		image_output_pdf(img, p->zlib);
	}

	//PDF 出力

	fp = p->fp;

	pdf_put_object_top(p, pi, "XObject");

	fprintf(fp, "/Subtype/Image/Width %d/Height %d/ColorSpace/%s/BitsPerComponent %d",
		img->width, img->height,
		(img->coltype == IMAGE_COLTYPE_GRAY)? "DeviceGray": "DeviceRGB",
		(!fjpeg && img->imgbits == 1)? 1: 8);

	if(fjpeg)
	{
		fputs("/Filter/DCTDecode", fp);
		
		pdf_put_stream_file(p, piimg->filename);
	}
	else
		pdf_put_stream_compressed(p);

	//画像を解放

	app_image_free(piimg);
}
