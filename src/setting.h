/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

//===========================
// 設定データ
//===========================

typedef struct _SettingData SettingData;
typedef struct _Font Font;
typedef struct _ImageItem ImageItem;

extern SettingData *g_set;

//単位
enum
{
	UNIT_TYPE_DEFAULT,	//デフォルト値
	UNIT_TYPE_SET,		//直接値がセットされている
	UNIT_TYPE_X,		//フォントのデフォルトサイズの倍率
	UNIT_TYPE_W			//フォントサイズの倍率
};

//------------ 各データ

/* フォント指定 */

typedef struct _FontSpec
{
	int32_t unit,	//単位 (SET or X)
		size,		//フォントサイズ (1pt = 1000)
		height;		//高さ
	struct _FontItem *item;
}FontSpec;

/* 用紙上の位置や幅 */

typedef struct _PaperPos
{
	int32_t val;	//1000 = 1pt (w の場合、1.0 = 1000)
	int unit;		//単位 (SET/W)
}PaperPos;

/* 画像 */

typedef struct _ImageInfo
{
	uint8_t base,	//基準位置
		align;		//配置
	int32_t width,	//用紙上のサイズ (-1 で 100%)
		height,
		x,y;		//位置の指定
	ImageItem *item;	//NULL でなし
}ImageInfo;

enum
{
	IMAGEINFO_BASE_PAPER = 0,
	IMAGEINFO_BASE_BLEED,
	IMAGEINFO_BASE_BODY,
	IMAGEINFO_BASE_TILE,

	IMAGEINFO_ALIGN_CENTER = 0,
	IMAGEINFO_ALIGN_IN_TOP,
	IMAGEINFO_ALIGN_IN_BOTTOM,
	IMAGEINFO_ALIGN_OUT_TOP,
	IMAGEINFO_ALIGN_OUT_BOTTOM
};

//------------ アイテム

/* フォントアイテム (定義された分だけ) */

typedef struct _FontItem
{
	mListItem i;

	char name[32];			//定義名
	int32_t index,			//フォントインデックス
		size,				//デフォルトのフォントサイズ (1000 = 1pt)
		flags,
		dakuten_horz_x,		//濁点結合の位置 (文字サイズに対する倍率。1.0 = 1000)
		dakuten_horz_y,
		dakuten_vert_x,
		dakuten_vert_y;
	Font *font;			//同じフォントファイルとインデックスなら、同じポインタになる
	char filename[0];	//フォントファイル名
}FontItem;

enum
{
	FONTITEM_FLAGS_HORZ_HALF = 1<<0	//横組時、半角幅対象の約物は常に半角幅
};

/* 圏点アイテム */

typedef struct _KentenItem
{
	mListItem i;

	uint32_t code;	//文字コード (Unicode/CID[最上位ビットON])
	int32_t pos;	//1.0=1000
	FontSpec font;
	char name[0];	//定義名
}KentenItem;

/* 全角幅セットのアイテム */

typedef struct
{
	mListItem i;

	char name[32];		//定義名
	uint32_t code[1];	//Unicode (0 で終了)
}FullWidthItem;

//----------------

/* 見出し */

typedef struct _CaptionLayout
{
	uint8_t indent,	//字下げ
		width,		//行幅
		before,		//前の行空き
		after,		//後の行空き
		align,		//行方向の揃え
		sep;		//ページの分割可能か
	FontSpec font;
}CaptionLayout;

enum
{
	CAPTION_LAYOUT_ALIGN_CENTER,
	CAPTION_LAYOUT_ALIGN_LINE
};

//-------------------

/* レイアウトアイテム */

typedef struct _LayoutItem
{
	mListItem i;

	char name[32];		//定義名
	uint8_t mode,
		line_align,			//文字送り方向の配置
		body_align,			//行送り方向の配置
		index_number,		//目次のページ数の表記方法
		indent_head_first,	//字下げ
		indent_head_wrap,
		indent_foot,		//字上げ
		ruby_small_kana,	//小書きかなの変換
		ruby_align,			//ルビ縦組時の配置
		hang_flag,			//ぶら下げ組をするか
		reduce_flag,		//詰め処理をするか
		break_type,			//禁則処理
		indent_kakko,		//始め括弧が行頭に来た時の字下げ
		gsub,				//GSUB 置き換えフラグ
		bkgndimg_no_page;	//背景画像を表示しないページ
	int32_t line_num,		//行の数
		dash_width;			//全角ダッシュの描画幅 (文字サイズに対する倍率。1.0 = 1000)
	uint32_t index_sp_char,	//目次の空白部分の文字 (0 で空白。上位ビット ON で CID)
		textcol,			//テキスト色
		bkgndcol;			//背景色

	FontSpec basefont,	//基本フォント
		ruby_font,		//ルビフォント
		kenten_font;	//圏点フォント

	PaperPos sp_head,	//天の余白
		sp_foot,		//地の余白
		sp_gutter,		//のど側の余白
		sp_column,		//段間
		line_len,		//行長
		line_feed;		//行送り

	CaptionLayout caption[3];	//見出し(0:大見出し、1:中見出し、2:小見出し)

	ImageInfo bkgndimg;	//背景画像
	ImageItem *bkgnd_img_left,	//左右ページ時の背景
		*bkgnd_img_right;

	FullWidthItem *item_fullwidth;	//全角幅扱いの約物文字のセット (0 でなし)
}LayoutItem;

enum
{
	LAYOUT_MODE_VERT,	//縦組１段
	LAYOUT_MODE_VERT2,	//縦組２段
	LAYOUT_MODE_HORZ	//横組１段
};

enum
{
	LAYOUT_ALIGN_NORMAL,
	LAYOUT_ALIGN_CENTER,
	LAYOUT_ALIGN_BOTTOM
};

enum
{
	LAYOUT_INDEX_NUM_NUM2,	//半角数字
	LAYOUT_INDEX_NUM_NUM3,	//1/3幅数字
	LAYOUT_INDEX_NUM_NUM,	//プロポーショナル幅数字
	LAYOUT_INDEX_NUM_KANJI	//漢数字
};

enum
{
	LAYOUT_RUBY_SMALL_KANA_BIG,	//大文字に変換
	LAYOUT_RUBY_SMALL_KANA_RAW	//そのまま
};

enum
{
	LAYOUT_RUBY_ALIGN_TOP,
	LAYOUT_RUBY_ALIGN_CENTER
};

enum
{
	LAYOUT_BREAK_BOOK,
	LAYOUT_BREAK_NORMAL
};

enum
{
	LAYOUT_INDENT_KAKKO_F2W0,	//先頭二分空き、折返し天付き
	LAYOUT_INDENT_KAKKO_F1W0	//先頭全角空き、折返し天付き
};

enum
{
	LAYOUT_GSUB_RUBY = 1<<0,	//ルビ用
	LAYOUT_GSUB_NLCK = 1<<1,	//印刷標準字体
	LAYOUT_GSUB_HKNA = 1<<2,	//横組かな
	LAYOUT_GSUB_VKNA = 1<<3		//縦組かな
};

enum
{
	LAYOUT_NOPAGE_BLANK = 1<<0,	//空ページ
	LAYOUT_NOPAGE_IMAGE = 1<<1	//画像ページ
};

//-------------------

//出力設定

typedef struct
{
	uint8_t antialias,	//0:指定なし、1:OFF、2:ON
		hinting,		//ヒンティング
		grid,			//グリッド描画
		pdf_fixed;		//PDF ファイル名固定
	int format,
		dpi,			//dpi,width,height はいずれか一つ
		width,
		height;
	uint32_t grid_col;	//グリッド色
	mStr str_prefix;
}SettingOutput;

enum
{
	OUTPUT_HINTING_OFF,
	OUTPUT_HINTING_NORMAL,
	OUTPUT_HINTING_LIGHT,
	OUTPUT_HINTING_MONO
};

/* PDF 設定 */

typedef struct
{
	uint8_t	no_unicode;		//ToUnicode セットしない

	mStr str_title,
		str_author,
		str_subject,
		str_outline;
}SettingPDF;

/* 柱・ノンブルの設定 (共通) */

typedef struct
{
	uint8_t pos,	//配置
		no_page;	//表示しないページ
	FontSpec font;	//フォント
	PaperPos vspace,	//基本版面からの上下の余白
		hspace;			//基本版面の小口側の端からの余白
}TitleNumberSet;

enum
{
	TITLENUMBER_POS_HEAD,	//天
	TITLENUMBER_POS_FOOT	//地
};

//-------------------

/* 設定データ */

struct _SettingData
{
	uint8_t bleed,			//裁ち切り (mm)
		page_type,			//ページタイプ
		image_bits,			//画像ビット数 (0,1,8)
		number_hide_page;	//隠しノンブルにするページ

	uint16_t paper_w,	//用紙のサイズ (mm)
		paper_h,
		number_hide_x,	//隠しノンブルの位置 (mm)
		number_hide_y;
	uint32_t number_hide_col;	//隠しノンブルの背景色

	SettingOutput output;	//出力設定
	SettingPDF pdf;			//PDF 設定

	TitleNumberSet title,	//柱・ノンブル
		number;
	PaperPos title_number_space;	//ノンブルと柱の余白

	mList list_font,	//フォント定義
		list_layout,	//レイアウト定義
		list_kenten,	//圏点定義
		list_fullwidth;	//全角幅文字のセット

	//

	FontItem *fontitem_base;	//base フォント
	LayoutItem *layout_base;	//基本レイアウト
};

enum
{
	PAGETYPE_TWO,	//見開き
	PAGETYPE_ONE,	//単ページ
	PAGETYPE_TWO_1P	//見開き2P分を1Pとする
};

enum
{
	TITLE_PAGE_ODD,
	TITLE_PAGE_DOUBLE
};

enum
{
	TITLENUMBER_PAGE_ALL   = 1<<0,	//全てのページ
	TITLENUMBER_PAGE_EVEN  = 1<<1,	//見開きの偶数ページ
	TITLENUMBER_PAGE_BLANK = 1<<2,	//空白ページ
	TITLENUMBER_PAGE_IMAGE = 1<<3	//画像ページ
};

/* func */

int convert_mm_to_pt(int mm);

void setting_new(void);
void setting_free(void);

const char *setting_add_layout(const char *name,const char *name_copy,LayoutItem **dst);
LayoutItem *setting_search_layout(const char *name);

const char *setting_add_font(const char *name,const char *filename,int index,double size,int *dakuten,int flags);
FontItem *setting_search_font(const char *name);
void setting_load_ftfont(int antialias);

KentenItem *setting_search_kenten(const char *name,int *dst_no);
KentenItem *setting_add_kenten(const char *name);

FullWidthItem *setting_search_fullwidth(const char *name);
const char *setting_add_fullwidth(const char *name,const char *text);

void setting_set_fontspec(FontSpec *pi,FontSpec *font,double defsize);
const char *setting_check_value(void);
