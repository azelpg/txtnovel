/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/********************************
 * 画像: PDF 出力用
 ********************************/

#include <mlk.h>
#include <mlk_zlib.h>

#include "image.h"
#include "app.h"


/* 24bit 変換 */

static int _convert_24bit(Image *p)
{
	uint8_t **ppbuf,*ps,*pd;
	int ix,iy;

	ppbuf = p->ppbuf;

	for(iy = p->height; iy; iy--)
	{
		pd = *(ppbuf++);
		ps = pd;
		
		for(ix = p->width; ix; ix--, pd += 3, ps += 4)
		{
			pd[0] = ps[0];
			pd[1] = ps[1];
			pd[2] = ps[2];
		}
	}

	return p->width * 3;
}

/* 8bit 変換 */

static int _convert_8bit(Image *p)
{
	uint8_t **ppbuf,*ps,*pd;
	int ix,iy;

	ppbuf = p->ppbuf;

	for(iy = p->height; iy; iy--)
	{
		pd = *(ppbuf++);
		ps = pd;
		
		for(ix = p->width; ix; ix--, ps += 4)
		{
			*(pd++) = ps[0];
		}
	}

	return p->width;
}

/* 1bit 変換 */

static int _convert_1bit(Image *p)
{
	uint8_t **ppbuf,*ps,*pd;
	int ix,iy;
	uint8_t val,f;

	ppbuf = p->ppbuf;

	for(iy = p->height; iy; iy--)
	{
		pd = *(ppbuf++);
		ps = pd;
		f = 0x80;
		val = 0;
		
		for(ix = p->width; ix; ix--, ps += 4)
		{
			if(!f)
			{
				*(pd++) = val;
				f = 0x80;
				val = 0;
			}
		
			if(*ps == 255)
				val |= f;

			f >>= 1;
		}

		*pd = val;
	}

	return (p->width + 7) / 8;
}

/* 圧縮
 *
 * return: 圧縮後のサイズ */

static int _compress_file(Image *p,mZlib *zlib,int line_bytes)
{
	uint8_t **ppbuf;
	int ret,iy;

	mZlibEncReset(zlib);

	ppbuf = p->ppbuf;

	for(iy = p->height; iy; iy--, ppbuf++)
	{
		ret = mZlibEncSend(zlib, *ppbuf, line_bytes);
		if(ret)
			app_enderrno(ret, NULL);
	}

	//終了

	ret = mZlibEncFinish(zlib);
	if(ret)
		app_enderrno(ret, NULL);

	return mZlibEncGetSize(zlib);
}

/** PDF 出力用データに変換し、ファイルに圧縮する
 *
 * ※ JPEG の場合は、元のファイルのまま出力 */

int image_output_pdf(Image *p,mZlib *zlib)
{
	int pitch;

	//イメージ変換

	switch(p->imgbits)
	{
		case 1:
			pitch = _convert_1bit(p);
			break;
		case 8:
			pitch = _convert_8bit(p);
			break;
		default:
			pitch = _convert_24bit(p);
	}

	return _compress_file(p, zlib, pitch);
}
