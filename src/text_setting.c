/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * テキスト: 設定コマンドの処理
 ************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <mlk.h>
#include <mlk_str.h>
#include <mlk_list.h>
#include <mlk_string.h>
#include <mlk_util.h>

#include "app.h"
#include "setting.h"
#include "image.h"

#include "cmpstring.h"
#include "text.h"
#include "text_cmd_set.h"


//--------------------

enum
{
	GETSTRING_ARG,
	GETSTRING_ITEM_NAME,
	GETSTRING_ITEM_VALUE
};

//--------------------


/* mStr に1文字追加 */

static void _add_str(mStr *str,int32_t c)
{
	mStrAppendUnichar(str, c & CHAR_MASK_CODE);
}

/* 次の文字列を取得 (値、項目名、項目値)
 *
 * 後ろの空白はスキップする。※前の空白はスキップしない
 *
 * type:
 * 	[0] 1つの引数値。return = '}'
 *  [1] 項目名。return = ':'
 *  [2] 項目値。return = '; or }'
 * str: UTF-8
 * return: 次の文字 (終端はエラー扱い) */

static int32_t _get_string(TextData *p,int type,int32_t c,mStr *str)
{
	mStrEmpty(str);

	if(c == '\''
		&& (type == GETSTRING_ARG || type == GETSTRING_ITEM_VALUE))
	{
		//'' で囲まれた文字列

		while(1)
		{
			c = text_setting_getchar(p);

			if(c == '\'')
				break;
			else if(c == -1)
				app_enderr("'' で囲まれていません");
			else
				_add_str(str, c);
		}

		c = text_setting_skip_getchar(p);
	}
	else
	{
		//通常文字列
		// ARG / 空白 } まで
		// ITEM_NAME / 空白 : ; } まで ("} ;" の場合エラー)
		// ITEM_VALUE / 空白 ; } まで

		while(1)
		{
			if(c == -1)
				app_enderr("{ 内が途中で終了しています");

			if(c == ' ' || c == '}')
			{
				if(c == '}' && type == GETSTRING_ITEM_NAME)
					app_enderr("項目に値がありません");
				
				break;
			}
			else if(c == ':' && type == GETSTRING_ITEM_NAME)
				break;
			else if(c == ';' && type != GETSTRING_ARG)
			{
				if(type == GETSTRING_ITEM_NAME)
					app_enderr("項目に値がありません");
			
				break;
			}
			else
				_add_str(str, c);

			c = text_setting_getchar(p);
		}

		//空白スキップ

		if(c == ' ')
			c = text_setting_skip_getchar(p);
	}

	//次の文字判定

	if((type == GETSTRING_ARG && c != '}')
		|| (type == GETSTRING_ITEM_NAME && c != ':')
		|| (type == GETSTRING_ITEM_VALUE && c != ';' && c != '}'))
	{
		app_enderr("{} 内の無効な文字です");
	}

	return c;
}

/* @def 用、{} 内の文字列取得。'}' まで読み込み */

static void _get_raw_string(TextData *p,mStr *str)
{
	int32_t c;

	mStrEmpty(str);

	while(1)
	{
		c = text_get_char(p);

		if(c == -1)
			app_enderr("{ 内が途中で終了しています");

		c &= CHAR_MASK;

		if(c == '}')
			break;
		else if(c & CHAR_F_BSLASH)
		{
			//\+char の場合は元に戻す

			mStrAppendChar(str, '\\');
			_add_str(str, c);
		}	
		else
			//改行も含めてセット
			_add_str(str, c);
	}
}


//=======================
// {} 内の値を取得
//=======================
/* '{' の後、空白がスキップされた状態。
 * '}' まで読み込む。 */


/* 関数を実行 */

static void _run_cmd_func(TextData *p,const CommandDef *pdef,CommandParam *param)
{
	int ret;

	if(!pdef->func) return;
	
	ret = (pdef->func)(p, param);

	if(ret)
	{
		switch(ret)
		{
			case CMDFUNC_RET_STR_INVALID:
				app_enderr2("値が正しくありません", p->str.buf);
				break;
			case CMDFUNC_RET_ITEM_UNDEF:
				app_enderr2("未定義の項目名です", p->str.buf);
				break;
			case CMDFUNC_RET_ITEM_INVALID:
				app_enderr2("値が正しくありません", p->str2.buf);
				break;
		}
	}
}

/* {} から複数項目を取得 */

static void _get_value_multi(TextData *p,int32_t c,const CommandDef *pdef,CommandParam *param)
{
	//開始

	if(pdef->flags & CMDFLAG_MULTI_PROC)
		_run_cmd_func(p, pdef, param);

	//

	param->multi_proc = MULTI_PROC_ITEM;

	while(c != '}')
	{
		//':' までの項目名
		
		_get_string(p, GETSTRING_ITEM_NAME, c, &p->str);

		if(mStrIsEmpty(&p->str))
			app_enderr("項目名が空です");

		//空白スキップ

		c = text_setting_skip_getchar(p);

		//; } までの値

		c = _get_string(p, GETSTRING_ITEM_VALUE, c, &p->str2);

		//ここで実行しないと、エラー時の位置が正しくない
		//str = 項目名、str2 = 値

		_run_cmd_func(p, pdef, param);

		//; なら、空白スキップ

		if(c == ';')
			c = text_setting_skip_getchar(p);
	}

	//終了

	if(pdef->flags & CMDFLAG_MULTI_PROC)
	{
		param->multi_proc = MULTI_PROC_END;

		_run_cmd_func(p, pdef, param);
	}
}


//=======================
// main
//=======================


/* 定義データからコマンド名検索 */

static const CommandDef *_search_command(char *name,mlkbool in_layout)
{
	const CommandDef *p;
	int len,num,i;
	uint8_t dat[32];
	const uint32_t *ps,*pcmp;

	len = strlen(name);
	num = (len + 4) / 4 - 1; //4byte目以降の4byte単位の数

	memset(dat, 0, 32);
	dat[0] = len;
	memcpy(dat + 1, name, len);

	//

	p = (in_layout)? g_command_def_layout: g_command_def;

	for( ; p->val != CMDVAL_END; p++)
	{
		ps = (const uint32_t *)dat;
		pcmp = (const uint32_t *)(g_cmpstring + p->name_pos);

		if(*ps == *pcmp)
		{
			ps++;
			pcmp++;
			
			for(i = num; i && *ps == *pcmp; i--, ps++, pcmp++);

			if(!i) return p;
		}
	}

	return NULL;
}

/** 各設定コマンドを処理して、'} ;' まで読み込む or グループ時はそのまま
 *
 * c: '{' or ';'
 * cmdname: 先頭文字は常に英小文字
 * defname: なければ空文字列。末尾に '*' がある場合あり
 * return: 1 で @start-body */

int text_proc_setting_command(TextData *p,int32_t c,char *cmdname,char *defname)
{
	const CommandDef *pdef;
	CommandParam param;

	//定義データから検索

	pdef = _search_command(cmdname, (p->curlayout != 0));
	if(!pdef)
		app_enderr2("未定義のコマンド、または、@layout の内外で使えないコマンドです", cmdname);

	//{} が必要か

	if(pdef->val == CMDVAL_NONE)
	{
		if(c != ';')
			app_enderr2("{} の引数は必要ありません", cmdname);
	}
	else
	{
		if(c != '{')
			app_enderr2("{} が必要です", cmdname);
	}

	//定義名が必要

	if((pdef->flags & CMDFLAG_HAVE_DEFNAME) && *defname == 0)
		app_enderr2("[] で定義名の指定が必要です", cmdname);

	//@start-body

	if(pdef->flags & CMDFLAG_START_BODY)
		return 1;

	//---- param

	param.defname = defname;
	param.multi_proc = 0;
	param.def_param = pdef->param;

	//---- 処理

	if(pdef->val == CMDVAL_NONE || pdef->val == CMDVAL_GROUP)
	{
		//引数なし/グループ

		_run_cmd_func(p, pdef, &param);
	}
	else if(pdef->val == CMDVAL_RAW)
	{
		//{} 内すべてが文字列対象 (@def)

		_get_raw_string(p, &p->str);

		_run_cmd_func(p, pdef, &param);
	}
	else
	{
		//---- 文字列 or 複数項目
		
		//'{' の後の空白スキップ

		c = text_setting_skip_getchar(p);

		//

		if(pdef->val == CMDVAL_MULTI)
			_get_value_multi(p, c, pdef, &param);
		else
		{
			//1つの文字列
			
			_get_string(p, GETSTRING_ARG, c, &p->str);

			_run_cmd_func(p, pdef, &param);
		}
	}

	return 0;
}


//=======================
// 値取得サブ
//=======================


/* (multi) フォントの name,size の項目値を取得 */

static int _get_font_spec(TextData *p,FontSpec *dst)
{
	char *name,*val;

	name = p->str.buf;
	val = p->str2.buf;

	if(text_cmp(name, "name"))
		text_get_string_fontname(p, val, dst);
	else if(text_cmp(name, "size"))
		text_get_string_fontsize(p, val, dst);
	else
		return CMDFUNC_RET_ITEM_UNDEF;

	return 0;
}

/* @paper:size の値を取得
 *
 * return: 1 で無効な値 */

static int _get_string_papersize(TextData *p,char *pc)
{
	int w,h;

	w = h = -1;

	if(text_cmpi(pc, "a5"))
		w = 148, h = 210;
	else if(text_cmpi(pc, "a6"))
		w = 105, h = 148;
	else if(text_cmpi(pc, "b5"))
		w = 182, h = 257;
	else if(text_cmpi(pc, "b6"))
		w = 128, h = 182;
	else if(*pc >= '0' && *pc <= '9')
	{
		//WxH<mm>

		w = strtol(pc, &pc, 10);

		if(*pc == 'x')
			h = strtol(pc + 1, &pc, 10);

		if(w >= 1000 || h >= 1000) return 1;
	}

	if(w <= 0 || h <= 0) return 1;

	g_set->paper_w = w;
	g_set->paper_h = h;

	return 0;
}


//=======================
// 各値処理
//=======================


/* @output:jpeg */

static void _get_output_jpeg(TextData *p,const char *name,const char *val)
{
	int n;

	if(text_cmp(name, "quality"))
		g_app->jpeg_quality = text_get_string_int(p, val, 0, 100);
	else if(text_cmp(name, "sampling"))
	{
		n = text_get_string_int(p, val, 0, 999);

		if(n != 444 && n != 422 && n != 420)
			app_enderr2("値が正しくありません", val);

		g_app->jpeg_sampling = n;
	}
	else
		app_enderr2("未定義の項目名です", name);
}

/* @output */

int _cmdval_output(TextData *p,CommandParam *param)
{
	char *val;
	int pos;

	pos = text_search_item_name(p->str.buf, g_comitem_output);
	if(pos < 0) return CMDFUNC_RET_ITEM_UNDEF;
	
	val = p->str2.buf;

	switch(pos)
	{
		//prefix
		case CMPSTR_POS_PREFIX:
			mStrCopy(&g_set->output.str_prefix, &p->str2);
			break;
		//format
		case CMPSTR_POS_FORMAT:
			g_set->output.format = text_get_string_index(p, val, APP_FORMAT_LIST_STR) + 1;
			break;
		//dpi
		case CMPSTR_POS_DPI:
			g_set->output.dpi = text_get_string_int(p, val, 1, 10000);
			g_set->output.width = 0;
			g_set->output.height = 0;
			break;
		//width
		case CMPSTR_POS_WIDTH:
			g_set->output.width = text_get_string_int(p, val, 1, 100000);
			g_set->output.dpi = 0;
			g_set->output.height = 0;
			break;
		//height
		case CMPSTR_POS_HEIGHT:
			g_set->output.height = text_get_string_int(p, val, 1, 100000);
			g_set->output.dpi = 0;
			g_set->output.width = 0;
			break;
		//aa
		case CMPSTR_POS_AA:
			g_set->output.antialias = text_get_string_yesno(p, val) + 1;
			break;
		//hinting
		case CMPSTR_POS_HINTING:
			g_set->output.hinting = text_get_string_index(p, val, "off;normal;light;mono");
			break;
		//grid
		case CMPSTR_POS_GRID:
			g_set->output.grid = text_get_string_yesno(p, val);
			break;
		//grid-color
		case CMPSTR_POS_GRID_COLOR:
			g_set->output.grid_col = text_get_string_rgb(p, val);
			break;
		//pdf-fixed
		case CMPSTR_POS_PDF_FIXED:
			g_set->output.pdf_fixed = text_get_string_yesno(p, val);
			break;
		//jpeg
		case CMPSTR_POS_JPEG:
			text_get_string_comma_item(p, &p->str2, _get_output_jpeg);
			break;
	}

	return 0;
}

/* @paper */

int _cmdval_paper(TextData *p,CommandParam *param)
{
	char *val;
	int pos;

	pos = text_search_item_name(p->str.buf, g_comitem_paper);
	if(pos < 0) return CMDFUNC_RET_ITEM_UNDEF;

	val = p->str2.buf;

	switch(pos)
	{
		//size
		case CMPSTR_POS_SIZE:
			if(_get_string_papersize(p, val))
				return CMDFUNC_RET_ITEM_INVALID;
			break;
		//bleed
		case CMPSTR_POS_BLEED:
			g_set->bleed = text_get_string_int(p, val, 0, 255);
			break;
		//page
		case CMPSTR_POS_PAGE:
			g_set->page_type = text_get_string_index(p, val, "two;one;two-1");
			break;
	}

	return 0;
}

/* @pdf */

int _cmdval_pdf(TextData *p,CommandParam *param)
{
	char *val;
	int pos;

	pos = text_search_item_name(p->str.buf, g_comitem_pdf);
	if(pos < 0) return CMDFUNC_RET_ITEM_UNDEF;

	val = p->str2.buf;

	switch(pos)
	{
		//title
		case CMPSTR_POS_TITLE:
			mStrCopy(&g_set->pdf.str_title, &p->str2);
			break;
		//author
		case CMPSTR_POS_AUTHOR:
			mStrCopy(&g_set->pdf.str_author, &p->str2);
			break;
		//subject
		case CMPSTR_POS_SUBJECT:
			mStrCopy(&g_set->pdf.str_subject, &p->str2);
			break;
		//no-unicode
		case CMPSTR_POS_NO_UNICODE:
			g_set->pdf.no_unicode = text_get_string_yesno(p, val);
			break;
		//outline
		case CMPSTR_POS_OUTLINE:
			mStrCopy(&g_set->pdf.str_outline, &p->str2);
			break;
	}

	return 0;
}

/* @include */

int _cmdval_include(TextData *p,CommandParam *param)
{
	text_open_input(p, p->str.buf, NULL);

	return 0;
}

/* @def */

int _cmdval_def(TextData *p,CommandParam *param)
{
	DefItem *pi;
	int len;

	//すでに存在する場合、削除

	pi = text_search_defitem(p, param->defname);
	if(pi)
		mListDelete(&p->list_def, MLISTITEM(pi));

	//追加

	len = p->str.len + 1;

	pi = (DefItem *)mListAppendNew(&p->list_def, sizeof(DefItem) + len);
	if(!pi) app_enderr_alloc();

	strcpy(pi->name, param->defname);

	pi->hash = mCalcStringHash(pi->name);

	memcpy(pi->str, p->str.buf, len);

	return 0;
}

/* @font */

typedef struct
{
	char *filename; //省略で base と同じ
	int index,
		flags,
		dakuten[4]; //horz-x,y:vert-x,y
	double size;
}_font_data;

static void _font_free_data(void *data)
{
	mFree(((_font_data *)data)->filename);
}

int _cmdval_font(TextData *p,CommandParam *param)
{
	_font_data *dat = (_font_data *)p->data;
	char *name,*val;
	const char *err;
	int n[2];

	if(param->multi_proc == MULTI_PROC_FIRST)
	{
		//データを作成

		dat = (_font_data *)text_alloc_data(p, sizeof(_font_data), _font_free_data);

		dat->size = 8.5;
		dat->dakuten[0] = 700;
		dat->dakuten[2] = 700; //0.7
	}
	else if(param->multi_proc == MULTI_PROC_END)
	{
		//終了

		err = setting_add_font(param->defname, dat->filename, dat->index, dat->size, dat->dakuten, dat->flags);
		if(err) app_enderr(err);

		text_free_data(p);
	}
	else
	{
		//項目
		
		name = p->str.buf;
		val = p->str2.buf;

		if(text_cmp(name, "file"))
		{
			if(!dat->filename)
				dat->filename = text_get_textbase_filename(val);
		}
		else if(text_cmp(name, "index"))
			dat->index = atoi(val);
		else if(text_cmp(name, "size"))
			dat->size = text_get_string_fontsize(p, val, NULL);
		else if(text_cmp(name, "dakuten-horz"))
		{
			n[0] = n[1] = 0;
			
			text_get_string_comma_double_to_int(p, val, 2, n);

			dat->dakuten[0] = n[0];
			dat->dakuten[1] = n[1];
		}
		else if(text_cmp(name, "dakuten-vert"))
		{
			n[0] = n[1] = 0;
			
			text_get_string_comma_double_to_int(p, val, 2, n);

			dat->dakuten[2] = n[0];
			dat->dakuten[3] = n[1];
		}
		else if(text_cmp(name, "horz-half"))
			dat->flags |= FONTITEM_FLAGS_HORZ_HALF;
		else
			return CMDFUNC_RET_ITEM_UNDEF;
	}

	return 0;
}

/* @kenten[] */

int _cmdval_kenten(TextData *p,CommandParam *param)
{
	KentenItem *item;
	char *name,*val;

	if(param->multi_proc == MULTI_PROC_FIRST)
	{
		//データを作成 or 取得
		// :存在するものは上書き

		param->ptr = setting_add_kenten(param->defname);
	}
	else if(param->multi_proc == MULTI_PROC_ITEM)
	{
		item = (KentenItem *)param->ptr;
		name = p->str.buf;
		val = p->str2.buf;

		if(text_cmp(name, "font"))
			text_get_string_fontname(p, val, &item->font);
		else if(text_cmp(name, "fontsize"))
			text_get_string_fontsize(p, val, &item->font);
		else if(text_cmp(name, "char"))
			item->code = text_get_string_char(p, val);
		else if(text_cmp(name, "pos"))
			item->pos = lround(strtod(val, NULL) * 1000);
	}

	return 0;
}

/* @fullwidth-set[] */

int _cmdval_fullwidth_set(TextData *p,CommandParam *param)
{
	const char *err;

	err = setting_add_fullwidth(param->defname, p->str.buf);
	if(err)
		app_enderr(err);

	return 0;
}

/* @image-type */

int _cmdval_image_type(TextData *p,CommandParam *param)
{
	int n;

	n = text_get_string_index(p, NULL, "auto;mono;gray");

	if(n == 2) n = 8; //gray

	g_set->image_bits = n;

	return 0;
}

/* @page-title, @page-number の共通項目
 *
 * return: 1 で処理した */

static int _common_title_number(TextData *p,char *name,char *val,TitleNumberSet *dst)
{
	int pos;

	pos = text_search_item_name(name, g_comitem_page_title_number);
	if(pos < 0) return 0;

	switch(pos)
	{
		//font
		case CMPSTR_POS_FONT:
			text_get_string_fontname(p, val, &dst->font);
			break;
		//fontsize
		case CMPSTR_POS_FONTSIZE:
			text_get_string_fontsize(p, val, &dst->font);
			break;
		//pos
		case CMPSTR_POS_POS:
			dst->pos = text_get_string_index(p, val, "head;foot");
			break;
		//vspace
		case CMPSTR_POS_VSPACE:
			text_get_string_paperpos(p, val, &dst->vspace);
			break;
		//hspace
		case CMPSTR_POS_HSPACE:
			text_get_string_paperpos(p, val, &dst->hspace);
			break;
		//no-page
		case CMPSTR_POS_NO_PAGE:
			dst->no_page = text_get_string_flags(p, val, "all;even;blank;image");
			break;
	}

	return 1;
}

/* @page-title */

int _cmdval_page_title(TextData *p,CommandParam *param)
{
	char *name,*val;

	name = p->str.buf;
	val = p->str2.buf;

	if(_common_title_number(p, name, val, &g_set->title))
		return 0;
	else if(text_cmp(name, "numspace"))
		text_get_string_paperpos(p, val, &g_set->title_number_space);
	else
		return CMDFUNC_RET_ITEM_UNDEF;

	return 0;
}

/* @page-number */

int _cmdval_page_number(TextData *p,CommandParam *param)
{
	char *name,*val;

	name = p->str.buf;
	val = p->str2.buf;

	if(_common_title_number(p, name, val, &g_set->number))
		return 0;
	else if(text_cmp(name, "hide-page"))
		g_set->number_hide_page = text_get_string_flags(p, val, "all;even;blank;image");
	else if(text_cmp(name, "hide-x"))
		g_set->number_hide_x = text_get_string_int(p, val, 0, 1000);
	else if(text_cmp(name, "hide-y"))
		g_set->number_hide_y = text_get_string_int(p, val, 0, 1000);
	else if(text_cmp(name, "hide-color"))
		g_set->number_hide_col = text_get_string_rgb(p, val);
	else
		return CMDFUNC_RET_ITEM_UNDEF;

	return 0;
}

//------------ レイアウト

/* 終了 */

static void _func_endlayout(TextData *p)
{
	p->curlayout = NULL;
}

/* @layout */

int _cmdval_layout(TextData *p,CommandParam *param)
{
	const char *err;
	char *sp;
	LayoutItem *pi;

	mStrSetText(&p->str, param->defname);

	sp = strchr(p->str.buf, ':');
	if(sp) *sp = 0;

	//すでに存在するレイアウトなら、設定値を上書き

	err = setting_add_layout(p->str.buf, (sp)? sp + 1: NULL, &pi);
	if(err) app_enderr(err);

	//設定中

	p->curlayout = pi;
	p->group_func = _func_endlayout;

	return 0;
}

/* @mode */

int _cmdval_mode(TextData *p,CommandParam *param)
{
	p->curlayout->mode = text_get_string_index(p, NULL, "vert;vert2;horz");

	return 0;
}

/* @space */

int _cmdval_layout_space(TextData *p,CommandParam *param)
{
	char *name;
	PaperPos *dst;

	name = p->str.buf;

	if(text_cmp(name, "head"))
		dst = &p->curlayout->sp_head;
	else if(text_cmp(name, "foot"))
		dst = &p->curlayout->sp_foot;
	else if(text_cmp(name, "gutter"))
		dst = &p->curlayout->sp_gutter;
	else if(text_cmp(name, "column"))
		dst = &p->curlayout->sp_column;
	else
		return CMDFUNC_RET_ITEM_UNDEF;

	text_get_string_paperpos(p, p->str2.buf, dst);

	return 0;
}

/* @font */

int _cmdval_layout_font(TextData *p,CommandParam *param)
{
	return _get_font_spec(p, &p->curlayout->basefont);
}

/* @line */

int _cmdval_line(TextData *p,CommandParam *param)
{
	char *name,*val;

	name = p->str.buf;
	val = p->str2.buf;

	if(text_cmp(name, "len"))
		text_get_string_paperpos(p, val, &p->curlayout->line_len);
	else if(text_cmp(name, "num"))
		p->curlayout->line_num = text_get_string_int(p, val, 1, 1000);
	else if(text_cmp(name, "feed"))
		text_get_string_paperpos(p, val, &p->curlayout->line_feed);
	else
		return CMDFUNC_RET_ITEM_UNDEF;

	return 0;
}

/* @align */

int _cmdval_layout_align(TextData *p,CommandParam *param)
{
	p->curlayout->line_align = text_get_string_index(p, NULL, "top;center;bottom");

	return 0;
}

/* @body-align */

int _cmdval_layout_bodyalign(TextData *p,CommandParam *param)
{
	p->curlayout->body_align = text_get_string_index(p, NULL, "top;center;bottom");

	return 0;
}

/* @indent */

int _cmdval_indent(TextData *p,CommandParam *param)
{
	char *name,*val;
	int n[2],cnt;

	name = p->str.buf;
	val = p->str2.buf;

	if(text_cmp(name, "head"))
	{
		cnt = text_get_string_int_comma(p, val, 2, 0, 255, n);
		if(cnt)
		{
			if(cnt == 1) n[1] = n[0];

			p->curlayout->indent_head_first = n[0];
			p->curlayout->indent_head_wrap = n[1];
		}
	}
	else if(text_cmp(name, "foot"))
		p->curlayout->indent_foot = text_get_string_int(p, val, 0, 255);
	else
		return CMDFUNC_RET_ITEM_UNDEF;

	return 0;
}

/* @index */

int _cmdval_index(TextData *p,CommandParam *param)
{
	char *name,*val;

	name = p->str.buf;
	val = p->str2.buf;

	if(text_cmp(name, "space"))
		p->curlayout->index_sp_char = text_get_string_char(p, val);
	else if(text_cmp(name, "number"))
		p->curlayout->index_number = text_get_string_index(p, val, "num2;num3;num;kanji");
	else
		return CMDFUNC_RET_ITEM_UNDEF;

	return 0;
}

/* @dash-width */

int _cmdval_dash_width(TextData *p,CommandParam *param)
{
	int n;

	n = (int)(strtod(p->str.buf, NULL) * 1000 + 0.5);

	if(n < 0) n = 0;
	else if(n > 1000) n = 1000;

	p->curlayout->dash_width = n;

	return 0;
}

/* @text-color */

int _cmdval_text_color(TextData *p,CommandParam *param)
{
	p->curlayout->textcol = text_get_string_rgb(p, p->str.buf);

	return 0;
}

/* @background */

int _cmdval_background(TextData *p,CommandParam *param)
{
	char *name,*val;

	name = p->str.buf;
	val = p->str2.buf;

	if(text_cmp(name, "color"))
		p->curlayout->bkgndcol = text_get_string_rgb(p, val);
	else if(text_cmp(name, "image"))
		p->curlayout->bkgndimg.item = text_add_imagefile(val);
	else if(text_cmp(name, "image-l"))
		p->curlayout->bkgnd_img_left = text_add_imagefile(val);
	else if(text_cmp(name, "image-r"))
		p->curlayout->bkgnd_img_right = text_add_imagefile(val);
	else if(text_cmp(name, "size"))
		text_get_string_imgsize(p, val, &p->curlayout->bkgndimg);
	else if(text_cmp(name, "pos"))
	{
		if(text_get_string_imgpos(p, val, &p->curlayout->bkgndimg))
			return CMDFUNC_RET_ITEM_INVALID;
	}
	else if(text_cmp(name, "no-page"))
		p->curlayout->bkgndimg_no_page = text_get_string_flags(p, val, "blank;image");
	else
		return CMDFUNC_RET_ITEM_UNDEF;

	return 0;
}

/* @caption1,2,3 */

int _cmdval_caption(TextData *p,CommandParam *param)
{
	int pos;
	char *val;
	CaptionLayout *dst;

	pos = text_search_item_name(p->str.buf, g_comitem_caption);
	if(pos < 0) return CMDFUNC_RET_ITEM_UNDEF;

	dst = p->curlayout->caption + param->def_param;
	val = p->str2.buf;

	switch(pos)
	{
		//font
		case CMPSTR_POS_FONT:
			text_get_string_fontname(p, val, &dst->font);
			break;
		//fontsize
		case CMPSTR_POS_FONTSIZE:
			text_get_string_fontsize(p, val, &dst->font);
			break;
		//indent
		case CMPSTR_POS_INDENT:
			dst->indent = text_get_string_int(p, val, 0, 255);
			break;
		//width
		case CMPSTR_POS_WIDTH:
			dst->width = text_get_string_int(p, val, 1, 255);
			break;
		//before
		case CMPSTR_POS_BEFORE:
			dst->before = text_get_string_int(p, val, 0, 255);
			break;
		//after
		case CMPSTR_POS_AFTER:
			dst->after = text_get_string_int(p, val, 0, 255);
			break;
		//align
		case CMPSTR_POS_ALIGN:
			dst->align = text_get_string_index(p, val, "center;line");
			break;
		//sep
		case CMPSTR_POS_SEP:
			dst->sep = text_get_string_yesno(p, val);
			break;
	}

	return 0;
}

/* @ruby */

int _cmdval_ruby(TextData *p,CommandParam *param)
{
	char *name,*val;
	LayoutItem *dst = p->curlayout;

	name = p->str.buf;
	val = p->str2.buf;

	if(text_cmp(name, "font"))
		text_get_string_fontname(p, val, &dst->ruby_font);
	else if(text_cmp(name, "fontsize"))
		text_get_string_fontsize(p, val, &dst->ruby_font);
	else if(text_cmp(name, "small-kana"))
		dst->ruby_small_kana = text_get_string_index(p, val, "big;raw");
	else if(text_cmp(name, "align"))
		dst->ruby_align = text_get_string_index(p, val, "top;center");
	else
		return CMDFUNC_RET_ITEM_UNDEF;

	return 0;
}

/* @process */

int _cmdval_process(TextData *p,CommandParam *param)
{
	char *name,*val;

	name = p->str.buf;
	val = p->str2.buf;

	if(text_cmp(name, "hang"))
		p->curlayout->hang_flag = text_get_string_yesno(p, val);
	else if(text_cmp(name, "reduce"))
		p->curlayout->reduce_flag = text_get_string_yesno(p, val);
	else if(text_cmp(name, "break"))
		p->curlayout->break_type = text_get_string_index(p, val, "book;normal");
	else if(text_cmp(name, "indent-kakko"))
		p->curlayout->indent_kakko = text_get_string_index(p, val, "f2w0;f1w0");
	else if(text_cmp(name, "fullwidth"))
	{
		if(!(*val))
			p->curlayout->item_fullwidth = NULL;
		else
		{
			p->curlayout->item_fullwidth = setting_search_fullwidth(val);

			if(!p->curlayout->item_fullwidth)
				app_enderr2("指定名の @fullwidth-set がありません", val);
		}
	}
	else
		return CMDFUNC_RET_ITEM_UNDEF;

	return 0;
}

/* @gsub */

int _cmdval_gsub(TextData *p,CommandParam *param)
{
	p->curlayout->gsub = text_get_string_flags(p, NULL, "ruby;nlck;hkna;vkna");

	return 0;
}
