/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * main
 *********************************/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <signal.h>

#include <mlk.h>
#include <mlk_list.h>
#include <mlk_str.h>
#include <mlk_string.h>
#include <mlk_charset.h>
#include <mlk_argparse.h>
#include <mlk_util.h>
#include <mlk_dir.h>
#include <mlk_file.h>
#include <mlk_rand.h>

#include "app.h"
#include "setting.h"
#include "image.h"
#include "font/font.h"
#include "font/fontio.h"


//-----------------

AppData *g_app;

#define VERSION_TEXT "txtnovel ver 1.0.2\nCopyright (c) 2022-2023 Azel\n\nThis software is released under the MIT License."

void app_read_text(void);
void app_output_layout(void);
void app_output_file(void);

void _app_getoption(int argc,char **argv);

//-----------------


/* 目次アイテム破棄 */

static void _destroy_indexitem(mList *list,mListItem *item)
{
	mFree(((IndexItem *)item)->title);
}

/* 画像アイテム破棄 */

static void _destroy_imageitem(mList *list,mListItem *item)
{
	image_free(((ImageItem *)item)->img);
}

/* 作業ディレクトリ削除 */

static void _delete_tmpdir(void)
{
	mDir *dir;
	mStr str = MSTR_INIT;

	mStrSetLen(&g_app->str_tmpdir, g_app->tmpdir_pos);

	//残っているファイルを削除

	dir = mDirOpen(g_app->str_tmpdir.buf);
	if(!dir) return;

	while(mDirNext(dir))
	{
		if(!mDirIsSpecName(dir))
		{
			mDirGetFilename_str(dir, &str, TRUE);
			mDeleteFile(str.buf);
		}
	}

	mDirClose(dir);

	mStrFree(&str);

	//ディレクトリを削除

	mDeleteDir(g_app->str_tmpdir.buf);
}

/** 解放処理 */

void app_free(int err)
{
	AppData *p = g_app;

	if(!p) return;

	if(p->free_proc)
		(p->free_proc)(p->proc_ptr, err);

	setting_free();

	//

	if(mStrIsnotEmpty(&p->str_tmpdir))
		_delete_tmpdir();

	mStrFree(&p->str_input);
	mStrFree(&p->str_output);
	mStrFree(&p->str_tmpdir);
	mStrFree(&p->str_cachedir);

	mListDeleteAll(&p->list_index);
	mListDeleteAll(&p->list_image);

	mFree(p->buf_body);

	mFree(p);

	//FreeType 終了
	// :画像出力時に初期化されている

	freetype_finish();
}


//============== 初期化


/* 作業ディレクトリ作成 */

static void _create_tmpdir(void)
{
	char *name;
	mlkerr ret;

	name = mGetProcessName();

	//"/tmp" -> カレントディレクトリの順で試す

	mStrSetFormat(&g_app->str_tmpdir, "/tmp/txtnovel-%s", name);

	ret = mCreateDir(g_app->str_tmpdir.buf, 0700);

	if(ret && ret != MLKERR_EXIST)
	{
		mStrSetFormat(&g_app->str_tmpdir, "tmp_txtnovel%s", name);

		ret = mCreateDir(g_app->str_tmpdir.buf, 0700);
		
		if(ret && ret != MLKERR_EXIST)
			app_enderr2("作業ディレクトリが作成できません", g_app->str_tmpdir.buf);
	}

	//

	mFree(name);

	mStrPathAppendDirSep(&g_app->str_tmpdir);

	g_app->tmpdir_pos = g_app->str_tmpdir.len;
}

/* シグナルハンドラ */

static void _signal_handle(int sig)
{
	g_app->signal_flag = 1;
}

/* 初期化 */

static void _app_init(void)
{
	struct sigaction sa;
	char *pc;

	mInitLocale();

	mRandXor_init(NULL, time(NULL));

	//AppData

	g_app = (AppData *)mMalloc0(sizeof(AppData));
	if(!g_app) return;

	g_app->list_index.item_destroy = _destroy_indexitem;
	g_app->list_image.item_destroy = _destroy_imageitem;

	g_app->jpeg_quality = 86;
	g_app->jpeg_sampling = 420;

	//作業ディレクトリ

	_create_tmpdir();

	//環境変数

	pc = getenv("TXTNOVEL_CACHE_DIR");
	if(pc)
		mStrSetText(&g_app->str_cachedir, pc);

	//

	setting_new();

	//シグナル

	mMemset0(&sa, sizeof(struct sigaction));
	sa.sa_handler = _signal_handle;

	sigaction(SIGINT, &sa, NULL);
	sigaction(SIGKILL, &sa, NULL);
}


//=================


/* メイン処理 */

static void _proc_main(void)
{
	//テキストから本文データ作成

	app_read_text();

	//レイアウトしてファイルに出力

	app_output_layout();

	//本文解放
	
	mFree(g_app->buf_body);
	g_app->buf_body = NULL;

	//PDF/画像出力

	app_output_file();
}

/* フォント名一覧 */

static void _put_fontnames(void)
{
	int ret;

	ret = fontio_put_names(g_app->str_input.buf);
	if(ret) app_enderrno(ret, g_app->str_input.buf);
}

/** main */

int main(int argc,char **argv)
{
	_app_init();

	_app_getoption(argc, argv);

	if(g_app->flags & APP_FLAGS_FONTNAMES)
		_put_fontnames();
	else
		_proc_main();

	app_free(0);

	return 0;
}


//===========================
// オプション
//===========================


/* dpi,width,height の数値を取得 */

static int _get_optval_imagesize(mArgParse *p,char *arg)
{
	int n;

	n = strtol(arg, NULL, 10);

	if(n <= 0)
	{
		printf("--%s: invalid value\n", p->curopt->longopt);
		app_exit(1);
		return 0;
	}

	return n;
}


/* --format */

static void _opt_format(mArgParse *p,char *arg)
{
	g_app->format = mStringGetSplitTextIndex(arg, -1, APP_FORMAT_LIST_STR, ';', TRUE);

	if(g_app->format == -1)
	{
		printf("--format: invalid value\n");
		app_exit(1);
	}

	g_app->format++;
}

/* --prefix */

static void _opt_prefix(mArgParse *p,char *arg)
{
	mStrSetText_locale(&g_app->str_output, arg, -1);
}

/* --dpi */

static void _opt_dpi(mArgParse *p,char *arg)
{
	g_app->dpi = _get_optval_imagesize(p, arg);
	g_app->width = g_app->height = 0;
}

/* --width */

static void _opt_width(mArgParse *p,char *arg)
{
	g_app->width = _get_optval_imagesize(p, arg);
	g_app->dpi = g_app->height = 0;
}

/* --height */

static void _opt_height(mArgParse *p,char *arg)
{
	g_app->height = _get_optval_imagesize(p, arg);
	g_app->dpi = g_app->width = 0;
}

/* --aa */

static void _opt_antialias(mArgParse *p,char *arg)
{
	g_app->antialias = 2;
}

/* --no-aa */

static void _opt_no_antialias(mArgParse *p,char *arg)
{
	g_app->antialias = 1;
}

/* --grid */

static void _opt_grid(mArgParse *p,char *arg)
{
	g_app->flags |= APP_FLAGS_DRAW_GRID;
}

/* --no-pdfsep */

static void _opt_no_pdfsep(mArgParse *p,char *arg)
{
	g_app->flags |= APP_FLAGS_NO_PDFSEP;
}

/* --no-cache */

static void _opt_no_cache(mArgParse *p,char *arg)
{
	g_app->flags |= APP_FLAGS_NO_CACHE;
}

/* --fontnames */

static void _opt_fontnames(mArgParse *p,char *arg)
{
	g_app->flags |= APP_FLAGS_FONTNAMES;
}

/* --version */

static void _opt_version(mArgParse *p,char *arg)
{
	puts(VERSION_TEXT);
	app_exit(0);
}

/* ヘルプ表示 */

static void _put_help(const char *exename)
{
	mPutUTF8_stdout(
"テキストファイルから組版して、PDF または画像で出力します。\n"
"詳しい使い方は、" APP_DOCDIR "/manual/index.html\n\n");

	printf("Usage:\n %s [option] <textfile(UTF-8)>\n %s --fontnames <fontfile>\n\n",
		exename, exename);

	mPutUTF8_stdout(
"Options:\n"
"  -p,--prefix [STR]  出力ファイルの先頭パス (default=out)\n"
"                     この後に、ページ番号と拡張子が追加されます。\n"
"  -f,--format [STR]  出力フォーマット (default=pdf)\n"
"                     pdf,bmp,png,jpeg,psd\n"
"  -d,--dpi [INT]     DPI\n"
"  -w,--width [INT]   幅 (px)\n"
"  -h,--height [INT]  高さ (px)\n"
"       (default=600dpi)\n"
"       画像出力時のサイズ。いずれか1つだけ指定してください。\n"
"  -a,--aa            (画像出力時) アンチエイリアスを ON\n"
"  -A,--no-aa         (画像出力時) アンチエイリアスを OFF\n"
"       (default: 300 dpi 以上で ON)\n"
"  -g,--grid          (画像出力時) グリッド描画を常に ON\n"
"  -s,--no-pdfsep     (PDF 出力時) PDF 分割を行わない\n"
"  -n,--no-cache      フォントキャッシュのファイルを作成しない\n"
"  --fontnames        フォントファイルの、フォント名一覧を表示\n"
"  -V,--version       バージョン情報\n"
"  --help             ヘルプ\n"
);

	app_exit(0);
}

/* --help */

static void _opt_help(mArgParse *p,char *arg)
{
	_put_help(p->argv[0]);
}


static mArgParseOpt g_opts[] = {
 {"prefix",'p',1, _opt_prefix},
 {"format",'f',1, _opt_format},
 {"dpi",'d',1, _opt_dpi},
 {"width",'w',1, _opt_width},
 {"height",'h',1, _opt_height},
 {"aa",'a',0, _opt_antialias},
 {"no-aa",'A',0, _opt_no_antialias},
 {"grid",'g',0, _opt_grid},
 {"no-pdfsep",'s',0, _opt_no_pdfsep},
 {"no-cache",'n',0, _opt_no_cache},
 {"fontnames",0,0, _opt_fontnames},
 {"version",'V',0, _opt_version},
 {"help",0,0, _opt_help},
 {0,0,0,0}
};

/* オプション取得 */

void _app_getoption(int argc,char **argv)
{
	mArgParse ap;
	int ret;

	mMemset0(&ap, sizeof(mArgParse));

	ap.argc = argc;
	ap.argv = argv;
	ap.opts = g_opts;

	ret = mArgParseRun(&ap);

	//解析エラー

	if(ret == -1)
		app_exit(1);

	//入力ファイル

	if(ret == argc)
		_put_help(argv[0]);

	mStrSetText_locale(&g_app->str_input, argv[ret], -1);
}

