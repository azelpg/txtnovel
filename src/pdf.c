/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * PDF データ
 *********************************/

#include <stdio.h>
#include <string.h>
#include <math.h>

#include <mlk.h>
#include <mlk_str.h>
#include <mlk_list.h>
#include <mlk_zlib.h>
#include <mlk_stdio.h>
#include <mlk_rand.h>
#include <mlk_util.h>

#include "app.h"
#include "setting.h"
#include "pdf.h"
#include "font/font.h"
#include "font/subsetfont.h"

#include "pdf_pv.h"



/** PDFData 解放 */

void pdf_free(PDFData *p)
{
	if(p)
	{
		mZlibFree(p->zlib);

		mStrFree(&p->str_tmp);

		mListDeleteAll(&p->list_obj);
		mListDeleteAll(&p->list_objstrm);
		mListDeleteAll(&p->list_font);
		mListDeleteAll(&p->list_resource);

		if(p->fp) fclose(p->fp);
		if(p->fpstrm) fclose(p->fpstrm);
		if(p->fpcomp) fclose(p->fpcomp);
		if(p->fpconts) fclose(p->fpconts);
		if(p->fpsubset) fclose(p->fpsubset);
		if(p->fpcomstrm) fclose(p->fpcomstrm);
		if(p->fpcomobjstrm) fclose(p->fpcomobjstrm);

		mFree(p->workbuf);
		
		mFree(p);
	}
}

/* 作業ファイルを開く */

static FILE *_open_tmpfile(const char *name)
{
	char *fname;
	FILE *fp;

	fname = app_set_tmpfile(name);

	fp = mFILEopen(fname, "w+b");
	if(!fp)
		app_enderrno(MLKERR_OPEN, fname);

	return fp;
}

/** PDFData 確保
 *
 * paperw,h: 用紙全体のサイズ */

void pdf_new(PDFData **dst,int paperw,int paperh)
{
	PDFData *p;

	*dst = p = (PDFData *)mMalloc0(sizeof(PDFData));
	if(!p) app_enderr_alloc();

	p->start_objno = 1;
	p->paper_fullw = paperw;
	p->paper_fullh = paperh;

	//作業用バッファ

	p->workbuf = (uint8_t *)mMalloc(PDF_WORKBUF_SIZE);
	if(!p->workbuf)
		app_enderr_alloc();

	//作業ファイル

	p->fpstrm = _open_tmpfile(TMPFILENAME_PDF_OBJSTRM);
	p->fpcomp = _open_tmpfile(TMPFILENAME_PDF_COMP);
	p->fpconts = _open_tmpfile(TMPFILENAME_PDF_CONTENTS);
	p->fpcomstrm = _open_tmpfile(TMPFILENAME_PDF_COMMON_STREAM);
	p->fpcomobjstrm = _open_tmpfile(TMPFILENAME_PDF_COMMON_OBJSTM);

	//zlib 初期化

	p->zlib = mZlibEncNew(0, 9, 15, 8, 0);
	if(!p->zlib) app_enderr_alloc();

	mZlibSetIO_stdio(p->zlib, p->fpcomp);
}

/** PDF ファイル共通で出力するデータを処理した後 */

void pdf_end_init(PDFData *p)
{
	p->size_comstrm = ftell(p->fpcomstrm);
	p->size_comobjstrm = ftell(p->fpcomobjstrm);
}


//=========================
// 開く・閉じる
//=========================


/* PDF ファイル開始時、共通データをセット */

static void _startfile_common(PDFData *p)
{
	PDFFontItem *pi;
	PDFObjItem *piobj;
	uint32_t offset;

	offset = ftell(p->fp);

	//データをコピー

	pdf_copy_file(p, p->fpcomstrm, p->fp, p->size_comstrm);
	pdf_copy_file(p, p->fpcomobjstrm, p->fpstrm, p->size_comobjstrm);

	//予約したオブジェクトの追加

	MLK_LIST_FOR(p->list_font, pi, PDFFontItem)
	{
		piobj = pdf_add_object(p, &pi->subset);
		piobj->offset += offset;
		
		piobj = pdf_add_object(p, &pi->cmap);
		if(piobj) piobj->offset += offset;

		pdf_add_object_in_stream(p, &pi->type0_h, FALSE);
		pdf_add_object_in_stream(p, &pi->type0_v, FALSE);
		pdf_add_object_in_stream(p, &pi->cidfont_h, FALSE);
		pdf_add_object_in_stream(p, &pi->cidfont_v, FALSE);
		pdf_add_object_in_stream(p, &pi->desc, FALSE);
		pdf_add_object_in_stream(p, &pi->hmetrics, FALSE);
		pdf_add_object_in_stream(p, &pi->vmetrics, FALSE);
	}
}

/** PDF 出力ファイルを開く */

void pdf_openfile(PDFData *p,const char *filename)
{
	FILE *fp;
	uint8_t b[] = {0xF6,0xE4,0xFC,0xDF,'\n'};

	//ファイル開く

	fp = p->fp = mFILEopen(filename, "wb");
	if(!fp)
		app_enderrno(MLKERR_OPEN, filename);

	//クリア

	p->cur_objno = p->start_objno;
	p->pageobj_num = 0;
	p->objno_outline = 0;

	rewind(p->fpstrm);

	app_image_reset_pdf();

	//ヘッダ

	fputs("%PDF-1.5\n%", fp);

	fwrite(b, 1, 5, fp);

	//共通のデータ (フォント)

	_startfile_common(p);

	//共通のグラフィック状態

	pdf_putadd_graphic_state(p);

	//ルートのページノード
	// :実際のデータは最後に書き込むため、番号を予約

	p->obj_rootpage.objno = p->cur_objno++;
}

/** PDF 全体を終了し、閉じる
 *
 * 現在のページデータはそのまま残して、含まない。
 *
 * page_start,end: ファイル内に含まれるページ番号の範囲 */

void pdf_endfile(PDFData *p,int page_start,int page_end)
{
	if(!p->fp) return;

	//---- 圧縮オブジェクト

	//アウトライン

	pdf_putadd_outline(p, page_start, page_end);

	//ルートのページツリーノード

	pdf_putadd_root_pagenode(p);

	//Info 辞書

	pdf_putadd_info(p);

	//カタログ辞書

	pdf_putadd_catalog(p);

	//残りの圧縮オブジェクトを出力

	pdf_put_objectstrem(p);

	//-----

	//クロスリファレンスストリーム

	pdf_put_crossref_stream(p);

	//トレーラー

	fprintf(p->fp, "startxref\n%u\n%%EOF\n", p->offset_crossref);

	//

	fclose(p->fp);
	p->fp = NULL;

	mListDeleteAll(&p->list_obj);
	mListDeleteAll(&p->list_resource);
}


//=========================
// フォント
//=========================


/* サブセットフォントを作成し、各情報取得 */

static void _create_subset(PDFData *p,FontItem *fontitem,PDFFontDat *dat)
{
	SubsetFontDat subset;
	Font *font;
	char *fname;
	int ret;

	font = fontitem->font;

	//サブセットフォントを出力
	// :出力中は、終端が常にフォントの末尾である必要があるので、
	// :ファイルを開いたままでの使い回しはできない。

	fname = app_set_tmpfile(TMPFILENAME_PDF_SUBSET);

	p->fpsubset = mFILEopen(fname, "wb");
	if(!p->fpsubset) app_enderrno(MLKERR_OPEN, fname);

	subset.unimap = font->unimap;
	subset.gidmap = font->gidmap;

	ret = create_subsetfont(p->fpsubset, fontitem->filename, fontitem->index, &subset);

	fclose(p->fpsubset);
	p->fpsubset = 0;

	if(ret)
		app_enderrno(ret, "サブセットフォントの作成エラー");

	//

	font->subset_glyph_num = subset.out_glyph_num;

	dat->gidmap_outsrc = subset.gidmap_outsrc;

	//メトリクス情報取得

	if(font_get_pdf_metrics(font, subset.gidmap_outsrc, dat->info))
	{
		mFree(subset.gidmap_outsrc);
		app_enderr_alloc();
	}
}

/* フォント名をセット */

static void _create_fontname(mStr *str,const char *name,int no)
{
	char m[16];
	int len,i;

	if(name)
	{
		len = strlen(name);
		if(len > 120) len = 120;
	}
	else
	{
		//フォント名がない場合、適当に作成

		len = snprintf(m, 16, "Font%d", no);
		name = m;
	}

	//タグ名 + フォント名 (最大127文字)

	mStrEmpty(str);

	for(i = 0; i < 6; i++)
		mStrAppendChar(str, mRandXor_getIntRange(NULL, 'A', 'Z'));

	mStrAppendChar(str, '+');
	mStrAppendText_len(str, name, len);
}

/** フォントを追加
 *
 * PDF 分割時、すべてのフォントは共通で同じデータになるので、
 * すべてのフォントのストリームと圧縮オブジェクトはあらかじめ別ファイルに作成しておく。 */

void pdf_add_font(PDFData *p,FontItem *fontitem,FontSubsetInfo *info)
{
	PDFFontDat dat;
	int err = 1;

	mMemset0(&dat, sizeof(PDFFontDat));

	dat.font = fontitem->font;
	dat.info = info;

	//サブセット作成

	_create_subset(p, fontitem, &dat);

	//アイテム作成

	dat.item = pdf_add_fontitem(p, &dat);
	if(!dat.item) goto ERR;

	//フォント名

	_create_fontname(&p->str_tmp, dat.font->postname, dat.item->subset.objno);

	dat.fontname = p->str_tmp.buf;

	//出力

	pdf_put_fontinfo(p, &dat);

	//

	err = 0;
ERR:
	mFree(info->buf_horz);
	mFree(info->buf_vert);
	mFree(dat.gidmap_outsrc);

	if(err) app_enderr_alloc();
}


//=========================
// ページ
//=========================


/* リソース辞書の追加
 *
 * return: オブジェクト番号 */

static int _add_resource_dict(PDFData *p)
{
	PDFPageState *st = &p->pagest;
	mStr *str = &p->str_tmp;
	PDFResourceItem *pi;
	PDFObjstrmItem *piobj;
	PDFFontItem *pift;
	ImageItem *piimg;
	uint32_t hash;
	int n;

	//----- 文字列作成

	mStrEmpty(str);

	mStrSetFormat(str, "<</ExtGState<</GS1 %d 0 R>>", p->objno_gs);

	//フォント

	if(st->has_font)
	{
		mStrAppendText(str, "/Font<<");
	
		MLK_LIST_FOR(p->list_font, pift, PDFFontItem)
		{
			n = pift->font->pdf_use_flags;

			if(n & PDF_FONT_USE_F_HORZ)
				mStrAppendFormat(str, "/F%d %d 0 R", pift->font->pdf_resno_horz, pift->type0_h.objno);

			if(n & PDF_FONT_USE_F_VERT)
				mStrAppendFormat(str, "/F%d %d 0 R", pift->font->pdf_resno_vert, pift->type0_v.objno);
		}

		mStrAppendText(str, ">>");
	}

	//画像

	if(st->imgnum)
	{
		mStrAppendText(str, "/XObject<<");

		for(n = 0; n < st->imgnum; n++)
		{
			piimg = st->img[n];
			
			mStrAppendFormat(str, "/I%d %d 0 R", piimg->pdf_resno, piimg->pdf_objno);
		}

		mStrAppendText(str, ">>");
	}

	//ProcSet

	mStrAppendText(str, "/ProcSet[/PDF/Text/ImageB/ImageC]>>\n");

	//----- すでにあるか

	hash = mCalcStringHash(str->buf);

	MLK_LIST_FOR(p->list_resource, pi, PDFResourceItem)
	{
		if(pi->hash == hash && strcmp(pi->str, str->buf) == 0)
			return pi->objno;
	}

	//----- 新規追加

	pi = (PDFResourceItem *)mListAppendNew(&p->list_resource, sizeof(PDFResourceItem) + str->len + 1);
	if(!pi) app_enderr_alloc();

	//オブジェクト

	piobj = pdf_add_object_in_stream(p, NULL, TRUE);

	fwrite(str->buf, 1, str->len, p->fpstrm);

	//

	pi->objno = piobj->objno;
	pi->hash = hash;

	memcpy(pi->str, str->buf, str->len + 1);

	return pi->objno;
}

/* 文字列の終了 */

static void _end_string(PDFData *p)
{
	if(p->pagest.has_lastchar)
	{
		fputs(">Tj ", p->fpconts);

		p->pagest.has_lastchar = FALSE;
	}
}

/** ページ内容をクリア */

void pdf_clear_page(PDFData *p)
{
	PDFFontItem *pift;

	mMemset0(&p->pagest, sizeof(PDFPageState));

	//ページオブジェクトの番号を予約
	// :見開き2Pの目次のため

	p->obj_curpage.objno = p->cur_objno++;

	//コンテンツ

	rewind(p->fpconts);

	fputs("q /GS1 gs ", p->fpconts);

	p->pagest.first_size = ftell(p->fpconts);

	//使用フォントのフラグを OFF

	MLK_LIST_FOR(p->list_font, pift, PDFFontItem)
	{
		pift->font->pdf_use_flags = 0;
	}
}

/** 各1Pごとのページ番号をセット (目次用) */

void pdf_set_pageno(PDFData *p,int pageno)
{
	//現在ページが、目次内の番号だった場合、ページオブジェクト番号をセット
	// :見開き2Pでも、各ページごとにセットする。

	app_index_set_pdf_objno(pageno, p->obj_curpage.objno);
}

/** 描画後、ページを追加 */

void pdf_add_page(PDFData *p)
{
	PDFObjItem *pict;
	PDFObjstrmItem *piobj;
	FILE *fpstrm;
	int size,fempty,no;

	size = ftell(p->fpconts);
	fempty = (size == p->pagest.first_size);

	//最後の命令

	fputc('Q', p->fpconts);
	size++;

	//圧縮オブジェクト出力

	pdf_put_objectstrem_max(p);

	//空ページなら、コンテンツとリソースはなし

	if(!fempty)
	{
		//リソース辞書

		no = _add_resource_dict(p);

		//コンテンツストリーム

		pict = pdf_add_object(p, NULL);

		pdf_put_object_top(p, pict, NULL);

		pdf_put_stream(p, p->fpconts, p->fp, size);
	}

	//ページオブジェクト

	piobj = pdf_add_object_in_stream(p, &p->obj_curpage, TRUE);

	piobj->piobj->flags |= PDFOBJITEM_F_PAGEOBJECT;

	p->pageobj_num++;

	//ページオブジェクト書き込み

	fpstrm = p->fpstrm;

	fprintf(fpstrm, "<</Type/Page/Parent %d 0 R", p->obj_rootpage.objno);

	if(!fempty)
		fprintf(fpstrm, "/Resources %d 0 R/Contents %d 0 R", no, pict->objno);

	fputs(">>\n", fpstrm);
}

/** 画像を描画 */

void pdf_draw_image(PDFData *p,ImageItem *piimg,int x,int y,int w,int h)
{
	pdf_add_image(p, piimg);

	pdf_put_draw(p, p->fpconts, "q %p 0 0 %p %p %y cm /I%d Do Q",
		w, h, x, y + h, piimg->pdf_resno);
}

/** 矩形塗りつぶしの描画 (色はそのまま) */

void pdf_draw_fillbox(PDFData *p,int x,int y,int w,int h)
{
	pdf_put_draw(p, p->fpconts, "%p %p %p %p re f", x, p->paper_fullh - y - h, w, h);
}

/** 矩形塗りつぶしの描画 (色指定) */

void pdf_draw_fillbox_col(PDFData *p,int x,int y,int w,int h,uint32_t col)
{
	pdf_put_draw(p, p->fpconts, "%c %p %p %p %p re f",
		col, x, p->paper_fullh - y - h, w, h);

	p->pagest.col = col & 0xffffff;
}

/** 塗りつぶしの色をセット
 *
 * 現在色と同じなら、何もしない */

void pdf_set_color(PDFData *p,uint32_t col)
{
	col &= 0xffffff;

	if(p->pagest.col != col)
	{
		pdf_put_draw(p, p->fpconts, "%c", col);
	
		p->pagest.col = col;
	}
}

/** フォントの変更 */

void pdf_draw_change_font(PDFData *p,Font *font,int fontsize,mlkbool horz)
{
	int no;

	_end_string(p);

	//テキストの開始

	if(!p->pagest.has_text)
	{
		fputs("BT ", p->fpconts);

		p->pagest.has_text = TRUE;
	}

	//

	no = (horz)? font->pdf_resno_horz: font->pdf_resno_vert;

	pdf_put_draw(p, p->fpconts, "/F%d %p Tf", no, fontsize);

	//使用フォントのフラグ

	font->pdf_use_flags |= (horz)? PDF_FONT_USE_F_HORZ: PDF_FONT_USE_F_VERT;

	//ページデータ

	p->pagest.has_font = TRUE;
	p->pagest.font_ascent = lround((double)font->ascent / font->emsize * fontsize);
	p->pagest.font_descent = lround((double)font->descent / font->emsize * fontsize);
	p->pagest.font_vmid = fontsize / 2;
}

/** グリフの描画
 *
 * x,y: 左上位置 */

void pdf_draw_glyph(PDFData *p,Font *font,int x,int y,uint16_t gid,uint8_t flags)
{
	PDFPageState *st = &p->pagest;
	FILE *fp = p->fpconts;
	int fnew,fnewrot;

	//GID = 0 だけ使われているフォントは、PDF に含めない

	if(!font->gidmap) return;

	//グリフの基準位置

	if(flags & PDF_DRAWGLYPH_F_HORZ)
		y += st->font_ascent;
	else
		x += st->font_vmid;

	y = p->paper_fullh - y;

	//新しい文字列にするか
	// :前の文字がない or 座標を指定する時 or 90度回転で新しい文字列

	fnewrot = ((flags & PDF_DRAWGLYPH_F_VROTATE)
		&& (!st->flast_rotate || !(flags & PDF_DRAWGLYPH_F_NO_COORD)));

	fnew = (fnewrot
		|| !st->has_lastchar
		|| !(flags & PDF_DRAWGLYPH_F_NO_COORD));

	//前の文字列を終了

	if(st->has_lastchar && fnew)
		fputs(">Tj ", fp);

	//移動
	// :Td で移動すると、その位置が基準位置となる。

	if(fnew)
	{
		if(fnewrot)
		{
			//テキスト空間の (0,0) に戻って、90度回転して、移動
			// :90度回転の後はダッシュ描画だけなので、行列は戻さなくて良い

			if(st->text_x || st->text_y)
				pdf_put_draw(p, fp, "%p %p Td", -st->text_x, -st->text_y);
			else
				fputs("T* ", fp);

			pdf_put_draw(p, fp, "0 -1 1 0 %p %p Tm", x - st->font_descent, y + st->font_ascent);
	
			st->flast_rotate = TRUE;
			st->text_x = st->text_y = 0;
		}
		else
		{
			pdf_put_draw(p, fp, "%p %p Td", x - st->text_x, y - st->text_y);

			st->text_x = x;
			st->text_y = y;
		}
	}

	//文字列開始

	if(fnew)
		fputc('<', fp);

	//GID

	gid = font->gidmap[gid];

	fprintf(fp, "%04X", gid);

	//

	st->has_lastchar = TRUE;
}

/** テキストの終了 */

void pdf_end_text(PDFData *p)
{
	_end_string(p);

	if(p->pagest.has_text)
	{
		fputs("ET ", p->fpconts);

		p->pagest.has_text = FALSE;
		p->pagest.text_x = p->pagest.text_y = 0;
	}
}

