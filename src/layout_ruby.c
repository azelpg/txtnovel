/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * レイアウト処理: ルビ文字列
 *********************************/

#include <stdio.h>

#include <mlk.h>
#include <mlk_list.h>

#include "app.h"
#include "setting.h"
#include "layout.h"
#include "font/font.h"



/** RubyChar 追加 */

RubyChar *layout_ruby_addchar(LayoutData *p,BodyChar *ch)
{
	RubyChar *pi;

	pi = (RubyChar *)mListAppendNew(&p->list_rubych, sizeof(RubyChar));
	if(!pi) app_enderr_alloc();

	pi->gid = layout_get_char_to_gid(p->rubyfont, ch, &pi->code);

	return pi;
}

/** RubyData 追加 */

RubyData *layout_ruby_add(LayoutData *p)
{
	RubyData *pi;
	
	pi = (RubyData *)mListAppendNew(&p->list_ruby, sizeof(RubyData));
	if(!pi) app_enderr_alloc();

	return pi;
}

/** RubyData 追加前の位置から、最初のデータ取得
 *
 * return: NULL で、1つも追加されていない */

RubyData *layout_ruby_getdat_lasttop(LayoutData *p,mListItem *last)
{
	if(!last)
		return (RubyData *)p->list_ruby.top;
	else if(last == p->list_ruby.bottom)
		return NULL;
	else
		return (RubyData *)last->next;
}

/** RubyData 削除 */

void layout_ruby_delete(LayoutData *p,RubyData *pr)
{
	RubyChar *pch,*next;
	int i;

	pch = pr->chtop;

	for(i = pr->len; i; i--, pch = next)
	{
		next = (RubyChar *)pch->i.next;

		mListDelete(&p->list_rubych, MLISTITEM(pch));
	}

	mListDelete(&p->list_ruby, MLISTITEM(pr));
}


//========================
// 文字列追加
//========================


/* ルビ文字として追加 */

static void _func_add_rubychar(BodyChar *pc,void *param)
{
	LayoutData *p = (LayoutData *)param;

	//Unicode: 小書きかなを大文字に変換

	if(p->layout->ruby_small_kana == LAYOUT_RUBY_SMALL_KANA_BIG
		&& pc->type == BODYCHAR_TYPE_UNICODE)
	{
		pc->code = layout_ruby_kana_to_big(pc->code);
	}

	layout_ruby_addchar(p, pc);
}

/* ルビ文字の設定 */

static void _set_rubychar(LayoutData *p,RubyData *pr)
{
	RubyChar *pch;
	Font *font;
	int i,fruby,vert;
	uint16_t gid;

	fruby = (p->layout->gsub & LAYOUT_GSUB_RUBY);

	pch = pr->chtop;
	font = p->rubyfont;

	for(i = pr->len; i; i--, pch = (RubyChar *)pch->i.next)
	{
		gid = pch->gid;
		vert = !p->is_horz;

		//vert

		if(vert)
			gid = font_get_gsub(font, FONT_DATA_GSUB_VERT, gid);

		//

		if(vert && pch->code >= 0x20 && pch->code <= 0x7e)
		{
			//縦書き、欧文
			// :描画側で90度回転

			if(gid == pch->gid)
			{
				pch->flags |= RUBYCHAR_FLAG_VROTATE;
				vert = FALSE;
			}
		}
		else
		{
			//--- 縦書き/横書き

			//ruby

			if(fruby)
				gid = font_get_gsub(font, FONT_DATA_GSUB_RUBY, gid);
		}

		//

		pch->gid = gid;

		pch->width = font_regist_glyph(font, gid, pch->code, p->ruby_fontsize, vert);

		pr->ruby_width += pch->width;
	}
}

/** ルビ付き文字列の追加 */

void layout_ruby_addset(LayoutData *p,int ruby_type)
{
	StrData *pstr;
	RubyData *pr,*prtop;
	mListItem *lastch,*lastruby;
	int i,cnt,fcenter,fjyukugo;

	//ルビタイプ

	fcenter = (p->is_horz || p->layout->ruby_align == LAYOUT_RUBY_ALIGN_CENTER);

	switch(ruby_type)
	{
		case RUBY_TYPE_SET_JYUKUGO:
			ruby_type = (fcenter)? RUBY_TYPE_JYUKUGO_CENTER: RUBY_TYPE_JYUKUGO_TOP;
			break;
		case RUBY_TYPE_SET_MONO:
			ruby_type = (fcenter)? RUBY_TYPE_MONO_CENTER: RUBY_TYPE_MONO_TOP;
			break;
		case RUBY_TYPE_JYUKUGO_CENTER:
		case RUBY_TYPE_MONO_CENTER:
			fcenter = TRUE;
			break;
		default:
			fcenter = FALSE;
			break;
	}

	fjyukugo = (ruby_type == RUBY_TYPE_JYUKUGO_TOP || ruby_type == RUBY_TYPE_JYUKUGO_CENTER);

	//-------

	pr = NULL;
	cnt = 0;
	lastruby = p->list_ruby.bottom;

	while(1)
	{
		if(body_is_finish(p->buf))
		{
			p->buf += 2;

			//最後の熟語ルビ
			if(fjyukugo) pr->flags |= RUBY_FLAG_LAST;
			break;
		}

		//前の熟語ルビのフラグを ON

		if(pr && fjyukugo)
			pr->flags |= RUBY_FLAG_NEXT;

		//ルビ追加

		pr = layout_ruby_add(p);

		pr->type = ruby_type;

		if(fcenter) pr->flags |= RUBY_FLAG_CENTER;

		//親文字 (約物に関係なく、すべて通常文字として扱う)

		pstr = layout_str_add_chars_pack(p);

		pstr->classno = STRCLASS_WITH_RUBY;
		pstr->ruby = pr;

		layout_str_set_normal(p, pstr);

		//ルビ

		pr->parent = pstr;
		pr->width = pstr->width;

		lastch = p->list_rubych.bottom;
		
		pr->len = body_readchars(&p->buf, _func_add_rubychar, p);

		pr->chtop = (lastch)? (RubyChar *)lastch->next: (RubyChar *)p->list_rubych.top;

		if(fjyukugo && cnt)
			pr->flags |= RUBY_FLAG_PREV;

		_set_rubychar(p, pr);

		//

		cnt++;
	}

	//---- 熟語ルビ時、モノルビに変更

	if(fjyukugo)
	{
		ruby_type = (fcenter)? RUBY_TYPE_MONO_CENTER: RUBY_TYPE_MONO_TOP;
	
		if(cnt == 1)
			//[] 内に1つだけ
			pr->type = ruby_type;
		else
		{
			//[] 内のすべてが親文字列に収まる場合、すべてモノルビにする
			
			pr = prtop = layout_ruby_getdat_lasttop(p, lastruby);

			for(i = cnt; i; i--, pr = (RubyData *)pr->i.next)
			{
				if(pr->width < pr->ruby_width)
					break;
			}

			if(!i)
			{
				for(pr = prtop, i = cnt; i; i--, pr = (RubyData *)pr->i.next)
					pr->type = ruby_type;
			}
		}
	}
}


//==========================
// ルビの配置 (サブ)
//==========================


/** (*remain) のはみ出し幅を、w1 と w2 の小さい方で減らす
 *
 * return: 減らした分が返る */

int layout_ruby_sub_over(int w1,int w2,int *remain)
{
	int n;

	if(*remain <= 0) return 0;

	if(w2 < w1) w1 = w2;

	if(w1 <= 0) return 0;

	n = *remain;
	if(n > w1) n = w1;

	*remain -= n;
	
	return n;
}

/** 後ろの通常文字にルビを掛ける
 *
 * return: 掛けた幅 */

int layout_ruby_after_over_normal(LayoutData *p,RubyData *pr,int *remain)
{
	StrData *pstr;
	int n,max;

	if(*remain <= 0) return 0;

	pstr = (StrData *)(pr->parent)->i.next;
	if(!pstr) return 0;

	//かな、長音記号、小書きの仮名、終わり括弧、句点、読点、中点、分離禁止文字、漢字など(中付き時)
	
	if((pstr->class_flags & STRCLASS_F_RUBY_OVER_AFTER)
		|| ((pr->flags & RUBY_FLAG_CENTER) && pstr->classno == STRCLASS_NONE))
	{
		n = pstr->width + pstr->fix_before + pstr->fix_after;

		max = p->ruby_fontsize;
		if(pr->flags & RUBY_FLAG_CENTER) max /= 2;
	
		n = layout_ruby_sub_over(n, max, remain);

		if(n) pr->ruby_over_after = n;

		return n;
	}

	return 0;
}

/** 前の通常文字にルビを掛ける
 *
 * return: 掛けた幅 */

int layout_ruby_before_over_normal(LayoutData *p,RubyData *pr,int *remain)
{
	StrData *pstr,*pstr2;
	int n,max;

	if(*remain <= 0) return 0;

	//現在位置が行頭なら、何もしない
	// :行調整中。段落取得時はセットされていない。
	
	if(pr->parent->flags & STR_FLAG_LINE_TOP) return 0;

	//

	pstr = (StrData *)(pr->parent)->i.prev;
	if(!pstr) return 0;

	//かな、長音記号、小書きの仮名、始め括弧、中点、分離禁止文字、漢字など(中付き時)

	if((pstr->class_flags & STRCLASS_F_RUBY_OVER_BEFORE)
		|| ((pr->flags & RUBY_FLAG_CENTER) && pstr->classno == STRCLASS_NONE))
	{
		//前の文字に、2つ前の文字のルビが後ろに掛かっている場合があるため、
		//前の文字に掛けたときに、前のルビとこのルビが繋がらないようにする必要がある。
		//前の文字にルビが掛かっていれば、余白としてルビサイズ分を空ける。

		pstr2 = (StrData *)pstr->i.prev;

		if(!pstr2 || !pstr2->ruby)
			//前に文字がない、前がルビではない
			n = 0;
		else
		{
			//掛かっている分と余白を減らす
			
			n = pstr2->ruby->ruby_over_after;
			if(n) n += p->ruby_fontsize;

			n = -n;
		}

		n += pstr->width + pstr->fix_before + pstr->fix_after;

		max = p->ruby_fontsize;
		if(pr->flags & RUBY_FLAG_CENTER) max /= 2;

		n = layout_ruby_sub_over(n, max, remain);

		if(n) pr->ruby_shift = -n;

		return n;
	}

	return 0;
}

/** 親文字の字間を前後に均等に空ける */

void layout_ruby_add_space_parent(StrData *pstr,int remain)
{
	StrChar *pstrch;
	int len,n;

	pstrch = pstr->chtop;
	len = pstr->len;
	n = remain / len / 2;
	remain -= n * len * 2;

	for(; len; len--, pstrch = (StrChar *)pstrch->i.next)
	{
		pstrch->sp_before = n + ((remain-- > 0)? 1: 0);
		pstrch->sp_after  = n + ((remain-- > 0)? 1: 0);
	}
}

/* 親文字の字間を前後に均等に空ける
 *
 * 前後のはみ出し分を考慮して中央寄せ*/

static void _add_space_parent_center(StrData *pstr,int remain,int before,int after)
{
	StrChar *pstrch;
	int len,n;

	remain -= (before > after)? before - after: after - before;

	pstrch = pstr->chtop;
	len = pstr->len;
	n = remain / len / 2;
	remain -= n * len * 2;

	if(after > before)
		pstrch->sp_before = after - before;

	for(; len; len--, pstrch = (StrChar *)pstrch->i.next)
	{
		pstrch->sp_before += n + ((remain-- > 0)? 1: 0);
		pstrch->sp_after  += n + ((remain-- > 0)? 1: 0);

		if(len == 1 && before > after)
			pstrch->sp_after += before - after; 
	}
}


//===============================
// ルビの配置
//===============================


/* モノルビ (肩付き) */

static void _setruby_mono_top(LayoutData *p,RubyData *pr,int flags)
{
	StrData *pstr;
	int remain;

	if(pr->width >= pr->ruby_width) return;

	//--- ルビの方が長い

	pstr = pr->parent;
	remain = pr->ruby_width - pr->width;

	//後ろにルビを掛ける (優先)

	if(!(flags & LAYOUT_SETRUBY_F_DISABLE_AFTER))
		layout_ruby_after_over_normal(p, pr, &remain);
	
	//前の文字に掛ける

	layout_ruby_before_over_normal(p, pr, &remain);

	//残りは親文字の字間に追加
	// :行頭/行末時は、先頭/終端に揃える

	if(remain > 0)
	{
		pstr->width = pr->width + remain;

		pr->flags |= RUBY_FLAG_ADJUST_SP;

		layout_ruby_add_space_parent(pstr, remain);
	}
}

/* モノルビ (中付き) */

static void _setruby_mono_center(LayoutData *p,RubyData *pr,int flags)
{
	StrData *pstr;
	int rubyw,pw,remain,before,after;

	rubyw = pr->ruby_width;
	pw = pr->width;

	if(rubyw <= pw)
	{
		//---- 親文字列内に収まる

		pr->chtop->sp_before = (pw - rubyw) / 2;
	}
	else
	{
		//---- ルビの方が長い
	
		pr->flags |= RUBY_FLAG_ADJUST_CENTER;

		remain = rubyw - pw;

		//前後に掛ける

		before = layout_ruby_before_over_normal(p, pr, &remain);

		if(!(flags & LAYOUT_SETRUBY_F_DISABLE_AFTER))
			after = layout_ruby_after_over_normal(p, pr, &remain);

		//残りは親字間を空ける

		if(remain > 0)
		{
			pstr = pr->parent;

			pstr->width = pw + remain;

			_add_space_parent_center(pstr, remain, before, after);
		}
	}
}

/* グループルビ(1)
 *
 * 先頭と終端に空きを入れる。
 * 親とルビの文字数が同じなら、中央寄せになる。 */

static void _setruby_group(LayoutData *p,RubyData *pr,int flags)
{
	StrData *pstr;
	StrChar *pstrch;
	RubyChar *prch;
	int rubyw,pw,remain,len,i,sp,top;

	rubyw = pr->ruby_width;
	pw = pr->width;

	if(pw == rubyw) return;

	if(rubyw < pw)
	{
		//---- 親の方が長い

		remain = pw - rubyw;
		len = pr->len;
		sp = remain / len;

		if(sp > p->ruby_fontsize && len > 1)
		{
			//先頭・終端の空きは、最大でルビサイズx1まで
			// :ルビが1文字の場合は、対象外

			top = p->ruby_fontsize;
			remain -= top * 2;
			sp = remain / (len - 1);
			remain -= sp * (len - 1);
		}
		else
		{
			top = sp / 2;
			remain -= sp * len;
		}

		//

		prch = pr->chtop;

		for(i = 0; i < len; i++, prch = (RubyChar *)prch->i.next)
		{
			if(!i)
				//先頭
				prch->sp_before = top;
			else
				prch->sp_before = sp + ((remain-- > 0)? 1: 0);
		}
	}
	else
	{
		//---- ルビの方が長い

		pstr = pr->parent;

		pstr->width = rubyw;

		remain = rubyw - pw;
		len = pstr->len;
		sp = remain / len;
		remain = remain - sp * len;

		pstrch = pstr->chtop;

		for(i = 0; i < len; i++, pstrch = (StrChar *)pstrch->i.next)
		{
			if(!i)
				//先頭
				pstrch->sp_before = sp / 2;
			else
				pstrch->sp_before = sp + ((remain-- > 0)? 1: 0);
		}
	}
}

/* グループルビ(2)
 *
 * 先頭と終端を揃える */

static void _setruby_group2(LayoutData *p,RubyData *pr,int flags)
{
	StrData *pstr;
	StrChar *pstrch;
	RubyChar *prch;
	int rubyw,pw,remain,n,len;

	rubyw = pr->ruby_width;
	pw = pr->width;

	if(pw == rubyw) return;

	if(rubyw < pw)
	{
		//親の方が長い
		// :親の端に合わせて、ルビ字間を空ける。ただし、ルビが1文字なら先頭

		remain = pw - rubyw;

		if(pr->len > 1)
		{
			prch = pr->chtop;
			len = pr->len - 1;
			n = remain / len;
			remain -= n * len;

			for(; len; len--, prch = (RubyChar *)prch->i.next)
				prch->sp_after = n + ((remain-- > 0)? 1: 0);
		}
	}
	else
	{
		//ルビの方が長い
		// :ルビの端に合わせて、親文字の字間を空ける。親が1文字なら、先頭

		pstr = pr->parent;

		pstr->width = rubyw;

		remain = rubyw - pw;

		if(pstr->len > 1)
		{
			pstrch = pstr->chtop;
			len = pstr->len - 1;
			n = remain / len;
			remain -= n * len;

			for(; len; len--, pstrch = (StrChar *)pstrch->i.next)
				pstrch->sp_after = n + ((remain-- > 0)? 1: 0);
		}
	}
}

/* @lr */

static void _setruby_long(LayoutData *p,RubyData *pr,int flags)
{
	int rubyw,pw;

	rubyw = pr->ruby_width;
	pw = pr->width;

	if(pw == rubyw) return;

	if(pw < rubyw)
	{
		//ルビの方が長い: @gr2 グループルビと同じ
		
		_setruby_group2(p, pr, 0);
	}
	else
	{
		//親の方が長い
		// :ルビはベタ組で、ルビ先頭に空き

		pr->chtop->sp_before = (pw - rubyw) / 2;
	}
}


typedef void (*func_ruby_setruby)(LayoutData *p,RubyData *pr,int flags);

static func_ruby_setruby g_setruby_funcs[] = {
	_setruby_mono_top, _setruby_mono_center,
	layout_setruby_jyukugo_top, layout_setruby_jyukugo_center,
	_setruby_group, _setruby_group2, _setruby_long
};

/** 現在の行のすべてのルビの長さと字間を設定
 *
 * 段落行の取得後。
 * 行の折返しは考えない。 */

void layout_ruby_set_line(LayoutData *p)
{
	RubyData *pr;

	for(pr = p->pagedat.pr_top; pr; pr = (RubyData *)pr->i.next)
		(g_setruby_funcs[pr->type])(p, pr, 0);
}


//=================================
// 行頭/行末の処理
//=================================


/* 肩付き時、行頭のルビ調整
 *
 * はみ出しを戻す & 行頭を揃えて、後ろに字間を空ける。
 * (後ろにはみ出しているルビはそのまま) */

static void _adjust_line_top(LayoutData *p,RubyData *pr)
{
	StrData *pstr;
	StrChar *pstrch;
	int n,remain,len;

	pr->ruby_shift = 0;

	//前はみ出し分を含めた、親字間の空き量

	remain = pr->ruby_width - pr->width - pr->ruby_over_after;
	if(remain <= 0) return;

	pstr = pr->parent;
	pstr->width = pr->width + remain;

	//字間を後ろに空ける

	pstrch = pstr->chtop;
	len = pstr->len;
	n = remain / len;
	remain -= n * len;

	for(; len; len--, pstrch = (StrChar *)pstrch->i.next)
	{
		pstrch->sp_before = 0;
		pstrch->sp_after  = n + ((remain-- > 0)? 1: 0);
	}
}

/** 行頭のルビを調整
 *
 * ルビが上にはみ出している or 前のルビからのはみ出しで下にずれている or (肩付き)親字間が空いている時。 */

void layout_ruby_adjust_top(LayoutData *p,RubyData *pr)
{
	if(pr->flags & RUBY_FLAG_CENTER)
	{
		//中付き

		if(pr->flags & RUBY_FLAG_ADJUST_CENTER)
			layout_ruby_reset_normal(p, pr, 0);
	}
	else
	{
		//肩付き

		if(pr->ruby_shift != 0
			|| (pr->flags & RUBY_FLAG_ADJUST_SP))
		{
			_adjust_line_top(p, pr);
		}
	}
}

/** 行末でモノルビ/熟語ルビが後ろにはみ出している時、前の文字のみに掛けるように再セット
 *
 * return: 以前の親文字列幅から増えた分 */

int layout_ruby_reset_before(LayoutData *p,RubyData *pr)
{
	int n;

	if(pr->type == RUBY_TYPE_JYUKUGO_CENTER)
	{
		//中付きの熟語 (熟語の先頭から pr までを再セット)
		
		n = layout_ruby_jyukugo_center_reset(p, NULL, pr);
	}
	else
	{
		n = pr->parent->width;
		
		layout_ruby_reset_normal(p, pr, LAYOUT_SETRUBY_F_DISABLE_AFTER);

		n = pr->parent->width - n;
	}

	return n;
}

/** ルビを通常通りに再セット
 *
 * - 行頭はみ出し時(中付き)。
 * - 行末はみ出し時(中付きの熟語は対象外)。
 * - 行末のルビが収まらず、次行に送られる時。
 *
 * flags: ルビセットのフラグ */

void layout_ruby_reset_normal(LayoutData *p,RubyData *pr,int flags)
{
	StrData *pstr = pr->parent;

	if(pr->type == RUBY_TYPE_JYUKUGO_CENTER)
	{
		//中付きの熟語ルビ (行頭から終端までを再セット)
		
		layout_ruby_jyukugo_center_reset(p, pr, NULL);
	}
	else
	{
		//親文字列をベタ組にリセット

		layout_str_reset_width(p, pstr);

		//ルビをリセット

		pr->ruby_shift = 0;
		pr->ruby_over_after = 0;
		pr->flags &= ~RUBY_FLAG_ADJUST_SP;

		//再セット

		(g_setruby_funcs[pr->type])(p, pr, flags);
	}
}

/** (肩付き) 行末に確定した時、親字間が空いている場合、行末に揃える
 *
 * この時点ではみ出しは処理済み。 */

void layout_ruby_adjust_bottom(RubyData *pr)
{
	StrData *pstr;
	StrChar *pstrch;
	int remain,len,n;

	pstr = pr->parent;

	remain = pstr->width - pr->width;
	pstrch = pstr->chtop;
	len = pstr->len;

	n = remain / len;
	remain -= n * len;

	for(; len; len--, pstrch = (StrChar *)pstrch->i.next)
	{
		pstrch->sp_before = n + ((remain-- > 0)? 1: 0);
		pstrch->sp_after  = 0;
	}
}


