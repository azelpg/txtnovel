/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * 本文データ
 *********************************/

#include <string.h>

#include <mlk.h>

#include "bodydata.h"


//---------------

#define _GETBUF16(p)  *((uint16_t *)(p))

//---------------


/* 3byte 取得 */

static uint32_t _getbuf24(uint8_t *ps)
{
	return (ps[0] << 16) | (ps[1] << 8) | ps[2];
}

/** 現在位置が終端か */

mlkbool body_is_finish(uint8_t *buf)
{
	return (_GETBUF16(buf) == BODYCHAR_FINISH);
}

/** 次の文字/コマンドを取得
 *
 * 終端の場合は、位置を移動しない。 */

void body_getchar(uint8_t **ppbuf,BodyChar *dst)
{
	uint8_t *buf = *ppbuf;
	uint16_t v;
	int type;

	if(!dst->next)
	{
		dst->next = TRUE;
		return;
	}

	dst->next = TRUE;

	v = _GETBUF16(buf);
	if(v == BODYCHAR_FINISH)
	{
		dst->type = BODYCHAR_TYPE_FINISH;
		return;
	}

	//

	buf += 2;
	
	if(v == BODYCHAR_CID)
	{
		//CID

		dst->code = _GETBUF16(buf);

		buf += 2;
		type = BODYCHAR_TYPE_CID;
	}
	else if(v == BODYCHAR_UVS)
	{
		//UVS (CMD の範囲内のため、先に判定)

		dst->uvs = _getbuf24(buf);
		dst->code = _getbuf24(buf + 3);

		buf += 6;
		type = BODYCHAR_TYPE_UVS;
	}
	else if(v >= BODYCHAR_CMD_FIRST && v <= BODYCHAR_CMD_LAST)
	{
		//コマンド

		dst->code = v - BODYCHAR_CMD_FIRST;

		type = BODYCHAR_TYPE_COMMAND;
	}
	else if(v >= BODYCHAR_HI_FIRST && v <= BODYCHAR_HI_LAST)
	{
		//Unicode 32bit

		dst->code = 0x10000 + ((v - 0xD800) << 10) + _GETBUF16(buf);

		buf += 2;
		type = BODYCHAR_TYPE_UNICODE;
	}
	else
	{
		//Unicode
		
		dst->code = v;

		type = (v == BODYCHAR_ENTER)? BODYCHAR_TYPE_ENTER: BODYCHAR_TYPE_UNICODE;
	}

	dst->type = type;

	*ppbuf = buf;
}

/** 1文字読み込んで、終端なら次の位置へ
 *
 * return: TRUE で終端 */

mlkbool body_getchar_finish(uint8_t **ppbuf,BodyChar *ch)
{
	body_getchar(ppbuf, ch);

	if(ch->type == BODYCHAR_TYPE_FINISH)
	{
		*ppbuf += 2;
		return TRUE;
	}

	return FALSE;
}

/** コマンドの複数文字を読み込み (FINISH が終端)
 *
 * 文字が読まれるごとに関数が実行される。
 *
 * return: 文字数 */

int body_readchars(uint8_t **ppbuf,void (*func)(BodyChar *ch,void *param),void *param)
{
	BodyChar ch;
	int len = 0;

	ch.next = 1;

	while(1)
	{
		if(body_getchar_finish(ppbuf, &ch))
			break;

		(func)(&ch, param);

		len++;
	}

	return len;
}

/** ポインタ値を読み込み */

void *body_read_ptr(uint8_t **ppbuf)
{
	void *ptr;

	ptr = *((void **)(*ppbuf));

	*ppbuf += sizeof(void *);

	return ptr;
}

/** 指定サイズで読み込み */

void body_read_size(uint8_t **ppbuf,void *dst,int size)
{
	memcpy(dst, *ppbuf, size);

	*ppbuf += size;
}

/** 行の終端を判定して読み込み
 *
 * 終端/改行/改ページ系コマンド。
 * 改行の後に改ページが続く場合、その改行は無視する。
 *
 * return: ENDTYPE_* */

int body_getchar_line(uint8_t **ppbuf,BodyChar *ch)
{
	int fenter = 0;

	body_getchar(ppbuf, ch);

	//改行の場合、次を読み込む

	if(ch->type == BODYCHAR_TYPE_ENTER)
	{
		body_getchar(ppbuf, ch);
		fenter = 1;
	}

	//
	
	if(ch->type == BODYCHAR_TYPE_FINISH)
		return ENDTYPE_FINISH;
	else if(ch->type == BODYCHAR_TYPE_COMMAND)
	{
		//改ページ系
		
		switch(ch->code)
		{
			case BODYCMD_NEWPAGE:
				return ENDTYPE_NEWPAGE;
			case BODYCMD_NEWCOLUMN:
				return ENDTYPE_NEWCOLUMN;
			case BODYCMD_NEWPAGE_ODD:
				return ENDTYPE_NEWPAGE_ODD;
			case BODYCMD_NEWPAGE_EVEN:
				return ENDTYPE_NEWPAGE_EVEN;
		}
	}

	//改行後が対象外の場合、現在の文字を保持

	if(fenter)
	{
		ch->next = 0;
		return ENDTYPE_ENTER;
	}
	
	return ENDTYPE_NONE;
}


//===========================
// 文字判定
//===========================


/** 改ページ系のコマンドか */

mlkbool bodych_is_cmd_new(BodyChar *p)
{
	return (p->type == BODYCHAR_TYPE_COMMAND
		&& p->code >= BODYCMD_NEWPAGE && p->code <= BODYCMD_NEWPAGE_EVEN);
}

/** 文字タイプを取得 (一体文字を扱う際)
 *
 * return: [0]通常 [1]分離禁止の先頭 [2]欧文 */

int bodych_get_type(BodyChar *p)
{
	uint32_t c;

	if(p->type != BODYCHAR_TYPE_UNICODE)
		return FALSE;

	c = p->code;

	if(c == 0x2014 || c == 0x2015 || c == U'…' || c == U'‥' || c == U'〳' || c == U'〴')
		return 1;
	else if(c >= 0x21 && c <= 0x7F)
		return 2;

	return 0;
}

/** 分離禁止文字の2文字目か */

mlkbool bodych_is_nosep_next(BodyChar *p,uint32_t c1)
{
	if(p->type != BODYCHAR_TYPE_UNICODE)
		return FALSE;

	if(c1 == U'〳' || c1 == U'〴')
		return (p->code == U'〵');
	else
		return (p->code == c1);
}

/** 欧文か */

mlkbool bodych_is_euro(BodyChar *p)
{
	return (p->type == BODYCHAR_TYPE_UNICODE && p->code >= 0x21 && p->code <= 0x7F);
}
