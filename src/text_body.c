/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * テキスト: 本文コマンドの処理
 ************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <mlk.h>
#include <mlk_str.h>
#include <mlk_buf.h>
#include <mlk_list.h>

#include "app.h"
#include "setting.h"
#include "bodydata.h"

#include "cmpstring.h"
#include "text.h"
#include "text_cmd_body.h"


//------------

//layout_class.c
mlkbool is_unicode_kana(uint32_t c);

//------------



/* 関数を実行 */

static void _run_cmd_func(TextData *p,const BodyCmdDef *pdef,BodyCmdParam *param)
{
	int ret;

	if(!pdef->func) return;
	
	ret = (pdef->func)(p, param);

	if(ret)
	{
		switch(ret)
		{
			case CMDFUNC_RET_STR_UNFOUND_NAME:
				app_enderr2("指定名の定義が存在しません", p->str.buf);
				break;
			case CMDFUNC_RET_STR_INVALID:
				app_enderr2("値が正しくありません", p->str.buf);
				break;
			case CMDFUNC_RET_ITEM_UNDEF:
				app_enderr2("未定義の項目名です", p->str.buf);
				break;
			case CMDFUNC_RET_ALLOC:
				app_enderr_alloc();
				break;
		}
	}
}


//=========================
// 引数処理
//=========================


/* [] 内を文字列として取得 (空文字列の場合あり。';' で終わっている場合は空)
 *
 * ※CID 文字はエラー
 *
 * return: ']' or ';' */

static int32_t _arg_string(TextData *p,int32_t c,const BodyCmdDef *pdef)
{
	mStr *str = &p->str;

	mStrEmpty(str);

	if(c == ';') return c;

	//[] 内

	while(1)
	{
		c = text_body_getchar(p);

		if(c == -1 || c == '\n')
			app_enderr("[] が途中で終了しています");

		if(c == ']') break;

		if(c & CHAR_F_CID)
			app_enderr("この [] 内で CID 文字は使用できません");

		mStrAppendUnichar(str, c & CHAR_MASK_CODE);
	}
	
	return c;
}

/* ルビの引数を処理。']' まで進む
 *
 * '|' がない形式。仮名かどうかで判断する。
 * CID は親文字とする。
 *
 * return: ']' */

static int32_t _arg_ruby(TextData *p,const BodyCmdDef *pdef)
{
	int32_t c;
	int type = 0,len = 0,fkana;

	//引数

	while(1)
	{
		c = text_body_getchar(p);

		if(c == -1 || c == '\n')
			app_enderr("[] が途中で終了しています");

		if(c == ']')
		{
			if(!len) app_enderr("親文字かルビがありません");

			if(!type)
				app_enderr("ルビの文字がありません");

			break;
		}

		//仮名かどうかで、切り替えを判断

		fkana = (c & CHAR_F_CID)? 0: is_unicode_kana(c);
		
		if(type != fkana)
		{
			if(!len) app_enderr("親文字かルビがありません");
			
			type ^= 1;
			len = 0;

			text_add_body_end(p);
		}

		//文字
			
		text_add_body_char(p, c);
		len++;
	}

	//終端 (ルビの終了と、全体の終了)

	text_add_body_end(p);
	text_add_body_end(p);

	return c;
}

/* ルビの引数を処理。']' まで進む
 *
 * [...|...] の形式。
 * CID 文字可。
 *
 * return: ']' */

static int32_t _arg_ruby2(TextData *p,const BodyCmdDef *pdef)
{
	int32_t c;
	int type = 0,len = 0;

	//引数

	while(1)
	{
		c = text_body_getchar(p);

		if(c == -1 || c == '\n')
			app_enderr("[] が途中で終了しています");

		if(c == ']')
		{
			if(!len) app_enderr("親文字/ルビの文字数が 0 です");

			if(!type)
				app_enderr("ルビの文字がありません");

			break;
		}
		else if(c == '|')
		{
			//親文字/ルビ切り替え

			if(!len) app_enderr("親文字/ルビの文字数が 0 です");
			
			type ^= 1;
			len = 0;

			text_add_body_end(p);
		}
		else
		{
			//文字
			
			text_add_body_char(p, c);
			len++;
		}
	}

	//終端 (ルビの終了と、全体の終了)

	text_add_body_end(p);
	text_add_body_end(p);

	return c;
}

/* 複数の本文文字の引数。']' まで進む
 *
 * CID 文字可
 *
 * return: ']' */

static int32_t _arg_chars(TextData *p,const BodyCmdDef *pdef)
{
	int32_t c,len = 0;

	while(1)
	{
		c = text_body_getchar(p);

		if(c == -1 || c == '\n')
			app_enderr("[] が途中で終了しています");

		if(c == ']')
		{
			if(!len && (pdef->flags & CMDFLAG_ARG))
				app_enderr("文字数が 0 です");
			
			break;
		}
		else
		{
			text_add_body_char(p, c);
			len++;
		}
	}

	text_add_body_end(p);

	return c;
}

/* 複数項目。'] ;' まで進む
 *
 * "[NAME=VAL, ...]"
 * CID 文字不可。
 *
 * c: '[' or ';'
 * return: '] ;' */

static int32_t _arg_multi(TextData *p,int32_t c,const BodyCmdDef *pdef,BodyCmdParam *param)
{
	mStr *str,*strname,*strval;

	strname = &p->str;
	strval = &p->str2;

	mStrEmpty(strname);
	mStrEmpty(strval);

	//開始時

	_run_cmd_func(p, pdef, param);

	//----- 引数あり

	if(c == '[')
	{
		param->proc = CMDPROC_ITEM;

		str = strname;

		while(1)
		{
			c = text_body_getchar(p);

			if(c == -1 || c == '\n')
				app_enderr("[] が途中で終了しています");

			if(c & CHAR_F_CID)
				app_enderr("この [] 内で CID 文字は使用できません");

			if(c == '=')
			{
				//名前 -> 値に切り替え

				if(!strname->len)
					app_enderr("[] 内の項目名の長さが 0 です");
				
				str = strval;
				mStrEmpty(str);
			}
			else if(c == ',' || c == ']')
			{
				//関数を呼ぶ (値なしで来る場合もあり)。新規項目へ
				// : ",]" の場合、名前の長さが 0

				if(strname->len)
					_run_cmd_func(p, pdef, param);

				if(c == ']') break;

				str = strname;
				mStrEmpty(strname);
				mStrEmpty(strval);
			}
			else
			{
				mStrAppendUnichar(str, c & CHAR_MASK_CODE);
			}
		}
	}

	//終了時

	param->proc = CMDPROC_END;

	_run_cmd_func(p, pdef, param);

	return c;
}


//=======================
// main
//=======================


/* 定義データからコマンド名検索 */

static const BodyCmdDef *_search_command(char *name)
{
	const BodyCmdDef *p;
	int len,num,i;
	uint8_t dat[32];
	const uint32_t *ps,*pcmp;

	len = strlen(name);
	num = (len + 4) / 4 - 1;

	memset(dat, 0, 32);
	dat[0] = len;
	memcpy(dat + 1, name, len);

	//開始位置
	// :中央位置の先頭文字より後ろなら、中央位置から

	p = g_bodycmd_def;
	i = sizeof(g_bodycmd_def) / sizeof(BodyCmdDef) / 2;

	if(dat[1] > g_cmpstring[g_bodycmd_def[i].name_pos + 1])
		p += i;

	//検索

	for( ; p->val != CMDVAL_END; p++)
	{
		ps = (const uint32_t *)dat;
		pcmp = (const uint32_t *)(g_cmpstring + p->name_pos);

		if(*ps == *pcmp)
		{
			ps++;
			pcmp++;

			for(i = num; i && *ps == *pcmp; i--, ps++, pcmp++);

			if(!i) return p;
		}
	}

	return NULL;
}

/** 本文コマンドを処理して、"] ;" の位置まで進む
 *
 * c: '[' or ';'
 * return: 最後の文字 (] or ;)、または -1 で終端 */

int32_t text_proc_body_command(TextData *p,int32_t c,char *cmdname)
{
	const BodyCmdDef *pdef;
	BodyCmdParam param;

	//検索

	pdef = _search_command(cmdname);
	if(!pdef)
		app_enderr2("未定義の本文コマンドです", cmdname);

	//@end-body

	if(pdef->flags & CMDFLAG_END_BODY)
		return -1;

	//引数がない

	if(c == ';' && (pdef->flags & CMDFLAG_ARG))
		app_enderr2("引数が必要です", cmdname);

	//コマンド番号を追加

	if(!(pdef->flags & CMDFLAG_NO_ADD_CMDNO))
		text_add_body_cmdno(p, pdef->cmdno);

	//引数取得

	param.proc = 0;
	param.cmdno = pdef->cmdno;

	switch(pdef->val)
	{
		//ルビ引数
		case CMDVAL_RUBY:
			c = _arg_ruby(p, pdef);
			break;
		//ルビ引数 (...|...)
		case CMDVAL_RUBY2:
			c = _arg_ruby2(p, pdef);
			break;
		//文字列挙
		case CMDVAL_CHARS:
			c = _arg_chars(p, pdef);
			break;
		//文字列 or 引数なし
		case CMDVAL_STR:
			c = _arg_string(p, c, pdef);
			break;
		//複数項目
		// :関数は各項目ごとに実行
		case CMDVAL_MULTI:
			c = _arg_multi(p, c, pdef, &param);
			break;
	}

	//']' の後に ';' があるか

	if(c == ']' && text_readchar_next(p, ';'))
		c = ';';

	//(複数項目以外) 関数実行
	// [!] 上の ';' の処理をしてから行う

	if(pdef->val != CMDVAL_MULTI)
		_run_cmd_func(p, pdef, &param);

	return c;
}


//=======================
// 各コマンド関数
//=======================
/* [] 内が空の場合もあるので、注意。 */


#define _INDEXSTR_PAGENUM "normal;yes;no;hide"


/* @kt:圏点 */

int _bodycmd_kenten(TextData *p,BodyCmdParam *param)
{
	char *name = p->str.buf;
	KentenItem *pi;
	int no;

	if(!(*name))
		no = 0;
	else
	{
		pi = setting_search_kenten(name, &no);
		if(!pi) return CMDFUNC_RET_STR_UNFOUND_NAME;
	}

	//番号

	text_add_body_byte(p, no);

	return 0;
}

/* @bs:傍線 */

int _bodycmd_bousen(TextData *p,BodyCmdParam *param)
{
	int n;

	if(!p->str.len)
		n = 0;
	else
		n = text_get_string_index(p, NULL, "line;double") + 1;

	text_add_body_byte(p, n);

	return 0;
}

/* @d:定義文 */

int _bodycmd_def(TextData *p,BodyCmdParam *param)
{
	DefItem *pi;

	pi = text_search_defitem(p, p->str.buf);
	if(!pi) return CMDFUNC_RET_STR_UNFOUND_NAME;

	mStrSetFormat(&p->str2, "@def[%s]", pi->name);

	text_open_input(p, p->str2.buf, pi);

	return 0;
}

/* @include */

int _bodycmd_include(TextData *p,BodyCmdParam *param)
{
	text_open_input(p, p->str.buf, NULL);

	return 0;
}

/* @layout */

int _bodycmd_layout(TextData *p,BodyCmdParam *param)
{
	LayoutItem *pi;

	if(!p->str.len)
		pi = g_set->layout_base;
	else
	{
		pi = setting_search_layout(p->str.buf);
		if(!pi) return CMDFUNC_RET_STR_UNFOUND_NAME;
	}

	text_add_body_ptr(p, pi);

	return 0;
}

/* @font */

int _bodycmd_font(TextData *p,BodyCmdParam *param)
{
	FontSpec fs;
	char *pcname,*pcsize;

	mMemset0(&fs, sizeof(FontSpec));

	pcname = p->str.buf;
	pcsize = NULL;

	text_get_split_next(pcname, ',', &pcsize);

	//

	if(*pcname)
		text_get_string_fontname(p, pcname, &fs);

	if(pcsize && (*pcsize))
		text_get_string_fontsize(p, pcsize, &fs);

	//書き込み
	// :デフォルト値、単位 x は、実際のレイアウト時にセットする

	if(!mBufAppend(&p->buf_body, &fs, sizeof(FontSpec)))
		return CMDFUNC_RET_ALLOC;

	return 0;
}

/* @align,@body-align */

int _bodycmd_align(TextData *p,BodyCmdParam *param)
{
	uint8_t dat;

	text_get_string_body_align(p, &dat, (param->cmdno == BODYCMD_BODYALIGN));

	if(!mBufAppend(&p->buf_body, &dat, 1))
		return CMDFUNC_RET_ALLOC;

	return 0;
}

/* @pagecnt */

int _bodycmd_pagecnt(TextData *p,BodyCmdParam *param)
{
	uint32_t n;

	n = strtoul(p->str.buf, NULL, 10);
	if(!n) return CMDFUNC_RET_STR_INVALID;

	if(!mBufAppend(&p->buf_body, &n, 4))
		return CMDFUNC_RET_ALLOC;

	return 0;
}

/* @pagenum */

int _bodycmd_pagenumber(TextData *p,BodyCmdParam *param)
{
	uint8_t n;

	if(!p->str.len)
		n = 0;
	else
		n = text_get_string_index(p, p->str.buf, _INDEXSTR_PAGENUM);

	if(!mBufAppend(&p->buf_body, &n, 1))
		return CMDFUNC_RET_ALLOC;

	return 0;
}

/* @ihead */

int _bodycmd_indent_head(TextData *p,BodyCmdParam *param)
{
	uint8_t dat[2];

	text_get_string_body_indent(p, dat);

	if(!mBufAppend(&p->buf_body, dat, 2))
		return CMDFUNC_RET_ALLOC;

	return 0;
}

/* @ifoot */

int _bodycmd_indent_foot(TextData *p,BodyCmdParam *param)
{
	uint8_t dat[2];

	text_get_string_body_indent(p, dat);

	if(!mBufAppend(&p->buf_body, dat, 1))
		return CMDFUNC_RET_ALLOC;

	return 0;
}

/* @foot */

int _bodycmd_foot(TextData *p,BodyCmdParam *param)
{
	uint8_t dat[6],ind[2];

	*((uint16_t *)dat) = BODYCHAR_CMD_FIRST + BODYCMD_ALIGN;
	*((uint16_t *)(dat + 3)) = BODYCHAR_CMD_FIRST + BODYCMD_INDENT_FOOT;

	if(p->str.len)
	{
		text_get_string_body_indent(p, ind);
	
		dat[2] = BODY_ALIGN_BOTTOM;
		if(ind[0] & 0x80) dat[2] |= 0x80;
		
		dat[5] = ind[0];
	}
	else
	{
		//省略時
		dat[2] = BODY_ALIGN_TOP;
		dat[5] = 0;
	}

	if(!mBufAppend(&p->buf_body, dat, 6))
		return CMDFUNC_RET_ALLOC;

	return 0;
}

/* @index, @index-page */

typedef struct
{
	char *name,*title;
}_indexdat;

static void _free_indexdat(void *data)
{
	mFree(((_indexdat *)data)->name);
	mFree(((_indexdat *)data)->title);
}

int _bodycmd_index(TextData *p,BodyCmdParam *param)
{
	IndexItem *pi;
	char *name,*val;
	_indexdat *dat;

	dat = (_indexdat *)p->data;

	if(param->proc == CMDPROC_FIRST)
	{
		//開始

		text_alloc_data(p, sizeof(_indexdat), _free_indexdat);
	}
	else if(param->proc == CMDPROC_ITEM)
	{
		//項目
		
		name = p->str.buf;
		val = p->str2.buf;

		if(text_cmp(name, "name"))
			mStrdup_free(&dat->name, val);
		else if(text_cmp(name, "title"))
			mStrdup_free(&dat->title, val);
		else
			return CMDFUNC_RET_ITEM_UNDEF;
	}
	else
	{
		//終了
		
		pi = app_add_index_item(dat->name, dat->title);
		if(pi)
			text_add_body_ptr(p, pi);

		text_free_data(p);
	}

	return 0;
}

/* @img */

typedef struct
{
	uint8_t page,
		pagenum,
		flags;
	ImageInfo info;
}_imagedat;

int _bodycmd_image(TextData *p,BodyCmdParam *param)
{
	char *name,*val;
	_imagedat *dat;

	dat = (_imagedat *)p->data;

	if(param->proc == CMDPROC_FIRST)
	{
		//開始

		text_alloc_data(p, sizeof(_imagedat), NULL);
	}
	else if(param->proc == CMDPROC_ITEM)
	{
		//項目
		
		name = p->str.buf;
		val = p->str2.buf;

		if(text_cmp(name, "file"))
			dat->info.item = text_add_imagefile(val);
		else if(text_cmp(name, "size"))
			text_get_string_imgsize(p, val, &dat->info);
		else if(text_cmp(name, "pos"))
		{
			if(text_get_string_imgpos(p, val, &dat->info))
				app_enderr2("値が正しくありません", val);
		}
		else if(text_cmp(name, "page"))
			dat->page = text_get_string_index(p, val, "cur;ins;ins-l;ins-r");
		else if(text_cmp(name, "pagenum"))
			dat->pagenum = text_get_string_index(p, val, _INDEXSTR_PAGENUM);
		else if(text_cmp(name, "pdfsep"))
			dat->flags |= BODY_IMG_FLAGS_PDFSEP;
		else
			return CMDFUNC_RET_ITEM_UNDEF;
	}
	else
	{
		//---- 終了

		mBuf *buf = &p->buf_body;

		if(!dat->info.item) dat->info.item = IMAGE_ITEM_NOFILE;

		//

		if(!mBufAppend(buf, &dat->info, sizeof(ImageInfo))
			|| !mBufAppend(buf, &dat->page, 1)
			|| !mBufAppend(buf, &dat->pagenum, 1)
			|| !mBufAppend(buf, &dat->flags, 1))
			return CMDFUNC_RET_ALLOC;

		text_free_data(p);
	}

	return 0;
}
