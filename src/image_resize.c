/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/**********************************
 * 画像: リサイズ
 **********************************/

#include <math.h>

#include <mlk.h>
#include <mlk_util.h>

#include "image.h"


//------------------

typedef struct
{
	int srcw,dstw,tap;
	double *pweight; //重み
	uint16_t *pindex;
}ResizeParam;

#define _RESIZE_RANGE 2

//------------------



/* 重み spline16 */

static double _weight_spline16(double d)
{
	if(d < 1.0)
		return (d - 1) * (5 * d * d - 4 * d - 5) / 5;
	else if(d < 2)
		return (5 * d - 12) * (d - 1) * (d - 2) / -15;
	else
		return 0;
}


//======================
// パラメータ
//======================


/* ResizeParam 解放 */

static void _free_param(ResizeParam *p)
{
	mFree(p->pweight);
	mFree(p->pindex);

	p->pweight = NULL;
	p->pindex = NULL;
}

/* 縮小パラメータセット
 *
 * return: 0 以外でエラー */

static int _set_param(ResizeParam *p,int srcw,int dstw)
{
	int i,j,pos,tap;
	double *dwork,*pw,dsum,dscale,dscale_rev,d;
	uint16_t *pi;

	p->srcw = srcw;
	p->dstw = dstw;

	//1px あたりの処理ピクセル

	tap = p->tap = (int)((double)srcw / dstw * _RESIZE_RANGE * 2 + 0.5);

	//バッファ確保

	p->pweight = (double *)mMalloc(sizeof(double) * dstw * tap);
	p->pindex = (uint16_t *)mMalloc(2 * dstw * tap);

	if(!p->pweight || !p->pindex)
	{
		_free_param(p);
		return 1;
	}

	//作業用バッファ

	dwork = (double *)mMalloc(sizeof(double) * tap);
	if(!dwork)
	{
		_free_param(p);
		return 1;
	}

	//--------------

	pw = p->pweight;
	pi = p->pindex;

	dscale = (double)dstw / srcw;
	dscale_rev = (double)srcw / dstw;

	for(i = 0; i < dstw; i++)
	{
		pos = floor((i - _RESIZE_RANGE + 0.5) * dscale_rev + 0.5);
		dsum = 0;

		for(j = 0; j < tap; j++, pi++)
		{
			if(pos < 0)
				*pi = 0;
			else if(pos >= srcw)
				*pi = srcw - 1;
			else
				*pi = pos;

			d = fabs((pos + 0.5) * dscale - (i + 0.5));

			dwork[j] = _weight_spline16(d);

			dsum += dwork[j];
			pos++;
		}

		for(j = 0; j < tap; j++)
			*(pw++) = dwork[j] / dsum;
	}

	mFree(dwork);

	return 0;
}


//===========================
// リサイズ
//===========================


/* 水平リサイズ */

static void _resize_horz(Image *p,uint8_t **pptmp,ResizeParam *param)
{
	uint8_t **ppsrc,*pd,*ps,*psy;
	int ix,iy,i,n,tap;
	uint16_t *pi;
	double *pw,dw,c[3];

	ppsrc = p->ppbuf;
	tap = param->tap;

	for(iy = p->height; iy; iy--)
	{
		pd = *(pptmp++);
		psy = *(ppsrc++);
		pw = param->pweight;
		pi = param->pindex;

		for(ix = param->dstw; ix; ix--, pd += 4)
		{
			c[0] = c[1] = c[2] = 0;

			for(i = tap; i; i--, pw++, pi++)
			{
				ps = psy + (*pi << 2);
				dw = *pw;

				c[0] += ps[0] * dw;
				c[1] += ps[1] * dw;
				c[2] += ps[2] * dw;
			}

			//

			for(i = 0; i < 3; i++)
			{
				n = lround(c[i]);

				if(n < 0) n = 0;
				else if(n > 255) n = 255;

				pd[i] = n;
			}
		}
	}
}

/* 垂直リサイズ */

static void _resize_vert(Image *p,uint8_t **pptmp,ResizeParam *param)
{
	uint8_t **ppdst,*pd,*ps;
	int ix,iy,i,n,tap,xpos;
	uint16_t *pi;
	double *pw,dw,c[3];

	ppdst = p->ppbuf;

	pw = param->pweight;
	pi = param->pindex;
	tap = param->tap;

	for(iy = p->height; iy; iy--)
	{
		pd = *(ppdst++);
		xpos = 0;
	
		for(ix = p->width; ix; ix--, pd += 4, xpos += 4)
		{
			c[0] = c[1] = c[2] = 0;

			for(i = 0; i < tap; i++)
			{
				ps = pptmp[pi[i]] + xpos;
				dw = pw[i];

				c[0] += ps[0] * dw;
				c[1] += ps[1] * dw;
				c[2] += ps[2] * dw;
			}

			//

			for(i = 0; i < 3; i++)
			{
				n = lround(c[i]);

				if(n < 0) n = 0;
				else if(n > 255) n = 255;

				pd[i] = n;
			}
		}

		pw += tap;
		pi += tap;
	}
}

/* 拡大時 */

static mlkbool _resize_nearest(Image *p,int width,int height)
{
	uint8_t **pptmp;
	uint32_t **ppsrc,**ppdst,*pd,*psY;
	int ix,iy,sw,sh;

	//結果用

	pptmp = (uint8_t **)mAllocArrayBuf(width * 4, height);
	if(!pptmp) return FALSE;

	//

	ppdst = (uint32_t **)pptmp;
	ppsrc = (uint32_t **)p->ppbuf;
	sw = p->width;
	sh = p->height;

	for(iy = 0; iy < height; iy++)
	{
		pd = *(ppdst++);
		psY = ppsrc[iy * sh / height];
		
		for(ix = 0; ix < width; ix++)
		{
			*(pd++) = *(psY + ix * sw / width);
		}
	}

	//入れ替え

	mFreeArrayBuf((void **)p->ppbuf, sh);

	p->ppbuf = pptmp;
	p->width = width;
	p->height = height;

	return TRUE;
}

/** リサイズ
 *
 * ビット数は、リサイズ後の状態に合わせる (縮小: MONO -> GRAY) */

mlkbool image_resize(Image *p,int width,int height)
{
	uint8_t **pptmp;
	ResizeParam param;
	int err = 1,srch;

	if(width < 1) width = 1;
	if(height < 1) height = 1;

	//同じ

	if(p->width == width && p->height == height)
		return TRUE;

	//拡大

	if(p->width < width || p->height < height)
		return _resize_nearest(p, width, height);

	//------ 縮小

	srch = p->height;

	//水平リサイズ結果用バッファ

	pptmp = (uint8_t **)mAllocArrayBuf_align(width * 4, 16, srch);
	if(!pptmp) return FALSE;

	//水平リサイズ

	if(_set_param(&param, p->width, width))
		goto ERR;

	_resize_horz(p, pptmp, &param);

	_free_param(&param);

	//元のバッファを解放して、再確保

	mFreeArrayBuf((void **)p->ppbuf, srch);

	p->ppbuf = (uint8_t **)mAllocArrayBuf_align(width * 4, 16, height);
	if(!p->ppbuf) goto ERR;

	p->width = width;
	p->height = height;

	//垂直リサイズ

	if(_set_param(&param, srch, height))
		goto ERR;

	_resize_vert(p, pptmp, &param);

	_free_param(&param);

	//ビット数

	if(p->imgbits == 1) p->imgbits = 8;

	//

	err = 0;
	
ERR:
	mFreeArrayBuf((void **)pptmp, srch);
	
	return !err;
}
