/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

typedef struct _Image Image;
typedef struct _mZlib mZlib;

struct _Image
{
	int width,
		height,
		filetype,	//ファイルタイプ
		coltype,	//カラータイプ (JPEG 時は元のファイルから。それ以外はイメージから)
		imgbits;	//(読み込み時) 色から判断したビット数。1,8,24
	uint8_t **ppbuf;
};

enum
{
	IMAGE_FILETYPE_RAW,		//生のイメージデータ
	IMAGE_FILETYPE_JPEG,	//JPEG
};

enum
{
	IMAGE_COLTYPE_GRAY,
	IMAGE_COLTYPE_RGB
};


void image_free(Image *p);
Image *image_new(int width,int height);
Image *image_load(const char *filename,int bits);
void image_savefile(Image *p,const char *filename,int format,int bits,int dpi);

void image_clear(Image *p,uint32_t col);
void image_setpixel(Image *p,int x,int y,uint32_t col);
void image_setpixel_alpha(Image *p,int x,int y,uint32_t col,int a);
void image_drawline_horz(Image *p,int x,int y,int w,uint32_t col);
void image_drawline_vert(Image *p,int x,int y,int h,uint32_t col);
void image_drawline_horz_aa(Image *p,int x,double y,int w,double h,uint32_t col);
void image_drawline_vert_aa(Image *p,double x,int y,double w,int h,uint32_t col);
void image_fillbox(Image *p,int x,int y,int w,int h,uint32_t col);

void image_fill_tile(Image *p,Image *src);
void image_blt(Image *p,int x,int y,Image *src);
void image_draw_grid(Image *p,int x,int y,int w,int h,int len,int feed,int num,int fhorz,double mulw,double mulh,int pagex,uint32_t col);

/* resize */

mlkbool image_resize(Image *p,int width,int height);

/* pdf */

int image_output_pdf(Image *p,mZlib *zlib);
