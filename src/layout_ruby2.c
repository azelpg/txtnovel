/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * レイアウト処理: 熟語ルビ
 *********************************/

#include <stdio.h>

#include <mlk.h>

#include "setting.h"
#include "layout.h"


/* ペアが１つ、または、すべて親文字内に収まっている場合は、モノルビに変換されている。
 * 熟語ルビは、常に２つ以上のペアがある状態。
 *
 * ルビは常に先頭から順にセットし、前のルビを変更しないこと。 */

//------------

//前後に掛けられるか
#define _IS_YES_OVER(p)  ((p) != (RubyData *)1)
//前後が熟語ルビで、掛けられるか
#define _IS_JRUBY(p)     ((p) && (p) != (RubyData *)1)
//前後が通常文字で、掛けられるか
#define _IS_NORMAL(p)    (!(p))

//------------


/* 前後の熟語ルビを取得
 *
 * (RubyData*)1 なら、通常文字も含めて、前後に必ず掛けない */

static void _get_prev_next(RubyData *pr,int flags,RubyData **ppprev,RubyData **ppnext)
{
	RubyData *prev,*next;

	prev = (pr->flags & RUBY_FLAG_PREV)? (RubyData *)pr->i.prev: NULL;
	next = (pr->flags & RUBY_FLAG_NEXT)? (RubyData *)pr->i.next: NULL;

	//行頭なら、掛けない

	if(pr->parent->flags & STR_FLAG_LINE_TOP)
		prev = (RubyData *)1;

	//後ろに掛けない

	if(flags & LAYOUT_SETRUBY_F_DISABLE_AFTER)
		next = (RubyData *)1;

	*ppprev = prev;
	*ppnext = next;
}

/* 前の熟語ルビに掛けられる最大幅を取得
 *
 * 次の熟語ルビは設定されていないのが前提。 */

static int _get_prev_over_width(RubyData *prev)
{
	return prev->width - prev->ruby_width - prev->ruby_shift;
}


//=========================
// 肩付き
//=========================


/** 熟語ルビ (肩付き) */

void layout_setruby_jyukugo_top(LayoutData *p,RubyData *pr,int flags)
{
	RubyData *pr_prev,*pr_next;
	int rubyw,pw,remain,rsize,n;

	//前の熟語ルビからのはみ出し分は減らす
	// :前に処理したルビによって、現在の ruby_shift が < 0 になることはない。

	rubyw = pr->ruby_width;
	pw = pr->width - pr->ruby_shift;

	//収まる場合、何もしない

	if(rubyw <= pw) return;

	//

	_get_prev_next(pr, flags, &pr_prev, &pr_next);

	remain = rubyw - pw;
	rsize = p->ruby_fontsize;

	//後ろの通常文字/熟語ルビに掛ける
	// :同時に、前にも掛かる場合がある。

	if(_IS_YES_OVER(pr_next))
	{
		if(!pr_next)
			layout_ruby_after_over_normal(p, pr, &remain);
		else
		{
			//次の熟語ルビに x1 まで掛ける

			n = layout_ruby_sub_over(pr_next->width - pr_next->ruby_width, rsize, &remain);
			if(n)
			{
				pr_next->ruby_shift = n;
				pr->ruby_over_after = n;
			}
		}
	}

	//前の通常文字/熟語ルビに掛ける

	if(_IS_YES_OVER(pr_prev))
	{
		if(!pr_prev)
			layout_ruby_before_over_normal(p, pr, &remain);
		else
		{
			//前の熟語ルビに x1 まで掛ける

			n = _get_prev_over_width(pr_prev);
		
			n = layout_ruby_sub_over(n, rsize, &remain);
			if(n)
				pr->ruby_shift = -n;
		}
	}

	//親字間に空きを追加

	if(remain > 0)
	{
		pr->parent->width = pr->width + remain;

		pr->flags |= RUBY_FLAG_ADJUST_SP;

		layout_ruby_add_space_parent(pr->parent, remain);
	}
}


//===============================
// 中付き
//===============================


/* 熟語ルビ (中付き、通常) */

static void _setruby_jyukugo_center_normal(LayoutData *p,RubyData *pr,int flags)
{
	RubyData *pr_prev,*pr_next;
	int rubyw,pw,remain,max,min,n,max_prev,max_next;

	//前の熟語ルビからのはみ出し分は除外

	rubyw = pr->ruby_width;
	pw = pr->width - pr->ruby_shift;

	//収まる場合
	// :中央寄せなどは最後の調整で行う

	if(rubyw <= pw) return;

	//

	_get_prev_next(pr, flags, &pr_prev, &pr_next);

	remain = rubyw - pw;
	min = p->ruby_fontsize / 2; //親文字に載せる最小ルビ幅

	//前後の熟語ルビに掛けられる最大幅
	// :親文字には最低でも x0.5 のルビが載ること。

	max_prev = max_next = 0;

	if(_IS_JRUBY(pr_next))
		max_next = pr_next->width - min;

	if(_IS_JRUBY(pr_prev))
		max_prev = _get_prev_over_width(pr_prev);

	//---- 前後の熟語ルビに均等に掛ける

	max = p->ruby_fontsize * 3 / 2; //x1.5
	n = remain / 2; //前後均等に
	if(max > n) max = n;

	if(max_next > 0 && max_prev > 0)
	{
		//後ろ
	
		n = layout_ruby_sub_over(max_next, max, &remain);
		if(n)
		{
			pr_next->ruby_shift = n;
			pr->ruby_over_after = n;

			max_next -= n;
		}

		//前

		n = layout_ruby_sub_over(max_prev, max, &remain);
		if(n)
		{
			pr->ruby_shift = -n;
			max_prev -= n;
		}

		if(remain <= 0) return;
	}

	//----- 残りを個別に掛ける

	max = p->ruby_fontsize * 3 / 2; //x1.5

	//後ろの熟語ルビに x1.5 まで掛ける

	if(max_next > 0)
	{
		n = layout_ruby_sub_over(max_next, max, &remain);

		pr_next->ruby_shift += n;
		pr->ruby_over_after += n;
	}

	//前の熟語ルビに x1.5 まで掛ける

	if(max_prev > 0)
	{
		n = layout_ruby_sub_over(max_prev, max, &remain);

		pr->ruby_shift -= n;
	}

	//後ろの通常文字に x0.5 まで掛ける

	if(_IS_NORMAL(pr_next))
		layout_ruby_after_over_normal(p, pr, &remain);

	//前の通常文字に x0.5 まで掛ける

	if(_IS_NORMAL(pr_prev))
		layout_ruby_before_over_normal(p, pr, &remain);

	//親字間に空きを追加

	if(remain > 0)
	{
		pr->parent->width = pr->width + remain;

		layout_ruby_add_space_parent(pr->parent, remain);
	}
}

/* 熟語ルビ (中付き、前掛け優先) */

static void _setruby_jyukugo_center_before(LayoutData *p,RubyData *pr,int flags)
{
	RubyData *pr_prev,*pr_next;
	int remain,max,min,n;

	min = p->ruby_fontsize / 2; //親文字に載せる最小幅
	max = p->ruby_fontsize * 3 / 2; //x1.5

	_get_prev_next(pr, flags, &pr_prev, &pr_next);

	//前に掛ける (ルビが収まる場合でも)

	if(_IS_YES_OVER(pr_prev))
	{
		remain = pr->ruby_width - min; //ルビを他の文字に掛けられる最大幅

		if(!pr_prev)
		{
			//通常文字

			if(!(flags & LAYOUT_SETRUBY_F_DISABLE_BEFORE_NORMAL))
				layout_ruby_before_over_normal(p, pr, &remain);
		}
		else
		{
			//前の熟語ルビに x1.5 まで掛ける

			n = _get_prev_over_width(pr_prev);

			n = layout_ruby_sub_over(n, max, &remain);
			if(n)
				pr->ruby_shift = -n;
		}
	}

	//残りのルビ幅

	remain = pr->ruby_width - pr->width + pr->ruby_shift;
	if(remain <= 0) return;

	//後ろに掛ける

	if(_IS_YES_OVER(pr_next))
	{
		if(!pr_next)
		{
			//通常文字

			layout_ruby_after_over_normal(p, pr, &remain);
		}
		else
		{
			//後ろの熟語ルビに x1.5 まで掛ける

			n = layout_ruby_sub_over(pr_next->width - min, max, &remain);
			if(n)
			{
				pr_next->ruby_shift = n;
				pr->ruby_over_after = n;
			}
		}
	}

	//親字間に空きを追加

	if(remain > 0)
	{
		pr->parent->width = pr->width + remain;

		layout_ruby_add_space_parent(pr->parent, remain);
	}
}

/* 指定範囲の熟語ルビをリセット */

static void _jyukugo_reset(LayoutData *p,RubyData *top,RubyData *end)
{
	RubyData *pr;

	for(pr = top; pr != end; pr = (RubyData *)pr->i.next)
	{
		pr->ruby_shift = 0;
		pr->ruby_over_after = 0;
		pr->chtop->sp_before = 0;

		layout_str_reset_width(p, pr->parent);
	}
}

/* ルビの位置を調整 */

static void _jyukugo_adjust_pos(LayoutData *p,RubyData *top,RubyData *end)
{
	RubyData *pr,*next;

	for(pr = top; pr != end; pr = (RubyData *)pr->i.next)
	{
		next = (RubyData *)pr->i.next;
		if(next == end) next = NULL;

		if(next && next->ruby_shift < 0
			&& pr->width - pr->ruby_width + next->ruby_shift > 0)
		{
			//次のルビから、前にはみ出しがあって、ルビ全体で親に余りがある場合、次のルビと繋げる

			pr->ruby_shift = 0;
			pr->chtop->sp_before = pr->width - pr->ruby_width + next->ruby_shift;
		}
		else if((!next || next->ruby_shift >= 0)
			&& pr->ruby_shift == 0 && pr->width > pr->ruby_width)
		{
			//前後のルビからのはみ出しがなく、親に余りがある場合、中央寄せ

			pr->chtop->sp_before = (pr->width - pr->ruby_width) / 2;
		}
	}
}

/* 親字間に空き量があるか */

static mlkbool _is_parent_space(RubyData *top,RubyData *end)
{
	for(; top != end; top = (RubyData *)top->i.next)
	{
		if(top->width < top->parent->width)
			return TRUE;
	}

	return FALSE;
}

/* 前掛け優先で再セット
 *
 * [!] リセットは予め行うこと */

static void _setruby_before(LayoutData *p,RubyData *top,RubyData *end,int flags,int is_bottom)
{
	RubyData *pr;

	for(pr = top; pr != end; pr = (RubyData *)pr->i.next)
	{
		//行末ルビの調整時は、終端で常に後ろに掛けない

		if(is_bottom && (RubyData *)pr->i.next == end)
			flags |= LAYOUT_SETRUBY_F_DISABLE_AFTER;
	
		_setruby_jyukugo_center_before(p, pr, flags);
	}
}

/* (調整時) 通常の状態で再セット
 *
 * is_bottom: 行末ルビの調整か */

static void _setruby_normal(LayoutData *p,RubyData *top,RubyData *end,int is_bottom)
{
	RubyData *pr;
	int flags = 0;

	_jyukugo_reset(p, top, end);

	for(pr = top; pr != end; pr = (RubyData *)pr->i.next)
	{
		if(is_bottom && (RubyData *)pr->i.next == end)
			flags |= LAYOUT_SETRUBY_F_DISABLE_AFTER;
	
		_setruby_jyukugo_center_normal(p, pr, flags);
	}
}

/* 全体が収まっている場合、中央寄せになるように調整 */

static void _adjust_center(LayoutData *p,RubyData *top,RubyData *end,int is_bottom)
{
	RubyData *pr,*next;
	int remain,n;

	//親の空き幅を取得

	remain = 0;

	for(pr = top; pr != end; pr = (RubyData *)pr->i.next)
	{
		next = (RubyData *)pr->i.next;
		if(next == end) next = NULL;

		n = pr->width - pr->ruby_width - pr->ruby_shift;
		if(next && next->ruby_shift < 0) n += next->ruby_shift;

		if(n > 0) remain += n;
	}

	if(!remain) return;

	//先頭に空きを追加し、前掛け優先(前の通常文字には掛けない)で再セット

	_jyukugo_reset(p, top, end);

	top->ruby_shift = remain / 2;

	_setruby_before(p, top, end, LAYOUT_SETRUBY_F_DISABLE_BEFORE_NORMAL, is_bottom);

	//親字間が空いた場合、通常に戻す

	if(_is_parent_space(top, end))
		_setruby_normal(p, top, end, is_bottom);
}

/* 熟語ルビを最終調整 */

static void _adjust_finish(LayoutData *p,RubyData *top,RubyData *end,int is_bottom)
{
	if(_is_parent_space(top, end))
	{
		//--- ルビが収まっていない場合、前掛け優先で再セット
		
		//前の通常文字に掛けない

		_jyukugo_reset(p, top, end);

		_setruby_before(p, top, end, LAYOUT_SETRUBY_F_DISABLE_BEFORE_NORMAL, is_bottom);

		//前の通常文字に掛ける

		if(_is_parent_space(top, end))
		{
			_jyukugo_reset(p, top, end);

			_setruby_before(p, top, end, 0, is_bottom);
		}
	}
	else
	{
		//収まっている場合、可能なら中央寄せ
		
		_adjust_center(p, top, end, is_bottom);
	}

	//位置調整

	_jyukugo_adjust_pos(p, top, end);
}

/** 熟語ルビ (中付き、行取得後のセット時) */

void layout_setruby_jyukugo_center(LayoutData *p,RubyData *pr,int flags)
{
	RubyData *top,*end;

	pr->flags |= RUBY_FLAG_ADJUST_CENTER;

	//通常処理 (後ろ掛けを優先)

	_setruby_jyukugo_center_normal(p, pr, flags);

	//最後のルビなら、全体を調整

	if(pr->flags & RUBY_FLAG_LAST)
	{
		//先頭と終端

		for(top = pr; top->flags & RUBY_FLAG_PREV; top = (RubyData *)top->i.prev);

		end = (RubyData *)pr->i.next;

		//調整

		_adjust_finish(p, top, end, FALSE);
	}
}


//=======================


/* 全体の幅を取得 */

static int _get_width(RubyData *top,RubyData *end)
{
	int w = 0;

	for(; top != end; top = (RubyData *)top->i.next)
		w += top->parent->width;

	return w;
}

/** (中付きの熟語ルビ) 行調整時、ルビを再セット
 *
 * top: 行頭のルビ。NULL で、熟語の先頭か行頭から end まで。
 * end: 行末のルビ。NULL で、top から終端まで。
 *
 * return: 以前の状態から増えた幅 */

int layout_ruby_jyukugo_center_reset(LayoutData *p,RubyData *top,RubyData *end)
{
	int lastw,w,is_bottom;

	//行末ルビの調整か
	
	is_bottom = !top;

	//先頭と終端

	if(!top)
	{
		for(top = end; top->flags & RUBY_FLAG_PREV; top = (RubyData *)top->i.prev)
		{
			if(top->parent->flags & STR_FLAG_LINE_TOP) break;
		}
	}
	else
	{
		for(end = top; end->flags & RUBY_FLAG_NEXT; end = (RubyData *)end->i.next);
	}

	end = (RubyData *)end->i.next;

	//通常通りで再セット

	lastw = _get_width(top, end);

	_setruby_normal(p, top, end, is_bottom);

	//最終調整

	_adjust_finish(p, top, end, is_bottom);

	//増えた幅

	w = _get_width(top, end);
	w -= lastw;
	if(w < 0) w = 0;

	return w;
}


