/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

typedef struct _mZlib mZlib;

//1 = PDF のオブジェクトを圧縮しない
#define DEBUG_PDF_UNCOMP  0

#define PDF_WORKBUF_SIZE  8192

//オブジェクトアイテム

typedef struct
{
	mListItem i;

	int objno,
		objno_strm,		//親の圧縮オブジェクトの番号 (0 でなし)
		index;			//圧縮オブジェクトのインデックス
	uint32_t offset,	//ストリームオブジェクト時、オフセット位置
		flags;
}PDFObjItem;

#define PDFOBJITEM_F_PAGEOBJECT 1  //ページオブジェクト

//圧縮オブジェクトのアイテム

typedef struct
{
	mListItem i;

	int objno;
	int32_t offset;	//データの先頭からのオフセット
	PDFObjItem *piobj;
}PDFObjstrmItem;

//番号予約のデータ

typedef struct
{
	int objno;	//0 でなし
	uint32_t offset;	//各ファイル先頭からの位置
}PDFReservedObject;

//使用するフォントのアイテム (全体で固定)

typedef struct
{
	mListItem i;

	PDFReservedObject type0_h,	//* 0 の場合あり
		type0_v,	//*
		cidfont_h,	//*
		cidfont_v,	//*
		desc,
		subset,
		cmap,		//*
		hmetrics,	//*
		vmetrics;	//*
	Font *font;
}PDFFontItem;

#define PDF_FONT_USE_F_VERT  1
#define PDF_FONT_USE_F_HORZ  2

//リソース辞書のアイテム (PDF ファイルごとにリセット)

typedef struct
{
	mListItem i;

	int objno;
	uint32_t hash;
	char str[0];	//辞書文字列
}PDFResourceItem;

//出力ページの状態

typedef struct
{
	uint8_t	has_font,	//フォントが一つでも使われたか
		has_lastchar,	//前のテキスト文字があるか
		has_text,		//テキストオブジェクト内か
		flast_rotate;	//前の文字が90度回転か
	int first_size,
		font_ascent,	//現在のフォントサイズの、用紙単位での ascent
		font_descent,
		font_vmid,		//現在のフォントサイズの、縦書きの中央位置
		text_x,			//テキストの前回 Td 位置
		text_y,
		imgnum;			//使用画像の数
	uint32_t col;		//現在の塗りつぶし色
	ImageItem *img[4];	//使用される画像
}PDFPageState;

//PDF データ

typedef struct _PDFData
{
	int cur_objno,		//現在のオブジェクト番号
		start_objno,	//PDF ファイルごとで共通のオブジェクトを含んだ、最初のオブジェクト番号
		pageobj_num,	//ページオブジェクトの数
		paper_fullw,
		paper_fullh,
		objno_info,
		objno_root,
		objno_gs,
		objno_outline;	//0 でなし
	uint32_t offset_crossref,
		size_comstrm,
		size_comobjstrm;

	PDFReservedObject obj_rootpage,
		obj_curpage;	//現在ページ

	PDFPageState pagest;	//ページの状態

	mStr str_tmp;
	
	mList list_obj,		//すべてのオブジェクトのリスト
		list_objstrm,	//現在の圧縮オブジェクトのリスト
		list_font,		//使用するフォントのリスト
		list_resource;	//リソース辞書のリスト

	FILE *fp,			//PDF 出力
		*fpstrm,		//圧縮オブジェクトのデータ
		*fpcomp,		//zlib 圧縮の出力
		*fpconts,		//ページ内容
		*fpsubset,		//フォントサブセット出力
		*fpcomstrm,		//PDF 共通データ: ストリーム
		*fpcomobjstrm;	//PDF 共通データ: 圧縮オブジェクト
	uint8_t *workbuf;	//読み書き用バッファ
	mZlib *zlib;
	PDFObjItem *cur_objstrm;	//現在の圧縮オブジェクト (NULL でなし)
}PDFData;

/* フォント追加時 */

typedef struct
{
	Font *font;
	FontSubsetInfo *info;
	char *fontname;		//サブセットタグ付きのフォント名
	uint16_t *gidmap_outsrc;	//outGID -> srcGID
	PDFFontItem *item;
}PDFFontDat;

//-------------

/* sub */

PDFObjItem *pdf_add_object(PDFData *p,PDFReservedObject *obj);
PDFObjstrmItem *pdf_add_object_in_stream(PDFData *p,PDFReservedObject *obj,mlkbool fwrite);
void pdf_start_write_object(PDFData *p,PDFObjItem *pi);
void pdf_start_write_objstrm(PDFData *p,PDFObjstrmItem *pi);

void pdf_put_coord(FILE *fp,int val,mlkbool fspace);
void pdf_put_double(FILE *fp,double val,mlkbool fspace);

uint32_t pdf_read_file_buf(FILE *fp,int size,uint8_t **ppdst);
void pdf_copy_file(PDFData *p,FILE *fpin,FILE *fpout,int size);
int pdf_compress_file(PDFData *p,FILE *fp,int size);
void pdf_put_stream(PDFData *p,FILE *fpin,FILE *fpout,int size);
void pdf_put_stream_zlib(PDFData *p,FILE *fpin,FILE *fpout,int size);
void pdf_put_stream_compressed(PDFData *p);
void pdf_put_stream_file(PDFData *p,const char *filename);

void pdf_put_object_top(PDFData *p,PDFObjItem *pi,const char *type);
void pdf_put_crossref_stream(PDFData *p);
void pdf_put_objectstrem(PDFData *p);
void pdf_put_objectstrem_max(PDFData *p);

void pdf_put_string_utf16(PDFData *p,const char *name,const char *str);

void pdf_putadd_graphic_state(PDFData *p);
void pdf_putadd_info(PDFData *p);
void pdf_putadd_catalog(PDFData *p);
void pdf_putadd_root_pagenode(PDFData *p);
void pdf_putadd_outline(PDFData *p,int start,int end);

void pdf_put_draw(PDFData *p,FILE *fp,const char *format,...);
void pdf_add_image(PDFData *p,ImageItem *piimg);

/* font */

PDFFontItem *pdf_add_fontitem(PDFData *p,PDFFontDat *dat);
void pdf_put_fontinfo(PDFData *p,PDFFontDat *dat);
