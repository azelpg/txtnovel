/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * レイアウト処理: 文字列
 *********************************/

#include <stdio.h>

#include <mlk.h>
#include <mlk_list.h>

#include "app.h"
#include "setting.h"
#include "layout.h"
#include "font/font.h"


//-------------------

typedef struct
{
	LayoutData *layout;
	StrData *pstr;
}_addchardat;

//-------------------


/* strch に文字を追加 */

static void _func_add_strchar(BodyChar *pc,void *param)
{
	_addchardat *p = (_addchardat *)param;

	layout_str_addchar(p->layout, p->pstr, pc);
}

/** StrChar 追加
 *
 * [!] 各文字の詳細は、後で設定する */

StrChar *layout_str_addchar(LayoutData *p,StrData *pistr,BodyChar *ch)
{
	StrChar *pi;

	pi = (StrChar *)mListAppendNew(&p->list_strch, sizeof(StrChar));
	if(!pi) app_enderr_alloc();

	pi->gid = layout_get_char_to_gid(pistr->font->font, ch, &pi->code);
	pi->kenten = p->kenten;

	return pi;
}

/** StrData 追加
 *
 * [!] 行の取得時以外で使わないこと */

StrData *layout_str_add(LayoutData *p)
{
	StrData *pi;
	CaptionLayout *cl;
	
	pi = (StrData *)mListAppendNew(&p->list_str, sizeof(StrData));
	if(!pi) app_enderr_alloc();

	pi->bousen = p->bousen;

	if(p->linedat.caption)
	{
		//見出し
		// :見出しは常に行の最初に指定されている。

		cl = p->linedat.caption;

		pi->font = cl->font.item;
		pi->fontsize = cl->font.size;
	}
	else
	{
		//現在のフォント
		
		pi->font = p->font;
		pi->fontsize = p->fontsize;
	}

	return pi;
}

/** StrData 削除
 *
 * list: NULL で、list_str */

void layout_str_delete(LayoutData *p,mList *list,StrData *pstr)
{
	StrChar *pch,*next;
	int i;

	if(!list) list = &p->list_str;

	if(pstr->ruby)
		layout_ruby_delete(p, pstr->ruby);

	//

	pch = pstr->chtop;

	for(i = pstr->len; i; i--, pch = next)
	{
		next = (StrChar *)pch->i.next;

		mListDelete(&p->list_strch, MLISTITEM(pch));
	}

	mListDelete(list, MLISTITEM(pstr));
}

/** StrChar 追加前の位置から、最初のデータ取得 */

StrChar *layout_str_getchar_lasttop(LayoutData *p,mListItem *last)
{
	return (last)? (StrChar *)last->next: (StrChar *)p->list_strch.top;
}

/** StrData 追加前の位置から、最初のデータ取得
 *
 * return: NULL で、1つも追加されていない */

StrData *layout_str_getdat_lasttop(LayoutData *p,mListItem *last)
{
	if(!last)
		return (StrData *)p->list_str.top;
	else if(last == p->list_str.bottom)
		return NULL;
	else
		return (StrData *)last->next;
}

/** コマンドの後の複数文字を、一体として str に追加 */

StrData *layout_str_add_chars_pack(LayoutData *p)
{
	StrData *pi;
	mListItem *last;
	_addchardat dat;

	pi = layout_str_add(p);

	last = p->list_strch.bottom;

	dat.layout = p;
	dat.pstr = pi;

	pi->len = body_readchars(&p->buf, _func_add_strchar, &dat);

	pi->chtop = layout_str_getchar_lasttop(p, last);

	return pi;
}

/** ベタ組での文字列幅をセット。親字間もクリアする */

void layout_str_reset_width(LayoutData *p,StrData *pstr)
{
	StrChar *pch = pstr->chtop;
	int i;

	pstr->width = 0;

	for(i = pstr->len; i; i--, pch = (StrChar *)pch->i.next)
	{
		pstr->width += pch->width;

		pch->sp_before = pch->sp_after = 0;
	}
}


//========================
// 追加 + 設定
//========================


/** 文字列に1文字のみを追加 & 設定 */

StrData *layout_str_addset_one(LayoutData *p,BodyChar *ch)
{
	StrData *pstr;
	int cno;

	//文字クラスは、Unicode から取得

	if(ch->type == BODYCHAR_TYPE_UNICODE)
		cno = layout_get_char_class(ch->code, (p->layout->break_type == LAYOUT_BREAK_BOOK));
	else
		cno = 0;

	//

	pstr = layout_str_add(p);

	pstr->classno = cno;
	pstr->class_flags = layout_get_class_flags(cno);
	pstr->chtop = layout_str_addchar(p, pstr, ch);
	pstr->len = 1;

	//

	layout_str_set_normal(p, pstr);
	layout_str_set_sub_yakumotu(p, pstr, ch);

	return pstr;
}

/** 通常文字を、一体として扱う範囲で追加
 *
 * 対象が1文字だったとしても、そのまま追加
 *
 * type: [1]分離禁止文字 [2]欧文 */

void layout_str_addset_area(LayoutData *p,int type)
{
	BodyChar *pc = &p->bchar;
	StrData *pstr;
	StrChar *pstrch;
	uint32_t pcode;
	int ret;

	pstr = layout_str_add(p);

	pstr->classno = (type == 1)? STRCLASS_NOSEP: STRCLASS_EURO;
	pstr->class_flags = layout_get_class_flags(pstr->classno);

	while(1)
	{
		//現在の文字を追加

		pstrch = layout_str_addchar(p, pstr, pc);

		if(!pstr->chtop) pstr->chtop = pstrch;

		pstr->len++;

		//分離禁止は2文字のみ
	
		if(type == 1 && pstr->len == 2) break;

		//次の文字
		
		pcode = pc->code;
		
		body_getchar(&p->buf, pc);
	
		if(type == 1)
			ret = bodych_is_nosep_next(pc, pcode);
		else
			ret = bodych_is_euro(pc);

		//一体として扱わない文字なら終了
		// :現在の文字をそのまま次の文字として扱う

		if(!ret)
		{
			pc->next = 0;
			break;
		}
	}

	//設定

	layout_str_set_normal(p, pstr);
}

/** 濁点・圏点の文字を追加/設定
 *
 * code: Unicode/CID
 * fkenten: TRUE で圏点 */

StrData *layout_str_addset_subchar(LayoutData *p,StrData *ins,
	uint32_t code,FontItem *font,int32_t fontsize,int fkenten)
{
	StrData *pi;
	StrChar *pch;
	BodyChar ch;
	
	pi = (StrData *)mListInsertNew(&p->list_str, MLISTITEM(ins), sizeof(StrData));
	pch = (StrChar *)mListAppendNew(&p->list_strch, sizeof(StrChar));

	if(!pi || !pch) app_enderr_alloc();

	pi->chtop = pch;
	pi->len = 1;
	pi->font = font;
	pi->fontsize = fontsize;

	if(fkenten) pch->flags |= STRCHAR_FLAG_KENTEN;

	//GID

	if(code & (1<<31))
	{
		ch.type = BODYCHAR_TYPE_CID;
		ch.code = code & 0xffff;
	}
	else
	{
		ch.type = BODYCHAR_TYPE_UNICODE;
		ch.code = code;
	}

	pch->gid = layout_get_char_to_gid(font->font, &ch, &pch->code);

	//濁点結合文字がない場合、通常の濁点

	if(code == 0x3099 && !pch->gid)
	{
		ch.code =  u'゛';
		
		pch->gid = layout_get_char_to_gid(font->font, &ch, &pch->code);
	}

	//

	layout_str_set_normal(p, pi);

	return pi;
}


//========================
// 文字列と文字の設定
//========================


/** 横組/縦組の通常文字列を設定
 *
 * [!] 文字クラスは別途設定すること */

void layout_str_set_normal(LayoutData *p,StrData *pi)
{
	if(p->is_horz)
		layout_str_set_horz_normal(p, pi);
	else
		layout_str_set_vert_normal(p, pi);
}

/** (通常文字列/1文字) 約物などの場合、各設定
 *
 * 横組/縦組の通常設定を行った後。 */

void layout_str_set_sub_yakumotu(LayoutData *p,StrData *pi,BodyChar *ch)
{
	StrChar *pch;
	Font *font;
	FullWidthItem *fi;
	uint32_t *pcode;
	int cno,to_half;
	uint16_t gid;

	pch = pi->chtop;
	cno = pi->classno;
	font = pi->font->font;

	if(cno == STRCLASS_ENSPACE)
	{
		//欧文間隔: 横書きは1/3幅、縦書きは1/4幅
		
		pch->width = pi->width = (p->is_horz)? pi->fontsize / 3: pi->fontsize / 4;
	}
	else if(cno == STRCLASS_JASPACE)
	{
		//和字間隔: 全角幅

		pch->width = pi->width = pi->fontsize;
	}
	else if(pi->class_flags & STRCLASS_F_HALF_WIDTH)
	{
		//--- 約物、半角幅対象のもの

		//常にフル幅にする対象の場合、除外
		// :ch は常に Unicode

		fi = p->layout->item_fullwidth;

		if(fi)
		{
			for(pcode = fi->code; *pcode; pcode++)
			{
				if(*pcode == ch->code)
					return;
			}
		}

		//半角幅にできるか

		to_half = FALSE; //全角幅を半角幅にするか

		if(!p->is_horz)
			to_half = TRUE;
		else
		{
			//横組

			if(pi->font->flags & FONTITEM_FLAGS_HORZ_HALF)
				//強制的に半角幅
				to_half = TRUE;
			else
			{
				gid = font_get_gsub(font, FONT_DATA_GSUB_HWID, pch->gid);

				if(gid != pch->gid)
				{
					//GSUB 'hwid'
					// :全角幅のグリフも登録されている
					
					pch->gid = gid;

					pch->width = pch->raw_width = font_regist_glyph(font, gid, pch->code, pi->fontsize, FALSE);
					pi->width = pch->width;
				}
				else if(font_get_gid_list(font, FONT_DATA_GPOS_HALT, pch->gid))
				{
					//GPOS 'halt' にある

					to_half = TRUE;
				}
				else
					return;
			}
		}

		//半角幅対象

		pi->flags |= STR_FLAG_HW;

		//全角幅→半角幅

		if(to_half)
		{
			pch->width = pi->width = pi->fontsize / 2;

			//始め括弧 (配置時に位置をずらす必要がある)

			if(cno == STRCLASS_START_KAKKO)
				pi->flags |= STR_FLAG_HW_POSUP;
		}
	}
}

/** 文字列を縦書き通常で設定
 *
 * [!] 文字クラスは別途設定すること */

void layout_str_set_vert_normal(LayoutData *p,StrData *pi)
{
	StrChar *pch;
	Font *font;
	int i,flags,vert;
	uint16_t gid,src;

	flags = p->layout->gsub;

	pch = pi->chtop;
	font = pi->font->font;

	for(i = pi->len; i; i--, pch = (StrChar *)pch->i.next)
	{
		gid = pch->gid;
		vert = TRUE;

		//

		if(pch->code >= 0x20 && pch->code <= 0x7e)
		{
			//--- 欧文 (ルビ親文字の場合、空白を含む)

			gid = font_get_gsub(font, FONT_DATA_GSUB_VERT, gid);

			//描画側で90度回転

			if(gid == pch->gid)
			{
				pch->flags |= STRCHAR_FLAG_VROTATE;
				vert = FALSE;
			}
		}
		else
		{
			//nlck

			if(flags & LAYOUT_GSUB_NLCK)
				gid = font_get_gsub(font, FONT_DATA_GSUB_NLCK, gid);

			//vkna
			// :縦書きグリフに置き換わるので、その場合 vert は必要ない

			src = gid;

			if(flags & LAYOUT_GSUB_VKNA)
				gid = font_get_gsub(font, FONT_DATA_GSUB_VKNA, gid);

			//vert

			if(src == gid)
				gid = font_get_gsub(font, FONT_DATA_GSUB_VERT, gid);

			//圏点は常にルビ扱い

			if(pch->flags & STRCHAR_FLAG_KENTEN)
				gid = font_get_gsub(font, FONT_DATA_GSUB_RUBY, gid);
		}

		//

		pch->gid = gid;

		pch->width = pch->raw_width = font_regist_glyph(font, gid, pch->code, pi->fontsize, vert);

		pi->width += pch->width;
	}
}

/** 文字列を、横書きとして設定
 *
 * [!] 文字クラスは別途設定すること */

void layout_str_set_horz_normal(LayoutData *p,StrData *pi)
{
	StrChar *pch;
	Font *font;
	int i,flags;
	uint16_t gid;

	pch = pi->chtop;
	font = pi->font->font;

	flags = p->layout->gsub;

	for(i = pi->len; i; i--, pch = (StrChar *)pch->i.next)
	{
		gid = pch->gid;
		
		//nlck

		if(flags & LAYOUT_GSUB_NLCK)
			gid = font_get_gsub(font, FONT_DATA_GSUB_NLCK, gid);

		//hkna

		if(flags & LAYOUT_GSUB_HKNA)
			gid = font_get_gsub(font, FONT_DATA_GSUB_HKNA, gid);

		//圏点は常にルビ扱い

		if(pch->flags & STRCHAR_FLAG_KENTEN)
			gid = font_get_gsub(font, FONT_DATA_GSUB_RUBY, gid);

		//

		pch->gid = gid;

		pch->width = pch->raw_width = font_regist_glyph(font, gid, pch->code, pi->fontsize, FALSE);

		pi->width += pch->width;
	}
}

/** 文字列を、縦中横として設定
 *
 * fprop: グリフ置き換えを行わない */

void layout_str_set_vert_tateyoko(LayoutData *p,StrData *pi,int fprop)
{
	StrChar *pch;
	Font *font;
	int i,len;
	uint16_t gid;

	pch = pi->chtop;
	len = pi->len;
	font = pi->font->font;

	pi->classno = STRCLASS_TATEYOKO;
	pi->width = pi->fontsize; //縦に全角幅

	for(i = 0; i < len; i++, pch = (StrChar *)pch->i.next)
	{
		gid = pch->gid;

		if(!fprop)
		{
			//3文字以上なら、1/3 幅
			
			if(len >= 3)
				gid = font_get_gsub(font, FONT_DATA_GSUB_TWID, gid);

			//グリフがそのままなら、半角幅

			if(gid == pch->gid)
				gid = font_get_gsub(font, FONT_DATA_GSUB_HWID, gid);
		}

		//

		pch->gid = gid;

		pch->width = pch->raw_width = font_regist_glyph(font, gid, pch->code, pi->fontsize, FALSE);

		pch->flags |= STRCHAR_FLAG_TATEYOKO;
	}
}

