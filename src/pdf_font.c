/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * PDF: フォント
 *********************************/

#include <stdio.h>

#include <mlk.h>
#include <mlk_file.h>
#include <mlk_stdio.h>
#include <mlk_list.h>
#include <mlk_unicode.h>

#include "app.h"
#include "pdf.h"
#include "setting.h"
#include "font/font.h"

#include "pdf_pv.h"


/* Type0 フォント辞書 */

static void _put_font_type0(PDFData *p,PDFFontDat *dat,int vert)
{
	FILE *fp = p->fpcomobjstrm;
	PDFReservedObject *obj;

	obj = (vert)? &dat->item->type0_v: &dat->item->type0_h;
	if(!obj->objno) return;

	obj->offset = ftell(fp);

	//

	fprintf(fp,
		"<</Type/Font/Subtype/Type0/BaseFont/%s/Encoding/Identity-%c/DescendantFonts[%d 0 R]",
		dat->fontname, (vert)? 'V': 'H',
		(vert)? dat->item->cidfont_v.objno: dat->item->cidfont_h.objno);

	if(dat->item->cmap.objno)
		fprintf(fp, "/ToUnicode %d 0 R", dat->item->cmap.objno);

	fputs(">>\n", fp);
}

/* CIDFont 辞書
 *
 * [!!] 縦書きと横書き両方で使うなら、一つの CIDFont を共通で使ってもよいが、
 * 横書きの幅が 0 のグリフで、一部問題がある。
 * 源ノ明朝など、U+3099 などの横書き幅が 0 になっている合成用のグリフを、縦書きと横書き両方で使う場合、
 * 共通の CIDFont 辞書で /W のデータを幅 0 にすると、縦書き時に半角幅分右にずれる。 */

static void _put_font_cidfont(PDFData *p,PDFFontDat *dat,int vert)
{
	FILE *fp = p->fpcomobjstrm;
	Font *font;
	PDFReservedObject *obj;

	obj = (vert)? &dat->item->cidfont_v: &dat->item->cidfont_h;
	if(!obj->objno) return;
	
	font = dat->font;

	obj->offset = ftell(fp);

	//----

	fprintf(fp, "<</Type/Font/Subtype/%s/BaseFont/%s/FontDescriptor %d 0 R",
		(font->flags & FONT_FLAGS_CFF)? "CIDFontType0": "CIDFontType2",
		dat->fontname, dat->item->desc.objno);

	fputs("/CIDSystemInfo<</Registry(Adobe)/Ordering(Identity)/Supplement 0>>", fp);

	//メトリクス

	if(vert)
	{
		fprintf(fp, "/DW2[%d -1000]", font_convert_value(font, font->ascent));

		if(dat->info->vert_num)
			fprintf(fp, "/W2 %d 0 R", dat->item->vmetrics.objno);
	}
	else
	{
		fputs("/DW 1000", fp);

		if(dat->info->horz_num)
			fprintf(fp, "/W %d 0 R", dat->item->hmetrics.objno);
	}

	fputs(">>\n", fp);
}

/* 横書きの幅を出力 */

static void _put_font_horz_width(PDFData *p,PDFFontDat *dat)
{
	FILE *fp = p->fpcomobjstrm;
	uint16_t *buf,*top,*cur;
	int num;

	dat->item->hmetrics.offset = ftell(fp);

	//

	buf = dat->info->buf_horz;
	num = dat->info->horz_num;

	fputc('[', fp);

	while(num)
	{
		//GID が連続している範囲
		// :top=先頭、buf=連続しない次の位置(終端含む)

		top = buf;
		buf += 2;
		num--;

		for(; num && buf[-2] + 1 == *buf; num--, buf += 2);

		//出力

		for(cur = top; cur != buf; )
		{
			//top = 先頭位置、cur = 次の位置

			top = cur;
			cur += 2;

			//

			if(cur != buf && cur[-1] == cur[1])
			{
				//同じ幅の範囲 (first last width)
				
				for(cur += 2; cur != buf && cur[-1] == cur[1]; cur += 2);

				fprintf(fp, "%d %d %d ", *top, cur[-2], (int16_t)top[1]);
			}
			else
			{
				//幅が異なる範囲を、連続で出力

				for(; cur != buf; cur += 2)
				{
					if(cur[-1] == cur[1])
					{
						cur -= 2;
						break;
					}
				}

				fprintf(fp, "%d[", *top);

				for(; top != cur; top += 2)
				{
					fprintf(fp, "%d", (int16_t)top[1]);

					if(top + 2 != cur)
						fputc(' ', fp);
				}

				fputc(']', fp);
			}
		}
	}

	fputs("]\n", fp);
}

/* 縦書きのメトリクスを出力 */

static void _put_font_vert_metrics(PDFData *p,PDFFontDat *dat)
{
	FILE *fp = p->fpcomobjstrm;
	uint16_t *buf,*top;
	int num;

	dat->item->vmetrics.offset = ftell(fp);

	//

	buf = dat->info->buf_vert;
	num = dat->info->vert_num;

	fputc('[', fp);

	while(num)
	{
		//GID が連続している範囲
		// :top=先頭、buf=連続しない次の位置(終端含む)

		top = buf;
		buf += 3;
		num--;

		for(; num && buf[-3] + 1 == *buf; num--, buf += 3);

		//出力
		// :poppler では、"start end width mid yorig" の形式だと、
		// :一部の文字で幅がおかしくなるので、使わない。

		fprintf(fp, "%d[", *top);

		for(; top != buf; top += 3)
		{
			fprintf(fp, "%d 500 %d", (int16_t)top[1], (int16_t)top[2]);

			if(top + 3 != buf)
				fputc(' ', fp);
		}

		fputc(']', fp);
	}

	fputs("]\n", fp);
}

/* FontDescriptor 辞書 */

static void _put_font_descriptor(PDFData *p,PDFFontDat *dat)
{
	FILE *fp = p->fpcomobjstrm;
	Font *font;
	int flags;

	font = dat->font;

	dat->item->desc.offset = ftell(fp);

	//

	flags = 4; //Symbolic
	if(font->flags & FONT_FLAGS_MONOSPACE) flags |= 1;

	fprintf(fp, "<</Type/FontDescriptor/FontName/%s/Flags %d/ItalicAngle 0/Ascent %d/Descent %d",
		dat->fontname, flags,
		font_convert_value(font, font->ascent),
		font_convert_value(font, font->descent));

	fprintf(fp, "/FontBBox[%d %d %d %d]",
		font_convert_value(font, font->box_minx),
		font_convert_value(font, font->box_miny),
		font_convert_value(font, font->box_maxx),
		font_convert_value(font, font->box_maxy));

	fprintf(fp, "/CapHeight %d/StemV 0",
		font_convert_value(font, font->cap_height));

	if(font->flags & FONT_FLAGS_CFF)
		fputs("/FontFile3", fp);
	else
		fputs("/FontFile2", fp);

	fprintf(fp, " %d 0 R>>\n", dat->item->subset.objno);
}

/* フォントデータ */

static void _put_font_data(PDFData *p,PDFFontDat *dat)
{
	FILE *fpout = p->fpcomstrm;
	char *fname;
	mlkfoff datsize;

	//サブセットのファイルを開く

	fname = app_set_tmpfile(TMPFILENAME_PDF_SUBSET);

	mGetFileSize(fname, &datsize);

	p->fpsubset = mFILEopen(fname, "rb");
	if(!p->fpsubset) app_enderrno(MLKERR_OPEN, fname);

	//
	
	dat->item->subset.offset = ftell(fpout);

	//出力

	fprintf(fpout, "%d 0 obj<<", dat->item->subset.objno);

	if(dat->font->flags & FONT_FLAGS_CFF)
		fputs("/Subtype/CIDFontType0C", fpout);
	else
		fprintf(fpout, "/Length1 %d", (int)datsize);

	pdf_put_stream_zlib(p, p->fpsubset, fpout, datsize);

	//

	fclose(p->fpsubset);
	p->fpsubset = 0;
}

/* ToUnicode CMap データ出力 */

static void _put_font_cmap(PDFData *p,PDFFontDat *dat)
{
	FILE *fp;
	Font *font;
	uint16_t *pmap,u16[2];
	uint32_t *puni,code;
	int gid,gnum;

	font = dat->font;
	gnum = font->subset_glyph_num;

	//------ fpconts に出力

	fp = p->fpconts;

	rewind(fp);

	fputs("/CIDInit /ProcSet findresource begin\n12 dict begin\nbegincmap\n", fp);

	fprintf(fp, "/CMapName /%s-UTF16 def\n", dat->fontname);

	fputs("/CMapType 2 def\n"
	"/CIDSystemInfo <<\n"
	" /Registry (Adobe)\n"
	" /Ordering (UCS)\n"
	" /Supplement 0\n"
	">> def\n"
	"1 begincodespacerange\n"
	"<0000> <FFFF>\n"
	"endcodespacerange\n", fp);

	fprintf(fp, "%d beginbfchar\n", gnum - 1);

	//outGID -> Unicode (UTF-16BE)
	// :GID = 0 は除く。
	// :コードが 0 の場合、'?' として扱う。

	puni = font->unimap;
	pmap = dat->gidmap_outsrc + 1;

	for(gid = 1; gid < gnum; gid++, pmap++)
	{
		code = puni[*pmap] & FONT_UNIMAP_MASK_CODE;
		if(!code) code = '?';

		fprintf(fp, "<%04X> ", gid);

		if(code <= 0xFFFF)
			fprintf(fp, "<%04X>\n", code);
		else
		{
			mUnicharToUTF16(code, u16, 2);
			
			fprintf(fp, "<%04X%04X>\n", u16[0], u16[1]);
		}
	}

	//

	fputs("endbfchar\n"
	"endcmap\n"
	"CMapName currentdict /CMap defineresource pop\n"
	"end\n"
	"end\n", fp);

	//---- fpcomstrm に出力

	dat->item->cmap.offset = ftell(p->fpcomstrm);

	fprintf(p->fpcomstrm, "%d 0 obj<<", dat->item->cmap.objno);
	
	pdf_put_stream_zlib(p, fp, p->fpcomstrm, -1);
}

/** フォントアイテムの追加 */

PDFFontItem *pdf_add_fontitem(PDFData *p,PDFFontDat *dat)
{
	PDFFontItem *pi;
	FontSubsetInfo *info;
	int no;

	pi = (PDFFontItem *)mListAppendNew(&p->list_font, sizeof(PDFFontItem));
	if(!pi) return NULL;

	pi->font = dat->font;

	//オブジェクト番号を予約

	no = p->start_objno;
	info = dat->info;

	if(info->has_horz)
	{
		pi->type0_h.objno = no++;
		pi->cidfont_h.objno = no++;
	}

	if(info->has_vert)
	{
		pi->type0_v.objno = no++;
		pi->cidfont_v.objno = no++;
	}

	pi->desc.objno = no++;
	pi->subset.objno = no++;

	if(!g_set->pdf.no_unicode)
		pi->cmap.objno = no++;

	if(info->horz_num)
		pi->hmetrics.objno = no++;

	if(info->vert_num)
		pi->vmetrics.objno = no++;

	//

	p->start_objno = no;

	return pi;
}

/** フォントの各情報を出力
 *
 * PDF 分割時は、すべてのフォントデータは共通で出力するので、
 * オブジェクト番号を予約して、各データは別ファイルに出力しておく。 */

void pdf_put_fontinfo(PDFData *p,PDFFontDat *dat)
{
	//Type 0

	_put_font_type0(p, dat, FALSE);
	_put_font_type0(p, dat, TRUE);

	//CIDFont

	_put_font_cidfont(p, dat, FALSE);
	_put_font_cidfont(p, dat, TRUE);

	//FontDescriptor

	_put_font_descriptor(p, dat);

	//横書きの幅

	if(dat->info->horz_num)
		_put_font_horz_width(p, dat);

	//縦書きのメトリクス

	if(dat->info->vert_num)
		_put_font_vert_metrics(p, dat);

	//フォントデータ
	// :fpconts にサブセットデータが出力されている

	_put_font_data(p, dat);

	//CMap

	if(dat->item->cmap.objno)
		_put_font_cmap(p, dat);
}

