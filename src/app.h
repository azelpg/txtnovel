/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

typedef struct _Image Image;
typedef struct _LayoutItem LayoutItem;

/* 目次アイテム */

typedef struct _IndexItem
{
	mListItem i;

	int32_t pageno,		//タイトルがあるページ位置
		ypos,			//タイトルのY位置
		save_pageno,	//記録したページ位置 (セットされなかった場合、0)
		pdf_objno,		//PDF ページオブジェクト番号 (アウトライン用)
		line_len,		//タイトル文字列の行頭からの長さ (基本フォントのサイズ単位に合わせている)
		line_remain;	//行の残り幅 (字上げなども処理済み)
	LayoutItem *layout;	//ページのレイアウト
	char *title;		//PDF アウトラインのタイトル (NULL でなし)
	char name[0];		//定義名
}IndexItem;

/* 画像のアイテム (画像ファイルごと) */

typedef struct _ImageItem
{
	mListItem i;

	int	pdf_objno,	//画像のオブジェクト番号 (0 で未処理)
		pdf_resno;	//リソースの番号 (全体として固定値)
	Image *img;		//使うときに読み込む
	char filename[0];	//ファイル名
}ImageItem;

#define IMAGE_ITEM_NOFILE  ((ImageItem *)1)

/* アプリデータ */

typedef struct
{
	uint16_t jpeg_quality,	//[0-100]
		jpeg_sampling;		//444,422,420

	int format,		//出力フォーマット (0 で指定なし)
		dpi,		//画像の DPI (0 でなし)
		width,		//画像の幅 (0 でなし)
		height,		//画像の高さ (0 でなし)
			//dpi,width,height のいずれかが 0 以外で、それを使う
		antialias,		//アンチエイリアス (0:指定なし、1:OFF、2:ON)
		tmpdir_pos,		//str_tmpdir、ファイル名を追加する時の区切り位置
		last_pageno,	//出力する最後のページ位置
		signal_flag;
	uint32_t flags;

	//経過用
	int prog_curno; //0 で経過処理なし
	uint32_t prog_pos[20];

	mStr str_input,		//入力ファイル名
		str_output,		//出力名 (拡張子は除く)
		str_tmpdir,		//作業用ディレクトリ (ファイル名もセットして使う)
		str_cachedir;	//フォントキャッシュディレクトリ

	mList list_index,	//目次タイトル
		list_image;		//画像ファイル

	//

	void *proc_ptr;	//処理中のデータ
	void (*free_proc)(void *ptr,int err); //処理中のデータを解放する関数
	void (*puterr_proc)(void *ptr); //処理中のエラーメッセージ表示 (通常のエラーの前に表示。NULL でなし)

	uint8_t *buf_body;	//本文データ
}AppData;

extern AppData *g_app;

/* 出力フォーマット
 * [変更時] output.c(拡張子), image.c(出力) */

enum
{
	OUT_FORMAT_NONE,
	OUT_FORMAT_PDF,
	OUT_FORMAT_BMP,
	OUT_FORMAT_PNG,
	OUT_FORMAT_JPEG,
	OUT_FORMAT_PSD
};

#define APP_FORMAT_LIST_STR  "pdf;bmp;png;jpeg;psd"

/* フラグ */
enum
{
	APP_FLAGS_NO_PDFSEP = 1<<0,	//PDF分割をしない
	APP_FLAGS_NO_CACHE = 1<<1,	//フォントのキャッシュを作成しない
	APP_FLAGS_FONTNAMES = 1<<2,	//フォント名の一覧を出力
	APP_FLAGS_DRAW_GRID = 1<<3	//グリッド描画を常に ON
};

/* マクロ */

#define APPERR_PUT_ERR    -1	//すでにエラーを表示した
#define APP_ERRMES_ALREADY	((const char *)1)  //すでにエラーを表示した

#define APPNAME_CREATOR  "txtnovel"

#define TMPFILENAME_LAYOUT       "layout"
#define TMPFILENAME_FONT_READ    "fontread"
#define TMPFILENAME_SUBSET_CFF   "subset_cff"
#define TMPFILENAME_OUTPUT_PDF   "pdf_output"
#define TMPFILENAME_PDF_OBJSTRM  "pdf_objstrm"
#define TMPFILENAME_PDF_CONTENTS "pdf_contents"
#define TMPFILENAME_PDF_COMP     "pdf_comp"
#define TMPFILENAME_PDF_SUBSET   "pdf_subset"
#define TMPFILENAME_PDF_COMMON_STREAM "pdf_com_stream"
#define TMPFILENAME_PDF_COMMON_OBJSTM "pdf_com_objstm"

/* func */

void app_free(int err);
void app_exit(int ret);
void app_end_proc(void);
void app_end_signal(void);

void app_puterr(const char *mes,const char *mes2);
void app_enderr(const char *mes);
void app_enderr2(const char *mes,const char *mes2);
void app_enderrno(mlkerr err,const char *mes2);
void app_enderr_alloc(void);
void app_puterrno(mlkerr err,const char *mes2);
const char *app_get_errmes(mlkerr err);

char *app_set_tmpfile(const char *fname);
void app_delete_tmpfile(const char *fname);

void app_progress_init(uint32_t max);
void app_progress_setpos(uint32_t pos);

IndexItem *app_search_index_item(const char *name);
IndexItem *app_add_index_item(const char *name,const char *title);
void app_index_set_pdf_objno(int page,int objno);

ImageItem *app_search_image(const char *filename);
ImageItem *app_add_image(const char *filename);
void app_image_load(ImageItem *pi);
void app_image_free(ImageItem *pi);
void app_image_reset_pdf(void);

