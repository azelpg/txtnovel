/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

enum
{
	LFILE_CMD_LAYOUT = -1,	//レイアウト変更
};

/* ページフラグ */
enum
{
	LFILE_PAGEF_COLUMN2 = 1<<0,		//2段組の場合、2段目の位置
	LFILE_PAGEF_HAVE_BODY = 1<<1,	//本文データがある
	LFILE_PAGEF_HAVE_NUMBER = 1<<2,	//ノンブルあり
	LFILE_PAGEF_HAVE_TITLE = 1<<3,	//柱あり
	LFILE_PAGEF_HAVE_INDEX = 1<<4,	//目次のタイトル行を含む
	LFILE_PAGEF_HAVE_IMAGE = 1<<5,	//画像あり
	LFILE_PAGEF_PDF_SEP = 1<<6		//PDF 分割
};

/* 文字フラグ */
enum
{
	LFILE_CHARF_HAVE_X_ABS = 1<<0,	//x位置あり (int32、絶対位置)
	LFILE_CHARF_HAVE_Y_ABS = 1<<1,	//y位置あり
	LFILE_CHARF_HAVE_X_REL = 1<<2,	//x位置 (uint16、相対位置)
	LFILE_CHARF_HAVE_Y_REL = 1<<3,	//y位置
	LFILE_CHARF_VROTATE = 1<<4,		//欧文を90度回転
	LFILE_CHARF_TATEYOKO = 1<<5,	//縦組において横書きをする (縦中横、柱など)
	LFILE_CHARF_PAPER_ABS = 1<<6,	//裁ち切り含む、用紙上の絶対位置
	LFILE_CHARF_DASH_CHAR = 1<<7	//全角ダッシュの文字
};

#define LFILE_CHARF_END  0xff
