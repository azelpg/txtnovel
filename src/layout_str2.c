/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * レイアウト処理: 段落の処理
 *********************************/

#include <stdio.h>

#include <mlk.h>

#include "app.h"
#include "setting.h"
#include "layout.h"



/** 1行分の、文字列間の規定の空き量をセット
 *
 * 折り返しなどは判断しない。 */

void layout_str_setline_rule_space(LayoutData *p)
{
	StrData *pstr,*next,*prev;
	int c,size;

	pstr = p->pagedat.pstr_top;
	prev = NULL;

	for(; pstr; prev = pstr, pstr = next)
	{
		next = (StrData *)pstr->i.next;
	
		//半角幅対象外

		if(!(pstr->flags & STR_FLAG_HW)) continue;

		//

		c = pstr->classno;
		
		size = pstr->fontsize;
		size = size - size / 2; //1/2 空き

		if(c == STRCLASS_START_KAKKO)
		{
			//始め括弧 (前が始め括弧/句点以外ならアキ)

			if(!prev || (prev->classno != STRCLASS_START_KAKKO && prev->classno != STRCLASS_KUTEN))
				pstr->sp_before = size;
		}
		else if(c == STRCLASS_END_KAKKO)
		{
			//終わり括弧 (次が読点/句点/始め括弧/終わり括弧以外ならアキセット)

			if(!next || !(next->class_flags & STRCLASS_F_ENDKAKKO_NEXT_BETA))
				pstr->sp_after = size;
		}
		else if(c == STRCLASS_KUTEN)
		{
			//句点 (次が終わり括弧以外ならアキセット)
			// :次が始め括弧の場合、間のアキは固定として扱うので、句点の後ろにアキを付けて、始め括弧の前には付けない。
			// :行末を除く句点の後ろアキは、常に固定アキとして扱われる。

			if(!next || next->classno != STRCLASS_END_KAKKO)
				pstr->sp_after = size;
		}
		else
		{
			//読点 (次が終わり括弧/始め括弧以外ならアキセット)

			if(!next || (next->classno != STRCLASS_END_KAKKO && next->classno != STRCLASS_START_KAKKO))
				pstr->sp_after = size;
		}
	}
}

/** 行頭の処理
 *
 * return: 実際の行頭が返る。NULL で終端 */

StrData *layout_proc_line_top(LayoutData *p,StrData *pstr)
{
	StrData *next;
	int n;

	//折り返し後の行頭が和字間隔で、前後が和字間隔でない場合、削除
	// :連続で和字間隔になっているものは除外

	if(pstr != p->pagedat.pstr_top //段落行頭ではない
		&& pstr->classno == STRCLASS_JASPACE
		&& ((StrData *)pstr->i.prev)->classno != STRCLASS_JASPACE //前が和字間隔でない
		&& (!pstr->i.next || ((StrData *)pstr->i.next)->classno != STRCLASS_JASPACE) ) //次が和字間隔でない
	{
		next = (StrData *)pstr->i.next;
		
		layout_str_delete(p, NULL, pstr);

		if(!next) return NULL;

		pstr = next;
	}

	//行頭フラグ

	pstr->flags |= STR_FLAG_LINE_TOP;

	if(pstr != p->pagedat.pstr_top)
		pstr->flags |= STR_FLAG_LINE_WRAP_TOP;

	//始め括弧が行頭に来た場合の規定空き
	// :段落行頭は、1/2 か全角アキを固定アキとする。折り返しは天付き

	if(pstr->classno == STRCLASS_START_KAKKO
		&& (pstr->flags & STR_FLAG_LINE_TOP)
		&& pstr->sp_before)
	{
		if(!(pstr->flags & STR_FLAG_LINE_WRAP_TOP))
			pstr->fix_before += (p->layout->indent_kakko == LAYOUT_INDENT_KAKKO_F1W0)? pstr->fontsize: pstr->sp_before;
	
		pstr->sp_before = 0;
	}

	//行頭・見出しの字下げ
	// :見出し時は固定字下げ。

	n = (pstr->flags & STR_FLAG_LINE_WRAP_TOP)
		? p->linedat.indent_head_wrap: p->linedat.indent_head_first;

	if(p->linedat.caption)
		n = p->linedat.caption->indent;

	pstr->fix_before += p->base_fontsize * n;

	//行頭のルビ調整

	if(pstr->ruby)
		layout_ruby_adjust_top(p, pstr->ruby);

	return pstr;
}


//===============================
// 行末処理
//===============================


/* 1文字進める
 *
 * [!] pstr_next が終端ではないこと */

static void _line_char_forward(LayoutData *p)
{
	p->linedat.pstr_bottom = p->linedat.pstr_next;
	p->linedat.pstr_next = (StrData *)(p->linedat.pstr_next)->i.next;
}

/* 1文字戻る
 *
 * return: TRUE で、これ以上戻れない */

static int _line_char_back(LayoutData *p)
{
	if(p->linedat.pstr_top == p->linedat.pstr_bottom)
		return TRUE;

	p->linedat.pstr_next = p->linedat.pstr_bottom;
	p->linedat.pstr_bottom = (StrData *)(p->linedat.pstr_bottom)->i.prev;

	return FALSE;
}

/* 指定位置を行末とした場合、規定空きを詰めて行長に収められるか
 *
 * 規定空きの 1/2 幅を、1/4 幅まで詰める。(横組は 0 まで詰め可)
 * ただし、行末の後ろは常に 0 まで詰められる。
 *
 * bottom: 行末の位置 (NULL で現在の行末)
 * enable_hang: 行末のぶら下げを有効にする
 * return: 1 で収まる */

static int _is_can_line_pack(LayoutData *p,StrData *bottom,mlkbool enable_hang)
{
	StrData *pstr;
	layoutpos len,after,before;
	int fhorz,freduce;

	//すべて詰めた場合の長さ

	if(!bottom) bottom = p->linedat.pstr_bottom;

	fhorz = p->is_horz;
	freduce = p->layout->reduce_flag;
	len = 0;

	for(pstr = p->linedat.pstr_top; 1; pstr = (StrData *)pstr->i.next)
	{
		//行末がぶら下げ可能なら、全体を長さに含めず終了

		if(pstr == bottom && enable_hang && (pstr->class_flags & STRCLASS_F_HANG) && p->layout->hang_flag)
			break;

		//固定長さ

		len += pstr->width + pstr->fix_before + pstr->fix_after;

		//追加する規定空き

		before = pstr->sp_before;
		after = pstr->sp_after;

		if(freduce && !(pstr->classno == STRCLASS_KUTEN && pstr != bottom))
		{
			//行末以外の句点の空きは固定なので、そのまま。
			//横書き時は 0 まで詰め可。
			//縦書き時は1/4幅まで可。

			if(fhorz)
				before = after = 0;
			else
				before /= 2, after /= 2;
		}

		len += before;

		//行末の後ろ空きは追加しない

		if(pstr == bottom) break;

		len += after;
	}

	//判定

	return (len <= p->linedat.line_length_max);
}

/* 現在行を詰めて、行末に1文字持ってこれるなら、進む
 *
 * (ぶら下げ有効)
 * return: 1 で進めた */

static int _run_line_pack(LayoutData *p)
{
	StrData *bottom;

	bottom = p->linedat.pstr_next;
	if(!bottom) return 0;

	if(_is_can_line_pack(p, bottom, TRUE))
	{
		_line_char_forward(p);
		return 1;
	}

	return 0;
}


//------------------------


/* 最初の行末位置を取得
 *
 * 行長を超えるか終端まで */

static void _line_find_bottom(LayoutData *p)
{
	StrData *pstr;
	layoutpos len,w,linemax;

	linemax = p->linedat.line_length_max;
	pstr = p->linedat.pstr_top;
	len = 0;

	for(; pstr; pstr = (StrData *)pstr->i.next)
	{
		//行末となる文字の後ろの規定空きは判定に含めない
		
		w = pstr->width + pstr->fix_before + pstr->fix_after + pstr->sp_before;

		if(len + w > linemax) break;

		len += w + pstr->sp_after;
	}

	if(pstr == p->linedat.pstr_top)
		app_enderr("1文字列だけで行長を超えています。\n行長が短いか、字上げが大きすぎます。");

	//

	p->linedat.pstr_next = pstr;
	p->linedat.pstr_bottom = (pstr)? (StrData *)pstr->i.prev: (StrData *)p->list_str.bottom;

	//1/2幅以上の空きがある場合、詰めて次の文字が入るなら、進める

	if(pstr && linemax - len >= p->base_fontsize / 2)
	{
		if(_is_can_line_pack(p, pstr, FALSE))
			_line_char_forward(p);
	}
}

/* 行末禁則の処理
 *
 * 行末が始め括弧以外になるまで、詰めるか戻る */

static void _line_proc_break_foot(LayoutData *p)
{
	StrData *bottom;

	bottom = p->linedat.pstr_bottom;

	//行末が始め括弧以外になるまで詰める

	do
	{
		if(!_run_line_pack(p))
		{
			//途中で詰められなくなった場合、最初の位置に戻って、1文字戻っていく。
			//この場合、次の行頭は初め括弧になるので、行頭禁則には当てはまらない。

			p->linedat.pstr_bottom = bottom;
			
			do
			{
				if(_line_char_back(p)) break;

			} while(p->linedat.pstr_bottom->class_flags & STRCLASS_F_BREAK_FOOT);

			return;
		}
	
	} while(p->linedat.pstr_bottom->class_flags & STRCLASS_F_BREAK_FOOT);

	//この時点で、すべて詰めることができた状態だが、次行が行頭禁則になる場合がある。
}

/* 次の行頭が、行頭禁則に当たるかどうか
 *
 * fnext: 次の行頭の、さらに次の文字を対象とする
 * return: 0=当てはまらない、1=当てはまる、2=当てはまる＆ぶら下げ可能 */

static int _is_next_char_break(LayoutData *p,int fnext)
{
	StrData *pstr;

	pstr = p->linedat.pstr_next;
	if(!pstr) return 0;

	if(fnext)
	{
		pstr = (StrData *)pstr->i.next;
		if(!pstr) return 0;
	}

	//[終わり括弧、ハイフン、区切り約物、中点、句点、読点、繰返し記号、長音記号、小書きの仮名]
	//※禁則が書籍用の場合、一部の文字はクラスが変更されている。

	if(pstr->class_flags & STRCLASS_F_HANG)
		return 2;
	else if(pstr->class_flags & STRCLASS_F_BREAK_HEAD)
		return 1;

	return 0;
}

/* 行末禁則・行頭禁則の処理 */

static void _line_proc_break(LayoutData *p)
{
	StrData *bottom,*first_bottom = NULL;
	int n,fhang;

	//行末禁則
	// :終端行で、初め括弧で終わっている場合は除外。

	if((p->linedat.pstr_bottom->class_flags & STRCLASS_F_BREAK_FOOT)
		&& p->linedat.pstr_next)
	{
		first_bottom = p->linedat.pstr_bottom;

		_line_proc_break_foot(p);
	}

	//次行の行頭禁則

	bottom = p->linedat.pstr_bottom;

	n = _is_next_char_break(p, FALSE);

	if(n)
	{
		//行頭禁則にならない位置まで詰める。
		// :すべて詰められた場合は、行末禁則には当たらない。

		fhang = 0; //一つ前がぶら下げか

		do
		{
			//ぶら下げ (句点/読点)
			// :行頭の次が行頭禁則でないこと。
			// :一つ前でぶら下げが行われていれば対象外。

			if(n == 2 && !fhang
				&& p->layout->hang_flag
				&& !_is_next_char_break(p, TRUE))
			{
				_line_char_forward(p);

				fhang = 1;

				break;
			}

			//詰められなかった場合、最初の位置に戻って、1文字戻っていく。
			// :行末禁則を行って詰めていた場合は、行末禁則前の位置まで戻る。
			// :戻った後が、行末禁則に当たる場合があるので、行末禁則も判定する。
			// :空ける場合、ぶら下げは行わない。

			if(!_run_line_pack(p))
			{
				fhang = 0;
				p->linedat.pstr_bottom = (first_bottom)? first_bottom: bottom;
				
				do
				{
					if(_line_char_back(p)) break;

				} while(_is_next_char_break(p, FALSE)
					|| (p->linedat.pstr_bottom->class_flags & STRCLASS_F_BREAK_FOOT));

				break;
			}

			//詰めた結果、ぶら下げになるか
			// :次がまだ行頭禁則の場合がある。
			// :一つ前がぶら下げで、更に詰めた結果、ぶら下げを解除する場合あり。

			fhang = (n == 2 && p->layout->hang_flag);

			//

			n = _is_next_char_break(p, FALSE);

		} while(n);

		//ぶら下げ

		if(fhang)
			p->linedat.pstr_bottom->flags |= STR_FLAG_HANGED;
	}
}

/* 行末ではみ出したルビの調整処理 */

static void _line_ruby_over(LayoutData *p)
{
	RubyData *pr;

	pr = p->linedat.pstr_bottom->ruby;

	if(!pr || !pr->ruby_over_after) return;

	//----- 後ろにはみ出している (モノルビ/熟語ルビ)

	//前掛けのみで再配置。収まるならそのまま
	// :次の文字にルビが掛からなくなるので、親文字の空きが増える場合あり

	if(layout_ruby_reset_before(p, pr) <= 0)
		return;

	//まだはみ出している場合、現在の行長を詰められるか

	if(_is_can_line_pack(p, NULL, FALSE))
		return;

	//詰められない場合、戻る
	// :行末禁則に当てはまる場合があるので、該当しなくなるまで戻る。

	do
	{
		if(_line_char_back(p)) break;

	} while(p->linedat.pstr_bottom->class_flags & STRCLASS_F_BREAK_FOOT);

	//行末だった文字は次の行頭に送られるので、次行のためにルビを再配置
	// :行頭ルビの調整は、次行で行われる。

	layout_ruby_reset_normal(p, pr, 0);
}

/* 行末が確定した後の行末処理 */

static void _line_set_bottom(LayoutData *p)
{
	StrData *pstr;
	RubyData *pr;

	pstr = p->linedat.pstr_bottom;

	pstr->flags |= STR_FLAG_LINE_BOTTOM;

	//ぶら下げ時

	if(pstr->flags & STR_FLAG_HANGED)
	{
		pstr->width = 0;
		pstr->sp_after = 0;
	}

	//行末ルビの調整
	// :この時点で、下にはみ出してはいない。

	pr = pstr->ruby;

	if(pr)
	{
		if(pr->flags & RUBY_FLAG_ADJUST_SP)
		{
			//肩付きで、親字間が空いている場合、行末に揃える。
			// :段落末の場合は、除外。
		
			if(p->linedat.pstr_next)
				layout_ruby_adjust_bottom(pr);
		}
		else if(pr->type == RUBY_TYPE_JYUKUGO_CENTER)
		{
			//中付きの熟語ルビの場合、熟語先頭から行末までを調整

			layout_ruby_jyukugo_center_reset(p, NULL, pr);
		}
	}
}

/** 行末を確定し、処理 */

void layout_proc_line_bottom(LayoutData *p)
{
	//最初の行末位置を取得

	_line_find_bottom(p);

	//見出しまたは目次の行の場合、折り返しはできない

	if(p->linedat.pstr_next
		&& (p->linedat.caption || p->linedat.index))
		app_enderr("見出し/目次の行は折り返して2行以上にできません");

	//行頭・行末禁則

	_line_proc_break(p);

	//行末のルビはみ出しを処理

	_line_ruby_over(p);

	//行末の処理

	_line_set_bottom(p);
}


//===============================
// 行調整 (規定の空きを詰める)
//===============================


/* 詰める対象の情報をセット
 *
 * x = 0:対象外、1:対象
 * y = 詰めの最大幅
 *
 * return: 詰め最大幅の合計 */

static layoutpos _packspace_set_info(LayoutData *p)
{
	StrData *pstr,*bottom;
	layoutpos w = 0,n,fhorz;

	pstr = p->linedat.pstr_top;
	bottom = p->linedat.pstr_bottom;
	fhorz = p->is_horz;

	for(; pstr != p->linedat.pstr_next; pstr = (StrData *)pstr->i.next)
	{
		if(pstr->sp_before || pstr->sp_after)
		{
			if(pstr->classno == STRCLASS_KUTEN && pstr != bottom)
				//行末以外の句点は、固定空きとする
				pstr->x = 0;
			else
			{
				//行末の後ろ空き or 横書き時は、常に 0 まで詰め可
			
				if(pstr == bottom && pstr->sp_after)
					n = pstr->sp_after;
				else if(fhorz)
					n = (pstr->sp_before)? pstr->sp_before: pstr->sp_after;
				else
					n = pstr->fontsize / 4;
			
				pstr->x = 1;
				pstr->y = n;

				w += n;
			}
		}
		else
			pstr->x = 0;
	}

	return w;
}

/* 規定の空きを詰める */

static void _line_pack_space(LayoutData *p,layoutpos remain)
{
	StrData *pstr;
	layoutpos max,n;
	double d;

	max = _packspace_set_info(p);

	//1文字ごとの割合

	d = (double)((max < remain)? max: remain) / max;

	//均等に詰める

	pstr = p->linedat.pstr_top;

	for(; pstr != p->linedat.pstr_next; pstr = (StrData *)pstr->i.next)
	{
		if(pstr->x)
		{
			n = (layoutpos)(pstr->y * d + 0.5);

			if(n > remain) n = remain;

			if(pstr->sp_before)
				pstr->sp_before -= n;
			else
				pstr->sp_after -= n;

			remain -= n;
			if(remain <= 0) break;
		}
	}

	//余りが出た場合、全体を 1 ずつ減らす

	if(remain)
	{
		pstr = p->linedat.pstr_top;

		for(; pstr != p->linedat.pstr_next; pstr = (StrData *)pstr->i.next)
		{
			if(pstr->x)
			{
				if(pstr->sp_before)
				{
					pstr->sp_before--;
					remain--;
				}
				else if(pstr->sp_after)
				{
					pstr->sp_after--;
					remain--;
				}

				if(!remain) break;
			}
		}
	}
}


//===============================
// 行調整 (字間を空ける)
//===============================


/* 空ける対象の情報をセット
 *
 * x = 0:対象外、1:欧文間隔、2:他、空けられる対象
 * y = 空きの最大幅
 *
 * return: 対象の数 */

static int _addspace_set_info(LayoutData *p)
{
	StrData *pstr;
	int c,flag,pflag,cprev,num;

	pstr = p->linedat.pstr_top;
	pflag = 1;
	cprev = 0;
	num = 0;

	for(; pstr != p->linedat.pstr_next; pstr = (StrData *)pstr->i.next)
	{
		c = pstr->classno;
		flag = 0;
		
		if(c == STRCLASS_ENSPACE)
		{
			//欧文間隔
			
			pstr->x = 1;
			pstr->y = pstr->width - pstr->fontsize / 2;
			num++;
		}
		else
		{
			//空けられない文字か
			
			flag = (pstr->class_flags & STRCLASS_F_NO_ADD_SPACE);

			//行頭以外で、前後の文字が
			//[始め括弧/終わり括弧/句点/読点/中点/ハイフン/区切り約物/和字間隔] 以外の場合。
			//また、分離禁止文字/ルビ文字間。

			if((!pflag && !flag)
				&& !(c == STRCLASS_NOSEP && cprev == STRCLASS_NOSEP)
				&& !(c == STRCLASS_WITH_RUBY && cprev == STRCLASS_WITH_RUBY))
			{
				pstr->x = 2;
				pstr->y = pstr->fontsize / 4;
				num++;
			}
			else
				pstr->x = 0;
		}

		pflag = flag;
		cprev = c;
	}

	return num;
}

/* 指定タイプを規定の最大幅まで空ける */

static int _addspace_rule(LayoutData *p,int type,int remain)
{
	StrData *pstr;
	int max,n;
	double d;

	//空きの最大幅

	pstr = p->linedat.pstr_top;
	max = 0;

	for(; pstr != p->linedat.pstr_next; pstr = (StrData *)pstr->i.next)
	{
		if(pstr->x == type)
			max += pstr->y;
	}

	if(!max) return remain;

	//1文字ごとの割合

	d = (double)((max < remain)? max: remain) / max;

	//空ける

	pstr = p->linedat.pstr_top;

	for(; pstr != p->linedat.pstr_next; pstr = (StrData *)pstr->i.next)
	{
		if(pstr->x == type)
		{
			n = (layoutpos)(pstr->y * d + 0.5);

			pstr->fix_before += n;
			pstr->width_before += n;
			
			remain -= n;
		}
	}

	return remain;
}

/* すべての対象文字を均等に空ける
 *
 * ※空けられる対象がない場合、空かない。 */

static void _addspace_last(LayoutData *p,int remain,int num)
{
	StrData *pstr;
	int n,add;

	pstr = p->linedat.pstr_top;

	n = remain / num;
	remain -= n * num;

	for(; pstr != p->linedat.pstr_next; pstr = (StrData *)pstr->i.next)
	{
		if(pstr->x)
		{
			add = n + ((remain-- > 0)? 1: 0);
		
			pstr->fix_before += add;
			pstr->width_before += add;
		}
	}
}

/* 行長に合わせて文字列の字間を空ける */

static void _line_add_space(LayoutData *p,layoutpos remain)
{
	int num;

	num = _addspace_set_info(p);
	if(!num) return;

	//欧文間隔:規定
	
	remain = _addspace_rule(p, 1, remain);
	if(remain <= 0) return;

	//それ以外:規定

	remain = _addspace_rule(p, 2, remain);
	if(remain <= 0) return;

	//対象文字を残り均等に

	_addspace_last(p, remain, num);
}


//==========================
// 行の長さ調整
//==========================


/** 行末までの長さを取得
 *
 * 行末の規定空きも含む。
 * ぶら下げの句点・読点は、幅に含まれない (width = 0 になっている)。 */

layoutpos layout_get_line_length(LayoutData *p)
{
	StrData *pstr = p->linedat.pstr_top;
	layoutpos w = 0;

	for(; pstr != p->linedat.pstr_next; pstr = (StrData *)pstr->i.next)
	{
		w += pstr->width + pstr->fix_before + pstr->fix_after + pstr->sp_before + pstr->sp_after;
	}

	return w;
}

/** 行の長さを調整 */

void layout_set_line_length(LayoutData *p)
{
	layoutpos w,len;

	len = p->linedat.line_length_max;

	//長さを計算

	w = layout_get_line_length(p);

	//長さを調整

	if(w > len)
		//行長より長い場合、詰める
		_line_pack_space(p, w - len);
	else if(p->linedat.pstr_next && w < len)
		//段落末行以外で行長未満の場合、空ける
		_line_add_space(p, len - w);

	//行末の空きに字上げ幅を追加

	p->linedat.pstr_bottom->fix_after += p->linedat.indent_foot_width;
}


//===============================
// 見出し/目次の設定
//===============================
/* 行の調整後、描画位置セットの前 */


/* 見出しの設定
 *
 * return: TRUE で、見出しの一部が次のページに送られるので、ページを終了 */

static int _setline_caption(LayoutData *p)
{
	CaptionLayout *cl;
	int lnum,maxnum,curnum;

	cl = p->linedat.caption;
	maxnum = p->layout->line_num;

	//見出し以外の現在行数
	// :見出しの1行分はすでに加算されているので、含めない

	curnum = p->pagedat.linenum - 1;

	//データ上の行数をセット

	lnum = curnum + cl->before + cl->width + cl->after;

	p->pagedat.lineno += cl->before;
	p->pagedat.linenum = lnum;

	//実際のページの行数をセット

	if((!cl->sep && lnum < maxnum)
		|| (cl->sep && lnum <= maxnum))
	{
		//すべてページ内に収まる場合
		// :分割不可の場合、後ろに1行以上空いている必要がある

		p->pagedat.linenum_page = lnum;

		return FALSE;
	}
	else if(cl->sep && curnum + cl->before + cl->width <= maxnum)
	{
		//分割可で、after 以降を分割可能

		p->pagedat.linenum_page = maxnum;
	}
	else if(cl->sep && curnum + cl->before <= maxnum)
	{
		//分割可で、width は入らないが before まで入る
		// :後ろにアキが付く場合あり

		p->pagedat.linenum_page = curnum + cl->before;
	}
	else if(cl->sep && cl->before)
	{
		//分割可で、before があって、すべて入らない場合

		p->pagedat.linenum_page = maxnum;
	}
	else
	{
		//見出しをすべて次のページへ
		// :ページ上の行数は、見出しの1行分を減らす。

		p->pagedat.linenum_page--;

		//次のページに収まるか

		lnum -= p->pagedat.linenum_page;

		if((!cl->sep && lnum >= maxnum)
			|| (cl->sep && lnum > maxnum))
			app_enderr("見出しがページの行数に収まりません");
	}

	return TRUE;

}

/* 目次行のセット */

static void _setline_index(LayoutData *p)
{
	IndexItem *pi = p->linedat.index;
	layoutpos w,size;

	//長さ (基本フォントのサイズ単位に合わせる)
	// :行末に、字上げの幅分のアキがあるので、除外。

	w = layout_get_line_length(p) - p->linedat.indent_foot_width;
	size = p->base_fontsize;

	w = (w + size - 1) / size * size;

	//

	pi->pageno = p->pageno;
	pi->ypos = p->pagedat.lineno * p->layout->line_feed.val;
	pi->line_len = w;
	pi->line_remain = p->line_length - p->linedat.indent_foot_width - w;
	pi->layout = p->layout;

	if(pi->line_remain <= 0)
		app_enderr("目次行の残り幅がありません");

	//ページに目次がある

	p->pagedat.has_index = TRUE;
}

/** 見出し/目次の行のセット
 *
 * return: TRUE で、見出しを強制的に次のページへ */

int layout_setline_caption_index(LayoutData *p)
{
	int fnext = 0;

	//見出し

	if(p->linedat.caption)
		fnext = _setline_caption(p);

	//目次

	if(p->linedat.index)
		_setline_index(p);

	return fnext;
}


//===============================
// 行の描画位置セット
//===============================


/** 現在の段落の、StrData の描画位置をセット
 *
 * x = 文字送り方向、y = 行送り方向。
 * 行位置もセット。
 * 折り返しで次のページに行く分も含む。 */

void layout_setline_drawpos(LayoutData *p)
{
	StrData *pstr,*ptop = NULL;
	layoutpos x,y,n,feed,linelen;
	int align,flags,basesize,lineno,yalign;

	feed = p->layout->line_feed.val;
	linelen = p->line_length;
	basesize = p->base_fontsize;
	lineno = p->pagedat.lineno;

	x = 0;
	y = lineno * feed;

	//文字送り方向の揃え
	// :目次タイトル時は無効

	align = p->linedat.line_align;
	if(p->linedat.index) align = 0;

	//0=何もしない
	//1=フォントのサイズで中央揃え
	yalign = 1;

	//見出しの行揃え

	if(p->linedat.caption)
	{
		yalign = 0;

		//中央
		
		if(p->linedat.caption->align == CAPTION_LAYOUT_ALIGN_CENTER)
		{
			if(p->linedat.caption->width == 1)
				yalign = 1;
			else
			{
				n = layout_get_lines_width(p, p->linedat.caption->width);
				y += (n - basesize) / 2;
			}
		}
	}

	//

	for(pstr = p->pagedat.pstr_top; pstr; pstr = (StrData *)pstr->i.next)
	{
		flags = pstr->flags;

		//行頭
	
		if(flags & STR_FLAG_LINE_TOP)
			ptop = pstr;

		//前の空き

		x += pstr->fix_before + pstr->sp_before;

		//
	
		pstr->x = x;
		pstr->y = y;
		pstr->lineno = lineno;

		x += pstr->width + pstr->fix_after + pstr->sp_after;

		//縦組で半角幅の始め括弧の場合、全角グリフを半角扱いにするので、位置を 1/2 幅ずらす

		if(pstr->flags & STR_FLAG_HW_POSUP)
			pstr->x -= pstr->fontsize - pstr->width;

		//y 揃え
		
		if(yalign)
			pstr->y += (basesize - pstr->fontsize) / 2;

		//行末

		if(flags & STR_FLAG_LINE_BOTTOM)
		{
			//文字送り方向の揃え

			if(align && x < linelen)
			{
				n = (align == BODY_ALIGN_CENTER)? (linelen - x) / 2: linelen - x;
				
				for(; 1; ptop = (StrData *)ptop->i.next)
				{
					ptop->x += n;
					
					if(ptop == pstr) break;
				}
			}
		
			//
		
			y += feed;
			x = 0;
			lineno++;
		}
	}
}
