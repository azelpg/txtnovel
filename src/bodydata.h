/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

//===========================
// 本文データ
//===========================

#define BODYCHAR_FINISH    0xFFFF	//終端
#define BODYCHAR_CID       0xFFFE	//CID [+2byte]
#define BODYCHAR_HI_FIRST  0xD800	//[+2byte] UTF-16 low
#define BODYCHAR_HI_LAST   0xDBFF
#define BODYCHAR_CMD_FIRST 0xDC00	//command
#define BODYCHAR_CMD_LAST  0xDFFF
#define BODYCHAR_UVS       0xDFFF	//UVS [+6byte]
#define BODYCHAR_ENTER     '\n'

/* コマンド番号 */

enum
{
	BODYCMD_NOOP,   //@-
	BODYCMD_JRUBY,	//@r,@jr
	BODYCMD_CRUBY,	//@c,@cr
	BODYCMD_MRUBY,	//@mr
	BODYCMD_GRUBY,	//@gr
	BODYCMD_GRUBY2,	//@gr2
	BODYCMD_LRUBY,	//@lr
	BODYCMD_TY,		//@ty
	BODYCMD_TY_PROP,	//@typ
	BODYCMD_DAKUTEN,	//@dk
	BODYCMD_KENTEN,		//@kt
	BODYCMD_BOUSEN,		//@bs
	BODYCMD_CAPTION1,
	BODYCMD_CAPTION2,
	BODYCMD_CAPTION3,
	BODYCMD_LAYOUT,
	BODYCMD_FONT,
	BODYCMD_NEWPAGE,	//@np
	BODYCMD_NEWCOLUMN,	//@nd
	BODYCMD_NEWPAGE_ODD,	//@nt
	BODYCMD_NEWPAGE_EVEN,	//@nm
	BODYCMD_ALIGN,
	BODYCMD_BODYALIGN,
	BODYCMD_PAGE_COUNT,
	BODYCMD_PAGE_NUMBER,
	BODYCMD_INDENT_HEAD,
	BODYCMD_INDENT_FOOT,
	BODYCMD_IMAGE,
	BODYCMD_PDFSEP,
	BODYCMD_INDEX,
	BODYCMD_INDEX_PAGE,
	BODYCMD_TITLE
};

/* @align */
enum
{
	BODY_ALIGN_TOP,
	BODY_ALIGN_CENTER,
	BODY_ALIGN_BOTTOM
};

/* @pagenum */
enum
{
	BODY_PAGENUM_NORMAL,
	BODY_PAGENUM_YES,
	BODY_PAGENUM_NO,
	BODY_PAGENUM_HIDE
};

/* @img */
enum
{
	BODY_IMG_PAGE_CUR = 0,
	BODY_IMG_PAGE_INS,
	BODY_IMG_PAGE_INS_LEFT,
	BODY_IMG_PAGE_INS_RIGHT,

	BODY_IMG_FLAGS_PDFSEP = 1
};

//----------------

typedef struct
{
	int32_t type,
		next;		//次のデータを取得するか
	uint32_t code,	//Unicode/CID, コマンド番号
		uvs;		//UVS セレクタ
}BodyChar;

enum
{
	BODYCHAR_TYPE_FINISH,	//終端
	BODYCHAR_TYPE_ENTER,	//改行
	BODYCHAR_TYPE_UNICODE,
	BODYCHAR_TYPE_UVS,
	BODYCHAR_TYPE_CID,
	BODYCHAR_TYPE_COMMAND
};

enum
{
	ENDTYPE_NONE,
	ENDTYPE_FINISH,
	ENDTYPE_ENTER,
	ENDTYPE_NEWPAGE,
	ENDTYPE_NEWCOLUMN,
	ENDTYPE_NEWPAGE_ODD,
	ENDTYPE_NEWPAGE_EVEN,
	ENDTYPE_CMD_AFTER,		//コマンド前のデータを先に出力して、改ページ
	ENDTYPE_END_PAGE		//現在のページを出力せず、次のデータへ
};

//----------------

mlkbool body_is_finish(uint8_t *buf);
void body_getchar(uint8_t **ppbuf,BodyChar *dst);
mlkbool body_getchar_finish(uint8_t **ppbuf,BodyChar *ch);
int body_readchars(uint8_t **ppbuf,void (*func)(BodyChar *ch,void *param),void *param);
void *body_read_ptr(uint8_t **ppbuf);
void body_read_size(uint8_t **ppbuf,void *dst,int size);
int body_getchar_line(uint8_t **ppbuf,BodyChar *ch);

mlkbool bodych_is_cmd_new(BodyChar *p);
int bodych_get_type(BodyChar *p);
mlkbool bodych_is_nosep_next(BodyChar *p,uint32_t c1);
mlkbool bodych_is_euro(BodyChar *p);
