/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * レイアウト処理
 *********************************/

#include <stdio.h>
#include <math.h>

#include <mlk.h>
#include <mlk_file.h>
#include <mlk_list.h>

#include "app.h"
#include "setting.h"
#include "layout.h"
#include "font/font.h"



/* LayoutData 解放 */

static void _free_layout(void *ptr,int err)
{
	LayoutData *p = (LayoutData *)ptr;

	//成功時

	if(!err)
	{
		app_progress_init(p->write_pagenum);

		g_app->last_pageno = p->last_write_pageno;
	}

	//

	mListDeleteAll(&p->list_str);
	mListDeleteAll(&p->list_str2);
	mListDeleteAll(&p->list_strch);
	mListDeleteAll(&p->list_ruby);
	mListDeleteAll(&p->list_rubych);
	mListDeleteAll(&p->list_title);
	mListDeleteAll(&p->list_image);

	if(p->fpout)
	{
		fclose(p->fpout);
		
		if(err) mDeleteFile(TMPFILENAME_LAYOUT);
	}

	mFree(p);
}

/* LayoutData 初期化 */

static LayoutData *_init_layout(void)
{
	LayoutData *p;
	char *fname;
	int w;

	p = (LayoutData *)mMalloc0(sizeof(LayoutData));
	if(!p) app_enderr_alloc();

	g_app->proc_ptr = p;
	g_app->free_proc = _free_layout;

	//

	p->buf = g_app->buf_body;
	p->pageno = 1;

	//レイアウト

	p->layout = g_set->layout_base;

	layout_change_layout(p);

	//出力ファイル

	fname = app_set_tmpfile(TMPFILENAME_LAYOUT);

	p->fpout = fopen(fname, "w+b");
	if(!p->fpout)
		app_enderrno(MLKERR_OPEN, fname);

	//ベースレイアウトの、基本版面サイズ (ノンブル・柱用)

	w = layout_get_lines_width(p, 0);

	if(p->is_horz)
	{
		p->base_layout_width = p->line_length;
		p->base_layout_height = w;
	}
	else
	{
		p->base_layout_width = w;
		p->base_layout_height = p->line_length;

		if(p->is_column)
			p->base_layout_height += p->layout->sp_column.val + p->line_length;
	}

	return p;
}

/** 本文データから、全ページレイアウトして、作業ファイルに出力 */

void app_output_layout(void)
{
	LayoutData *p;

	p = _init_layout();

	layout_proc_all(p);

	app_end_proc();
}


//======================
// sub
//======================


/** 現在ページが奇数位置かどうか
 *
 * 単一ページでは、常に奇数位置。 */

mlkbool layout_is_page_odd(LayoutData *p)
{
	return ((p->pageno & 1) || g_set->page_type == PAGETYPE_ONE);
}

/** (行取得中) 現在の処理行において、文字があるか */

mlkbool layout_has_curline_char(LayoutData *p)
{
	return (p->pagedat.pstr_top != (StrData *)p->list_str.bottom);
}

/** ページが新規の状態かどうか
 *
 * 改行による空行はデータに含むので、空ではない。
 * 2段組で下の位置にある場合、空ではない。 */

mlkbool layout_is_empty_page(LayoutData *p)
{
	return (!p->pagedat.linenum && !p->list_strch.top
		&& (!p->is_column || p->cur_column == 0));
}

/** ページ/段に文字があるかどうか
 *
 * 改行による空行、ノンブルなどは含まない。 */

mlkbool layout_has_page_char(LayoutData *p)
{
	return (p->list_strch.top != 0);
}

/** ページ位置を次の新しいページへ */

void layout_next_page(LayoutData *p)
{
	p->pageno++;
	p->cur_column = 0;
}

/** レイアウト変更時、現在の値をセット */

void layout_change_layout(LayoutData *p)
{
	LayoutItem *pl = p->layout;

	p->is_horz   = (pl->mode == LAYOUT_MODE_HORZ);
	p->is_column = (pl->mode == LAYOUT_MODE_VERT2);
	p->cur_column = 0;

	p->font = pl->basefont.item;
	p->fontsize = p->base_fontsize = pl->basefont.size;

	p->base_fontheight = (p->is_horz)? p->layout->basefont.height: p->base_fontsize;

	p->rubyfont = pl->ruby_font.item->font;
	p->ruby_fontsize = pl->ruby_font.size;

	p->line_length = pl->line_len.val;

	//字下げ/字上げ
	p->indent_head_first = pl->indent_head_first;
	p->indent_head_wrap = pl->indent_head_wrap;
	p->indent_foot = pl->indent_foot;

	p->align = pl->line_align;
	p->body_align = pl->body_align;
}

/** 行データに、現在の設定をセット
 *
 * 行の開始時、レイアウト変更時 */

void layout_set_linedat_cur(LayoutData *p)
{
	p->linedat.indent_head_first = p->indent_head_first;
	p->linedat.indent_head_wrap = p->indent_head_wrap;
	p->linedat.indent_foot = p->indent_foot;
	p->linedat.line_align = p->align;
}

/** ページデータに、現在の設定をセット
 *
 * ページの開始時、レイアウト変更時 */

void layout_set_pagedat_cur(LayoutData *p)
{
	p->pagedat.body_align = p->body_align;
}

/** 行送り方向の幅を取得
 *
 * num: 行数。0 で基本版面の行数 */

layoutpos layout_get_lines_width(LayoutData *p,int num)
{
	if(!num) num = p->layout->line_num;

	return p->layout->line_feed.val * (num - 1) + p->base_fontheight;
}

/** Unicode/CID から GID 取得
 *
 * pcode: GID->Unicode マップ用の Unicode が入る */

uint16_t layout_get_char_to_gid(Font *font,BodyChar *ch,uint32_t *pcode)
{
	uint16_t gid;

	if(ch->type == BODYCHAR_TYPE_CID)
	{
		//CID

		gid = font_get_cid_to_gid(font, ch->code);
		*pcode = 0;
	}
	else
	{
		//Unicode
		
		if(ch->type == BODYCHAR_TYPE_UVS)
			gid = font_get_uvs_gid(font, ch->uvs, ch->code);
		else
			gid = font_get_unicode_gid(font, ch->code);

		*pcode = ch->code;
	}

	return gid;
}

