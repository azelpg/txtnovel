/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

#define CFF_KEY2(k)  ((12<<8)|(k))

/* INDEX データ */

typedef struct
{
	int count,			//個数
		offsize;		//オフセット値サイズ
	uint32_t data_size, //全データのサイズ
		data_pos,		//データの位置 (ファイル先頭からの位置)
		offset_pos,		//オフセットデータの位置
		next_pos;		//INDEX の次のデータの位置
}CFFIndex;

/* DICT 項目データ */

typedef struct
{
	mListItem i;
	uint16_t key,	//キー値
		len,		//データのバイト数 (値+キー)
		valnum;		//書き込む nval の個数 (0 で dat を使う)
	int32_t nval[3];	//キーの直前の3つの整数値 (n2 n1 n0 key)
	uint8_t dat[0];		//キー,値の生データ
}CFFDictItem;


//DICT 作成時の関数の戻り値
enum
{
	CFF_DICT_RET_NORMAL,	//元データをコピー or 新規値を処理(数値のバイト数は自動)
	CFF_DICT_RET_32BIT,		//個数分をすべて 32bit 値として強制出力 (一時書き込み)
	CFF_DICT_RET_NO_OUTPUT	//出力しない
};


typedef int (*FontIO_writeDICT_func)(CFFDictItem *pi,int pos,void *param);

/*---------------*/

mlkerr fontio_cff_start(FontIO *p);
mlkerr fontio_cff_move_cid_charset(FontIO *p,int *dst_cidnum);

/* read */

int fontio_cff_setpos(FontIO *p,uint32_t pos);
int fontio_cff_read_offset(FontIO *p,uint32_t *dst);
uint32_t fontio_cff_read_offset_buf(uint8_t **ppbuf,uint8_t offsize);

mlkerr fontio_cff_read_INDEX(FontIO *p,CFFIndex *dst,int skip);
mlkerr fontio_cff_read_INDEX_pos(FontIO *p,uint32_t offset,CFFIndex *dst);
mlkerr fontio_cff_goto_INDEX_data(FontIO *p,CFFIndex *index,int no,uint32_t *dst_size);
mlkerr fontio_cff_read_INDEX_offset_data(FontIO *p,CFFIndex *index,uint8_t **dst);
uint8_t *fontio_cff_get_INDEX_buf_data(CFFIndex *index,uint8_t *buf,int no,uint32_t *dst_size);
uint32_t fontio_cff_get_INDEX_buf_datasize_flag(CFFIndex *index,uint8_t *buf,const uint16_t *flagbuf);

mlkerr fontio_cff_read_DICT(FontIO *p,mList *list,uint32_t dictsize);
mlkerr fontio_cff_read_DICT_INDEX(FontIO *p,CFFIndex *index,int no,mList *list);
CFFDictItem *fontio_cff_search_DICT_key(mList *list,uint16_t key);
int fontio_cff_add_DICT_item(mList *list,uint16_t key,int v2,int v1,int v0,int vnum);

/* write */

uint8_t fontio_cff_calc_offsize(uint32_t size);
uint8_t *fontio_cff_setbuf_offset(uint8_t *buf,uint32_t offset,uint8_t offsize);
int fontio_cff_write_offset(FontIO *p,uint32_t offset,uint8_t offsize);

int fontio_cff_write_INDEX_top(FontIO *p,int count,uint8_t offsize);
mlkerr fontio_cff_write_INDEX_empty(FontIO *p);
mlkerr fontio_cff_write_INDEX_count1(FontIO *p,const void *buf,uint32_t size);
mlkerr fontio_cff_write_INDEX_copy(FontIO *p,CFFIndex *index);

mlkerr fontio_cff_create_DICT(mBuf *buf,mList *list,FontIO_writeDICT_func func,void *param);
