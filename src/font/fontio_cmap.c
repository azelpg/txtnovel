/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * FontIO: cmap
 *********************************/

#include <stdio.h>
#include <stdlib.h>

#include <mlk.h>
#include <mlk_util.h>

#include "fontio.h"


//------------------

static const uint8_t g_prio_plat0[] = {2,0,3,1};

//------------------


//========================
// Unicode -> GID
//========================


/* format 0: byte */

static mlkerr _read_format0(FontIO *p)
{
	uint8_t *buf,*ps;
	uint16_t *pd;
	int ret,i;

	if(fontio_seek(p, 4))
		return MLKERR_DAMAGED;

	ret = fontio_read_alloc2(p, 256 * 2, 256, &buf);
	if(ret) return ret;

	//byte -> uint16

	ps = buf + 255;
	pd = (uint16_t *)buf + 255;

	for(i = 256; i; i--, ps--, pd--)
		*pd = *ps;

	//書き込み

	fontio_write_h32(p, 256);
	fontio_write_h32(p, 0);

	fwrite(buf, 1, 512, p->fpout);

	fontio_write_h32(p, 0);

	//

	mFree(buf);

	return 0;
}

/* format 4: segment */

static mlkerr _read_format4(FontIO *p)
{
	uint16_t *mapbuf = NULL,*pd;
	uint16_t len,segcnt2,end,start,rangeoff,start_uc;
	int16_t delta;
	int ret,cnt,toppos,pos,mapnum;

	if(fontio_read_format(p, "h2sh6s", &len, &segcnt2)
		|| len < 14
		|| segcnt2 < 4)
		return MLKERR_DAMAGED;

	//endCode 以降を読み込み

	ret = fontio_readbuf_alloc_set(p, len - 14);
	if(ret) return ret;

	//Unicode 値の最小と最大値を取得
	// :startCode の先頭、endCode の終端の1つ前

	ret = MLKERR_DAMAGED;

	if(fontio_readbuf16_pos(p, segcnt2 - 4, &end)
		|| fontio_readbuf16_pos(p, segcnt2 + 2, &start_uc))
		goto ERR;

	mapnum = end - start_uc + 1;

	//マップ用バッファ

	mapbuf = (uint16_t *)mMalloc0(mapnum * 2);
	if(!mapbuf)
	{
		ret = MLKERR_ALLOC;
		goto ERR;
	}

	//---------

	toppos = 0;

	for(cnt = segcnt2 / 2; cnt; cnt--, toppos += 2)
	{
		if(fontio_readbuf16_pos(p, toppos, &end))
			goto ERR;

		if(end == 0xffff) break;

		pos = toppos + segcnt2 + 2;
		
		if(fontio_readbuf16_pos(p, pos, &start)
			|| fontio_readbuf16_pos(p, pos + segcnt2, &delta)
			|| fontio_readbuf16_pos(p, pos + segcnt2 * 2, &rangeoff))
			goto ERR;

		//

		pd = mapbuf + (start - start_uc);

		if(!rangeoff)
		{
			//delta

			for(; start <= end; start++)
				*(pd++) = (start + delta) & 0xffff;
		}
		else
		{
			//配列

			if(fontio_readbuf_setpos(p, pos + segcnt2 * 2 + rangeoff))
				goto ERR;

			for(; start <= end; start++, pd++)
			{
				if(fontio_readbuf16(p, pd)) goto ERR;
			}
		}
	}

#if 0
	pd = mapbuf;

	for(cnt = 0; cnt < mapnum; cnt++, pd++)
	{
		if(*pd) printf("U+%04X: %d\n", start_uc + cnt, *pd);
	}
#endif

	//書き込み

	fontio_write_h32(p, mapnum);
	fontio_write_h32(p, start_uc);
	
	fwrite(mapbuf, 1, mapnum * 2, p->fpout);

	fontio_write_h32(p, 0);

	//

	ret = 0;

ERR:
	mFree(p->buftop);
	mFree(mapbuf);

	return ret;
}

/* format 6: trimmed */

static mlkerr _read_format6(FontIO *p)
{
	uint16_t start,cnt;
	int ret;

	if(fontio_read_format(p, "4shh", &start, &cnt))
		return MLKERR_DAMAGED;

	ret = fontio_readbuf_alloc_set(p, cnt * 2);
	if(ret) return ret;

	mConvertBuf_BE_16bit(p->buftop, cnt);

	//

	fontio_write_h32(p, cnt);
	fontio_write_h32(p, start);

	fwrite(p->buftop, 1, cnt * 2, p->fpout);

	fontio_write_h32(p, 0);

	//

	mFree(p->buftop);

	return 0;
}

/* (format12) 情報取得
 *
 * start が 0x10000 以上の位置で区切る。
 *
 * dst_start: 配列マップの最初の Unicode
 * dst_mapnum: 配列マップの数
 * dst_remnum: 0x10000 以上の残りのデータ数 */

static void _format12_getinfo(FontIO *p,uint32_t num,
	uint32_t *dst_start,uint32_t *dst_mapnum,uint32_t *dst_remnum)
{
	uint32_t *ps,start,end;

	ps = (uint32_t *)p->buftop;
	start = ps[0];
	end = 0;

	for(; num; num--, ps += 3)
	{
		if(ps[0] > 0xffff) break;

		end = ps[1];
	}

	//

	*dst_remnum = num;

	if(!end)
	{
		//0x10000 以上から始まっている

		*dst_start = 0;
		*dst_mapnum = 0;
	}
	else
	{
		*dst_start = start;
		*dst_mapnum = end - start + 1;
	}
}

/* format 12: unicode full */

static mlkerr _read_format12(FontIO *p)
{
	uint16_t *mapbuf = NULL,*pd;
	uint32_t *ps,num,start,end,gid,start_uc,mapnum,remnum;
	int ret;

	if(fontio_read_format(p, "10si", &num))
		return MLKERR_DAMAGED;

	//読み込み

	ret = fontio_readbuf_alloc_set(p, num * 12);
	if(ret) return ret;

	mConvertBuf_BE_32bit(p->buftop, num * 3);

	//情報取得

	_format12_getinfo(p, num, &start_uc, &mapnum, &remnum);

	//マップバッファ

	if(mapnum)
	{
		mapbuf = (uint16_t *)mMalloc0(mapnum * 2);
		if(!mapbuf)
		{
			mFree(p->buftop);
			return MLKERR_ALLOC;
		}
	}

	//配列マップ

	fontio_write_h32(p, mapnum);
	fontio_write_h32(p, start_uc);

	ps = (uint32_t *)p->buftop;

	for(; num; num--, ps += 3)
	{
		start = ps[0];

		if(start > 0xffff)
		{
			fwrite(mapbuf, 1, mapnum * 2, p->fpout);
			break;
		}
		
		end = ps[1];
		gid = ps[2];
		
		pd = mapbuf + (start - start_uc);
		
		for(; start <= end; start++, gid++)
			*(pd++) = gid;
	}

	mFree(mapbuf);

	//0x10000 以上は範囲値

	fontio_write_h32(p, remnum);

	for(; num; num--, ps += 3)
	{
		if(ps[1] > 0x10ffff) break;
	
		fontio_write_b24(p, ps[0]);
		fontio_write_b24(p, ps[1]);
		fontio_write_h16(p, ps[2]);
	}

	//

	mFree(p->buftop);

	return 0;
}

/* format 14: UVS */

static mlkerr _read_format14(FontIO *p)
{
	uint32_t len,num,code,offset,toppos,num2;
	uint16_t gid;
	int ret;

	if(fontio_read_format(p, "ii", &len, &num))
		return MLKERR_DAMAGED;

	ret = fontio_readbuf_alloc_set(p, len - 10);
	if(ret) return ret;

	//

	toppos = 0;
	ret = MLKERR_DAMAGED;
	
	for(; num; num--, toppos += 11)
	{
		if(fontio_readbuf_setpos(p, toppos)
			|| fontio_readbuf24(p, &code)
			|| fontio_readbuf_seek(p, 4)
			|| fontio_readbuf32(p, &offset))
			goto ERR;

		if(!offset) continue;

		if(fontio_readbuf_setpos(p, offset - 10)
			|| fontio_readbuf32(p, &num2))
			goto ERR;

		fontio_write_b24(p, code); //selector
		fontio_write_h32(p, num2);

		for(; num2; num2--)
		{
			if(fontio_readbuf24(p, &code)
				|| fontio_readbuf16(p, &gid))
				goto ERR;

			fontio_write_b24(p, code);
			fontio_write_h16(p, gid);
		}
	}

	fontio_write_b24(p, 0);

	//

	ret = 0;
ERR:
	mFree(p->buftop);

	return ret;
}

/* マップ読み込み
 *
 * out_offset: 成功した場合のみセットする */

static mlkerr _read_map(FontIO *p,uint32_t offset,int is_uvs,uint32_t *out_offset)
{
	uint16_t format;
	uint32_t outpos;
	int ret;

	if(fontio_setpos(p, offset)
		|| fontio_read16(p, &format))
		return MLKERR_DAMAGED;

	if((is_uvs && format != 14)
		|| (!is_uvs && format == 14))
		return MLKERR_UNSUPPORTED;

	outpos = ftell(p->fpout);

	switch(format)
	{
		case 0:
			ret = _read_format0(p);
			break;
		case 4:
			ret = _read_format4(p);
			break;
		case 6:
			ret = _read_format6(p);
			break;
		case 12:
			ret = _read_format12(p);
			break;
		case 14:
			ret = _read_format14(p);
			break;
		default:
			return MLKERR_UNSUPPORTED;
	}

	if(!ret)
	{
		*out_offset = outpos;

		//4byte境界

		fontio_write_align4(p);
	}

	return ret;
}


//========================


/* 比較関数 */

static int _cmpfunc_record(const void *ptr1,const void *ptr2)
{
	uint16_t *p1,*p2;
	int n1,n2;

	p1 = (uint16_t *)ptr1;
	p2 = (uint16_t *)ptr2;

	//plat: 3 < 0

	if(*p1 > *p2)
		return -1;
	else if(*p1 < *p2)
		return 1;

	//enc

	if(*p1 == 0)
	{
		//Unicode: 4 < 6 < 3 < 5

		n1 = g_prio_plat0[p1[1] - 3];
		n2 = g_prio_plat0[p2[1] - 3];
	}
	else
	{
		//Windows: 10 < 1

		n1 = (p1[1] == 1);
		n2 = (p2[1] == 1);
	}

	if(n1 < n2) return -1;
	else if(n1 > n2) return 1;

	return 0;
}

/* EncodingRecord を読み込み */

static mlkerr _read_record(FontIO *p,uint8_t **dstbuf,int *dstnum)
{
	uint8_t *buf,*ps,*pd,ver,num;
	uint16_t plat,enc;
	int ret,i,f,dnum;

	if(fontio_read_format(p, "hh", &ver, &num)
		|| ver != 0)
		return MLKERR_DAMAGED;

	//Record 読み込み

	ret = fontio_read_alloc(p, num * 8, &buf);
	if(ret) return ret;

	//BE -> HOST

	pd = buf;

	for(i = num; i; i--, pd += 8)
	{
		*((uint16_t *)pd) = mGetBufBE16(pd);
		*((uint16_t *)(pd + 2)) = mGetBufBE16(pd + 2);
		*((uint32_t *)(pd + 4)) = mGetBufBE32(pd + 4);
	}

	//非対応のものを詰める (Unicode 以外を削除)

	ps = pd = buf;
	dnum = 0;

	for(i = num; i; i--, ps += 8)
	{
		plat = *((uint16_t *)ps);
		enc  = *((uint16_t *)(ps + 2));

		f = 0;

		if(plat == 0)
			f = (enc >= 3 && enc <= 6);
		else if(plat == 3)
			f = (enc == 1 || enc == 10);

		if(f)
		{
			if(ps != pd)
				*((uint64_t *)pd) = *((uint64_t *)ps);

			pd += 8;
			dnum++;
		}
	}

	//優先度順に並べ替え

	qsort(buf, dnum, 8, _cmpfunc_record);

	//
	
	*dstbuf = buf;
	*dstnum = dnum;

	return 0;
}

/** 'cmap' から読み込んで出力
 *
 * offset は読み込まれなかった場合、0。 */

mlkerr fontio_cache_cmap(FontIO *p,uint32_t *offset_unimap,uint32_t *offset_uvs)
{
	FontTblInfo *ptbl;
	uint8_t *recbuf,*ps;
	uint16_t plat,enc;
	uint32_t offset;
	int ret,num,fout,flag;

	*offset_unimap = 0;
	*offset_uvs = 0;

	//

	ret = fontio_move_table(p, MLK_MAKE32_4('c','m','a','p'), &ptbl);
	if(ret) return ret;

	ret = _read_record(p, &recbuf, &num);
	if(ret) return ret;

	//サブテーブル処理
	// :非対応のフォーマットの場合、読み込まずに次へ

	fout = 0;  //出力済みデータのフラグ (1=unicode, 2=UVS)

	for(ps = recbuf; num && fout != 3; num--, ps += 8)
	{
		plat = *((uint16_t *)ps);
		enc  = *((uint16_t *)(ps + 2));
		offset = *((uint32_t *)(ps + 4)) + ptbl->offset;

		flag = (plat == 0 && enc == 5)? 2: 1;

		if(!(fout & flag))
		{
			ret = _read_map(p, offset, (flag == 2),
				(flag == 2)? offset_uvs: offset_unimap);

			if(ret == MLKERR_UNSUPPORTED)
				continue;
			else if(ret)
				goto ERR;
		
			fout |= flag;
		}
	}

ERR:
	mFree(recbuf);

	return ret;
}
