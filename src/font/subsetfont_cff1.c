/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * CFF 各処理
 ************************************/

#include <stdio.h>
#include <string.h>

#include <mlk.h>
#include <mlk_buf.h>
#include <mlk_list.h>
#include <mlk_stdio.h>

#include "fontio.h"
#include "fontio_cff.h"

#include "subsetfont.h"
#include "subsetfont_pv.h"
#include "subsetfont_cff.h"



//======================
// Top DICT
//======================


/** Top DICT 項目を読み込み & 調整 */

mlkerr subsetfont_cff_read_topDICTitem(SubsetFontCFF *p)
{
	mList *list = &p->list_topdic;
	CFFDictItem *pi,*next;
	int32_t key,n;

	for(pi = (CFFDictItem *)list->top; pi; pi = next)
	{
		next = (CFFDictItem *)pi->i.next;
	
		key = pi->key;

		if(key == 13 || key == 14 || key == 16 || key == CFF_KEY2(21) || key == CFF_KEY2(22))
		{
			//削除 (UniqueID, XUID, Encoding, PostScript, BaseFontName)

			mListDelete(list, MLISTITEM(pi));
		}
		else if(key == CFF_KEY2(30))
		{
			//ROS <CIDFont>

			p->is_cidfont = TRUE;

			pi->nval[2] = 391; //固定値にする (Adobe-Identity-0)
			pi->nval[1] = 392;
			pi->nval[0] = 0;
			pi->valnum = 3;
		}
		else if(key == 18)
		{
			//Private DICT のサイズとオフセット
			// :CID フォントでない場合、Font DICT 内にセットするので、削除

			p->offset_private = pi->nval[0];
			p->size_private = pi->nval[1];

			mListDelete(list, MLISTITEM(pi));
		}
		else if(key == CFF_KEY2(34))
		{
			//CIDCount (出力グリフ数で上書き)

			pi->nval[0] = p->b.out_glyph_num;
			pi->valnum = 1;
		}
		else if(key == 15 || key == 17 || key == CFF_KEY2(36) || key == CFF_KEY2(37))
		{
			//各オフセット値 (charset, CharStrings, FDArray, FDSelect)

			n = pi->nval[0];
			
			switch(key)
			{
				case 17: p->offset_charstrings = n; break;
				case CFF_KEY2(36): p->offset_fdarray = n; break;
				case CFF_KEY2(37): p->offset_fdselect = n; break;
			}

			pi->valnum = 1;
		}
	}

	//

	if(p->is_cidfont)
	{
		if(!p->offset_fdarray || !p->offset_fdselect)
			return MLKERR_DAMAGED;
	}
	else
	{
		//CID フォントでない場合、追加

		if(fontio_cff_add_DICT_item(list, CFF_KEY2(30), 391, 392, 0, 3) //ROS
			|| fontio_cff_add_DICT_item(list, CFF_KEY2(34), 0, 0, p->b.out_glyph_num, 1) //CIDCount
			|| fontio_cff_add_DICT_item(list, CFF_KEY2(36), 0, 0, 0, 1) //FDArray
			|| fontio_cff_add_DICT_item(list, CFF_KEY2(37), 0, 0, 0, 1)) //FDSelect
			return MLKERR_ALLOC;

		//ROS を先頭に

		pi = fontio_cff_search_DICT_key(list, CFF_KEY2(30));

		mListMoveToTop(list, MLISTITEM(pi));
	}

	return 0;
}


/* DICT データ作成の関数 */

static int _topdict_add_dict(CFFDictItem *pi,int pos,void *param)
{
	SubsetFontCFF *p = (SubsetFontCFF *)param;
	int32_t *pd;
	int key = pi->key;

	if(key == 15 || key == 17 || key == CFF_KEY2(36) || key == CFF_KEY2(37))
	{
		//charset, CharStrings, FDArray, FDSelect
		// :オフセット値は後で書き込むので、32bit 強制。書き込み位置を記録。

		switch(key)
		{
			case 15: pd = &p->outoffset_charset; break;
			case 17: pd = &p->outoffset_charstrings; break;
			case CFF_KEY2(36): pd = &p->outoffset_fdarray; break;
			default: pd = &p->outoffset_fdselect; break;
		}

		*pd = pos;

		return CFF_DICT_RET_32BIT;
	}

	return CFF_DICT_RET_NORMAL;
}

/** Top DICT INDEX 書き込み */

mlkerr subsetfont_cff_write_topDICT(SubsetFontCFF *p)
{
	mBuf buf;
	int ret,top;

	//DICT データ作成

	mBufInit(&buf);

	ret = fontio_cff_create_DICT(&buf, &p->list_topdic, _topdict_add_dict, p);
	if(ret) return ret;

	//INDEX 書き込み

	top = subsetfont_cff_get_outpos(p);

	ret = fontio_cff_write_INDEX_count1(p->b.io, buf.buf, buf.cursize);

	mBufFree(&buf);

	if(ret) return ret;

	//項目の書き込み位置を調整

	top += 3 + fontio_cff_calc_offsize(buf.cursize) * 2 + 1;

	p->outoffset_charset += top;
	p->outoffset_charstrings += top;
	p->outoffset_fdarray += top;
	p->outoffset_fdselect += top;
	
	return 0;
}


//=========================
// Font DICT 読み込み
//=========================


/* Font DICT 配列確保 */

static mlkerr _alloc_fontdict(SubsetFontCFF *p,int num)
{
	p->fontdict = (CFFFontDict *)mMalloc0(sizeof(CFFFontDict) * num);
	if(!p->fontdict) return MLKERR_ALLOC;

	p->fontdict_num = num;

	return 0;
}

/* Font DICT の指定位置に、値をセット
 *
 * sid: フォント名の SID (入力の値) */

static mlkerr _set_fontdict_val(SubsetFontCFF *p,int no,int sid,uint32_t offset_private,uint32_t size_private)
{
	CFFFontDict *fd;
	CFFDictItem *pi;
	int ret;

	fd = p->fontdict + no;

	fd->in_index = fd->out_index = no;
	fd->fontname_sid = sid;

	//Private DICT

	if(offset_private)
	{
		//DICT 読み込み
	
		if(fontio_cff_setpos(p->b.io, offset_private))
			return MLKERR_ALLOC;
	
		ret = fontio_cff_read_DICT(p->b.io, &fd->list_private, size_private);
		if(ret) return ret;

		//Local Subr

		pi = fontio_cff_search_DICT_key(&fd->list_private, 19); //Subr
		if(pi)
		{
			//INDEX 読み込み
			// :オフセット値は、Private DICT からの相対位置
			// :CharString データは後で読み込む
			
			ret = fontio_cff_read_INDEX_pos(p->b.io, offset_private + pi->nval[0], &fd->subr.index);
			if(ret) return ret;
		}
	}

	return 0;
}

/* <CIDFont> Font DICT をすべて読み込み */

static mlkerr _read_fontdict(SubsetFontCFF *p)
{
	CFFIndex index;
	mList list = MLIST_INIT;
	CFFDictItem *pi;
	int i,ret,sid;
	int32_t offset_priv,size_priv;

	//Font DICT INDEX 読み込み

	ret = fontio_cff_read_INDEX_pos(p->b.io, p->offset_fdarray, &index);
	if(ret) return ret;

	//確保

	ret = _alloc_fontdict(p, index.count);
	if(ret) return ret;

	//

	for(i = 0; i < index.count; i++)
	{
		//DICT 読み込み

		ret = fontio_cff_read_DICT_INDEX(p->b.io, &index, i, &list);
		if(ret) return ret;

		//FontName, Private の値取得

		sid = 0;
		offset_priv = 0;

		MLK_LIST_FOR(list, pi, CFFDictItem)
		{
			if(pi->key == CFF_KEY2(38))
				//FontName
				sid = pi->nval[0];
			else if(pi->key == 18)
			{
				//Private
				offset_priv = pi->nval[0];
				size_priv = pi->nval[1];
			}
		}

		mListDeleteAll(&list);

		//セット

		ret = _set_fontdict_val(p, i, sid, offset_priv, size_priv);
		if(ret) return ret;
	}

	return 0;
}

/** Font DICT 読み込み or セット */

mlkerr subsetfont_cff_read_fontDICT(SubsetFontCFF *p)
{
	int ret;

	if(p->is_cidfont)
		return _read_fontdict(p);
	else
	{
		//CID フォントでない場合、新規作成
		// :Private は、Top DICT の Private を使う

		ret = _alloc_fontdict(p, 1);
		if(ret) return ret;
		
		return _set_fontdict_val(p, 0, 0, p->offset_private, p->size_private);
	}
}


//=========================
// FDSelect 読み込み
//=========================


/* <CIDFont> FDSelect 読み込み */

static mlkerr _read_fdselect(SubsetFontCFF *p)
{
	FontIO *io = p->b.io;
	uint8_t *buf,format,index;
	uint16_t cnt,gid1,gid2;
	int gnum,len,fdnum;

	//FDSelect へ。format 読み込み

	if(fontio_cff_setpos(io, p->offset_fdselect)
		|| fontio_read(io, &format, 1))
		return MLKERR_DAMAGED;

	if(format != 0 && format != 3)
		return MLKERR_INVALID_VALUE;

	//入力グリフ分確保

	gnum = p->b.io->glyph_num;

	buf = p->fdselect_buf = (uint8_t *)mMalloc(gnum);
	if(!buf) return MLKERR_ALLOC;

	//読み込み ([srcGID] = index)

	if(format == 0)
	{
		//format 0: 配列

		if(fontio_read(io, buf, gnum))
			return MLKERR_DAMAGED;
	}
	else
	{
		//---- format 3: 範囲

		//数, 最初の GID

		if(fontio_read16(io, &cnt)
			|| fontio_read16(io, &gid1)
			|| gid1)
			return MLKERR_DAMAGED;

		//データ

		fdnum = p->fontdict_num;

		for(; cnt; cnt--)
		{
			if(fontio_read(io, &index, 1)
				|| fontio_read16(io, &gid2)
				|| index >= fdnum)
				return MLKERR_DAMAGED;

			len = gid2 - gid1;

			memset(buf, index, len);

			buf += len;
			gid1 = gid2;
		}
	}

	return 0;
}

/* <CIDFont> 出力用に調整
 *
 * fdselect は、使用しない GID のデータを削除。
 * それにより、使用されない FontDICT のデータを削除。 */

static mlkerr _set_out_fdselect(SubsetFontCFF *p)
{
	CFFFontDict *fdbuf,*fd,*fd_dst;
	uint8_t *buf,*pd,*tmpbuf;
	uint16_t *map;
	int i,src_fdnum,num,out_gnum;

	out_gnum = p->b.out_glyph_num;
	src_fdnum = p->fontdict_num;
	fdbuf = p->fontdict;
	buf = p->fdselect_buf;

	//fdselect 調整

	if(out_gnum != p->b.io->glyph_num)
	{
		//使用しない GID を詰める
		
		pd = buf;
		map = p->b.gidmap_outsrc;

		for(i = out_gnum; i; i--, map++)
			*(pd++) = buf[*map];

		//リサイズ

		buf = p->fdselect_buf = (uint8_t *)mRealloc(buf, out_gnum);
	}

	//実際に使用されている Font DICT のフラグを ON
	// :out_index = 0 で使われていない。
	// :作成時に out_index = in_index になっているので、クリア

	for(i = 0; i < src_fdnum; i++)
		fdbuf[i].out_index = 0;

	for(i = out_gnum, pd = buf; i; i--, pd++)
		fdbuf[*pd].out_index = 1;

	//使われていない Font DICT を削除して詰める
	// :out_index に、出力後のインデックス値をセット

	fd = fd_dst = fdbuf;
	num = 0;

	for(i = src_fdnum; i; i--, fd++)
	{
		if(fd->out_index)
		{
			//使用されている
			
			fd->out_index = num++;

			if(fd != fd_dst)
				*fd_dst = *fd;

			fd_dst++;
		}
		else
		{
			//削除

			subsetfont_cff_free_fontdict(fd);
		}
	}

	p->fontdict_num = num;

	//fdselect のインデックス値を置き換え
	// :tmpbuf = srcFD -> outFD のマップ

	tmpbuf = (uint8_t *)mMalloc0(src_fdnum);
	if(!tmpbuf) return MLKERR_ALLOC;

	for(i = p->fontdict_num, fd = fdbuf; i; i--, fd++)
		tmpbuf[fd->in_index] = fd->out_index;

	for(i = out_gnum, pd = buf; i; i--, pd++)
		*pd = tmpbuf[*pd];

	mFree(tmpbuf);

	return 0;
}

/** FDSelect を読み込んで、出力用に調整
 *
 * FDSelect = GID から FontDICT のインデックスを取得するためのマップ。
 * Font DICT も出力用に調整する。
 *
 * [!] Font DICT は先に読み込んでおく */

mlkerr subsetfont_cff_read_fdselect(SubsetFontCFF *p)
{
	int ret,i;
	CFFFontDict *fd;

	if(!p->is_cidfont)
	{
		//CID フォントでない場合、すべて 0

		p->fdselect_buf = (uint8_t *)mMalloc0(p->b.out_glyph_num);
		if(!p->fdselect_buf) return MLKERR_ALLOC;
	}
	else
	{
		//---- CID フォント

		//読み込み

		ret = _read_fdselect(p);
		if(ret) return ret;

		//調整

		ret = _set_out_fdselect(p);
		if(ret) return ret;
	}

	//Local Subr のデータを読み込み

	for(i = p->fontdict_num, fd = p->fontdict; i; i--, fd++)
	{
		ret = fontio_cff_read_INDEX_offset_data(p->b.io, &fd->subr.index, &fd->subr.buf);
		if(ret) return ret;
	}

	return 0;
}


//=========================
// String INDEX 読み書き
//=========================

typedef struct
{
	mListItem i;
	int len,
		out_sid;
	char str[0];
}StrItem;


/* バッファ or ファイルから読み込んで文字列追加
 *
 * str: NULL で、現在位置から読み込み */

static mlkerr _add_str_item_buf(SubsetFontCFF *p,mList *list,const char *str,int len)
{
	StrItem *pi;

	pi = (StrItem *)mListAppendNew(list, sizeof(StrItem) + len);
	if(!pi) return MLKERR_ALLOC;

	pi->len = len;
	pi->out_sid = 390 + list->num; //出力の SID (391〜。追加された順に)

	if(str)
		memcpy(pi->str, str, len);
	else
	{
		if(fontio_read(p->b.io, pi->str, len))
			return MLKERR_DAMAGED;
	}

	return 0;
}

/* 入力の SID から文字列追加 (SID 391 未満はそのまま)
 *
 * dst_sid: 出力の SID が入る */

static mlkerr _add_str_item_sid(SubsetFontCFF *p,mList *list,int sid,int *dst_sid)
{
	uint32_t size;
	int ret;

	if(sid < 391)
	{
		*dst_sid = sid;
		return 0;
	}

	ret = fontio_cff_goto_INDEX_data(p->b.io, &p->index_str, sid - 391, &size);
	if(ret) return ret;

	ret = _add_str_item_buf(p, list, NULL, size);
	if(ret) return ret;

	*dst_sid = ((StrItem *)list->bottom)->out_sid;

	return 0;
}

/* Top DICT で使われる SID の文字列を取得 & SID 置き換え */

static mlkerr _get_string_topdict(SubsetFontCFF *p,mList *list)
{
	CFFDictItem *pi;
	int ret,key,sid;

	MLK_LIST_FOR(p->list_topdic, pi, CFFDictItem)
	{
		key = pi->key;

		if(key <= 4 || key == CFF_KEY2(0))
		{
			//version,Notice,FullName,FamilyName,Weight,Copyright
			// :元の文字列を使う

			ret = _add_str_item_sid(p, list, pi->nval[0], &sid);
			if(ret) return ret;

			pi->nval[0] = sid;
			pi->valnum = 1;
		}

		//[!] ROS は Top DICT 読み込み時に 391,392,0 にセットされているので、そのまま
	}

	return 0;
}

/* <CIDFont> 各 Font DICT の FontName の文字列取得 & SID 置き換え */

static int _get_string_fontdict(SubsetFontCFF *p,mList *list)
{
	CFFFontDict *fd;
	int i,ret,sid;

	for(fd = p->fontdict, i = p->fontdict_num; i; i--, fd++)
	{
		ret = _add_str_item_sid(p, list, fd->fontname_sid, &sid);
		if(ret) return ret;

		fd->fontname_sid = sid;
	}

	return 0;
}

/* <非CIDフォント> FontDICT の FontName をセット
 *
 * Name INDEX のフォント名と同じにする。
 * [!] FamilyName/FullName は PostScript 名の形式ではないので、使わない。 */

static mlkerr _set_fontdict_string_nocid(SubsetFontCFF *p,mList *list)
{
	fontio_setpos(p->b.io, p->name_filepos);

	if(_add_str_item_buf(p, list, NULL, p->name_len))
		return MLKERR_ALLOC;

	p->fontdict->fontname_sid = ((StrItem *)list->bottom)->out_sid;

	return 0;
}

/** 使用する文字列のリストを取得
 *
 * 同時に、Top DICT/Font DICT の SID を置き換える。 */

mlkerr subsetfont_cff_get_string_list(SubsetFontCFF *p,mList *list)
{
	int ret;

	//"Adobe", "Identity" を先頭に追加

	if(_add_str_item_buf(p, list, "Adobe", 5)
		|| _add_str_item_buf(p, list, "Identity", 8))
		return MLKERR_ALLOC;

	//元が空の場合

	if(p->index_str.count == 0) return 0;

	//Top DICT の文字列

	ret = _get_string_topdict(p, list);
	if(ret) return ret;

	//Font DICT 内の FontName

	if(p->is_cidfont)
		ret = _get_string_fontdict(p, list);
	else
		ret = _set_fontdict_string_nocid(p, list);

	return ret;
}

/** リストから String INDEX 書き込み */

mlkerr subsetfont_cff_write_string_INDEX(SubsetFontCFF *p,mList *list)
{
	StrItem *pi;
	FontIO *io = p->b.io;
	uint32_t datasize,offset;
	uint8_t offsize;

	//全データサイズ計算

	datasize = 0;

	MLK_LIST_FOR(*list, pi, StrItem)
	{
		datasize += pi->len;
	}

	//オフセットサイズ

	offsize = fontio_cff_calc_offsize(datasize);

	//先頭書き込み

	if(fontio_cff_write_INDEX_top(io, list->num, offsize))
		return MLKERR_IO;

	//オフセット書き込み

	offset = 1;

	MLK_LIST_FOR(*list, pi, StrItem)
	{
		if(fontio_cff_write_offset(io, offset, offsize))
			return MLKERR_IO;
	
		offset += pi->len;
	}

	if(fontio_cff_write_offset(io, offset, offsize))
		return MLKERR_IO;

	//データ書き込み

	MLK_LIST_FOR(*list, pi, StrItem)
	{
		if(mFILEwriteOK(p->b.fpout, pi->str, pi->len))
			return MLKERR_IO;
	}

	return 0;
}

