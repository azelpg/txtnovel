/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/* テーブルデータ */

typedef struct
{
	uint32_t tag,cksum,offset,size,tmp;
}FontTblInfo;

/* テーブルデータバッファ */

typedef struct
{
	uint8_t *buf;
	uint32_t size,	//データサイズ
		size4;		//データサイズ (4byte 単位)
}FontTblBuf;

/* フォントI/O */

typedef struct _FontIO
{
	int is_cff,			//0:TrueType, 1:CFF
		glyph_num,		//maxp グリフ数
		cff_offsize;		//[in] CFF 読み込み用のオフセット値サイズ
	uint32_t cff_offset;	//[in] CFF のファイル位置

	FILE *fpin,
		*fpout;		//close 時に閉じない
	FontTblInfo *table;	//テーブルの配列。tag == 0 で終了

	//バッファ読み込み用
	uint8_t *bufcur,
		*buftop;
	uint32_t buf_size,
		buf_remain;
}FontIO;


/*--------------------*/

mlkerr fontio_openfile(FontIO **dst,const char *filename,int index);
void fontio_close(FontIO *p);

mlkerr fontio_put_names(const char *filename);

FontTblInfo *fontio_search_table(FontIO *p,uint32_t tag);
mlkerr fontio_move_table(FontIO *p,uint32_t tag,FontTblInfo **dst);
mlkerr fontio_move_table_checksize(FontIO *p,uint32_t tag,FontTblInfo **dst,uint32_t minsize);
mlkerr fontio_read_table(FontIO *p,uint32_t tag,FontTblInfo **dst_tbl,FontTblBuf *dst_buf,uint32_t minsize);

/* read */

int fontio_setpos(FontIO *p,uint32_t pos);
int fontio_seek(FontIO *p,int size);
int fontio_read16(FontIO *p,void *dst);
int fontio_read32(FontIO *p,void *dst);
int fontio_read(FontIO *p,void *buf,int size);
int fontio_read_format(FontIO *p,const char *format,...);
mlkerr fontio_read_alloc(FontIO *p,uint32_t size,uint8_t **dst);
mlkerr fontio_read_alloc2(FontIO *p,uint32_t allocsize,uint32_t readsize,uint8_t **dst);

void fontio_readbuf_set(FontIO *p,void *buf,uint32_t size);
mlkerr fontio_readbuf_alloc_set(FontIO *p,uint32_t size);
int fontio_readbuf_setpos(FontIO *p,uint32_t pos);
int fontio_readbuf_seek(FontIO *p,int pos);
int fontio_readbuf16(FontIO *p,void *dst);
int fontio_readbuf24(FontIO *p,void *dst);
int fontio_readbuf32(FontIO *p,void *dst);
int fontio_readbuf16_pos(FontIO *p,uint32_t pos,void *dst);

/* write */

void fontio_write_h16(FontIO *p,uint16_t v);
void fontio_write_h32(FontIO *p,uint32_t v);
void fontio_write_b24(FontIO *p,uint32_t v);
void fontio_write_h16_pos(FontIO *p,long pos,uint16_t v);
int fontio_write_b32_pos(FontIO *p,uint32_t pos,uint32_t v);
void fontio_write_align4(FontIO *p);

void fontio_calc_checksum(const uint8_t *buf,uint32_t size,uint32_t *cksum);
mlkerr fontio_write_table_buf(FontIO *p,FontTblInfo *table,FontTblBuf *buf);
mlkerr fontio_write_table_copy(FontIO *p,uint32_t tag);

/* table */

mlkerr fontio_table_head(FontIO *p,uint16_t *dst_emsize,int16_t *dst_box);
mlkerr fontio_table_head_locasize(FontIO *p,int *dst);
mlkerr fontio_table_os2(FontIO *p,int16_t *ascent,int16_t *descent,int16_t *capheight,uint16_t *fmono);
mlkerr fontio_table_hvhea_num(FontIO *p,int vert,uint16_t *num);
mlkerr fontio_table_name_postscript(FontIO *p,char **dst);

mlkerr fontio_cache_cmap(FontIO *p,uint32_t *offset_unimap,uint32_t *offset_uvs);
mlkerr fontio_cache_hvmtx(FontIO *p,int vert,uint16_t emsize,uint32_t *out_offset);
mlkerr fontio_cache_VORG(FontIO *p,uint32_t *out_offset);
mlkerr fontio_cache_GSUB(FontIO *p,const uint32_t *feature,uint32_t *out_offset,int num);
mlkerr fontio_cache_GPOS(FontIO *p,uint32_t *out_offset);
mlkerr fontio_cache_cidmap(FontIO *p,uint32_t *out_offset);
