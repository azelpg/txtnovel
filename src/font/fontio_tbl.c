/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * FontIO: テーブル
 *********************************/

#include <stdio.h>

#include <mlk.h>
#include <mlk_buf.h>
#include <mlk_util.h>
#include <mlk_unicode.h>
#include <mlk_opentype.h>

#include "fontio.h"
#include "fontio_cff.h"


/** 'head' から、emsize とボックス範囲を取得
 *
 * dst_box: minx,miny,maxx,maxy */

mlkerr fontio_table_head(FontIO *p,uint16_t *dst_emsize,int16_t *dst_box)
{
	int ret;

	ret = fontio_move_table_checksize(p, MLK_MAKE32_4('h','e','a','d'), NULL, 54);
	if(ret) return ret;

	if(fontio_seek(p, 18)
		|| fontio_read16(p, dst_emsize)
		|| fontio_read_format(p, "16s4h", dst_box))
		return MLKERR_DAMAGED;

	return 0;
}

/** 'head' から、loca オフセットサイズ取得 */

mlkerr fontio_table_head_locasize(FontIO *p,int *dst)
{
	int ret;
	uint16_t wd;

	ret = fontio_move_table_checksize(p, MLK_MAKE32_4('h','e','a','d'), NULL, 54);
	if(ret) return ret;

	if(fontio_seek(p, 50)
		|| fontio_read16(p, &wd))
		return MLKERR_DAMAGED;

	if(wd >= 2) return MLKERR_INVALID_VALUE;

	*dst = (wd == 0)? 2: 4;

	return 0;
}

/** 'OS/2' から情報取得
 *
 * capheight: なければ、0
 * fmono: 1 で等幅 */

mlkerr fontio_table_os2(FontIO *p,int16_t *ascent,int16_t *descent,int16_t *capheight,uint16_t *fmono)
{
	FontTblInfo *pt;
	int ret;
	uint8_t pn[10];

	*capheight = 0;
	*fmono = 0;

	ret = fontio_move_table(p, MLK_MAKE32_4('O','S','/','2'), &pt);
	if(ret) return ret;

	//panose

	if(fontio_seek(p, 32)
		|| fontio_read(p, pn, 10))
		return MLKERR_DAMAGED;

	//sTypoAscender, sTypoDescender

	if(fontio_seek(p, 68 - 42)
		|| fontio_read16(p, ascent)
		|| fontio_read16(p, descent))
		return MLKERR_DAMAGED;

	//CapHeight

	if(pt->size >= 96)
	{
		if(fontio_seek(p, 88 - 72)
			|| fontio_read16(p, capheight))
			return MLKERR_DAMAGED;
	}

	//等幅

	if(pn[0] == 2 || pn[0] == 4)
		//Latin, Latin Decorative
		*fmono = (pn[3] == 9);
	else if(pn[0] == 3 || pn[0] == 5)
		//Latin Hand Written, Latin Symbol
		*fmono = (pn[3] == 3);

	return 0;
}

/** hhea/vhea から、mtx 個数取得
 *
 * vert: 0 以外で、vhea
 * num: なければ、0 */

mlkerr fontio_table_hvhea_num(FontIO *p,int vert,uint16_t *num)
{
	int ret;

	*num = 0;

	ret = fontio_move_table_checksize(p,
		(vert)? MLK_MAKE32_4('v','h','e','a'): MLK_MAKE32_4('h','h','e','a'), NULL, 36);
	
	if(ret == MLKERR_UNFOUND)
		return 0;
	else if(ret)
		return ret;

	if(fontio_read_format(p, "34sh", num))
		return MLKERR_DAMAGED;

	return 0;
}



//=========================
// name
//=========================


/* 文字列を読み込み */

static mlkerr _read_name_string(FontIO *p,int platid,int len,char **dst)
{
	uint8_t *buf;
	int ret;

	ret = fontio_read_alloc2(p, len + 1, len, &buf);
	if(ret) return ret;

	//

	if(platid == 1)
	{
		//Mac
		
		buf[len] = 0;

		*dst = (char *)buf;
	}
	else
	{
		//UTF-16 BE

		mConvertBuf_BE_16bit(buf, len / 2);

		*dst = mUTF16toUTF8_alloc((mlkuchar16 *)buf, len / 2, NULL);

		mFree(buf);
	}

	return 0;
}

/* 名前を PostScript 名の形式に調整 (max = 127 文字) */

static void _adjust_name(char *name)
{
	char *ps,*pd,c;
	int len = 0;

	ps = pd = name;

	while(*ps && len < 127)
	{
		c = *(ps++);

		if((c >= 33 && c <= 126)
			&& c != '[' && c != ']' && c != '(' && c != ')'
			&& c != '{' && c != '}' && c != '<' && c != '>'
			&& c != '/' && c != '%')
		{
			*(pd++) = c;
			len++;
		}
	}

	*pd = 0;
}

/** 'name' から、フォントの PostScript 名取得
 *
 * nameID = 6 の文字列を取得。
 *
 * dst: 確保されたポインタが入る。*dst == NULL で見つからなかった or エラー */

mlkerr fontio_table_name_postscript(FontIO *p,char **dst)
{
	FontTblInfo *ptbl;
	char *name;
	uint16_t format,num,str_offset,v[6];
	int ret;

	*dst = NULL;

	ret = fontio_move_table_checksize(p, MLK_MAKE32_4('n','a','m','e'), &ptbl, 6);
	if(ret) return ret;

	fontio_read_format(p, "hhh", &format, &num, &str_offset);

	//NameRecord

	for(; num; num--)
	{
		if(fontio_read_format(p, "6h", v))
			return MLKERR_DAMAGED;

		if(v[3] == 6 //PostScript
			&& (v[0] == 0 || v[0] == 1 || v[0] == 3))
			break;
	}

	if(!num) return 0;

	//文字列読み込み

	if(fontio_setpos(p, ptbl->offset + str_offset + v[5]))
		return MLKERR_DAMAGED;

	ret = _read_name_string(p, v[0], v[4], &name);
	if(ret) return ret;

	//調整

	if(name)
	{
		_adjust_name(name);

		if(*name == 0)
		{
			mFree(name);
			name = NULL;
		}
	}

	//

	*dst = name;

	return 0;
}


//===========================
// hmtx/vmtx キャッシュ出力
//===========================


/* 書き込み */

static mlkerr _hvmtx_write(FontIO *p,uint16_t *buf,uint16_t emsize)
{
	mBuf arbuf;
	FILE *fp = p->fpout;
	uint16_t *ps,*ps1,*start_ps,wval[3];
	int i,arcnt,datnum,gcnt;
	long fpos;

	mBufInit(&arbuf);

	if(!mBufAlloc(&arbuf, 1024, 1024)) return MLKERR_ALLOC;

	//

	fpos = ftell(fp);

	fontio_write_h16(p, 0); //個数

	//

	ps = start_ps = buf;
	arcnt = 0;
	datnum = 0;

	for(i = p->glyph_num; i; )
	{
		//emsize でない範囲をスキップ
		// :先頭から emsize が連続する場合は除く

		if(*ps != emsize)
		{
			for(; i && *ps != emsize; i--, ps++);
		}

		//emsize が連続する範囲をスキップ

		ps1 = ps;

		for(; i && *ps == emsize; i--, ps++);

		//emsize が3個未満の連続
		// :次がある場合、非連続に含めて次を検索
		// :終端なら、残りをすべて非連続範囲にする

		if(ps - ps1 < 4)
		{
			if(i)
				continue;
			else
				ps1 = ps;
		}

		//---- emsize が4個以上連続している、または終端時

		//配列データ
		// :ps1 == start_ps の場合、emsize から始まる

		gcnt = ps1 - start_ps;

		if(gcnt)
		{
			wval[0] = start_ps - buf;
			wval[1] = wval[0] + gcnt - 1;
			wval[2] = arcnt;

			fwrite(wval, 1, 6, fp);

			wval[1] = gcnt;

			if(!mBufAppend(&arbuf, wval, 4)) //start,num
			{
				mBufFree(&arbuf);
				return MLKERR_ALLOC;
			}

			arcnt += gcnt;
			datnum++;
		}

		//ps1 - ps の範囲は省略 (emsize の連続)

		start_ps = ps;
	}

	//配列書き込み
	// [0] 位置 [1] 個数

	ps = (uint16_t *)arbuf.buf;

	for(i = arbuf.cursize / 4; i; i--, ps += 2)
		fwrite(buf + ps[0], 1, ps[1] * 2, fp);

	//個数

	fontio_write_h16_pos(p, fpos, datnum);

	fontio_write_align4(p);

	mBufFree(&arbuf);

	return 0;
}

/** hmtx/vmtx を読み込んでキャッシュ出力
 *
 * vert: 0 以外で vmtx
 * out_offset: なければ 0 */

mlkerr fontio_cache_hvmtx(FontIO *p,int vert,uint16_t emsize,uint32_t *out_offset)
{
	FontTblInfo *ptbl;
	uint8_t *buf,*ps,*pd;
	uint16_t num,val;
	int ret,i;

	*out_offset = 0;

	//個数

	ret = fontio_table_hvhea_num(p, vert, &num);
	if(ret) return ret;

	if(!num) return 0;

	//hmtx/vmtx 読み込み

	ret = fontio_move_table(p,
		(vert)? MLK_MAKE32_4('v','m','t','x'): MLK_MAKE32_4('h','m','t','x'), &ptbl);

	if(ret) return ret;

	if(ptbl->size < num * 4 + (p->glyph_num - num) * 2)
		return MLKERR_DAMAGED;

	//

	ret = fontio_read_alloc2(p, p->glyph_num * 2, ptbl->size, &buf);
	if(ret) return ret;

	//aw: BE->HOST, lsb を詰める

	ps = pd = buf;

	for(i = num; i; i--, ps += 4, pd += 2)
		*((uint16_t *)pd) = (ps[0] << 8) | ps[1];

	//残りを終端の値で埋める

	val = *((uint16_t *)pd - 1);

	for(i = p->glyph_num - num; i > 0; i--, pd += 2)
		*((uint16_t *)pd) = val;

	//書き込み

	*out_offset = ftell(p->fpout);

	ret = _hvmtx_write(p, (uint16_t *)buf, emsize);

	mFree(buf);

	return ret;
}


//=========================
// VORG 出力
//=========================


/** VORG を読み込んで出力
 *
 * out_offset: なければ 0 */

mlkerr fontio_cache_VORG(FontIO *p,uint32_t *out_offset)
{
	FontTblInfo *ptbl;
	uint8_t *buf;
	uint16_t maj,min,num;
	int ret;

	*out_offset = 0;

	//VORG

	ret = fontio_move_table(p, MLK_MAKE32_4('V','O','R','G'), &ptbl);

	if(ret == MLKERR_UNFOUND)
		return 0;
	else if(ret)
		return ret;

	//バージョン

	if(fontio_read_format(p, "hh", &maj, &min))
		return MLKERR_DAMAGED;

	if(!(maj == 1 && min == 0))
		return MLKERR_UNSUPPORTED;

	//読み込み

	ret = fontio_readbuf_alloc_set(p, ptbl->size - 4);
	if(ret) return ret;

	//16bit BE -> HOST

	buf = p->buftop;

	num = mGetBufBE16(buf + 2);

	mConvertBuf_BE_16bit(buf, 2 + num * 2);

	//書き込み

	*out_offset = ftell(p->fpout);

	fwrite(buf, 1, p->buf_size, p->fpout);

	fontio_write_align4(p);

	//

	mFree(p->buftop);

	return 0;
}


//=========================
// GSUB 出力
//=========================

static const uint32_t g_script_tags[] = {
	MLK_MAKE32_4('h','a','n','i'), MLK_MAKE32_4('k','a','n','a'),
	MLK_MAKE32_4('l','a','t','n'), 0
};


/* GSUB
 *
 * エラーの場合は、データなしとする */

static void _gsub_layout(FontIO *p,FontTblBuf *tblbuf,
	const uint32_t *feature,uint32_t *out_offset,int fnum)
{
	mOTLayout *pl;
	uint8_t *buf,*buf2;
	mOT_TABLE tbl,tbl_fea;
	uint32_t tag;
	int i,num;

	if(mOTLayout_new(&pl, tblbuf->buf, tblbuf->size, FALSE))
		return;

	//Script

	mOTLayout_searchScriptList(pl, g_script_tags, &tag);

	if(mOTLayout_getScript(pl, tag, &tbl)) goto ERR;

	//Lang

	if(mOTLayout_getLang(pl, 0, &tbl, &tbl)) goto ERR;

	//Feature

	for(i = 0; i < fnum; i++)
	{
		if(feature[i] == MLK_MAKE32_4('v','e','r','t'))
		{
			//vert 時、vert と vrt2 を読み込んで合成

			buf = buf2 = NULL;

			if(!mOTLayout_getFeature(pl, MLK_MAKE32_4('v','e','r','t'), &tbl, &tbl_fea))
			{
				if(mOTLayout_createGSUB_single(pl, &tbl_fea, &buf2)) continue;
			}
			
			if(!mOTLayout_getFeature(pl, MLK_MAKE32_4('v','r','t','2'), &tbl, &tbl_fea))
			{
				if(mOTLayout_createGSUB_single(pl, &tbl_fea, &buf))
				{
					mFree(buf2);
					continue;
				}
			}

			buf = mOpenType_combineData_GSUB_single(buf, buf2);
			if(!buf) continue;
		}
		else
		{
			//通常
			
			if(mOTLayout_getFeature(pl, feature[i], &tbl, &tbl_fea)
				|| mOTLayout_createGSUB_single(pl, &tbl_fea, &buf))
				continue;
		}

		//書き込み

		out_offset[i] = ftell(p->fpout);

		num = *((uint16_t *)buf);

		fwrite(buf, 1, 2 + num * 4, p->fpout);

		fontio_write_align4(p);

		mFree(buf);
	}

	//
ERR:
	mOTLayout_free(pl);
}

/** GSUB の複数 feature を読み込んで出力 (単一置換)
 *
 * feature: feature タグの配列 ('vert' は、vert+vert2)
 * out_offset: 配列。なければ 0
 * num: 配列の数 */

mlkerr fontio_cache_GSUB(FontIO *p,const uint32_t *feature,uint32_t *out_offset,int num)
{
	FontTblBuf tbuf;
	int ret;

	mMemset0(out_offset, num * 4);

	//GSUB 読み込み

	ret = fontio_read_table(p, MLK_MAKE32_4('G','S','U','B'), NULL, &tbuf, 0);

	if(ret == MLKERR_UNFOUND)
		return 0;
	else if(ret)
		return ret;

	//

	_gsub_layout(p, &tbuf, feature, out_offset, num);
	
	//

	mFree(tbuf.buf);

	return 0;
}


//=============================
// GPOS
//=============================


/* GPOS
 *
 * エラーの場合は、データなしとする */

static void _gpos_layout(FontIO *p,FontTblBuf *tblbuf,uint32_t *out_offset)
{
	mOTLayout *pl;
	uint8_t *buf;
	mOT_TABLE tbl,tbl_fea;
	uint32_t tag;
	int num;

	if(mOTLayout_new(&pl, tblbuf->buf, tblbuf->size, FALSE))
		return;

	//Script

	mOTLayout_searchScriptList(pl, g_script_tags, &tag);

	if(mOTLayout_getScript(pl, tag, &tbl)) goto ERR;

	//Lang

	if(mOTLayout_getLang(pl, 0, &tbl, &tbl)) goto ERR;

	//Feature

	if(mOTLayout_getFeature(pl, MLK_MAKE32_4('h','a','l','t'), &tbl, &tbl_fea)
		|| mOTLayout_createGPOS_single_gids(pl, &tbl_fea, &buf))
		goto ERR;

	//書き込み

	*out_offset = ftell(p->fpout);

	num = *((uint16_t *)buf);

	fwrite(buf, 1, 2 + num * 2, p->fpout);

	fontio_write_align4(p);

	mFree(buf);

	//
ERR:
	mOTLayout_free(pl);
}

/** GPOS 'halt' から、GID のリストを取得 */

mlkerr fontio_cache_GPOS(FontIO *p,uint32_t *out_offset)
{
	FontTblBuf tbuf;
	int ret;

	*out_offset = 0;

	//GPOS 読み込み

	ret = fontio_read_table(p, MLK_MAKE32_4('G','P','O','S'), NULL, &tbuf, 0);

	if(ret == MLKERR_UNFOUND)
		return 0;
	else if(ret)
		return ret;

	//

	_gpos_layout(p, &tbuf, out_offset);
	
	//

	mFree(tbuf.buf);

	return 0;
}


//=========================
// CID マップ出力
//=========================


/* マップを作成して書き込み */

static mlkerr _write_cidmap(FontIO *p,int format,uint16_t *buf,int cidnum,uint32_t *out_offset)
{
	FILE *fp = p->fpout;
	int i,gnum,j;
	uint16_t *ps,sid,num16,gid,wval[3];
	uint8_t num8;

	gnum = p->glyph_num;

	//------- マップの作成

	if(format == 0)
	{
		//format 0

		for(i = 1; i < gnum; i++)
		{
			if(fontio_read16(p, &sid))
				return MLKERR_DAMAGED;

			buf[sid] = i;
		}
	}
	else if(format == 1)
	{
		//format 1

		for(i = 1; i < gnum; )
		{
			if(fontio_read16(p, &sid)
				|| fontio_read(p, &num8, 1))
				return MLKERR_DAMAGED;

			for(j = num8 + 1; j; j--, i++, sid++)
				buf[sid] = i;
		}
	}
	else if(format == 2)
	{
		//format 2

		for(i = 1; i < gnum; )
		{
			if(fontio_read16(p, &sid)
				|| fontio_read16(p, &num16))
				return MLKERR_DAMAGED;

			for(j = num16 + 1; j; j--, i++, sid++)
				buf[sid] = i;
		}
	}

	//----- CID = GID の場合、なし

	ps = buf;

	for(i = 0; i < cidnum && *ps == i; i++, ps++);

	if(i == cidnum) return 0;

	//----- 書き込み

	*out_offset = ftell(fp);

	fontio_write_h16(p, 0); //個数一時

	ps = buf;
	num16 = 0;

	for(i = 0; i < cidnum; )
	{
		//GID 0 の部分をスキップ

		if(!(*ps))
		{
			for(; i < cidnum && !(*ps); i++, ps++);

			if(i == cidnum) break;
		}

		//GID が連続しない位置を検索
		// : j 〜 i - 1 の範囲は、GID が連続する

		gid = *(ps++);
		j = i++;

		for(; i < cidnum && ps[-1] + 1 == *ps; i++, ps++);
		
		//mDebug("%d-%d,%d\n", j, i - 1, gid);

		wval[0] = j;
		wval[1] = i - 1;
		wval[2] = gid;

		fwrite(wval, 1, 6, fp);

		num16++;
	}

	//個数

	fontio_write_h16_pos(p, *out_offset, num16);

	fontio_write_align4(p);

	return 0;
}

/** (CFF) CID->GID マップ出力
 *
 * out_offset: CFF ではない or CID フォントではない or CID = GID の場合、0 */

mlkerr fontio_cache_cidmap(FontIO *p,uint32_t *out_offset)
{
	uint16_t *mapbuf = NULL;
	int ret,cidnum;
	uint8_t format;

	*out_offset = 0;

	if(!p->is_cff) return 0;

	ret = fontio_cff_start(p);
	if(ret) return ret;

	//CID の場合、charset の位置へ移動 (CID 個数が 0 でなし)

	ret = fontio_cff_move_cid_charset(p, &cidnum);
	if(ret) return ret;

	if(!cidnum) return 0;

	//format

	if(fontio_read(p, &format, 1))
		return MLKERR_DAMAGED;

	//バッファ

	mapbuf = (uint16_t *)mMalloc0(cidnum * 2);
	if(!mapbuf) return MLKERR_ALLOC;

	//書き込み

	ret = _write_cidmap(p, format, mapbuf, cidnum, out_offset);

	mFree(mapbuf);

	return ret;
}


