/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * サブセットフォント作成
 ************************************/

#include <stdio.h>

#include <mlk.h>
#include <mlk_stdio.h>
#include <mlk_file.h>

#include "fontio.h"
#include "fontio_cff.h"

#include "subsetfont.h"
#include "subsetfont_pv.h"
#include "subsetfont_tt.h"
#include "subsetfont_cff.h"


/* 閉じる */

static void _subsetfont_close(SubsetFont *p)
{
	if(p)
	{
		if(p->io->is_cff)
			subsetfont_cff_free(p);
		else
			subsetfont_tt_free(p);

		fontio_close(p->io);

		mFree(p);
	}
}

/* サブセットフォントを出力 */

static mlkerr _output_subset(SubsetFont *p,FILE *fpout)
{
	int ret;

	p->fpout = fpout;
	p->io->fpout = fpout;

	if(p->io->is_cff)
		ret = subsetfont_cff_output(p);
	else
		ret = subsetfont_tt_output(p);

	return ret;
}

/** サブセットフォントの作成
 *
 * fontfile: 入力フォントファイル名 (UTF-8)
 * index: 複数のフォントが含まれる場合、対象のフォントのインデックス番号
 * dat: 入力情報 + 結果の情報 */

mlkerr create_subsetfont(FILE *fpout,const char *fontfile,int index,SubsetFontDat *dat)
{
	SubsetFont *p;
	FontIO *io;
	int ret;

	dat->gidmap_outsrc = NULL;

	//フォント読み込み

	ret = fontio_openfile(&io, fontfile, index);
	if(ret) return ret;

	//確保

	p = (SubsetFont *)mMalloc0((io->is_cff)? sizeof(SubsetFontCFF): sizeof(SubsetFontTT));
	if(!p)
	{
		fontio_close(io);
		return MLKERR_ALLOC;
	}

	p->io = io;
	p->pdat = dat;
	p->unimap = dat->unimap;
	p->gidmap_srcout = dat->gidmap;

	//出力

	ret = _output_subset(p, fpout);

	if(ret)
		mFree(p->gidmap_outsrc);
	else
	{
		dat->out_glyph_num = p->out_glyph_num;
		dat->in_glyph_num = io->glyph_num;
		dat->gidmap_outsrc = p->gidmap_outsrc;
	}
	
	//

	_subsetfont_close(p);

	return ret;
}


//===========================
// 内部関数
//===========================


/** unimap から、src <-> out GID のマップを作成 & 使用グリフ数計算 */

mlkerr subsetfont_create_gidmap(SubsetFont *p)
{
	uint16_t *pd;
	uint32_t *pmap;
	int i,gid;

	//src -> out
	// :使用しないものは 0xffff (CFF/CharStrings 書き込み時に使われる)

	pd = p->gidmap_srcout;
	pmap = p->unimap;
	gid = 0;

	for(i = p->io->glyph_num; i; i--, pmap++, pd++)
	{
		if(*pmap)
			*pd = gid++;
		else
			*pd = 0xffff;
	}

	//出力グリフ数

	p->out_glyph_num = gid;

	//out -> src (作業用)

	p->gidmap_outsrc = pd = (uint16_t *)mMalloc(p->out_glyph_num * 2);
	if(!pd) return MLKERR_ALLOC;

	pmap = p->unimap;
	gid = p->io->glyph_num;

	for(i = 0; i < gid; i++, pmap++)
	{
		if(*pmap)
			*(pd++) = i;
	}

	return 0;
}

