/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * FontIO: CFF
 *********************************/

#include <stdio.h>
#include <string.h>

#include <mlk.h>
#include <mlk_buf.h>
#include <mlk_list.h>
#include <mlk_stdio.h>

#include "fontio.h"
#include "fontio_cff.h"



/** CFF 読み込み開始
 *
 * なければ UNFOUND。
 * ヘッダのみ読み込む。 */

mlkerr fontio_cff_start(FontIO *p)
{
	FontTblInfo *ptbl;
	int ret;
	uint8_t b[3];

	ret = fontio_move_table(p, MLK_MAKE32_4('C','F','F',' '), &ptbl);
	if(ret) return ret;

	p->cff_offset = ptbl->offset;

	//ヘッダ

	if(fontio_read(p, b, 3)
		|| !(b[0] == 1 && b[1] == 0)
		|| fontio_seek(p, b[2] - 3))
		return MLKERR_DAMAGED;

	return 0;
}

/** CID の charset の位置へ
 *
 * dst_cidnum: CID 個数が入る。0 で CID ではない or charset がない */

mlkerr fontio_cff_move_cid_charset(FontIO *p,int *dst_cidnum)
{
	CFFIndex index;
	mList list = MLIST_INIT;
	CFFDictItem *pi;
	int ret,cidnum;

	*dst_cidnum = 0;

	//Name INDEX

	ret = fontio_cff_read_INDEX(p, &index, TRUE);
	if(ret) return ret;

	//Top DICT INDEX

	ret = fontio_cff_read_INDEX(p, &index, FALSE);
	if(ret) return ret;

	ret = fontio_cff_read_DICT_INDEX(p, &index, 0, &list);
	if(ret) return ret;

	//---------

	//ROS がない (CID ではない) 場合、なし

	pi = fontio_cff_search_DICT_key(&list, CFF_KEY2(30));
	if(!pi) goto END;

	//CID 数

	pi = fontio_cff_search_DICT_key(&list, CFF_KEY2(34));
	if(!pi) goto END;

	cidnum = pi->nval[0];

	//charset

	pi = fontio_cff_search_DICT_key(&list, 15);
	if(!pi || pi->nval[0] < 3)
		goto END;

	if(fontio_cff_setpos(p, pi->nval[0]))
		ret = MLKERR_DAMAGED;
	else
		*dst_cidnum = cidnum;

	//
END:
	mListDeleteAll(&list);

	return ret;
}


//==========================


/** CFF のオフセット位置をセット */

int fontio_cff_setpos(FontIO *p,uint32_t pos)
{
	return (fseek(p->fpin, p->cff_offset + pos, SEEK_SET) != 0);
}

/** オフセット値をファイルから読み込み */

int fontio_cff_read_offset(FontIO *p,uint32_t *dst)
{
	uint8_t b[4],*ptr;

	if(fontio_read(p, b, p->cff_offsize))
		return 1;

	ptr = b;
	
	*dst = fontio_cff_read_offset_buf(&ptr, p->cff_offsize);

	return 0;
}

/** バッファからオフセット値を読み込んで進む */ 

uint32_t fontio_cff_read_offset_buf(uint8_t **ppbuf,uint8_t offsize)
{
	uint8_t *ps = *ppbuf;
	uint32_t v;

	switch(offsize)
	{
		case 1:
			v = ps[0];
			break;
		case 2:
			v = (ps[0] << 8) | ps[1];
			break;
		case 3:
			v = (ps[0] << 16) | (ps[1] << 8) | ps[2];
			break;
		default:
			v = ((uint32_t)ps[0] << 24) | (ps[1] << 16) | (ps[2] << 8) | ps[3];
			break;
	}

	*ppbuf += offsize;

	return v;
}


//=========================
// INDEX 読み込み
//=========================


/** INDEX データ読み込み
 *
 * skip: 0 以外で、INDEX の次の位置へ進む */

mlkerr fontio_cff_read_INDEX(FontIO *p,CFFIndex *dst,int skip)
{
	uint16_t count;
	uint8_t offsize;
	int size;
	uint32_t offset;

	mMemset0(dst, sizeof(CFFIndex));

	//個数 (0 の場合あり)

	if(fontio_read16(p, &count))
		return MLKERR_DAMAGED;

	dst->count = count;

	if(count == 0) return MLKERR_OK;

	//Offset 型サイズ
	
	if(fontio_read(p, &offsize, 1))
		return MLKERR_DAMAGED;

	dst->offsize = offsize;

	//オフセットデータのサイズ

	size = offsize * (count + 1);

	//位置

	dst->offset_pos = ftell(p->fpin);
	dst->data_pos = dst->offset_pos + size;

	//最後のオフセット位置から、データサイズ取得

	p->cff_offsize = offsize;

	if(fontio_seek(p, size - offsize)
		|| fontio_cff_read_offset(p, &offset))
		return MLKERR_DAMAGED;

	offset--;

	//

	dst->data_size = offset;
	dst->next_pos = dst->data_pos + offset;

	//次の位置へ

	if(skip && fontio_setpos(p, dst->next_pos))
		return MLKERR_DAMAGED;

	return 0;
}

/** CFF オフセット位置を指定して、INDEX 読み込み
 *
 * offset: CFF 先頭からの位置 */

mlkerr fontio_cff_read_INDEX_pos(FontIO *p,uint32_t offset,CFFIndex *dst)
{
	if(fontio_cff_setpos(p, offset))
		return MLKERR_DAMAGED;

	return fontio_cff_read_INDEX(p, dst, FALSE);
}

/** INDEX の指定番号のデータ位置へ移動
 *
 * dst_size: データのサイズが入る */

mlkerr fontio_cff_goto_INDEX_data(FontIO *p,CFFIndex *index,int no,uint32_t *dst_size)
{
	uint32_t off1,off2;

	p->cff_offsize = index->offsize;

	if(no >= index->count
		|| fontio_setpos(p, index->offset_pos + no * index->offsize)
		|| fontio_cff_read_offset(p, &off1)
		|| fontio_cff_read_offset(p, &off2)
		|| off1 > off2
		|| fontio_setpos(p, index->data_pos + off1 - 1))
		return MLKERR_DAMAGED;

	*dst_size = off2 - off1;

	return 0;
}

/** INDEX のオフセット + データを、確保したバッファに読み込み
 *
 * 空の場合あり */

mlkerr fontio_cff_read_INDEX_offset_data(FontIO *p,CFFIndex *index,uint8_t **dst)
{
	uint8_t *buf;
	uint32_t size;

	if(!index->count) return 0;

	size = index->next_pos - index->offset_pos;

	buf = (uint8_t *)mMalloc(size);
	if(!buf) return MLKERR_ALLOC;

	if(fontio_setpos(p, index->offset_pos)
		|| fontio_read(p, buf, size))
	{
		mFree(buf);
		return MLKERR_DAMAGED;
	}

	*dst = buf;

	return 0;
}

/** INDEX の指定番号のデータ位置を取得 (バッファから)
 *
 * return: NULL でエラー */

uint8_t *fontio_cff_get_INDEX_buf_data(CFFIndex *index,uint8_t *buf,int no,uint32_t *dst_size)
{
	uint8_t *ps;
	uint32_t off1,off2;

	ps = buf + no * index->offsize;

	off1 = fontio_cff_read_offset_buf(&ps, index->offsize) - 1;
	off2 = fontio_cff_read_offset_buf(&ps, index->offsize) - 1;

	if(off2 > index->data_size) return NULL;
	
	*dst_size = off2 - off1;

	return buf + (index->data_pos - index->offset_pos) + off1;
}

/** INDEX のバッファから、全データの最大サイズを取得 (使用判定付き)
 *
 * flagbuf: 各データ分の配列。0xFFFF で使用しない */

uint32_t fontio_cff_get_INDEX_buf_datasize_flag(CFFIndex *index,uint8_t *buf,const uint16_t *flagbuf)
{
	uint32_t off1,off2,size = 0;
	int i;
	uint8_t offsize;

	offsize = index->offsize;

	off1 = fontio_cff_read_offset_buf(&buf, offsize);
	
	for(i = index->count; i; i--, flagbuf++)
	{
		off2 = fontio_cff_read_offset_buf(&buf, offsize);

		if(*flagbuf != 0xffff)
			size += off2 - off1;

		off1 = off2;
	}

	return size;
}


//======================
// DICT 読み込み
//======================


/* DICT 整数値を1つ読み込み */

static int32_t _read_dict_int(uint8_t *ps)
{
	uint8_t b;
	int32_t n = 0;

	b = *ps;

	if(b >= 32 && b <= 246)
	{
		//-107 .. 107

		n = b - 139;
	}
	else if(b >= 247 && b <= 250)
	{
		//108 .. 1131

		n = ((b - 247) << 8) + ps[1] + 108;
	}
	else if(b >= 251 && b <= 254)
	{
		//-1131 .. -108

		n = (-(b - 251) << 8) - ps[1] - 108;
	}
	else if(b == 28)
	{
		//-32768 .. 32767

		n = (int16_t)((ps[1] << 8) | ps[2]);
	}
	else if(b == 29)
	{
		//int32

		n = (int32_t)(((uint32_t)ps[1] << 24) | (ps[2] << 16) | (ps[3] << 8) | ps[4]);
	}

	return n;
}

/* バッファから DICT のデータ読み込み */

static mlkerr _read_DICT_data(mList *list,uint8_t *buf,uint32_t size)
{
	uint8_t *ps,*psend,*top,b;
	uint16_t key;
	int len;
	int32_t nval[3] = {0,0,0};
	CFFDictItem *pi;

	//top = 値の先頭位置
	ps = top = buf;
	psend = buf + size;

	while(ps < psend)
	{
		b = *ps;

		if(b <= 21)
		{
			//---- キー

			if(b == 12)
			{
				//2byte

				if(ps + 2 > psend) return MLKERR_DAMAGED;
				
				key = (12 << 8) | ps[1];
				ps += 2;
			}
			else
			{
				//1byte
				key = b;
				ps++;
			}

			len = ps - top;

			//追加

			if(len > 65535)
				return MLKERR_DAMAGED;

			pi = (CFFDictItem *)mListAppendNew(list, sizeof(CFFDictItem) + len);
			if(!pi) return MLKERR_ALLOC;

			pi->key = key;
			pi->len = len;
			pi->nval[0] = nval[0];
			pi->nval[1] = nval[1];
			pi->nval[2] = nval[2];

			memcpy(pi->dat, top, len);

			//

			top = ps;
			nval[0] = nval[1] = nval[2] = 0;
		}
		else if(b == 30)
		{
			//---- 実数値
			//上位4ビット or 下位4ビットが 15 で終了

			ps++;
			
			while(1)
			{
				if(ps >= psend) return MLKERR_DAMAGED;
				
				b = *(ps++);
				if((b & 0xf0) == 0xf0 || (b & 0x0f) == 0x0f) break;
			}
		}
		else
		{
			//----- 整数値、ほか

			if(b >= 32 && b <= 246)
				len = 1;
			else if(b >= 247 && b <= 254)
				len = 2;
			else if(b == 28)
				len = 3;
			else if(b == 29)
				len = 5;
			else
				return MLKERR_DAMAGED;

			if(ps + len > psend) return MLKERR_DAMAGED;

			nval[2] = nval[1];
			nval[1] = nval[0];
			nval[0] = _read_dict_int(ps);
			
			ps += len;
		}
	}

	return 0;
}

/** 現在位置の DICT データから、リストに読み込み
 *
 * size: DICT データのサイズ */

mlkerr fontio_cff_read_DICT(FontIO *p,mList *list,uint32_t dictsize)
{
	uint8_t *buf;
	int ret;

	ret = fontio_read_alloc(p, dictsize, &buf);
	if(ret) return ret;

	ret = _read_DICT_data(list, buf, dictsize);
	if(ret)
		mListDeleteAll(list);

	mFree(buf);
	
	return ret;
}

/** INDEX の指定番号の DICT データをリストで読み込み */

mlkerr fontio_cff_read_DICT_INDEX(FontIO *p,CFFIndex *index,int no,mList *list)
{
	uint32_t size;
	int ret;

	//移動

	ret = fontio_cff_goto_INDEX_data(p, index, no, &size);
	if(ret) return ret;

	//読み込み

	return fontio_cff_read_DICT(p, list, size);
}

/** DICT のリストからキーを検索 */

CFFDictItem *fontio_cff_search_DICT_key(mList *list,uint16_t key)
{
	CFFDictItem *pi;

	MLK_LIST_FOR(*list, pi, CFFDictItem)
	{
		if(pi->key == key) return pi;
	}

	return NULL;
}

/** DICT 新規項目追加
 *
 * return: 1 で失敗 */

int fontio_cff_add_DICT_item(mList *list,uint16_t key,int v2,int v1,int v0,int vnum)
{
	CFFDictItem *pi;

	pi = (CFFDictItem *)mListAppendNew(list, sizeof(CFFDictItem));
	if(!pi) return 1;

	pi->key = key;
	pi->nval[0] = v0;
	pi->nval[1] = v1;
	pi->nval[2] = v2;
	pi->valnum = vnum;

	return 0;
}


//=======================
// INDEX オフセット
//=======================


/** データサイズから、最小のオフセット型サイズを取得 (INDEX 用) */

uint8_t fontio_cff_calc_offsize(uint32_t size)
{
	//最後のオフセット値は +1 されるので、サイズに +1 した値で比較する

	size++;

	if(size < (1<<8))
		return 1;
	else if(size < (1<<16))
		return 2;
	else if(size < (1<<24))
		return 3;
	else
		return 4;
}

/** Offset 値をバッファにセット
 *
 * return: 次のバッファ位置 */

uint8_t *fontio_cff_setbuf_offset(uint8_t *buf,uint32_t offset,uint8_t offsize)
{
	switch(offsize)
	{
		case 1:
			buf[0] = (uint8_t)offset;
			break;
		case 2:
			buf[0] = (uint8_t)(offset >> 8);
			buf[1] = (uint8_t)offset;
			break;
		case 3:
			buf[0] = (uint8_t)(offset >> 16);
			buf[1] = (uint8_t)(offset >> 8);
			buf[2] = (uint8_t)offset;
			break;
		default:
			buf[0] = (uint8_t)(offset >> 24);
			buf[1] = (uint8_t)(offset >> 16);
			buf[2] = (uint8_t)(offset >> 8);
			buf[3] = (uint8_t)offset;
			break;
	}

	return buf + offsize;
}

/** Offset 値をファイルに書き込み
 *
 * return: 0 で成功 */

int fontio_cff_write_offset(FontIO *p,uint32_t offset,uint8_t offsize)
{
	uint8_t dat[4];

	fontio_cff_setbuf_offset(dat, offset, offsize);

	return mFILEwriteOK(p->fpout, dat, offsize);
}


//=========================
// INDEX 書き込み
//=========================


/** INDEX の先頭 3byte (個数、OffSize) を書き込み
 *
 * return: 0 で成功 */

int fontio_cff_write_INDEX_top(FontIO *p,int count,uint8_t offsize)
{
	uint8_t d[3];

	d[0] = (uint8_t)(count >> 8);
	d[1] = (uint8_t)count;
	d[2] = offsize;

	return mFILEwriteOK(p->fpout, d, 3);
}

/** 空の INDEX 書き込み */

mlkerr fontio_cff_write_INDEX_empty(FontIO *p)
{
	if(mFILEwrite0(p->fpout, 2))
		return MLKERR_IO;
	else
		return MLKERR_OK;
}

/** INDEX データを書き込み (個数=1)
 *
 * size: データのサイズ */

mlkerr fontio_cff_write_INDEX_count1(FontIO *p,const void *buf,uint32_t size)
{
	uint8_t d[11];
	uint8_t offsize;

	offsize = fontio_cff_calc_offsize(size);

	d[0] = 0;
	d[1] = 1;
	d[2] = offsize;

	fontio_cff_setbuf_offset(d + 3, 1, offsize);
	fontio_cff_setbuf_offset(d + 3 + offsize, size + 1, offsize);

	if(mFILEwriteOK(p->fpout, d, 3 + offsize * 2)
		|| mFILEwriteOK(p->fpout, buf, size))
		return MLKERR_IO;

	return MLKERR_OK;
}

/** INDEX データを元のまま書き込み */

mlkerr fontio_cff_write_INDEX_copy(FontIO *p,CFFIndex *index)
{
	uint8_t *buf;
	int ret;

	if(index->count == 0)
	{
		//個数が 0 の場合、空データ

		return fontio_cff_write_INDEX_empty(p);
	}
	else
	{
		//書き込み

		ret = fontio_cff_read_INDEX_offset_data(p, index, &buf);
		if(ret) return ret;

		if(fontio_cff_write_INDEX_top(p, index->count, index->offsize)
			|| mFILEwriteOK(p->fpout, buf, index->next_pos - index->offset_pos))
			ret = MLKERR_IO;

		mFree(buf);

		return ret;
	}
}


//======================
// DICT 書き込み
//======================


/* DICT のデータを追加
 *
 * force32: TRUE で 4byte 値を強制 (後で書き込むオフセット値) */

static mlkerr _add_dict_data(mBuf *buf,CFFDictItem *pi,mlkbool force32)
{
	int i,len,cnt;
	int32_t n;
	uint8_t dat[5];

	//元データをそのままコピー

	if(!pi->valnum)
	{
		if(!mBufAppend(buf, pi->dat, pi->len))
			return MLKERR_ALLOC;

		return 0;
	}

	//------ 新規値

	cnt = pi->valnum;

	//値

	for(i = 0; i < cnt; i++)
	{
		n = pi->nval[cnt - 1 - i];

		if(force32 || n < -32768 || n > 32767)
		{
			//5byte
			
			dat[0] = 29;
			dat[1] = (uint8_t)(n >> 24);
			dat[2] = (uint8_t)(n >> 16);
			dat[3] = (uint8_t)(n >> 8);
			dat[4] = (uint8_t)n;
			len = 5;
		}
		else if(-107 <= n && n <= 107)
		{
			//1byte
			
			dat[0] = n + 139;
			len = 1;
		}
		else if(108 <= n && n <= 1131)
		{
			//1byte
			
			n -= 108;
			dat[0] = (n >> 8) + 247;
			dat[1] = n & 255;
			len = 2;
		}
		else if(-1131 <= n && n <= -108)
		{
			//2byte
			
			n = -(n + 108);
			dat[0] = (n >> 8) + 251;
			dat[1] = n & 255;
			len = 2;
		}
		else
		{
			//3byte
			
			dat[0] = 28;
			dat[1] = (uint8_t)(n >> 8);
			dat[2] = (uint8_t)n;
			len = 3;
		}

		if(!mBufAppend(buf, dat, len))
			return MLKERR_ALLOC;
	}

	//キー

	n = pi->key;

	if(n >= CFF_KEY2(0))
	{
		dat[0] = n >> 8;
		dat[1] = (uint8_t)n;
		len = 2;
	}
	else
	{
		dat[0] = n;
		len = 1;
	}

	if(!mBufAppend(buf, dat, len))
		return MLKERR_ALLOC;

	return 0;
}

/** リスト項目から、書き込み用 DICT データの作成
 *
 * mBuf にデータを追加する。
 *
 * buf: 未確保なら、自動で確保する
 * list: DICT 項目のリスト
 * func: 各項目を処理する関数。
 *   pos: mBuf 内の現在位置
 *   CFF_DICT_RET_* を返す。 */

mlkerr fontio_cff_create_DICT(mBuf *buf,mList *list,FontIO_writeDICT_func func,void *param)
{
	CFFDictItem *pi;
	int ret;

	//未確保の場合、バッファ確保

	if(!buf->buf && !mBufAlloc(buf, 256, 256))
		return MLKERR_ALLOC;

	//データセット

	MLK_LIST_FOR(*list, pi, CFFDictItem)
	{
		ret = (func)(pi, buf->cursize, param);

		if(ret == CFF_DICT_RET_NO_OUTPUT) continue;

		//追加

		ret = _add_dict_data(buf, pi, (ret == CFF_DICT_RET_32BIT));
		if(ret)
		{
			mBufFree(buf);
			return ret;
		}
	}

	return 0;
}


