/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * CFF: CharString 処理
 ************************************/

#include <stdio.h>
#include <string.h>

#include <mlk.h>

#include "fontio.h"
#include "fontio_cff.h"

#include "subsetfont.h"
#include "subsetfont_pv.h"
#include "subsetfont_cff.h"
#include "subsetfont_cff_cs.h"


/* シーク */

static int _cs_seek(CharString *p,int size)
{
	if(p->cur.remain < size)
		return 1;

	p->cur.buf += size;
	p->cur.remain -= size;

	return 0;
}

/* 読み込み */

static int _cs_read(CharString *p,void *dst,int size)
{
	if(p->cur.remain < size) return 1;

	memcpy(dst, p->cur.buf, size);

	p->cur.buf += size;
	p->cur.remain -= size;

	return 0;
}


//=======================
// 引数スタック
//=======================


/* 引数スタックに追加 */

static int _push_arg(CharString *p,int32_t val)
{
	if(p->arg_num == CS_ARG_STACK_NUM)
		return MLKERR_INVALID_VALUE;

	p->arg[p->arg_num++] = val;

	return 0;
}

/* 引数スタッククリア */

static void _clear_arg(CharString *p)
{
	p->arg_num = 0;
}


//=======================
// サブルーチン
//=======================


/* サブルーチンの index 値を実際の値にする
 *
 * num: サブルーチンの数 */

static int _conv_subr_index(int index,int num)
{
	if(num < 1239)
		return index + 107;
	else if(num < 33900)
		return index + 1131;
	else
		return index + 32768;
}

/* サブルーチン呼び出し */

static int _proc_subr(CharString *p,int global)
{
	SubsetFontCFF *sf = p->sf;
	CFFCSDat *cs;
	uint8_t *buf;
	int no,fd = 0;
	uint32_t size;
	uint16_t val16[4];
	uint8_t val8[2];

	if(!p->arg_num || p->subr_num == CS_SUBR_NEST_NUM)
		return MLKERR_INVALID_VALUE;

	//番号 (直前の引数。まだ実際の値ではない)

	no = p->arg[--(p->arg_num)];

	//移動先のサブルーチンデータ

	if(global)
		cs = &sf->gsubr;
	else
	{
		//ローカル (gid = 出力後の GID)

		fd = sf->fdselect_buf[p->gid];

		cs = &sf->fontdict[fd].subr;
	}

	if(!cs->index.count) return MLKERR_INVALID_VALUE;

	//サブルーチン番号

	no = _conv_subr_index(no, cs->index.count);

	if(no < 0 || no >= cs->index.count)
		return MLKERR_INVALID_VALUE;

	//データ位置

	buf = fontio_cff_get_INDEX_buf_data(&cs->index, cs->buf, no, &size);
	if(!buf) return MLKERR_DAMAGED;

	//サブルーチンの値の情報を、作業ファイルに書き込む
	// :"<subr>{... subrno} callsubr" というように、サブルーチンの最後で、
	// :番号の数値を指定することも可能ではあるので (ほぼないとは思われるが)、
	// :直前の引数の時点での書き込み情報を使う必要がある。

	if(p->lastval.write_no != -1)
	{
		val16[0] = p->lastval.write_no;
		val16[1] = p->lastval.write_index;
		val16[2] = p->lastval.pos;
		val16[3] = no;
		val8[0] = p->lastval.bytes | (global? 0x80: 0);
		val8[1] = fd;
	
		fwrite(val16, 1, 2 * 4, sf->subrtmp_fp);
		fwrite(val8, 1, 2, sf->subrtmp_fp);
	}

	//現在状態をセット

	p->subrback[p->subr_num] = p->cur;
	p->subr_num++;

	//新しい処理
	// :すでに処理したサブルーチンなら、処理中はデータを書き込まない

	p->cur.buf = buf;
	p->cur.bufsize = p->cur.remain = size;
	p->cur.write_no = (global)? CS_WRITENO_GSUBR: CS_WRITENO_LSUBR + fd;
	p->cur.write_index = no;
	p->cur.write_flag = !(cs->map[no]);

	//使用サブルーチンフラグ ON

	cs->map[no] = 1;

	return 0;
}


//====================


/* hintmask, cntrmask */

static int _proc_hintmask(CharString *p)
{
	int n;

	//現在の引数は、vstem の値として扱う (幅の値は無視)
	// ".. hintmask" (hstem なし)
	// ".. hstemhm .. vstemhm .. hintmask" (vstem 複数)

	p->stem_num += p->arg_num / 2;
	p->arg_num = 0;

	//ステム数 x 1ビットのバイト値が続く

	n = (p->stem_num + 7) / 8;

	if(_cs_seek(p, n)) return MLKERR_DAMAGED;

	//

	_clear_arg(p);

	return 0;
}

/* ステム処理 */

static void _proc_stem(CharString *p)
{
	//奇数の場合、先頭に送り幅がある

	p->stem_num += p->arg_num / 2;

	_clear_arg(p);
}

/* コマンド処理 */

static mlkerr _proc_command(CharString *p,int cmd)
{
	int ret = 0;

	switch(cmd)
	{
		case 1:  //hstem
		case 18: //hstemhm
			_proc_stem(p);
			break;
			
		case 3:  //vstem
		case 23: //vstemhm
			_proc_stem(p);
			break;

		case 19: //hintmask
		case 20: //cntrmask
			ret = _proc_hintmask(p);
			break;

		//[!] サブルーチン関連では、last_hstem はそのまま
		case 10: //callsubr
			ret = _proc_subr(p, 0);
			break;
		case 29: //callgsubr
			ret = _proc_subr(p, 1);
			break;
		case 11: //return
			if(p->subr_num)
			{
				p->subr_num--;

				p->cur = p->subrback[p->subr_num];
			}
			break;
	
		default:
			_clear_arg(p);
			break;
	}

	return ret;
}

/* 数値 or コマンドを読み込み
 *
 * dst_cmd: -1 で数値 */

static mlkerr _read_val(CharString *p,int *dst_cmd,int32_t *dst_val)
{
	uint8_t b0,b[4];
	int cmd,pos,val_bytes = 0;
	int32_t val = 0;

	pos = p->cur.bufsize - p->cur.remain;

	_cs_read(p, &b0, 1);

	cmd = -1; //-1 で数値、それ以外でコマンド

	if(b0 == 28)
	{
		//3byte 数値 (int16)

		if(_cs_read(p, b, 2)) return MLKERR_DAMAGED;

		val = (int16_t)((b[0] << 8) | b[1]);

		val_bytes = 3;
	}
	else if(b0 >= 32 && b0 <= 246)
	{
		//1byte数値 (-107〜107)

		val = b0 - 139;
		val_bytes = 1;
	}
	else if(b0 >= 247 && b0 <= 254)
	{
		//2byte 数値 (108〜1131, -108〜-1131)

		if(_cs_read(p, b, 1)) return MLKERR_DAMAGED;

		if(b0 >= 247 && b0 <= 250)
			val = (b0 - 247) * 256 + b[0] + 108;
		else
			val = -((b0 - 251) * 256) - b[0] - 108;

		val_bytes = 2;
	}
	else if(b0 == 255)
	{
		//5byte 固定小数点数

		if(_cs_read(p, b, 4)) return MLKERR_DAMAGED;

		val = (int32_t)(((uint32_t)b[0] << 24) | (b[1] << 16) | (b[2] << 8) | b[3]);

		val_bytes = 5;
	}
	else if(b0 == 12)
	{
		//2byte 命令

		if(_cs_read(p, b, 1)) return MLKERR_DAMAGED;

		cmd = CFF_KEY2(b[0]);
	}
	else
	{
		//1byte 命令 (0-31)

		cmd = b0;
	}

	//直前の引数の情報

	if(cmd == -1)
	{
		p->lastval.pos = pos;
		p->lastval.bytes = val_bytes;
		p->lastval.write_no = (p->cur.write_flag)? p->cur.write_no: -1;
		p->lastval.write_index = p->cur.write_index;
	}

	//

	*dst_cmd = cmd;
	*dst_val = val;

	return 0;
}

/** グリフのデータを処理 */

mlkerr subsetfont_cff_proc_glyph(CharString *p,int in_gid)
{
	int ret,cmd;
	int32_t val = 0;

	p->cur.remain = p->cur.bufsize;
	p->cur.write_index = in_gid; //入力の GID
	p->cur.write_flag = 1;
	p->cur.write_no = CS_WRITENO_GLYPH;

	while(p->cur.remain)
	{
		ret = _read_val(p, &cmd, &val);
		if(ret) return ret;

		if(cmd == -1)
		{
			//数値

			ret = _push_arg(p, val);
			if(ret) return ret;
		}
		else
		{
			//コマンド

			if(cmd == 14) return 0; //endchar

			ret = _proc_command(p, cmd);
			if(ret) return ret;
		}
	}

	return 0;
}
