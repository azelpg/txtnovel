/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * CFF 各処理
 ************************************/

#include <stdio.h>
#include <stdlib.h>

#include <mlk.h>
#include <mlk_buf.h>
#include <mlk_list.h>
#include <mlk_stdio.h>

#include "fontio.h"
#include "fontio_cff.h"

#include "subsetfont.h"
#include "subsetfont_pv.h"
#include "subsetfont_cff.h"
#include "subsetfont_cff_cs.h"

#include "../app.h"


//=============================
// 使用サブルーチンのチェック
//=============================


/* サブルーチンが存在するか */

static int _has_subr(SubsetFontCFF *p)
{
	CFFFontDict *fd;
	int i;

	//Global
	
	if(p->gsubr.index.count) return 1;

	//Local

	for(fd = p->fontdict, i = p->fontdict_num; i; i--, fd++)
	{
		if(fd->subr.index.count) return 1;
	}

	return 0;
}

/* map バッファの確保 */

static mlkerr _alloc_subrmap(CFFCSDat *p)
{
	int cnt;

	cnt = p->index.count;

	//最初はフラグとして使うので、ゼロクリア

	if(cnt)
	{
		p->map = (uint16_t *)mMalloc0(cnt * 2);
		if(!p->map) return MLKERR_ALLOC;
	}

	return 0;
}

/* 初期化
 *
 * [!] 各サブルーチンがない時は、map = NULL */

static mlkerr _charstring_init(SubsetFontCFF *p)
{
	CFFFontDict *fd;
	int i,ret;

	//Global Subr

	ret = _alloc_subrmap(&p->gsubr);
	if(ret) return ret;

	//Local Subr (出力分)

	fd = p->fontdict;

	for(i = p->fontdict_num; i; i--, fd++)
	{
		ret = _alloc_subrmap(&fd->subr);
		if(ret) return ret;
	}

	//作業ファイル開く

	p->subrtmp_fp = mFILEopen(app_set_tmpfile(TMPFILENAME_SUBSET_CFF), "w+b");
	if(!p->subrtmp_fp) return MLKERR_OPEN;

	return 0;
}

/* 各グリフを読み込んで処理
 *
 * 使用サブルーチンのフラグを ON。
 * 作業ファイルに、サブルーチン呼び出しの情報を記録。 */

static mlkerr _proc_charstrings(SubsetFontCFF *p,CharString *cs)
{
	uint8_t *buf;
	uint32_t size;
	int i,gnum,ret,in_gid;

	//出力するグリフのみ処理

	gnum = p->b.out_glyph_num;

	for(i = 0; i < gnum; i++)
	{
		in_gid = p->b.gidmap_outsrc[i];

		//データ位置

		buf = fontio_cff_get_INDEX_buf_data(&p->charstr.index, p->charstr.buf, in_gid, &size);
		if(!buf) return MLKERR_DAMAGED;

		//処理

		mMemset0(cs, sizeof(CharString));

		cs->sf = p;
		cs->cur.buf = buf;
		cs->cur.bufsize = size;
		cs->gid = i; //出力 GID

		ret = subsetfont_cff_proc_glyph(cs, in_gid);
		if(ret) return ret;
	}

	return 0;
}

/* 作業データ比較関数 */

static int _cmpfunc_subr(const void *p1,const void *p2)
{
	uint16_t *pv1,*pv2;
	int i,v1,v2;

	pv1 = (uint16_t *)p1;
	pv2 = (uint16_t *)p2;

	//no < index < pos の順で並べる

	for(i = 3; i; i--)
	{
		v1 = *(pv1++);
		v2 = *(pv2++);

		if(v1 < v2)
			return -1;
		else if(v1 > v2)
			return 1;
	}

	return 0;
}

/* 作業データから、各データの先頭位置を取得
 *
 * データがなければ 0 */

static void _get_tmpfile_pos(SubsetFontCFF *p,uint16_t *buf,int num)
{
	int last,no,fd;
	uint32_t pos = 1;

	last = -1;

	//先頭が glyph

	if(*buf == CS_WRITENO_GLYPH)
		p->charstr.filepos = pos;

	//

	for(; num; num--, buf += 5, pos += 10)
	{
		no = *buf;

		if(no == CS_WRITENO_GSUBR)
		{
			//Global Subr
			
			if(last < CS_WRITENO_GSUBR)
				p->gsubr.filepos = pos;
		}
		else if(no >= CS_WRITENO_LSUBR)
		{
			//Local Subr

			fd = no - CS_WRITENO_LSUBR;
			
			if(last < CS_WRITENO_LSUBR + fd)
				p->fontdict[fd].subr.filepos = pos;
		}

		last = no;
	}
}

/* 作業ファイルのデータを昇順に並べ替え。
 * 各データの位置を取得 */

static mlkerr _sort_tmpfile_data(SubsetFontCFF *p)
{
	FILE *fp = p->subrtmp_fp;
	uint8_t *buf;
	int size,datnum;

	size = ftell(fp);
	datnum = size / 10;

	//読み込み

	buf = (uint8_t *)mMalloc(size);
	if(!buf) return MLKERR_ALLOC;

	rewind(fp);

	if(mFILEreadOK(fp, buf, size))
	{
		mFree(buf);
		return MLKERR_IO;
	}

	//並べ替え

	qsort(buf, datnum, 10, _cmpfunc_subr);

	//書き込み

	rewind(fp);

	if(mFILEwriteOK(fp, buf, size))
	{
		mFree(buf);
		return MLKERR_IO;
	}

	//各データの位置を取得

	_get_tmpfile_pos(p, (uint16_t *)buf, datnum);

	//

	mFree(buf);

	return 0;
}

/* srcIndex -> outIndex のマップ作成
 *
 * 値が 0xffff で、使用されない。
 * (INDEX の個数が uint16 なので、index = 0-65534 で、65535 は使われない)
 *
 * return: 出力数 */

static int _create_subr_map(SubsetFontCFF *p,CFFCSDat *cs)
{
	uint16_t *map;
	uint16_t no = 0;
	int i;

	map = cs->map;
	if(!map) return 0;
	
	for(i = cs->index.count; i; i--, map++)
	{
		if(*map)
			*map = no++;
		else
			*map = 0xffff;
	}

	return no;
}

/* グリフ処理後、データの処理 */

static mlkerr _proc_subr_data(SubsetFontCFF *p)
{
	CFFFontDict *fd;
	int ret,i;

	//作業データをソートして再書き込み。各位置を取得

	ret = _sort_tmpfile_data(p);
	if(ret) return ret;

	//Global Subr
	
	p->gsubr.outnum = _create_subr_map(p, &p->gsubr);

	//Local Subr

	fd = p->fontdict;

	for(i = p->fontdict_num; i; i--, fd++)
		fd->subr.outnum = _create_subr_map(p, &fd->subr);

	return 0;
}

/** CharStrings (グリフ) を読み込んで処理
 *
 * 使用されているサブルーチンの関連データを作業ファイルに出力し、
 * マップデータを作成。 */

mlkerr subsetfont_cff_proc_charstrings(SubsetFontCFF *p)
{
	CharString *cs;
	int ret;

	//サブルーチンがない場合は、何もしない

	if(!_has_subr(p)) return 0;

	//初期化

	ret = _charstring_init(p);
	if(ret) return ret;

	//グリフ処理

	cs = (CharString *)mMalloc(sizeof(CharString));
	if(!cs) return MLKERR_ALLOC;

	ret = _proc_charstrings(p, cs);

	mFree(cs);

	//データの処理

	return _proc_subr_data(p);
}


//===========================
// CharString/Subr 書き込み
//===========================
/* [!] 入力側で、Global/Local いずれのサブルーチンもない場合、
 *     map バッファと作業ファイルは存在しないので、注意。 */


//作業ファイルのデータ

typedef struct
{
	uint16_t no,
		index,		//INDEX の番号 [in] (0xffff で終端)
		pos,		//サブルーチンの番号の値の位置
		subrno;		//元のサブルーチンの番号
	uint8_t len,	//値のバイト数 (0x80 が ON で Global Subr)
		fd;			//Local Subr の場合、FontDICT の index [out]
}_subrval;

//各情報

typedef struct
{
	uint32_t filepos;
	int outnum,
		tmpdatno;
	CFFIndex *index;
	uint8_t *buf;
	uint16_t *map;
}_csinfo;


/* srcno から、各データを取得 */

static void _get_srcno_info(SubsetFontCFF *p,int srcno,_csinfo *dst)
{
	CFFCSDat *cs;

	if(srcno >= 0)
	{
		//Local Subr

		cs = &p->fontdict[srcno].subr;
		
		dst->tmpdatno = CS_WRITENO_LSUBR + srcno;
	}
	else if(srcno == -1)
	{
		//CharStrings

		cs = &p->charstr;

		dst->tmpdatno = CS_WRITENO_GLYPH;
	}
	else
	{
		//Global Subr

		cs = &p->gsubr;

		dst->tmpdatno = CS_WRITENO_GSUBR;
	}

	dst->index = &cs->index;
	dst->buf = cs->buf;
	dst->map = (srcno == -1)? p->b.gidmap_srcout: cs->map;
	dst->outnum = (srcno == -1)? p->b.out_glyph_num: cs->outnum;
	dst->filepos = cs->filepos;
}

/* 作業ファイルから次のデータ読み込み */

static void _read_tmpfile_data(SubsetFontCFF *p,_subrval *dst,_csinfo *info)
{
	if(mFILEreadOK(p->subrtmp_fp, &dst->no, 2 * 4)
		|| mFILEreadOK(p->subrtmp_fp, &dst->len, 2)
		|| dst->no != info->tmpdatno)
	{
		dst->index = 0xffff;
	}
}

/* サブルーチン番号の値を書き込み
 *
 * num: サブルーチン数
 * return: 書き込んだバイト数 */

static int _write_subr_no(SubsetFontCFF *p,int no,int num)
{
	uint8_t d[3];
	int len;

	//出力個数によって値が変化
	
	if(num < 1239)
		no -= 107;
	else if(num < 33900)
		no -= 1131;
	else
		no -= 32768;

	//データ

	if(-107 <= no && no <= 107)
	{
		d[0] = no + 139;
		len = 1;
	}
	else if(108 <= no && no <= 1131)
	{
		no -= 108;

		d[0] = (no >> 8) + 247;
		d[1] = (uint8_t)no;
		len = 2;
	}
	else if(-1131 <= no && no <= -108)
	{
		no = -(no + 108);
		d[0] = (no >> 8) + 251;
		d[1] = (uint8_t)no;
		len = 2;
	}
	else
	{
		d[0] = 28;
		d[1] = (uint8_t)(no >> 8);
		d[2] = (uint8_t)no;
		len = 3;
	}

	//書き込み

	fwrite(d, 1, len, p->b.fpout);

	return len;
}

/* データ書き込み (サブルーチン番号を書き換え)
 *
 * index: 元のサブルーチン番号
 * psize: 出力サイズが入る
 * pval: 最後の読み込みデータをセットする */

static mlkerr _write_subr_data(SubsetFontCFF *p,
	int index,uint8_t *buf,uint32_t *psize,_subrval *pval,_csinfo *info)
{
	_subrval val;
	int pos,len,outsize;
	CFFCSDat *cs;

	val = *pval;
	pos = 0;
	outsize = 0;

	do
	{
		//pos .. val.pos - 1 までを書き込み

		len = val.pos - pos;

		if(len)
		{
			if(mFILEwriteOK(p->b.fpout, buf + pos, len))
				return MLKERR_IO;

			outsize += len;
		}

		pos = val.pos;

		//サブルーチン番号の値を置き換えて出力

		if(val.len & 0x80)
			//Global Subr
			cs = &p->gsubr;
		else
			//Local Subr
			cs = &p->fontdict[val.fd].subr;

		outsize += _write_subr_no(p, cs->map[val.subrno], cs->outnum);

		pos += val.len & 0x7f;

		//次のデータ
		// :同じ CharString 内に次の callsubr がある場合、続ける

		_read_tmpfile_data(p, &val, info);

	} while(val.index == index);

	//終端まで出力

	len = *psize - pos;

	if(mFILEwriteOK(p->b.fpout, buf + pos, len))
		return MLKERR_IO;

	outsize += len;

	//

	*pval = val;
	*psize = outsize;

	return 0;
}

/** CharString or Subr INDEX 書き込み
 *
 * [!] Subr で出力が空になる場合は、あらかじめ処理しておくこと。
 * [!] CharStrings 書き込み時、作業ファイルがない場合がある。
 *
 * srcno: [-1] CharStrings [-2] GlobalSubr [0..] LocalSubr */

mlkerr subsetfont_cff_write_charstring_INDEX(SubsetFontCFF *p,int srcno)
{
	FILE *fpout = p->b.fpout;
	FontIO *io = p->b.io;
	uint8_t *buf;
	int i,ret,scnt,out_offsize;
	uint32_t size,offset_pos,offset_val;
	_subrval val;
	_csinfo info;

	_get_srcno_info(p, srcno, &info);

	//使用するグリフの全サイズから、オフセットサイズ取得

	size = fontio_cff_get_INDEX_buf_datasize_flag(info.index, info.buf, info.map);

	out_offsize = fontio_cff_calc_offsize(size);

	//INDEX 先頭書き込み

	if(fontio_cff_write_INDEX_top(io, info.outnum, out_offsize)
		|| fontio_cff_write_offset(io, 1, out_offsize))
		return MLKERR_IO;

	offset_pos = subsetfont_cff_get_outpos(p); //2番目のオフセットの先頭位置

	if(mFILEwrite0(fpout, out_offsize * info.outnum)) //オフセットデータ(tmp)
		return MLKERR_IO;

	//作業ファイルデータ先頭読み込み
	// [!] 一つもサブルーチンがない場合、作業ファイルがない
	// : filepos = 0 でデータなし、1〜でオフセット位置

	if(!info.filepos || !p->subrtmp_fp)
		val.index = 0xffff; //終端
	else
	{
		if(fseek(p->subrtmp_fp, info.filepos - 1, SEEK_SET))
			return MLKERR_IO;

		_read_tmpfile_data(p, &val, &info);
	}
	
	//データ

	scnt = info.index->count;
	offset_val = 1;

	for(i = 0; i < scnt; i++, info.map++)
	{
		//使用されない
		
		if(*info.map == 0xffff) continue;

		//データ位置

		buf = fontio_cff_get_INDEX_buf_data(info.index, info.buf, i, &size);
		if(!buf) return MLKERR_DAMAGED;

		//書き込み

		if(i == val.index)
		{
			//サブルーチン番号を置き換えて出力
			
			ret = _write_subr_data(p, i, buf, &size, &val, &info);
			if(ret) return ret;
		}
		else
		{
			//作業ファイルにデータがない場合、すべて出力
			// :CharString 内に call(g)subr が含まれない場合

			if(mFILEwriteOK(fpout, buf, size))
				return MLKERR_DAMAGED;
		}

		//オフセット値書き込み

		offset_val += size;

		if(fseek(fpout, offset_pos, SEEK_SET)
			|| fontio_cff_write_offset(io, offset_val, out_offsize)
			|| fseek(fpout, 0, SEEK_END))
			return MLKERR_IO;

		offset_pos += out_offsize;
	}

	return 0;
}


//=====================================
// FDArray (Font DICT INDEX) 書き込み
//=====================================


/* Font DICT データ作成の関数 */

static int _fontdict_add_dict(CFFDictItem *pi,int pos,void *param)
{
	if(pi->key == 18)
	{
		//Private
		// :len != 0 で、データがないため出力なし

		if(pi->len) return CFF_DICT_RET_NO_OUTPUT;

		*((int32_t *)param) = pos;

		return CFF_DICT_RET_32BIT;
	}

	return CFF_DICT_RET_NORMAL;
}

/* Private DICT データ作成の関数 */

static int _privdict_add_dict(CFFDictItem *pi,int pos,void *param)
{
	if(pi->key == 19)
	{
		//Subr

		pi->valnum = 1;

		*((int32_t *)param) = pos;

		return CFF_DICT_RET_32BIT;
	}

	return CFF_DICT_RET_NORMAL;
}

/* すべての Font DICT のデータ部分を作成
 *
 * INDEX でまとめて書き込む */

static mlkerr _create_fontdict_data(SubsetFontCFF *p,mBuf *dst)
{
	mList list = MLIST_INIT;
	CFFFontDict *fd;
	CFFDictItem *piname,*pipriv;
	int ret,i,pos;

	mBufInit(dst);

	//DICT 項目追加 (FontName, Private)
	// :すべての FontDICT 共通で使用する

	piname = (CFFDictItem *)mListAppendNew(&list, sizeof(CFFDictItem));
	pipriv = (CFFDictItem *)mListAppendNew(&list, sizeof(CFFDictItem));

	if(!piname || !pipriv) return MLKERR_ALLOC;

	piname->key = CFF_KEY2(38);
	piname->valnum = 1;

	pipriv->key = 18;
	pipriv->valnum = 2;

	//バッファ

	if(!mBufAlloc(dst, 1024, 1024))
	{
		mListDeleteAll(&list);
		return MLKERR_ALLOC;
	}

	//データ作成

	fd = p->fontdict;
	pos = 0;
	ret = 0;

	for(i = p->fontdict_num; i; i--, fd++)
	{
		//FontName SID
		piname->nval[0] = fd->fontname_sid;
	
		//Private: len = 1 で出力しない
		pipriv->len = (fd->list_private.num == 0);

		ret = fontio_cff_create_DICT(dst, &list, _fontdict_add_dict, &fd->outpos_private);
		if(ret) break;

		//DICT サイズ
		fd->write_tmp = dst->cursize - pos;
		
		pos = dst->cursize;
	}

	mListDeleteAll(&list);
	if(ret) mBufFree(dst);

	return ret;
}

/* Font DICT INDEX 書き込み */

static mlkerr _write_fontdict(SubsetFontCFF *p)
{
	CFFFontDict *fontdict;
	mBuf buf;
	int ret,i,fdnum;
	uint32_t offset;
	uint8_t offsize;

	//すべての DICT データを作成

	ret = _create_fontdict_data(p, &buf);
	if(ret) return ret;

	//

	fontdict = p->fontdict;
	fdnum = p->fontdict_num;
	ret = MLKERR_IO;

	//INDEX 先頭

	offsize = fontio_cff_calc_offsize(buf.cursize);

	if(fontio_cff_write_INDEX_top(p->b.io, p->fontdict_num, offsize))
		goto ERR;

	//オフセットデータ書き込み

	offset = 1;

	for(i = 0; i < fdnum; i++)
	{
		if(fontio_cff_write_offset(p->b.io, offset, offsize))
			goto ERR;

		offset += fontdict[i].write_tmp;
	}

	if(fontio_cff_write_offset(p->b.io, offset, offsize))
		goto ERR;

	//DICT データ書き込み

	offset = subsetfont_cff_get_outpos(p);

	if(mFILEwriteOK(p->b.fpout, buf.buf, buf.cursize))
		goto ERR;

	//outpos_private の位置を調整

	for(i = 0; i < fdnum; i++)
		fontdict[i].outpos_private += offset;

	//

	ret = 0;
ERR:
	mBufFree(&buf);

	return ret;
}

/* 各 Private DICT + Subr INDEX を書き込み */

static mlkerr _write_private_dict(SubsetFontCFF *p)
{
	FILE *fp = p->b.fpout;
	CFFFontDict *fd;
	CFFDictItem *pi;
	mBuf buf;
	int i,fdnum,ret = 0;
	int32_t outpos_subr;
	uint32_t offset;

	mBufInit(&buf);

	fdnum = p->fontdict_num;

	for(i = 0, fd = p->fontdict; i < fdnum; i++, fd++)
	{
		//Private がない

		if(!fd->list_private.num) continue;

		//Local Subr が一つも使用されない場合、Subr 項目を削除

		if(fd->subr.index.count && fd->subr.outnum == 0)
		{
			pi = fontio_cff_search_DICT_key(&fd->list_private, 19);
			if(pi)
				mListDelete(&fd->list_private, MLISTITEM(pi));
		}

		//DICT データ作成

		mBufReset(&buf);

		ret = fontio_cff_create_DICT(&buf, &fd->list_private, _privdict_add_dict, &outpos_subr);
		if(ret) break;

		//DICT 書き込み
		// :offset = Private DICT の先頭

		offset = subsetfont_cff_get_outpos(p);

		if(mFILEwriteOK(fp, buf.buf, buf.cursize))
		{
			ret = MLKERR_IO;
			break;
		}

		outpos_subr += offset;

		//Font DICT の Private の値を書き換え (29 <size> 29 <offset>)

		if(fseek(fp, fd->outpos_private + 1, SEEK_SET)
			|| mFILEwriteBE32(fp, buf.cursize)
			|| fseek(fp, 1, SEEK_CUR)
			|| mFILEwriteBE32(fp, offset)
			|| fseek(fp, 0, SEEK_END))
		{
			ret = MLKERR_IO;
			break;
		}

		//Local Subr INDEX

		if(fd->subr.outnum)
		{
			//サブルーチン書き込み
		
			ret = subsetfont_cff_write_charstring_INDEX(p, i);
			if(ret) break;

			//Private DICT の Subr の値を書き換え
			// :Private DICT の先頭を 0 とする

			if(fontio_write_b32_pos(p->b.io, outpos_subr + 1, buf.cursize))
			{
				ret = MLKERR_IO;
				break;
			}
		}
	}

	mBufFree(&buf);

	return ret;
}

/** FDArray 書き込み */

mlkerr subsetfont_cff_write_fdarray(SubsetFontCFF *p)
{
	int ret;

	p->offset_fdarray = subsetfont_cff_get_outpos(p);

	//Font DICT INDEX 書き込み

	ret = _write_fontdict(p);
	if(ret) return ret;

	//Private DICT

	return _write_private_dict(p);
}
