/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * TrueType: サブ関数
 ************************************/

#include <stdio.h>

#include <mlk.h>
#include <mlk_util.h>

#include "fontio.h"

#include "subsetfont.h"
#include "subsetfont_pv.h"
#include "subsetfont_tt.h"



/** loca オフセット読み込み
 *
 * pp: ポインタ位置。読み込み後、進む。 */

uint32_t subsetfont_tt_read_loca_offset(SubsetFontTT *p,uint8_t **pp)
{
	uint32_t v;
	uint8_t *ps = *pp;

	if(p->in_loca_size == 2)
	{
		//2byte
		
		v = ((ps[0] << 8) | ps[1]) * 2;
		*pp += 2;
	}
	else
	{
		//4byte

		v = ((uint32_t)ps[0] << 24) | (ps[1] << 16) | (ps[2] << 8) | ps[3];
		*pp += 4;
	}

	return v;
}

/** グリフデータ用のバッファを確保
 *
 * 最大のアウトラインデータサイズから確保する。 */

mlkerr subsetfont_tt_alloc_glyf_buf(SubsetFontTT *p)
{
	uint8_t *buf;
	int i;
	uint32_t off1,off2,max,len;

	//最大サイズ取得

	max = 1;
	buf = p->locabuf;

	off1 = subsetfont_tt_read_loca_offset(p, &buf);

	for(i = p->b.io->glyph_num; i; i--)
	{
		off2 = subsetfont_tt_read_loca_offset(p, &buf);

		len = off2 - off1;
		if(max < len) max = len;

		off1 = off2;
	}

	//確保

	max = (max + 3) & ~3;

	buf = (uint8_t *)mMalloc(max);
	if(!buf) return MLKERR_ALLOC;

	p->glyfbuf = buf;

	return 0;
}

/** 複合グリフの処理
 *
 * アウトラインデータ内で、参照する GID ごとに func が呼ばれる。
 * GID が範囲外の場合はエラーとなる。
 *
 * [関数]
 *  top = flags の位置
 *  戻り値 = エラーコード
 *
 * return: -1 で単純なグリフ */

mlkerr subsetfont_tt_glyf_composite(SubsetFontTT *p,uint8_t *buf,int len,
	SubsetFontTT_composite_func func,void *param)
{
	uint8_t *ps,*pend,*pstop;
	int16_t num;
	uint16_t gid,flags;
	int ret,in_gnum;

	if(len < 10) return MLKERR_INVALID_VALUE;

	//numberOfContours

	num = (int16_t)mGetBufBE16(buf);
	if(num >= 0) return -1;

	//複合グリフ

	ps = buf + 10;
	pend = buf + len;
	in_gnum = p->b.io->glyph_num;
	
	while(ps + 4 <= pend)
	{
		pstop = ps;
		
		flags = mGetBufBE16(ps);
		gid = mGetBufBE16(ps + 2);
		ps += 4;

		//引数をスキップ

		if(flags & 1) ps += 4; else ps += 2; //arg1,2

		if(flags & 8) ps += 2; //scale
		else if(flags & (1<<6)) ps += 4; //scale x,y
		else if(flags & (1<<7)) ps += 8; //2x2 matrix

		//関数

		if(gid >= in_gnum) return MLKERR_INVALID_VALUE;

		ret = (func)(_SF(p), pstop, gid, param);
		if(ret) return ret;

		//グリフが続くか

		if(! (flags & (1<<5)) ) break;
	}

	return 0;
}

/* 複合グリフの GID を、使用するグリフに追加 */

static int _glyf_add_composite(SubsetFont *p,uint8_t *top,uint16_t gid,void *param)
{
	if(!p->unimap[gid])
	{
		p->unimap[gid] = 1<<31;

		//新しく追加された場合、次回処理する
		p->gidmap_srcout[gid] = 1;
		*((int *)param) = 1;

		//printf("add GID %d\n", gid);
	}

	return 0;
}

/** 複合グリフの GID を、unimap に追加する */

/* [!] gidmap はまだ作成されていないので、unimap から処理するグリフを判定する。
   gidmap_srcout をフラグとして使う。
   複合グリフの GID は再帰的なので、新しく追加された GID がある場合は、再度処理を行う。
   ただし、深さが 1 以上になっているフォントはあまりない。 */

mlkerr subsetfont_tt_add_composite(SubsetFontTT *p)
{
	FontTblInfo *ptbl;
	FontIO *io;
	uint8_t *pl,*gbuf;
	uint32_t *pmap;
	uint16_t *pflag;
	uint32_t off1,off2,len,gtop;
	int i,ret,addflag;

	ret = fontio_move_table(p->b.io, MLK_MAKE32_4('g','l','y','f'), &ptbl);
	if(ret) return ret;

	//使用グリフのみ処理

	gbuf = p->glyfbuf;
	gtop = ptbl->offset;
	io = p->b.io;

	//処理フラグをクリア
	// (0=未処理、1=新しく追加、2=処理済み)
	mMemset0(p->b.gidmap_srcout, p->b.io->glyph_num * 2);

	do
	{
		pl = p->locabuf;
		pmap = p->b.unimap;
		pflag = p->b.gidmap_srcout;

		//新しい GID が追加された場合、1 になる
		addflag = 0;

		off1 = subsetfont_tt_read_loca_offset(p, &pl);

		for(i = p->b.io->glyph_num; i; i--, pmap++, pflag++)
		{
			off2 = subsetfont_tt_read_loca_offset(p, &pl);

			len = off2 - off1; //グリフデータがない場合 0

			if(len && *pmap && *pflag != 2)
			{
				//グリフを読み込んで、処理
				
				if(fontio_setpos(io, gtop + off1)
					|| fontio_read(io, gbuf, len))
					return MLKERR_DAMAGED;

				ret = subsetfont_tt_glyf_composite(p, gbuf, len, _glyf_add_composite, &addflag);

				if(ret == -1)
					//単純グリフなら、以降処理しない
					*pflag = 2;
				else if(ret)
					return ret;
			}

			off1 = off2;
		}
	} while(addflag);

	return 0;
}
