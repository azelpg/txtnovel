/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * CFF サブセット
 ************************************/

#include <stdio.h>

#include <mlk.h>
#include <mlk_stdio.h>
#include <mlk_list.h>
#include <mlk_file.h>
#include <mlk_util.h>

#include "fontio.h"
#include "fontio_cff.h"

#include "subsetfont.h"
#include "subsetfont_pv.h"
#include "subsetfont_cff.h"

#include "../app.h"


//========================
// 書き込み
//========================


/* Top DICT, String INDEX 書き込み */

static mlkerr _write_top_string(SubsetFontCFF *p)
{
	mList list = MLIST_INIT;
	int ret;

	//使用する文字列を取得 & SID 置き換え
	// :Top DICT を書き込む前に実行

	ret = subsetfont_cff_get_string_list(p, &list);
	if(ret) goto ERR;

	//Top DICT INDEX 書き込み

	ret = subsetfont_cff_write_topDICT(p);
	if(ret) goto ERR;

	//String INDEX 書き込み

	ret = subsetfont_cff_write_string_INDEX(p, &list);

ERR:
	mListDeleteAll(&list);
	
	return ret;
}

/* Charsets 書き込み
 *
 * すべて GID = CID としてセット */

static mlkerr _write_charsets(SubsetFontCFF *p)
{
	uint8_t d[5];

	//オフセット位置取得

	p->offset_charset = subsetfont_cff_get_outpos(p);

	//format = 2
	// :GID 1 から、SID の先頭位置, 個数 - 1

	d[0] = 2;

	if(p->b.out_glyph_num == 1)
	{
		//.notdef のみの場合、個数 = 0

		d[1] = d[2] = 0;

		if(mFILEwriteOK(p->b.fpout, d, 3))
			return MLKERR_IO;
	}
	else
	{
		mSetBufBE16(d + 1, 1);
		mSetBufBE16(d + 3, p->b.out_glyph_num - 2);

		if(mFILEwriteOK(p->b.fpout, d, 5))
			return MLKERR_IO;
	}

	return 0;
}

/* FDSelect 書き込み */

static mlkerr _write_fdselect(SubsetFontCFF *p)
{
	FILE *fp = p->b.fpout;
	uint8_t *buf,d[3],lastind;
	int gnum,i,next,cnt;

	p->offset_fdselect = subsetfont_cff_get_outpos(p);

	//format = 3, 個数

	d[0] = 3;
	d[1] = d[2] = 0;

	if(mFILEwriteOK(fp, d, 3))
		return MLKERR_IO;

	//データ書き込み
	// :GID 0 から順に、同じ index が連続する範囲を1個として書き込み

	buf = p->fdselect_buf;
	gnum = p->b.out_glyph_num;
	cnt = 0;

	for(i = 0; i < gnum; i = next)
	{
		//インデックスが異なる位置を検索 (= next)

		lastind = *(buf++);
		
		for(next = i + 1; next < gnum && *buf == lastind; next++, buf++);

		//書き込み (GID, index)

		mSetBufBE16(d, i);
		d[2] = lastind;

		if(mFILEwriteOK(fp, d, 3))
			return MLKERR_IO;

		cnt++;
	}

	//

	if(mFILEwriteBE16(fp, gnum)	//終端
		|| fseek(fp, p->offset_fdselect + 1, SEEK_SET)
		|| mFILEwriteBE16(fp, cnt)	//データ数
		|| fseek(fp, 0, SEEK_END))
		return MLKERR_IO;

	return 0;
}

/* 指定位置にオフセット値書き込み */

static mlkerr _write_offset(SubsetFontCFF *p,int writepos,uint32_t offset)
{
	if(fseek(p->b.fpout, writepos, SEEK_SET)
		|| mFILEwriteBE32(p->b.fpout, offset))
		return 1;

	return 0;
}

/* 最後に、Top DICT 項目のオフセット値書き込み */

static mlkerr _write_topdict_offset(SubsetFontCFF *p)
{
	if(_write_offset(p, p->outoffset_charset, p->offset_charset) //charset
		|| _write_offset(p, p->outoffset_charstrings, p->offset_charstrings) //CharStrings
		|| _write_offset(p, p->outoffset_fdselect, p->offset_fdselect) //FDSelect
		|| _write_offset(p, p->outoffset_fdarray, p->offset_fdarray)) //FDArray
		return MLKERR_IO;

	fseek(p->b.fpout, 0, SEEK_END);

	return 0;
}


//============================
// main
//============================


/* それぞれ読み込んで、Name INDEX まで書き込み */

static mlkerr _proc_top(SubsetFontCFF *p)
{
	FontIO *io = p->b.io;
	CFFIndex index_name,index_topdict;
	int ret;
	uint8_t head[4] = {1,0,4,4};

	//---- 以下、連続して読み込むこと

	//Name INDEX

	ret = fontio_cff_read_INDEX(io, &index_name, TRUE);
	if(ret) return ret;

	//Top DICT INDEX

	ret = fontio_cff_read_INDEX(io, &index_topdict, TRUE);
	if(ret) return ret;

	//String INDEX

	ret = fontio_cff_read_INDEX(io, &p->index_str, TRUE);
	if(ret) return ret;

	//Global Subr INDEX

	ret = fontio_cff_read_INDEX(io, &p->gsubr.index, FALSE);
	if(ret) return ret;

	ret = fontio_cff_read_INDEX_offset_data(io, &p->gsubr.index, &p->gsubr.buf);
	if(ret) return ret;

	//------ 

	//フォント名の位置とサイズ

	ret = fontio_cff_goto_INDEX_data(io, &index_name, 0, &p->name_len);
	if(ret) return ret;

	p->name_filepos = ftell(io->fpin);

	//Top DICT 項目読み込み

	ret = fontio_cff_read_DICT_INDEX(io, &index_topdict, 0, &p->list_topdic);
	if(ret) return ret;

	ret = subsetfont_cff_read_topDICTitem(p);
	if(ret) return ret;

	//---------

	//ヘッダ書き込み

	if(mFILEwriteOK(p->b.fpout, head, 4))
		return MLKERR_IO;

	//Name INDEX 書き込み

	return fontio_cff_write_INDEX_copy(io, &index_name);
}

/* データ読み込み */

static int _read_data(SubsetFontCFF *p)
{
	int ret;

	//Font DICT 読み込み or セット

	ret = subsetfont_cff_read_fontDICT(p);
	if(ret) return ret;

	//FDSelect 読み込み & 出力用に調整

	ret = subsetfont_cff_read_fdselect(p);
	if(ret) return ret;

	//CharStrings INDEX
	// :maxp のグリフ数と同じか

	ret = fontio_cff_read_INDEX_pos(p->b.io, p->offset_charstrings, &p->charstr.index);
	if(ret) return ret;

	if(p->charstr.index.count != p->b.io->glyph_num)
		return MLKERR_DAMAGED;

	ret = fontio_cff_read_INDEX_offset_data(p->b.io, &p->charstr.index, &p->charstr.buf);
	if(ret) return ret;

	//使用サブルーチンの判定
	// :サブルーチンが一つもない場合、CharStrings データは変化しないので、処理しない。
	// :また、その場合、作業ファイルは使わない。

	ret = subsetfont_cff_proc_charstrings(p);
	if(ret) return ret;

	return 0;
}

/* メイン処理 */

static int _proc_main(SubsetFontCFF *p)
{
	int ret;

	//それぞれ読み込んで、Name INDEX を書き込み

	ret = _proc_top(p);
	if(ret) return ret;

	//データ読み込み

	ret = _read_data(p);
	if(ret) return ret;
	
	//----- 書き込み

	//Top DICT INDEX, String INDEX

	ret = _write_top_string(p);
	if(ret) return ret;

	//Global Subr INDEX
	// :元が空、または使用グリフ内では1つも使用されていない場合、空にする

	if(p->gsubr.outnum == 0)
		ret = fontio_cff_write_INDEX_empty(p->b.io);
	else
		ret = subsetfont_cff_write_charstring_INDEX(p, -2);

	if(ret) return ret;

	//Charsets

	ret = _write_charsets(p);
	if(ret) return ret;

	//FDSelect

	ret = _write_fdselect(p);
	if(ret) return ret;

	//FDArray

	ret = subsetfont_cff_write_fdarray(p);
	if(ret) return ret;

	//CharStrings

	p->offset_charstrings = subsetfont_cff_get_outpos(p);

	ret = subsetfont_cff_write_charstring_INDEX(p, -1);
	if(ret) return ret;

	//Top DICT のオフセット値書き込み

	return _write_topdict_offset(p);
}


//==========================
// main
//==========================


/** CFF 解放 */

void subsetfont_cff_free(SubsetFont *sf)
{
	SubsetFontCFF *p = (SubsetFontCFF *)sf;
	CFFFontDict *fd;
	int i;

	mFree(p->fdselect_buf);

	mListDeleteAll(&p->list_topdic);

	subsetfont_cff_free_charstr(&p->gsubr);
	subsetfont_cff_free_charstr(&p->charstr);

	if(p->subrtmp_fp)
	{
		fclose(p->subrtmp_fp);

		app_delete_tmpfile(TMPFILENAME_SUBSET_CFF);
	}

	//Font DICT

	for(fd = p->fontdict, i = p->fontdict_num; i; i--, fd++)
		subsetfont_cff_free_fontdict(fd);

	mFree(p->fontdict);
}

/** CFF サブセット出力 */

mlkerr subsetfont_cff_output(SubsetFont *sf)
{
	SubsetFontCFF *p = (SubsetFontCFF *)sf;
	int ret;

	//CFF 開始

	ret = fontio_cff_start(sf->io);
	if(ret) return ret;

	//GID マップ作成

	ret = subsetfont_create_gidmap(sf);
	if(ret) return ret;

	//メイン処理

	return _proc_main(p);
}


//==========================
// サブ関数
//==========================


/** CFFCSDat を解放 */

void subsetfont_cff_free_charstr(CFFCSDat *p)
{
	mFree(p->buf);
	mFree(p->map);
}

/** CFFFontDict を解放 */

void subsetfont_cff_free_fontdict(CFFFontDict *p)
{
	mListDeleteAll(&p->list_private);

	subsetfont_cff_free_charstr(&p->subr);
}

/** 出力の現在位置を取得 */

uint32_t subsetfont_cff_get_outpos(SubsetFontCFF *p)
{
	return ftell(p->b.fpout);
}

