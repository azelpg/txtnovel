/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*****************************************
 * Font: FreeType
 *****************************************/

#include <math.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include FT_BITMAP_H
#include FT_OUTLINE_H

#include "mlk.h"
#include "mlk_charset.h"

#include "font.h"
#include "../image.h"


//---------------------

static FT_Library g_ftlib = 0;

#define _FACE(p) ((FT_Face)((p)->ft.face))

//---------------------


//===========================
// 初期化
//===========================


/** 初期化
 *
 * return: 0 で成功 */

int freetype_init(void)
{
	return FT_Init_FreeType(&g_ftlib);
}

/** フォント終了 */

void freetype_finish(void)
{
	if(g_ftlib)
		FT_Done_FreeType(g_ftlib);
}


//===========================
// Font
//===========================


/* FT_Face 作成 */

static FT_Face _create_face(const char *filename,int index)
{
	FT_Face face;
	FT_Error err;
	char *str;

	if(mLocaleCharsetIsUTF8())
		err = FT_New_Face(g_ftlib, filename, index, &face);
	else
	{
		//UTF-8 -> ロケール文字列

		str = mUTF8toLocale(filename, -1, NULL);
		if(!str) return NULL;

		err = FT_New_Face(g_ftlib, str, index, &face);

		mFree(str);
	}

	return (err)? NULL: face;
}

/** FreeType フォント解放 */

void __ft_face_free(Font *p)
{
	if(p->ft.face)
		FT_Done_Face(_FACE(p));
}

/** フォントファイルから作成
 *
 * filename: UTF-8
 * return: 0 で成功 */

int fontft_load(Font *p,const char *filename,int index,int antialias,int hinting)
{
	FT_Face face;

	//作成済み

	if(p->ft.face) return 0;

	//FT_Face 作成

	face = _create_face(filename, index);
	if(!face) return 1;

	p->ft.face = face;
	p->ft.render_mode = (antialias)? FT_RENDER_MODE_NORMAL: FT_RENDER_MODE_MONO;
	p->ft.load_flags = FT_LOAD_NO_BITMAP;

	switch(hinting)
	{
		//off
		case 0:
			p->ft.load_flags |= FT_LOAD_NO_HINTING;
			break;
		//normal
		case 1:
			p->ft.load_flags |= FT_LOAD_TARGET_NORMAL;
			break;
		//light
		case 2:
			p->ft.load_flags |= FT_LOAD_TARGET_LIGHT;
			break;
		//mono
		default:
			p->ft.load_flags |= FT_LOAD_TARGET_MONO;
			break;
	}

	return 0;
}

/** フォントサイズセット
 *
 * size: pt * 1000
 * dpi: 出力画像の DPI */

void fontft_set_size(Font *p,int size,double dpi)
{
	FT_Face face = _FACE(p);
	FT_Size_RequestRec req;

	if(p->ft.fontsize == size) return;

	p->ft.fontsize = size;

	//

	req.type = FT_SIZE_REQUEST_TYPE_NOMINAL;
	req.width = 0;
	req.height = floor((size / 1000.0) / 72 * dpi * (1<<6));
	req.horiResolution = 0;
	req.vertResolution = 0;

	FT_Request_Size(face, &req);

	//ascent,descent (26:6)

	p->ft.ascent  = FT_MulFix(-(p->ascent), face->size->metrics.y_scale);
	p->ft.descent = FT_MulFix(-(p->descent), face->size->metrics.y_scale);
}


//==========================
// グリフ描画
//==========================


/* GID から、ビットマップグリフをロード (横書き用)
 *
 * pos: 位置[26:6]が入っている。描画の左上位置が入る。 */

static mlkbool _loadglyph_horz(Font *p,uint16_t gid,mPoint *pos)
{
	FT_Face face = _FACE(p);

	//グリフスロットにロード

	if(FT_Load_Glyph(face, gid, p->ft.load_flags))
		return FALSE;

	//座標移動 (原点をグリフ左上へ。位置の少数部分を移動)

	FT_Outline_Translate(&face->glyph->outline,
		pos->x & 63, -(pos->y & 63) + p->ft.ascent);

	//ビットマップに変換

	if(FT_Render_Glyph(face->glyph, p->ft.render_mode))
		return FALSE;

	//

	pos->x = (pos->x >> 6) + face->glyph->bitmap_left;
	pos->y = (pos->y >> 6) - face->glyph->bitmap_top;

	return TRUE;
}

/* GID から、ビットマップグリフをロード (縦書き用) */

static mlkbool _loadglyph_vert(Font *p,uint16_t gid,int frotate,mPoint *pos)
{
	FT_Face face = _FACE(p);
	FT_Matrix matrix;
	int origin;

	//グリフスロットにロード

	if(FT_Load_Glyph(face, gid, p->ft.load_flags | FT_LOAD_VERTICAL_LAYOUT))
		return FALSE;

	//Y 原点 (VORG または ascent)

	origin = font_get_origin_y(p, gid);

	//

	if(frotate)
	{
		//90度回転
		// :回転後は、グリフの左・ベースライン位置が原点になる

		matrix.xx = matrix.yy = 0;
		matrix.xy = 1<<16;
		matrix.yx = ((FT_ULong)-1) << 16;

		FT_Outline_Transform(&face->glyph->outline, &matrix);

		FT_Outline_Translate(&face->glyph->outline, p->ft.descent + (pos->x & 63), -(pos->y & 63));
	}
	else
	{
		//原点をグリフ左上へ移動
		
		FT_Outline_Translate(&face->glyph->outline,
			pos->x & 63,
			-(pos->y & 63) + FT_MulFix(-origin, face->size->metrics.y_scale));
	}

	//ビットマップに変換

	if(FT_Render_Glyph(face->glyph, p->ft.render_mode))
		return FALSE;

	//

	pos->x = (pos->x >> 6) + face->glyph->bitmap_left;
	pos->y = (pos->y >> 6) - face->glyph->bitmap_top;
	
	return TRUE;
}

/* スロットにロードされているグリフを描画 (縦/横書き共通) */

static void _draw_glyph(Font *p,Image *img,int x,int y,uint32_t col)
{
	FT_Bitmap *bm;
	uint8_t *pbuf,*pb,f;
	int ix,iy,xend,yend,w,h,pitch;

	bm = &_FACE(p)->glyph->bitmap;
	
	w = bm->width;
	h = bm->rows;
	pbuf  = bm->buffer;
	pitch = bm->pitch;
	
	if(pitch < 0) pbuf += -pitch * (h - 1);
	
	//
	
	if(bm->pixel_mode == FT_PIXEL_MODE_MONO)
	{
		//1bit モノクロ

		xend = x + w;
		yend = y + h;

		for(iy = y; iy < yend; iy++, pbuf += pitch)
		{
			for(ix = x, f = 0x80, pb = pbuf; ix < xend; ix++)
			{
				if(*pb & f)
					image_setpixel(img, ix, iy, col);
				
				f >>= 1;
				if(!f) { f = 0x80; pb++; }
			}
		}
	}
	else
	{
		//8bit グレイスケール
		
		xend = x + w;
		yend = y + h;
		pitch -= w;

		for(iy = y; iy < yend; iy++, pbuf += pitch)
		{
			for(ix = x; ix < xend; ix++, pbuf++)
			{
				if(*pbuf)
					image_setpixel_alpha(img, ix, iy, col, *pbuf);
			}
		}
	}
}

/** グリフを横書き描画
 *
 * x,y: グリフの左上 (26:6) */

void fontft_draw_glyph_horz(Font *p,Image *img,int x,int y,uint16_t gid,uint32_t col)
{
	mPoint pos;

	pos.x = x;
	pos.y = y;

	if(_loadglyph_horz(p, gid, &pos))
		_draw_glyph(p, img, pos.x, pos.y, col);
}

/** グリフを縦書き描画
 *
 * x,y: グリフの左上 (26:6)
 * frotate: 90度回転 */

void fontft_draw_glyph_vert(Font *p,Image *img,int x,int y,uint16_t gid,uint32_t col,int frotate)
{
	mPoint pos;

	pos.x = x;
	pos.y = y;

	if(_loadglyph_vert(p, gid, frotate, &pos))
		_draw_glyph(p, img, pos.x, pos.y, col);
}
