/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

typedef struct _Image Image;

#define FONT_GSUB_NUM  7

enum
{
	FONT_DATA_UNIMAP,	//Unicode -> GID マップ
	FONT_DATA_UVS,		//異体字マップ
	FONT_DATA_HWIDTH,	//水平グリフ幅
	FONT_DATA_VWIDTH,	//垂直グリフ幅
	FONT_DATA_VORG,		//VORG
	FONT_DATA_CIDMAP,	//CID -> GID マップ
	FONT_DATA_GPOS_HALT, //GPOS (全角幅→半角幅)
	FONT_DATA_GSUB_VERT, //vert + vrt2
	FONT_DATA_GSUB_RUBY,
	FONT_DATA_GSUB_NLCK,
	FONT_DATA_GSUB_HKNA,
	FONT_DATA_GSUB_VKNA,
	FONT_DATA_GSUB_HWID,
	FONT_DATA_GSUB_TWID,

	FONT_DATA_NUM
};

/* FontFT */

typedef struct
{
	int32_t fontsize,
		render_mode,
		ascent,
		descent;
	uint32_t load_flags;
	void *face;	//FT_Face
}FontFT;

/* Font */

typedef struct _Font
{
	int16_t box_minx,
		box_miny,
		box_maxx,
		box_maxy,
		ascent,
		descent,
		cap_height,
		pdf_resno_horz,	//PDF でのリソース番号 (0 で未処理、-1 で使用しない)
		pdf_resno_vert;
	uint16_t flags,
		pdf_use_flags;	//ページごとに使用されたかのフラグ
	int emsize,
		glyph_num,
		subset_glyph_num,	//サブセットフォントのグリフ数
		refcnt;				//参照カウント
	uint32_t offset[FONT_DATA_NUM],	//データのオフセット位置
		data_size;		//data のバッファサイズ

	char *postname;		//PostScript 名 (NULL でなし。最大 127 文字)
	uint8_t *data;		//フォント情報のバッファ
	uint32_t *unimap;	//srcGID -> Unicode のマップ (サブセット用)
		//下位 24bit: Unicode (0 で対応するコードなし)。上位 8bit:フラグ
	uint16_t *gidmap;	//サブセットの srcGID -> outGID マップ
		//サブセット作成時、GID 0 以外が使用されていなければ NULL

	FontFT ft;	//FreeType
}Font;

enum
{
	FONT_FLAGS_CFF = 1<<0,			//CFF フォントか
	FONT_FLAGS_MONOSPACE = 1<<1		//等幅
};

enum
{
	FONT_UNIMAP_F_USE = 1<<31,			//使用
	FONT_UNIMAP_F_USE_HORZ = 1<<30,		//横書きで使用
	FONT_UNIMAP_F_USE_VERT = 1<<29,		//縦書きで使用
	FONT_UNIMAP_F_HORZ_WIDTH = 1<<28,	//横書きの送り幅が emsize と異なる
	FONT_UNIMAP_F_VERT_WIDTH = 1<<27,	//縦書きの送り幅が emsize と異なる

	FONT_UNIMAP_MASK_CODE = 0xffffff
};

/* サブセット情報 */

typedef struct _FontSubsetInfo
{
	uint8_t has_horz,	//横書きのグリフが使われているか
		has_vert;
	int horz_num,		//横書き用のメトリクス数
		vert_num;
	uint16_t *buf_horz,	//[GID,送り幅(int16)] x num
		*buf_vert;		//[GID,幅(int16),Y原点(int16)] x num (VORG があるものも含む)
}FontSubsetInfo;

//------------------

void font_free(Font *p);
int font_loadfile(Font **dst,const char *filename,int index);
void font_addref(Font *p);

int font_convert_value(Font *p,int v);
int font_regist_glyph(Font *p,uint16_t gid,uint32_t code,int fontsize,int vert);
void font_get_subset_info(Font *p,FontSubsetInfo *dst);
int font_get_pdf_metrics(Font *p,uint16_t *map_outsrc,FontSubsetInfo *dst);

/* data */

uint16_t font_get_unicode_gid(Font *p,uint32_t c);
uint16_t font_get_uvs_gid(Font *p,uint32_t sc,uint32_t c);
int16_t font_get_advance_width(Font *p,int vert,uint16_t gid);
int16_t font_get_origin_y(Font *p,uint16_t gid);
uint16_t font_get_gsub(Font *p,int datano,uint16_t gid);
int font_get_gid_list(Font *p,int datano,uint16_t gid);
uint16_t font_get_cid_to_gid(Font *p,uint16_t cid);

/* freetype */

int freetype_init(void);
void freetype_finish(void);

int fontft_load(Font *p,const char *filename,int index,int antialias,int hinting);
void fontft_set_size(Font *p,int size,double dpi);

void fontft_draw_glyph_horz(Font *p,Image *img,int x,int y,uint16_t gid,uint32_t col);
void fontft_draw_glyph_vert(Font *p,Image *img,int x,int y,uint16_t gid,uint32_t col,int frotate);

