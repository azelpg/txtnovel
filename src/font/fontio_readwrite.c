/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * FontIO: 読み書き
 *********************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <mlk.h>
#include <mlk_stdio.h>

#include "fontio.h"



//============================
// ファイルから
//============================


/** ファイル先頭からの位置をセット */

int fontio_setpos(FontIO *p,uint32_t pos)
{
	return (fseek(p->fpin, pos, SEEK_SET) != 0);
}

/** 現在位置からシーク */

int fontio_seek(FontIO *p,int size)
{
	return (fseek(p->fpin, size, SEEK_CUR) != 0);
}

/** 16bit 読み込み */

int fontio_read16(FontIO *p,void *dst)
{
	return mFILEreadBE16(p->fpin, dst);
}

/** 32bit 読み込み */

int fontio_read32(FontIO *p,void *dst)
{
	return mFILEreadBE32(p->fpin, dst);
}

/** 指定サイズ読み込み */

int fontio_read(FontIO *p,void *buf,int size)
{
	return (fread(buf, 1, size, p->fpin) != size);
}

/** フォーマットで読み込み
 *
 * h = 16bit, i = 32bit, s = skip
 * 前に数値 */

int fontio_read_format(FontIO *p,const char *format,...)
{
	va_list ap;
	char c;
	uint8_t b[4],*pd,*ps;
	int bytes,num,i;

	va_start(ap, format);

	while(1)
	{
		c = *(format++);
		if(!c) break;

		//数値

		num = 1;

		if(c >= '0' && c <= '9')
		{
			num = strtol(format - 1, (char **)&format, 10);

			c = *(format++);
			if(!c) break;
		}

		//

		switch(c)
		{
			case 'h':
				bytes = 2;
				break;
			case 'i':
				bytes = 4;
				break;
			default:
				if(fontio_seek(p, num)) goto ERR;
				continue;
		}

		//読み込み

		pd = va_arg(ap, uint8_t *);

		for(; num > 0; num--)
		{
			if(fread(b, 1, bytes, p->fpin) != bytes)
				goto ERR;

		#if defined(MLK_BIG_ENDIAN)

			memcpy(pd, b, bytes);
			pd += bytes;

		#else

			ps = b + bytes - 1;

			for(i = bytes; i; i--)
				*(pd++) = *(ps--);

		#endif
		}
	}

	va_end(ap);

	return 0;

ERR:
	va_end(ap);
	return 1;
}

/** 確保したバッファに読み込み */

mlkerr fontio_read_alloc(FontIO *p,uint32_t size,uint8_t **dst)
{
	uint8_t *buf;

	buf = (uint8_t *)mMalloc(size);
	if(!buf) return MLKERR_ALLOC;

	if(fontio_read(p, buf, size))
	{
		mFree(buf);
		return MLKERR_DAMAGED;
	}

	*dst = buf;

	return 0;
}

/** 確保したバッファに読み込み
 *
 * allocsize: 確保サイズの最小値。readsize の方が大きければ、readsize
 * readsize: 読み込むサイズ */

mlkerr fontio_read_alloc2(FontIO *p,uint32_t allocsize,uint32_t readsize,uint8_t **dst)
{
	uint8_t *buf;

	buf = (uint8_t *)mMalloc((allocsize > readsize)? allocsize: readsize);
	if(!buf) return MLKERR_ALLOC;

	if(fontio_read(p, buf, readsize))
	{
		mFree(buf);
		return MLKERR_DAMAGED;
	}

	*dst = buf;

	return 0;
}


//=========================
// バッファから読み込み
//=========================


/** バッファをセット */

void fontio_readbuf_set(FontIO *p,void *buf,uint32_t size)
{
	p->bufcur = p->buftop = (uint8_t *)buf;
	p->buf_size = p->buf_remain = size;
}

/** 確保したバッファに読み込んで、セット
 *
 * [!] p->buftop のメモリを解放すること */

mlkerr fontio_readbuf_alloc_set(FontIO *p,uint32_t size)
{
	uint8_t *buf;
	int ret;

	ret = fontio_read_alloc(p, size, &buf);
	if(ret) return ret;

	fontio_readbuf_set(p, buf, size);

	return 0;
}

/** 現在位置をセット */

int fontio_readbuf_setpos(FontIO *p,uint32_t pos)
{
	if(pos > p->buf_size) return 1;

	p->bufcur = p->buftop + pos;
	p->buf_remain = p->buf_size - pos;

	return 0;
}

/** 現在位置を移動 */

int fontio_readbuf_seek(FontIO *p,int pos)
{
	if(pos > p->buf_remain) return 1;

	p->bufcur += pos;
	p->buf_remain -= pos;

	return 0;
}

/** バッファから 16bit 値読み込み */

int fontio_readbuf16(FontIO *p,void *dst)
{
	if(p->buf_remain < 2) return 1;

	*((uint16_t *)dst) = (p->bufcur[0] << 8) | p->bufcur[1];

	p->bufcur += 2;
	p->buf_remain -= 2;

	return 0;
}

/** バッファから 24bit 値読み込み */

int fontio_readbuf24(FontIO *p,void *dst)
{
	uint8_t *ps = p->bufcur;

	if(p->buf_remain < 3) return 1;

	*((uint32_t *)dst) = (ps[0] << 16) | (ps[1] << 8) | ps[2];

	p->bufcur += 3;
	p->buf_remain -= 3;

	return 0;
}

/** バッファから 32bit 値読み込み */

int fontio_readbuf32(FontIO *p,void *dst)
{
	uint8_t *ps = p->bufcur;

	if(p->buf_remain < 4) return 1;

	*((uint32_t *)dst) = ((uint32_t)ps[0] << 24) | (ps[1] << 16) | (ps[2] << 8) | ps[3];

	p->bufcur += 4;
	p->buf_remain -= 4;

	return 0;
}

/** バッファの指定位置から 16bit 値読み込み (位置は移動しない) */

int fontio_readbuf16_pos(FontIO *p,uint32_t pos,void *dst)
{
	uint8_t *ps;

	if(pos + 2 > p->buf_size) return 1;

	ps = p->buftop + pos;

	*((uint16_t *)dst) = (ps[0] << 8) | ps[1];

	return 0;
}


//===========================
// 書き込み
//===========================


/** 16bit 値を出力 (host) */

void fontio_write_h16(FontIO *p,uint16_t v)
{
	fwrite(&v, 1, 2, p->fpout);
}

/** 32bit 値を出力 (host) */

void fontio_write_h32(FontIO *p,uint32_t v)
{
	fwrite(&v, 1, 4, p->fpout);
}

/** 24bit 値を出力 (BE) */

void fontio_write_b24(FontIO *p,uint32_t v)
{
	uint8_t b[3];

	b[0] = (uint8_t)(v >> 16);
	b[1] = (uint8_t)(v >> 8);
	b[2] = (uint8_t)v;

	fwrite(b, 1, 3, p->fpout);
}

/** 指定位置に 16bit 値を出力して、終端に戻る */

void fontio_write_h16_pos(FontIO *p,long pos,uint16_t v)
{
	fseek(p->fpout, pos, SEEK_SET);

	fwrite(&v, 1, 2, p->fpout);

	fseek(p->fpout, 0, SEEK_END);
}

/** 指定位置に 32bit(BE) を出力して、終端に戻る */

int fontio_write_b32_pos(FontIO *p,uint32_t pos,uint32_t v)
{
	FILE *fp = p->fpout;

	if(fseek(fp, pos, SEEK_SET)
		|| mFILEwriteBE32(fp, v)
		|| fseek(fp, 0, SEEK_END))
		return 1;

	return 0;
}

/** 出力の現在位置を 4byte 境界にする */

void fontio_write_align4(FontIO *p)
{
	uint32_t pos,tmp = 0;

	pos = ftell(p->fpout) & 3;

	if(pos)
	{
		tmp = 0;
		fwrite(&tmp, 1, 4 - pos, p->fpout);
	}
}


//===========================
// テーブル書き込み
//===========================


/** テーブルのチェックサム計算
 *
 * size: [!] 常に 4byte 単位で計算する
 * cksum: 初期値は 0 */

void fontio_calc_checksum(const uint8_t *buf,uint32_t size,uint32_t *cksum)
{
	uint32_t sum;

	sum = *cksum;
	size = (size + 3) / 4; //4byte 単位の個数

	//BE の 4byte 値として、加算していく

	for(; size; size--, buf += 4)
		sum += ((uint32_t)buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3];

	*cksum = sum;
}

/** テーブルデータの書き込み (バッファからすべて)
 *
 * - バッファのサイズは 4byte 単位であること。
 * - size が 4 の倍数でない場合は、自動で 0 を追加。
 *
 * table: 出力の情報がセットされる。
 *   tmp = 書き込まれたテーブルの先頭オフセット */

mlkerr fontio_write_table_buf(FontIO *p,FontTblInfo *table,FontTblBuf *buf)
{
	uint8_t *pbuf;
	uint32_t size,size4,cksum;

	pbuf = buf->buf;
	size = buf->size;
	size4 = (size + 3) & ~3;

	if(size != size4)
		memset(pbuf + size, 0, size4 - size);

	cksum = 0;
	fontio_calc_checksum(pbuf, size4, &cksum);

	//書き込み後の情報

	table->size = size;
	table->cksum = cksum;
	table->tmp = ftell(p->fpout);

	//書き込み

	if(fwrite(pbuf, 1, size4, p->fpout) != size4)
		return MLKERR_IO;
	
	return 0;
}

/** 指定テーブルをコピーして出力
 *
 * テーブルが存在しない場合は成功扱い。 */

mlkerr fontio_write_table_copy(FontIO *p,uint32_t tag)
{
	FontTblInfo *ptbl;
	FontTblBuf tb;
	int ret;

	//読み込み
	// :存在しない場合は成功

	ret = fontio_read_table(p, tag, &ptbl, &tb, 0);

	if(ret == MLKERR_UNFOUND)
		return MLKERR_OK;
	else if(ret)
		return ret;

	//出力オフセット位置

	ptbl->tmp = ftell(p->fpout);

	//書き込み

	if(fwrite(tb.buf, 1, tb.size4, p->fpout) != tb.size4)
		ret = MLKERR_IO;

	mFree(tb.buf);

	return ret;
}

