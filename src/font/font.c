/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * レイアウト用のフォント情報
 *********************************/

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "mlk.h"
#include "mlk_str.h"
#include "mlk_file.h"
#include "mlk_stdio.h"
#include "mlk_charset.h"
#include "mlk_zlib.h"

#include "font.h"
#include "fontio.h"

#include "../app.h"


/* 一度読み込んだフォントは、キャッシュを作成する */

//------------------

#define _ERR_OPEN_TMPFILE -1

static const uint32_t g_tag_gsub[] = {
	MLK_MAKE32_4('v','e','r','t'),
	MLK_MAKE32_4('r','u','b','y'), MLK_MAKE32_4('n','l','c','k'),
	MLK_MAKE32_4('h','k','n','a'), MLK_MAKE32_4('v','k','n','a'),
	MLK_MAKE32_4('h','w','i','d'), MLK_MAKE32_4('t','w','i','d')
};

void __ft_face_free(Font *p);

//------------------


//===========================
// フォントファイルから
//===========================


/* フォントファイルから読み込んで、データを書き込み */

static mlkerr _read_fontfile_write(Font *p,FILE *fp,const char *filename,int index)
{
	FontIO *io;
	int ret;
	int16_t v16[4];
	uint16_t u16;

	ret = fontio_openfile(&io, filename, index);
	if(ret) return ret;

	io->fpout = fp;

	//CFF

	if(io->is_cff)
		p->flags |= FONT_FLAGS_CFF;

	//オフセットを 0 にしないため、先頭に 4byte

	mFILEwrite0(fp, 4);

	//PostScript 名 (NULL でなし)

	fontio_table_name_postscript(io, &p->postname);

	//グリフ数

	p->glyph_num = io->glyph_num;

	//emsize, box

	ret = fontio_table_head(io, &u16, v16);
	if(ret) goto ERR;

	p->emsize = u16;
	p->box_minx = v16[0];
	p->box_miny = v16[1];
	p->box_maxx = v16[2];
	p->box_maxy = v16[3];

	//ascent, descent, capheight, monospace

	ret = fontio_table_os2(io, &p->ascent, &p->descent, &p->cap_height, &u16);
	if(ret) goto ERR;

	if(u16)
		p->flags |= FONT_FLAGS_MONOSPACE;

	//cmap

	ret = fontio_cache_cmap(io, p->offset + FONT_DATA_UNIMAP, p->offset + FONT_DATA_UVS);
	if(ret) goto ERR;

	//hmtx

	ret = fontio_cache_hvmtx(io, 0, p->emsize, p->offset + FONT_DATA_HWIDTH);
	if(ret) goto ERR;

	//vmtx

	ret = fontio_cache_hvmtx(io, 1, p->emsize, p->offset + FONT_DATA_VWIDTH);
	if(ret) goto ERR;

	//VORG

	ret = fontio_cache_VORG(io, p->offset + FONT_DATA_VORG);
	if(ret) goto ERR;

	//CID map

	ret = fontio_cache_cidmap(io, p->offset + FONT_DATA_CIDMAP);
	if(ret) goto ERR;

	//GSUB

	ret = fontio_cache_GSUB(io, g_tag_gsub, p->offset + FONT_DATA_GSUB_VERT, FONT_GSUB_NUM);
	if(ret) goto ERR;

	//GPOS

	ret = fontio_cache_GPOS(io, p->offset + FONT_DATA_GPOS_HALT);
	if(ret) goto ERR;

	//

	ret = 0;
ERR:
	fontio_close(io);

	return ret;
}

/* キャッシュデータを書き込み */

static mlkerr _write_cache(Font *p,FILE *fp)
{
	mZlib *zlib;
	int len,i,ret;
	long fpos;

	//version

	mFILEwriteBE16(fp, 0);

	//PostScript 名

	len = mStrlen(p->postname);

	mFILEwriteByte(fp, len);

	if(len) fwrite(p->postname, 1, len, fp);

	//--------

	//情報のサイズ

	mFILEwriteBE16(fp, 20);

	//各情報

	mFILEwriteBE16(fp, p->glyph_num);
	mFILEwriteBE16(fp, p->emsize);
	mFILEwriteBE16(fp, p->box_minx);
	mFILEwriteBE16(fp, p->box_miny);
	mFILEwriteBE16(fp, p->box_maxx);
	mFILEwriteBE16(fp, p->box_maxy);
	mFILEwriteBE16(fp, p->ascent);
	mFILEwriteBE16(fp, p->descent);
	mFILEwriteBE16(fp, p->cap_height);
	mFILEwriteBE16(fp, p->flags);

	//--------

	//データの数

	mFILEwriteBE16(fp, FONT_DATA_NUM);

	//オフセット

	for(i = 0; i < FONT_DATA_NUM; i++)
		mFILEwriteBE32(fp, p->offset[i]);
	
	//データサイズ

	mFILEwriteBE32(fp, p->data_size);

	fpos = ftell(fp);
	mFILEwriteBE32(fp, 0);

	//データ

	zlib = mZlibEncNew(8192, 6, -15, 8, 0);
	if(!zlib) return MLKERR_ALLOC;

	mZlibSetIO_stdio(zlib, fp);

	ret = mZlibEncSend(zlib, p->data, p->data_size);
	if(!ret)
	{
		ret = mZlibEncFinish(zlib);
		if(!ret)
		{
			fseek(fp, fpos, SEEK_SET);
			mFILEwriteBE32(fp, mZlibEncGetSize(zlib));
			fseek(fp, 0, SEEK_END);
		}
	}

	mZlibFree(zlib);

	return ret;
}

/* フォントファイルから読み込んで、キャッシュ作成 */

static mlkerr _read_fontfile(Font *p,FILE *fp,const char *filename,int index,const char *cachename)
{
	FILE *fpout;
	int ret;
	uint32_t size;

	//フォントから読み込んで、作業ファイルにデータを書き込み

	ret = _read_fontfile_write(p, fp, filename, index);
	if(ret) return ret;

	//データを読み込み

	size = ftell(fp);

	p->data = (uint8_t *)mMalloc(size);
	if(!p->data) return MLKERR_ALLOC;

	p->data_size = size;

	rewind(fp);

	if(fread(p->data, 1, size, fp) != size)
		return MLKERR_IO;

	//キャッシュデータを書き込み
	// :書き込めなかった場合も成功とする

	if(!(g_app->flags & APP_FLAGS_NO_CACHE))
	{
		fpout = mFILEopen(cachename, "wb");

		if(fpout)
		{
			_write_cache(p, fpout);

			fclose(fpout);
		}
	}

	return 0;
}


//===========================
// キャッシュから読み込み
//===========================


/* キャッシュから読み込み
 *
 * return: 0 で成功、-1 でメモリ確保エラー、それ以外でその他のエラー */

static int _read_cache(Font *p,FILE *fp)
{
	uint16_t wd,ar[10];
	uint8_t len;
	uint32_t uncompsize,compsize;
	int n;
	mZlib *zlib;

	//バージョン

	if(mFILEreadBE16(fp, &wd) || wd != 0)
		return 1;

	//PostScript 名

	if(mFILEreadByte(fp, &len)) return 1;

	if(len)
	{
		p->postname = (char *)mMalloc(len + 1);
		if(!p->postname) return -1;

		if(mFILEreadOK(fp, p->postname, len)) return 1;

		p->postname[len] = 0;
	}

	//情報

	if(mFILEreadBE16(fp, &wd)
		|| mFILEreadArrayBE16(fp, ar, 10) != 10
		|| fseek(fp, wd - 10 * 2, SEEK_CUR))
		return 1;

	p->glyph_num = ar[0];
	p->emsize = ar[1];
	p->box_minx = (int16_t)ar[2];
	p->box_miny = (int16_t)ar[3];
	p->box_maxx = (int16_t)ar[4];
	p->box_maxy = (int16_t)ar[5];
	p->ascent = (int16_t)ar[6];
	p->descent = (int16_t)ar[7];
	p->cap_height = (int16_t)ar[8];
	p->flags = ar[9];

	//オフセット

	if(mFILEreadBE16(fp, &wd)) return 1;

	n = (wd > FONT_DATA_NUM)? FONT_DATA_NUM: wd;

	if(mFILEreadArrayBE32(fp, p->offset, n) != n
		|| fseek(fp, (wd - n) * 4, SEEK_CUR))
		return 1;

	//データ

	if(mFILEreadBE32(fp, &uncompsize)
		|| mFILEreadBE32(fp, &compsize))
		return 1;

	p->data = (uint8_t *)mMalloc(uncompsize);
	if(!p->data) return -1;

	p->data_size = uncompsize;

	//

	zlib = mZlibDecNew(8192, -15);
	if(!zlib) return 1;

	mZlibSetIO_stdio(zlib, fp);

	n = mZlibDecReadOnce(zlib, p->data, uncompsize, compsize);

	mZlibFree(zlib);

	return (n != 0);
}


//===========================
// main
//===========================


/* 読み込み */

static mlkerr _proc_read(Font *p,mStr *str_cache,const char *filename,int index)
{
	FILE *fp;
	char *tmpname;
	int ret;

	//キャッシュファイル名

	mStrPathGetBasename(str_cache, filename);
	mStrReplaceChar(str_cache, '.', '_');
	mStrAppendFormat(str_cache, "-%d.cache", index);

	mStrPathJoin_before(str_cache, g_app->str_cachedir.buf);

	//キャッシュファイルから読み込み
	// :キャッシュがあり、更新日時が cache > font の場合は、読み込み。

	if(mCompareFileModify(str_cache->buf, filename) > 0)
	{
		fp = mFILEopen(str_cache->buf, "rb");

		if(fp)
		{
			ret = _read_cache(p, fp);

			fclose(fp);

			if(!ret)
				//成功
				return 0;
			else if(ret == -1)
				//メモリエラー。それ以外は失敗時へ
				return MLKERR_ALLOC;
		}
	}

	//キャッシュから読み込めなかった or フォントが更新された場合は、新規作成

	tmpname = app_set_tmpfile(TMPFILENAME_FONT_READ);
	
	fp = mFILEopen(tmpname, "w+b");
	if(!fp) return _ERR_OPEN_TMPFILE;

	ret = _read_fontfile(p, fp, filename, index, str_cache->buf);

	fclose(fp);

	//作業ファイル削除

	mDeleteFile(tmpname);

	return ret;
}

/* 確保 */

static int _alloc_buf(Font *p)
{
	//unimap

	p->unimap = (uint32_t *)mMalloc0(4 * p->glyph_num);
	if(!p->unimap) return 1;

	p->unimap[0] = FONT_UNIMAP_F_USE;

	return 0;
}

/** 解放 */

void font_free(Font *p)
{
	if(p)
	{
		p->refcnt--;
		if(p->refcnt > 0) return;

		//FreeType
		__ft_face_free(p);
	
		mFree(p->postname);
		mFree(p->data);
		mFree(p->unimap);
		mFree(p->gidmap);
		
		mFree(p);
	}
}

/** フォントファイルから読み込み
 *
 * return: 0 で成功。失敗時はエラー表示済み */

int font_loadfile(Font **dst,const char *filename,int index)
{
	Font *p;
	mStr str_cache = MSTR_INIT;
	int ret;

	//フォントの存在確認

	if(!mIsExistFile(filename))
	{
		app_puterr("フォントファイルが存在しません", filename);
		return 1;
	}

	//確保

	p = (Font *)mMalloc0(sizeof(Font));
	if(!p) return 1;

	p->refcnt = 1;

	//読み込み

	ret = _proc_read(p, &str_cache, filename, index);

	//確保

	if(!ret)
		ret = _alloc_buf(p);

	//エラー時

	if(ret)
	{
		font_free(p);
		p = NULL;

		//エラー

		if(ret == _ERR_OPEN_TMPFILE)
			app_puterrno(MLKERR_OPEN, g_app->str_tmpdir.buf);
		else
			app_puterrno(ret, filename);
	}

	mStrFree(&str_cache);

	*dst = p;

	return ret;
}

/** 参照カウンタを +1 */

void font_addref(Font *p)
{
	p->refcnt++;
}

/** フォントのデザイン単位を 1000 単位に変換して取得 */

int font_convert_value(Font *p,int v)
{
	if(p->emsize == 1000)
		return v;
	else
		return lround((double)v / p->emsize * 1000);
}


/** GID を使用するグリフとして登録し、用紙上の幅を取得
 *
 * code: 0 で、対応する Unicode はない */

int font_regist_glyph(Font *p,uint16_t gid,uint32_t code,int fontsize,int vert)
{
	int w;
	uint32_t v;

	w = font_get_advance_width(p, vert, gid);

	//GID -> Unicode マップ
	// :すでに設定されていても、フラグは追加する (縦書き/横書きでそれぞれ来る場合があるため)
	// :Unicode は、0 の場合のみ上書き。

	v = p->unimap[gid];

	if(!(v & FONT_UNIMAP_MASK_CODE))
		v |= code;

	if(vert)
	{
		v |= FONT_UNIMAP_F_USE_VERT;

		if(w != p->emsize)
			v |= FONT_UNIMAP_F_VERT_WIDTH;
	}
	else
	{
		v |= FONT_UNIMAP_F_USE_HORZ;

		if(w != p->emsize)
			v |= FONT_UNIMAP_F_HORZ_WIDTH;
	}

	p->unimap[gid] = v | FONT_UNIMAP_F_USE;

	//幅

	return lround((double)w / p->emsize * fontsize);
}

/** すべての使用グリフの列挙後、情報を取得 */

void font_get_subset_info(Font *p,FontSubsetInfo *dst)
{
	uint32_t *puni,v;
	int i,ascent,hnum,vnum,gnum;
	uint8_t has_horz,has_vert;

	//横書き/縦書きで、それぞれ実際にグリフが使われているか。
	//また、PDF で定義するメトリクスの数を取得 (emsize と同じ場合は除く)
	// :縦書きで VORG がある場合は、それも含む。
	// :使用されていない GID 0、または TrueType の複合グリフで参照されているだけのグリフは、含めない。

	puni = p->unimap;
	ascent = p->ascent;
	gnum = p->glyph_num;

	has_horz = has_vert = 0;
	hnum = vnum = 0;

	for(i = 0; i < gnum; i++)
	{
		v = *(puni++);

		if(!v) continue;

		//横書き

		if(v & FONT_UNIMAP_F_USE_HORZ)
		{
			has_horz = 1;

			if(v & FONT_UNIMAP_F_HORZ_WIDTH) hnum++;
		}

		//縦書き
		
		if(v & FONT_UNIMAP_F_USE_VERT)
		{
			has_vert = 1;

			if((v & FONT_UNIMAP_F_VERT_WIDTH)
				|| font_get_origin_y(p, i) != ascent)
				vnum++;
		}
	}

	dst->has_horz = has_horz;
	dst->has_vert = has_vert;
	dst->horz_num = hnum;
	dst->vert_num = vnum;
	dst->buf_horz = dst->buf_vert = NULL;
}

/** PDF 出力用、横書き/縦書きのメトリクスデータを取得
 *
 * return: 0 で成功 */

int font_get_pdf_metrics(Font *p,uint16_t *map_outsrc,FontSubsetInfo *dst)
{
	int i,outnum,ascent,origin,fconvw,emsize,w;
	uint16_t *pmap,*pd;
	uint32_t *puni,v;

	puni = p->unimap;
	outnum = p->subset_glyph_num;
	ascent = p->ascent;
	emsize = p->emsize;

	fconvw = (p->emsize != 1000);

	//横書き

	if(dst->horz_num)
	{
		pd = dst->buf_horz = (uint16_t *)mMalloc(4 * dst->horz_num);
		if(!pd) return 1;

		pmap = map_outsrc;

		for(i = 0; i < outnum; i++, pmap++)
		{
			if(puni[*pmap] & FONT_UNIMAP_F_HORZ_WIDTH)
			{
				w = font_get_advance_width(p, FALSE, *pmap);

				if(fconvw) w = w * 1000 / emsize;
				
				pd[0] = i;
				pd[1] = (uint16_t)w;
				pd += 2;
			}
		}
	}

	//縦書き
	
	if(dst->vert_num)
	{
		pd = dst->buf_vert = (uint16_t *)mMalloc(6 * dst->vert_num);
		if(!pd)
		{
			mFree(dst->buf_horz);
			return 1;
		}

		//幅は符号反転する

		pmap = map_outsrc;

		for(i = 0; i < outnum; i++, pmap++)
		{
			v = puni[*pmap];
			
			if(v & FONT_UNIMAP_F_USE_VERT)
			{
				origin = font_get_origin_y(p, *pmap);

				if((v & FONT_UNIMAP_F_VERT_WIDTH) || origin != ascent)
				{
					w = font_get_advance_width(p, TRUE, *pmap);

					if(fconvw)
					{
						w = w * 1000 / emsize;
						origin = origin * 1000 / emsize;
					}

					pd[0] = i;
					pd[1] = (uint16_t)-w;
					pd[2] = (uint16_t)origin;
					pd += 3;
				}
			}
		}
	}

	return 0;
}
