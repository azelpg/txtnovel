/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * FontIO
 *********************************/

#include <stdio.h>

#include <mlk.h>
#include <mlk_stdio.h>

#include "fontio.h"


/* SFNT テーブル読み込み */

static mlkerr _read_tables(FontIO *p)
{
	FontTblInfo *ptbl;
	uint16_t tnum;

	//テーブル数

	if(fontio_read_format(p, "h6s", &tnum))
		return MLKERR_DAMAGED;

	//確保

	mFree(p->table);

	p->table = (FontTblInfo *)mMalloc0(sizeof(FontTblInfo) * (tnum + 1));
	if(!p->table) return MLKERR_ALLOC;

	//テーブルレコード

	ptbl = p->table;

	for(; tnum; tnum--, ptbl++)
	{
		if(fontio_read_format(p, "iiii",
			&ptbl->tag, &ptbl->cksum, &ptbl->offset, &ptbl->size))
			return MLKERR_DAMAGED;
	}

	return 0;
}

/* ヘッダ読み込み */

static mlkerr _read_header(FontIO *p,int index)
{
	uint32_t ver,num,offset;
	uint16_t maj,min;
	int ret;

	if(fontio_read32(p, &ver))
		return MLKERR_DAMAGED;

	//'ttcf' の場合、指定インデックスに移動

	if(ver == 0x74746366)
	{
		if(fontio_read_format(p, "hhi", &maj, &min, &num)
			|| maj == 0)
			return MLKERR_DAMAGED;

		if(index < 0 || index >= num)
			index = 0;

		//オフセット位置に移動 + バージョン読み込み

		if(fontio_seek(p, index * 4)
			|| fontio_read32(p, &offset)
			|| fontio_setpos(p, offset)
			|| fontio_read32(p, &ver))
			return MLKERR_DAMAGED;
	}

	//---- オフセットテーブル

	//バージョン

	if(ver != 0x4F54544F && ver != 0x00010000)
		return MLKERR_FORMAT_HEADER;

	//テーブル読み込み

	ret = _read_tables(p);
	if(ret) return ret;

	//CFF2 は非対応

	if(fontio_search_table(p, MLK_MAKE32_4('C','F','F','2')))
		return MLKERR_UNSUPPORTED;

	//CFF か

	if(fontio_search_table(p, MLK_MAKE32_4('C','F','F',' ')))
		p->is_cff = 1;

	//maxp (グリフ数)

	ret = fontio_move_table_checksize(p, MLK_MAKE32_4('m','a','x','p'), NULL, 6);
	if(ret) return ret;

	if(fontio_seek(p, 4)
		|| fontio_read16(p, &maj))
		return MLKERR_DAMAGED;

	p->glyph_num = maj;

	return 0;
}

/* 作成して、ファイルを開く */

static mlkerr _create_open(FontIO **dst,const char *filename)
{
	FontIO *p;

	//確保

	p = (FontIO *)mMalloc0(sizeof(FontIO));
	if(!p) return MLKERR_ALLOC;

	//開く

	p->fpin = mFILEopen(filename, "rb");
	if(!p->fpin)
	{
		fontio_close(p);
		return MLKERR_OPEN;
	}

	*dst = p;

	return 0;
}


//==========================
// main
//==========================


/** 閉じる */

void fontio_close(FontIO *p)
{
	if(p)
	{
		mFILEclose(p->fpin);

		mFree(p->table);

		mFree(p);
	}
}

/** フォントファイルを開く */

mlkerr fontio_openfile(FontIO **dst,const char *filename,int index)
{
	FontIO *p;
	int ret;

	ret = _create_open(&p, filename);
	if(ret) return ret;

	//読み込み

	ret = _read_header(p, index);
	if(ret)
	{
		fontio_close(p);
		return ret;
	}

	*dst = p;

	return 0;
}


//==========================
// フォント名出力
//==========================


/* テーブルを読み込み、フォント名出力 */

static mlkerr _put_name(FontIO *p,int no)
{
	int ret;
	char *name;

	ret = _read_tables(p);
	if(ret) return ret;

	ret = fontio_table_name_postscript(p, &name);
	if(ret) return ret;

	if(name)
	{
		printf("[index=%d] %s\n", no, name);

		mFree(name);
	}

	return 0;
}

/* フォント名出力処理 */

static mlkerr _put_names_main(FontIO *p)
{
	uint16_t maj,min;
	uint32_t ver,fnum,offset,i;
	int ret;

	if(fontio_read32(p, &ver))
		return MLKERR_DAMAGED;

	if(ver == 0x74746366)
	{
		//ttcf
		
		if(fontio_read_format(p, "hhi", &maj, &min, &fnum)
			|| maj == 0)
			return MLKERR_DAMAGED;

		for(i = 0; i < fnum; i++)
		{
			if(fontio_setpos(p, 12 + i * 4)
				|| fontio_read32(p, &offset)
				|| fontio_setpos(p, offset)
				|| fontio_read32(p, &ver))
				return MLKERR_DAMAGED;

			ret = _put_name(p, i);
			if(ret) return ret;
		}
	}
	else
	{
		//単一フォント
		
		if(ver != 0x4F54544F && ver != 0x00010000)
			return MLKERR_FORMAT_HEADER;

		ret = _put_name(p, 0);
		if(ret) return ret;
	}

	return 0;
}

/** フォント名の一覧を出力 */

mlkerr fontio_put_names(const char *filename)
{
	FontIO *p;
	int ret;

	ret = _create_open(&p, filename);
	if(ret) return ret;

	ret = _put_names_main(p);

	fontio_close(p);

	return ret;
}


//==========================
// テーブル
//==========================


/** 指定テーブル検索 */

FontTblInfo *fontio_search_table(FontIO *p,uint32_t tag)
{
	FontTblInfo *pt;

	for(pt = p->table; pt->tag; pt++)
	{
		if(pt->tag == tag)
			return pt;
	}

	return NULL;
}

/** 指定テーブルの位置へ移動
 *
 * dst: 0 以外で、ポインタが入る
 * return: エラー */

mlkerr fontio_move_table(FontIO *p,uint32_t tag,FontTblInfo **dst)
{
	FontTblInfo *pt = p->table;

	for(pt = p->table; pt->tag; pt++)
	{
		if(pt->tag == tag)
		{
			if(fontio_setpos(p, pt->offset))
				return MLKERR_DAMAGED;

			if(dst) *dst = pt;

			return 0;
		}
	}

	return MLKERR_UNFOUND;
}

/** 指定テーブルに移動し、サイズが指定値以上あるかチェック */

mlkerr fontio_move_table_checksize(FontIO *p,uint32_t tag,FontTblInfo **dst,uint32_t minsize)
{
	FontTblInfo *ptbl;
	int ret;

	ret = fontio_move_table(p, tag, &ptbl);

	if(ret)
		return ret;
	else if(ptbl->size < minsize)
		return MLKERR_DAMAGED;
	else
	{
		if(dst) *dst = ptbl;
		
		return MLKERR_OK;
	}
}

/** 指定テーブルのデータを読み込み
 *
 * [!] 4byte 単位で読み込む
 *
 * minsize: 0 以外で、サイズが指定値以上かを判定 */

mlkerr fontio_read_table(FontIO *p,uint32_t tag,
	FontTblInfo **dst_tbl,FontTblBuf *dst_buf,uint32_t minsize)
{
	uint8_t *buf;
	FontTblInfo *ptbl;
	uint32_t size4;
	int ret;

	ret = fontio_move_table(p, tag, &ptbl);
	if(ret) return ret;

	if(minsize && ptbl->size < minsize)
		return MLKERR_DAMAGED;

	size4 = (ptbl->size + 3) & ~3;

	//読み込み

	ret = fontio_read_alloc(p, size4, &buf);
	if(ret) return ret;

	//

	if(dst_tbl) *dst_tbl = ptbl;

	dst_buf->buf = buf;
	dst_buf->size = ptbl->size;
	dst_buf->size4 = size4;

	return MLKERR_OK;
}


