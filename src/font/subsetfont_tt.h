/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/* TrueType 用 SubsetFont */

typedef struct
{
	SubsetFont b;

	int	in_loca_size,	//[in] loca テーブルのオフセットサイズ (2 or 4)
		is_loca_4to2;	//loca テーブルのオフセットが 4->2byte に変更されるか

	uint8_t *glyfbuf,	//グリフデータ用バッファ
		*locabuf;		//loca データ
}SubsetFontTT;

/* func */

typedef int (*SubsetFontTT_composite_func)(SubsetFont *p,uint8_t *top,uint16_t gid,void *param);

uint32_t subsetfont_tt_read_loca_offset(SubsetFontTT *p,uint8_t **pp);
mlkerr subsetfont_tt_alloc_glyf_buf(SubsetFontTT *p);
mlkerr subsetfont_tt_glyf_composite(SubsetFontTT *p,uint8_t *buf,int len,SubsetFontTT_composite_func func,void *param);
mlkerr subsetfont_tt_add_composite(SubsetFontTT *p);
