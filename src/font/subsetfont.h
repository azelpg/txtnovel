/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

typedef struct
{
	int out_glyph_num,	//出力グリフ数が入る
		in_glyph_num;	//入力グリフ数が入る

	uint32_t *unimap;	//元の GID に対応する Unicode のマップ
		//0 以外であり。最上位ビットが ON で使用とする。
		//入力フォントのグリフ数分あること。
		//TrueType の複合グリフは内部で追加されるので、含めなくて良い。(追加分は値が変更される)
	
	uint16_t *gidmap,	//src GID -> out GID のマップがセットされる
		//(入力フォントのグリフ数分確保していること)
		//値が 0xffff で使用されないグリフ
		*gidmap_outsrc;	//out GID -> src GID のマップ (確保してセットされる)
}SubsetFontDat;

mlkerr create_subsetfont(FILE *fpout,const char *fontfile,int index,SubsetFontDat *dat);

