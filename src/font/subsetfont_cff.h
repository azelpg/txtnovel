/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/* CharString/Subr */

typedef struct
{
	CFFIndex index;
	int outnum;			//出力数
	uint32_t filepos;	//作業データのファイル位置
	uint8_t *buf;		//INDEX のオフセット〜データ (常に読み込み)
	uint16_t *map;		//srcIndex -> outIndex のマップ。必要なければ NULL
}CFFCSDat;

/* Font DICT データ */

typedef struct
{
	uint8_t in_index,		//入力時のインデックス
		out_index;			//出力時のインデックス (前の FontDICT が削除された時に変化)
	uint16_t fontname_sid;	//FontName の SID
	int32_t outpos_private,	//[out] Private DICT サイズ・オフセットの書き込み位置
		write_tmp;			//[out] Font DICT 書き込み時の作業用
	mList list_private;		//Private 項目 (num == 0 でなし)
	CFFCSDat subr;			//Local Subr
}CFFFontDict;

/* CFF 用 SubsetFont */

typedef struct
{
	SubsetFont b;

	int is_cidfont,		//[in] CID フォントか (TopDICT に ROS があるか)
		fontdict_num;	//Font DICT 数

	uint32_t offset_charstrings, //[in/out] 各オフセット位置
		offset_charset,
		offset_private,
		offset_fdarray,
		offset_fdselect,
		size_private,
		name_filepos,		//フォント名 (Name INDEX 内) のファイル位置
		name_len;			//フォント名の長さ

	int32_t outoffset_charset,	//[out] TopDICT の各値の書き込み位置 (val=29 の次の位置)
		outoffset_charstrings,
		outoffset_fdarray,
		outoffset_fdselect;

	CFFFontDict *fontdict;	//Font DICT 配列
	uint8_t *fdselect_buf;	//FDSelect データ (outGID -> index のマップ)
	FILE *subrtmp_fp;		//サブルーチン関連の作業ファイル

	mList list_topdic;	//Top DICT 項目データ
	CFFIndex index_str;	//String INDEX
	CFFCSDat gsubr,		//Global Subr
		charstr;		//CharStrings
}SubsetFontCFF;


//------------------

/* sub */

void subsetfont_cff_free_charstr(CFFCSDat *p);
void subsetfont_cff_free_fontdict(CFFFontDict *pi);
uint32_t subsetfont_cff_get_outpos(SubsetFontCFF *p);

/* cff1 */

mlkerr subsetfont_cff_read_topDICTitem(SubsetFontCFF *p);
mlkerr subsetfont_cff_write_topDICT(SubsetFontCFF *p);

mlkerr subsetfont_cff_read_fontDICT(SubsetFontCFF *p);
mlkerr subsetfont_cff_read_fdselect(SubsetFontCFF *p);

mlkerr subsetfont_cff_get_string_list(SubsetFontCFF *p,mList *list);
mlkerr subsetfont_cff_write_string_INDEX(SubsetFontCFF *p,mList *list);

/* cff2 */

mlkerr subsetfont_cff_proc_charstrings(SubsetFontCFF *p);
mlkerr subsetfont_cff_write_charstring_INDEX(SubsetFontCFF *p,int srcno);
mlkerr subsetfont_cff_write_fdarray(SubsetFontCFF *p);

/* glyph */

typedef struct _CharString CharString;

mlkerr subsetfont_cff_proc_glyph(CharString *p,int in_gid);

