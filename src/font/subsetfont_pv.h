/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

typedef struct _FontIO FontIO;

/* SubsetFont */

typedef struct
{
	FontIO *io;
	SubsetFontDat *pdat;
	FILE *fpout;

	uint16_t *gidmap_outsrc;	//[work] outGID -> srcGID のマップ (x out_glyph_num)

	//SubsetFontDat のポインタのコピー
	uint32_t *unimap;
	uint16_t *gidmap_srcout;	//srcGID -> outGID のマップ (x in_glyph_num): 0xffff で出力しない

	int	out_glyph_num;	//出力グリフ数
}SubsetFont;

#define _SF(p)  ((SubsetFont *)(p))


/*--- func ---*/

mlkerr subsetfont_create_gidmap(SubsetFont *p);

mlkerr subsetfont_tt_output(SubsetFont *sf);
void subsetfont_tt_free(SubsetFont *sf);

mlkerr subsetfont_cff_output(SubsetFont *sf);
void subsetfont_cff_free(SubsetFont *sf);
