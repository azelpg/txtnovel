/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/************************************
 * TrueType サブセット作成
 ************************************/

#include <stdio.h>
#include <string.h>

#include <mlk.h>
#include <mlk_stdio.h>
#include <mlk_util.h>

#include "fontio.h"

#include "subsetfont.h"
#include "subsetfont_pv.h"
#include "subsetfont_tt.h"


//---------------

//出力するテーブルのタグ (昇順)
static const uint32_t g_put_tags[] = {
	MLK_MAKE32_4('c','v','t',' '),
	MLK_MAKE32_4('f','p','g','m'), MLK_MAKE32_4('g','l','y','f'),
	MLK_MAKE32_4('h','e','a','d'), MLK_MAKE32_4('h','h','e','a'),
	MLK_MAKE32_4('h','m','t','x'), MLK_MAKE32_4('l','o','c','a'),
	MLK_MAKE32_4('m','a','x','p'), MLK_MAKE32_4('p','r','e','p'), 0
};

//---------------



//=============================
// maxp
//=============================


/* maxp テーブル書き込み */

static mlkerr _write_table_maxp(SubsetFontTT *p)
{
	FontTblInfo *ptbl;
	FontTblBuf tb;
	int ret;

	ret = fontio_read_table(p->b.io, MLK_MAKE32_4('m','a','x','p'), &ptbl, &tb, 6);
	if(ret) return ret;

	//numGlyphs -> 使用するグリフ数

	mSetBufBE16(tb.buf + 4, p->b.out_glyph_num);

	//書き込み

	ret = fontio_write_table_buf(p->b.io, ptbl, &tb);

	mFree(tb.buf);

	return ret;
}


//=============================
// hhea/hmtx
//=============================


/* hmtx のデータを展開 & 出力用データに調整
 *
 * {int16 advanceWidth,lsb} x in_glyph_num に展開し、出力しないグリフデータを削除。
 * 終端で aw が連続する範囲の個数 - 1 を除外して、hmtx 数を取得する。
 *
 * hmtx: [in,out] buf,size が変更される
 * hmtx_num: 入力の hmtx 数
 * dst_hmtx_num: 出力の hmtx 数が入る */

static mlkerr _hmtx_expand(SubsetFontTT *p,FontTblBuf *hmtx,int hmtx_num,int *dst_hmtx_num)
{
	uint8_t *newbuf,*ps,*pd,aw0,aw1;
	int i,size,out_gnum,out_num;
	uint32_t val;
	uint16_t aw,awbk,*pmap;

	if(hmtx_num == 0) return MLKERR_INVALID_VALUE;

	//バッファ

	newbuf = (uint8_t *)mMalloc(p->b.io->glyph_num * 4);
	if(!newbuf) return MLKERR_ALLOC;

	out_gnum = p->b.out_glyph_num;

	//----- 展開

	//{aw,lsb} x in_num

	ps = hmtx->buf;
	size = hmtx_num * 4;

	memcpy(newbuf, ps, size);

	pd = newbuf + size;
	ps += size;

	//最後の advanceWidth を取得

	aw0 = ps[-4];
	aw1 = ps[-3];

	//残りを展開 ({lsb} -> {aw,lsb})

	for(i = p->b.io->glyph_num - hmtx_num; i > 0; i--, pd += 4, ps += 2)
	{
		pd[0] = aw0;
		pd[1] = aw1;
		pd[2] = ps[0];
		pd[3] = ps[1]; 
	}

	//バッファを入れ替え

	mFree(hmtx->buf);

	hmtx->buf = newbuf;

	//------ 出力データのセット

	//使用しないグリフのデータを詰める

	pd = newbuf;
	pmap = p->b.gidmap_outsrc;

	for(i = out_gnum; i; i--, pd += 4, pmap++)
	{
		val = *((uint32_t *)(newbuf + pmap[0] * 4));

		*((uint32_t *)pd) = val;
	}

	//終端から順に、advanceWidth が異なる位置を検索
	// : [i - 1] [i] で aw が異なった時に終了。i も個数に含む。
	// : i = 0 ですべて同じ

	i = out_gnum - 1;
	ps = newbuf + i * 4;
	aw = mGetBufBE16(ps);
	ps -= 4;

	for(; i; i--, ps -= 4)
	{
		awbk = (ps[0] << 8) | ps[1];
		if(aw != awbk) break;

		aw = awbk;
	}

	out_num = i + 1;

	//

	*dst_hmtx_num = out_num;

	hmtx->size = out_num * 4 + (out_gnum - out_num) * 2;

	//終端を lsb のみにする

	pd = newbuf + out_num * 4;
	ps = pd + 2;

	for(i = out_gnum - out_num; i > 0; i--, pd += 2, ps += 4)
	{
		pd[0] = ps[0];
		pd[1] = ps[1];
	}

	return MLKERR_OK;
}

/* hhea,hmtx テーブル書き込み */

static mlkerr _write_table_hhea_hmtx(SubsetFontTT *p)
{
	FontTblInfo *tbl_hhea,*tbl_hmtx;
	FontTblBuf hhea,hmtx;
	int ret,src_num,out_num;

	hhea.buf = hmtx.buf = 0;

	//hhea 読み込み

	ret = fontio_read_table(p->b.io, MLK_MAKE32_4('h','h','e','a'), &tbl_hhea, &hhea, 36);
	if(ret) return ret;

	src_num = mGetBufBE16(hhea.buf + 34);

	if(src_num > p->b.io->glyph_num)
		return MLKERR_INVALID_VALUE;

	//hmtx 読み込み

	ret = fontio_read_table(p->b.io, MLK_MAKE32_4('h','m','t','x'), &tbl_hmtx, &hmtx,
			src_num * 4 + (p->b.io->glyph_num - src_num) * 2);

	if(ret) goto ERR;

	//hmtx データ展開

	ret = _hmtx_expand(p, &hmtx, src_num, &out_num);
	if(ret) goto ERR;

	//-----------

	//hhea 書き込み

	mSetBufBE16(hhea.buf + 34, out_num); //numberOfHMetrics

	ret = fontio_write_table_buf(p->b.io, tbl_hhea, &hhea);
	if(ret) goto ERR;

	//hmtx 書き込み

	ret = fontio_write_table_buf(p->b.io, tbl_hmtx, &hmtx);
	if(ret) goto ERR;

	//

	ret = MLKERR_OK;
ERR:
	mFree(hhea.buf);
	mFree(hmtx.buf);

	return ret;
}


//=============================
// glyf/loca
//=============================


/* 複合グリフの GID 置き換え */

static int _glyf_composite(SubsetFont *p,uint8_t *top,uint16_t gid,void *param)
{
	gid = p->gidmap_srcout[gid];
	
	top += 2;
	top[0] = (uint8_t)(gid >> 8);
	top[1] = (uint8_t)gid;

	return 0;
}

/* glyf テーブル書き込み (loca,head より前に)
 *
 * loca のオフセットサイズを決定するため、先に実行する。 */

static mlkerr _write_table_glyf(SubsetFontTT *p)
{
	FontTblInfo *ptbl;
	FontIO *io = p->b.io;
	FILE *fp;
	uint8_t *pl,*gbuf;
	uint16_t *pmap;
	uint32_t off1,off2,len,tblsize,top,cksum;
	int i,ret,loca_shift;

	ret = fontio_move_table(io, MLK_MAKE32_4('g','l','y','f'), &ptbl);
	if(ret) return ret;

	//

	tblsize = 0;
	cksum = 0;
	loca_shift = p->in_loca_size / 2;

	fp = p->b.fpout;

	ptbl->tmp = ftell(fp);

	//使用するグリフだけ書き込み
	// :オフセット位置は基本的に 4byte 単位だが、一部のフォントで 4byte 単位でない場合あり。

	gbuf = p->glyfbuf;
	pmap = p->b.gidmap_outsrc;
	top = ptbl->offset;

	for(i = p->b.out_glyph_num; i; i--, pmap++)
	{
		pl = p->locabuf + (*pmap << loca_shift);
		off1 = subsetfont_tt_read_loca_offset(p, &pl);
		off2 = subsetfont_tt_read_loca_offset(p, &pl);

		len = off2 - off1; //グリフデータがない場合 0

		if(len)
		{
			//グリフデータ読み込み
			
			if(fontio_setpos(io, top + off1)
				|| fontio_read(io, gbuf, len))
				return MLKERR_DAMAGED;

			//4byte 単位でない場合

			if(len & 3)
			{
				memset(gbuf + len, 0, 4 - (len & 3));

				len = (len + 3) & ~3;
			}

			//複合グリフの GID 値を置き換え

			ret = subsetfont_tt_glyf_composite(p, gbuf, len, _glyf_composite, 0);
			if(ret && ret != -1) return ret;

			//書き込み

			fontio_calc_checksum(gbuf, len, &cksum);
			
			if(mFILEwriteOK(fp, gbuf, len))
				return MLKERR_IO;

			tblsize += len;
		}
	}

	//loca のオフセットに変更があるか

	p->is_loca_4to2 = (p->in_loca_size == 4 && tblsize <= 65535 * 2);

	//[!] tblsize は常に 4byte 単位なので、余白の追加などは必要ない

	ptbl->size = tblsize;
	ptbl->cksum = cksum;

	return 0;
}

/* loca テーブル書き込み
 *
 * [!] locabuf をそのまま使う */

static mlkerr _write_table_loca(SubsetFontTT *p)
{
	FontTblInfo *ptbl;
	FontTblBuf tb;
	uint8_t *ps,*pd;
	uint16_t *pmap;
	int out_locasize,i,out_gnum,loca_shift;
	uint32_t offset,off1,off2;

	out_gnum = p->b.out_glyph_num;
	loca_shift = p->in_loca_size / 2;

	//出力オフセットサイズ

	out_locasize = (p->is_loca_4to2)? 2: p->in_loca_size;

	//使用しないグリフのオフセットを除外して、上書き

	pd = p->locabuf;
	pmap = p->b.gidmap_outsrc;
	offset = 0;

	for(i = out_gnum; i; i--, pmap++)
	{
		ps = p->locabuf + (*pmap << loca_shift);
		off1 = subsetfont_tt_read_loca_offset(p, &ps);
		off2 = subsetfont_tt_read_loca_offset(p, &ps);

		//現在のオフセット書き込み

		if(out_locasize == 2)
		{
			mSetBufBE16(pd, offset >> 1);
			pd += 2;
		}
		else
		{
			mSetBufBE32(pd, offset);
			pd += 4;
		}

		//次のオフセット (4byte 単位)

		offset += (off2 - off1 + 3) & ~3;
	}

	//最後のオフセット

	if(out_locasize == 2)
		mSetBufBE16(pd, offset >> 1);
	else
		mSetBufBE32(pd, offset);

	//書き込み

	tb.buf = p->locabuf;
	tb.size = (out_gnum + 1) * out_locasize;

	ptbl = fontio_search_table(p->b.io, MLK_MAKE32_4('l','o','c','a'));

	return fontio_write_table_buf(p->b.io, ptbl, &tb);
}


//=============================
// head
//=============================


/* head テーブル書き込み
 *
 * [!] glyf の後 */

static mlkerr _write_table_head(SubsetFontTT *p)
{
	FontTblInfo *ptbl;
	FontTblBuf tb;
	int ret;

	ret = fontio_read_table(p->b.io, MLK_MAKE32_4('h','e','a','d'), &ptbl, &tb, 0);
	if(ret) return ret;

	//checkSumAdjustment = 0
	// :全体のチェックサムはしなくてよい

	mSetBufBE32(tb.buf + 8, 0);

	//indexToLocFormat
	// :元が Offset32 で、出力が Offset16 の場合は変更

	if(p->is_loca_4to2)
		mSetBufBE16(tb.buf + 50, 0);

	//書き込み

	ret = fontio_write_table_buf(p->b.io, ptbl, &tb);

	mFree(tb.buf);

	return ret;
}


//==============================
// sub
//==============================


/* 出力テーブルの選択
 *
 * FontTblInfo:tmp = 出力するなら 1、しないなら 0
 *
 * return: 出力テーブル数 */

static int _select_table(SubsetFontTT *p)
{
	FontTblInfo *pt;
	const uint32_t *ptag;
	int num = 0;

	for(pt = p->b.io->table; pt->tag; pt++)
	{
		pt->tmp = 0;

		//出力対象に含まれるか
	
		for(ptag = g_put_tags; *ptag && *ptag <= pt->tag; ptag++)
		{
			if(pt->tag == *ptag)
			{
				pt->tmp = 1;
				num++;
				break;
			}
		}
	}

	return num;
}

/* 書き込み前の処理 */

static mlkerr _before_write(SubsetFontTT *p)
{
	int ret;
	FontTblBuf tb;

	//loca データ読み込み

	ret = fontio_read_table(p->b.io, MLK_MAKE32_4('l','o','c','a'), NULL, &tb,
			(p->b.io->glyph_num + 1) * p->in_loca_size);

	if(ret) return ret;

	p->locabuf = tb.buf;

	//グリフデータ用バッファ確保 (loca から)

	ret = subsetfont_tt_alloc_glyf_buf(p);
	if(ret) return ret;

	//複合グリフの GID を追加

	ret = subsetfont_tt_add_composite(p);
	if(ret) return ret;

	//GID マップを作成

	ret = subsetfont_create_gidmap(_SF(p));
	if(ret) return ret;

	return 0;
}

/* ヘッダ + オフセットテーブル書き込み */

static mlkerr _write_header(SubsetFontTT *p,int tnum)
{
	FontTblInfo *pt;
	uint8_t d[16];
	int n;

	//ヘッダ

	n = mCalcMaxLog2(tnum);

	mSetBuf_format(d, ">ihhhh",
		0x00010000, tnum, 16 << n, n, tnum * 16 - (16 << n));

	if(fwrite(d, 1, 12, p->b.fpout) != 12)
		return MLKERR_IO;

	//テーブル
	// [!] タグの昇順に書き込む必要がある

	for(pt = p->b.io->table; pt->tag; pt++)
	{
		if(pt->tmp)
		{
			mSetBuf_format(d, ">iiii", pt->tag, pt->cksum, 0, 0);

			if(fwrite(d, 1, 16, p->b.fpout) != 16)
				return MLKERR_IO;
		}
	}

	return 0;
}

/* 全テーブルの書き込み後、テーブルレコードを再書き込み */

static int _write_table_record(SubsetFontTT *p)
{
	FILE *fp = p->b.fpout;
	FontTblInfo *pt;
	uint8_t d[16];

	if(fseek(fp, 12, SEEK_SET))
		return MLKERR_IO;

	for(pt = p->b.io->table; pt->tag; pt++)
	{
		if(pt->tmp)
		{
			mSetBuf_format(d, ">iiii", pt->tag, pt->cksum, pt->tmp, pt->size);

			if(fwrite(d, 1, 16, fp) != 16)
				return MLKERR_IO;
		}
	}

	fseek(fp, 0, SEEK_END);

	return 0;
}


//===========================
// main
//===========================


/** TrueType 解放 */

void subsetfont_tt_free(SubsetFont *sf)
{
	SubsetFontTT *p = (SubsetFontTT *)sf;

	mFree(p->glyfbuf);
	mFree(p->locabuf);
}

/** TrueType サブセット出力 */

mlkerr subsetfont_tt_output(SubsetFont *sf)
{
	SubsetFontTT *p = (SubsetFontTT *)sf;
	int ret,tnum;

	//loca オフセットサイズ取得

	ret = fontio_table_head_locasize(sf->io, &p->in_loca_size);
	if(ret) return ret;

	//出力テーブルを選択
	
	tnum = _select_table(p);

	//処理

	ret = _before_write(p);
	if(ret) return ret;

	//---- 書き込み

	//ヘッダ書き込み

	ret = _write_header(p, tnum);
	if(ret) return ret;

	//maxp

	ret = _write_table_maxp(p);
	if(ret) return ret;

	//hhea, hmtx

	ret = _write_table_hhea_hmtx(p);
	if(ret) return ret;

	//glyf

	ret = _write_table_glyf(p);
	if(ret) return ret;

	//loca
	// [!] locabuf が上書きされる

	ret = _write_table_loca(p);
	if(ret) return ret;

	//head (glyf の後)

	ret = _write_table_head(p);
	if(ret) return ret;

	//コピー (cvt, fpgm, prep)

	ret = fontio_write_table_copy(p->b.io, MLK_MAKE32_4('c','v','t',' '));
	if(ret) return ret;

	ret = fontio_write_table_copy(p->b.io, MLK_MAKE32_4('f','p','g','m'));
	if(ret) return ret;

	ret = fontio_write_table_copy(p->b.io, MLK_MAKE32_4('p','r','e','p'));
	if(ret) return ret;

	//テーブルレコードの再書き込み

	return _write_table_record(p);
}

