/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

#define CS_ARG_STACK_NUM  48  //引数スタック数
#define CS_SUBR_NEST_NUM  10  //サブルーチンネスト数最大

#define CS_WRITENO_GLYPH  0
#define CS_WRITENO_GSUBR  1
#define CS_WRITENO_LSUBR  256


/* 実行情報 */

typedef struct
{
	uint16_t write_no,
		write_index,	//サブルーチンの INDEX 番号
		write_flag;		//作業ファイルにデータを書き込むか
	int bufsize,
		remain;
	uint8_t *buf;
}CharStringRun;

/* 直前の引数の情報 */

typedef struct
{
	int pos,	//データの位置
		bytes,	//値のバイト数
		write_no,		//-1 で書き込まない
		write_index;
}CharStringLastVal;

/* CharString */

struct _CharString
{
	int gid,		//出力 GID (FDSelect から値を取得するため)
		arg_num,	//引数スタックの数
		stem_num,	//ステム数
		subr_num;	//サブルーチン ネスト数
	int32_t arg[CS_ARG_STACK_NUM];	//引数スタック

	CharStringRun cur,
		subrback[CS_SUBR_NEST_NUM];	//サブルーチンからの戻り

	CharStringLastVal lastval;	//直前の引数の情報

	SubsetFontCFF *sf;
};

