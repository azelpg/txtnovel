/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * Font: データ読込
 *********************************/

#include <mlk.h>

#include "font.h"


/* データの位置を取得
 *
 * return: 1 でデータなし */

static int _get_data_ptr(Font *p,int no,uint8_t **dst)
{
	uint32_t pos;

	pos = p->offset[no];

	if(!pos) return 1;

	*dst = p->data + pos;

	return 0;
}

/* 24bit 値を取得 */

static uint32_t _get_val24(uint8_t *ps)
{
	return (ps[0] << 16) | (ps[1] << 8) | ps[2];
}


//==========================
// Unicode
//==========================


/* U+10000 以上のデータから c の GID 取得 */

static uint16_t _search_unimap(uint32_t c,uint8_t *buf,uint32_t num)
{
	uint8_t *ps;
	int32_t low,high,mid;
	uint32_t min,max;

	low = 0;
	high = num - 1;

	while(low <= high)
	{
		mid = (low + high) >> 1;
		ps = buf + mid * 8;

		min = _get_val24(ps);
		max = _get_val24(ps + 3);
		
		if(min <= c && c <= max)
			return *((uint16_t *)(ps + 6)) + (c - min);
		else if(max < c)
			low = mid + 1;
		else
			high = mid - 1;
	}

	return 0;
}

/* UVS の GID 取得 */

static uint16_t _search_uvs(uint32_t c,uint8_t *buf,uint32_t num)
{
	uint8_t *ps;
	int32_t low,high,mid;
	uint32_t code;

	if(!num) return 0;

	low = 0;
	high = num - 1;

	while(low <= high)
	{
		mid = (low + high) >> 1;
		ps = buf + mid * 5;

		code = _get_val24(ps);
		
		if(c == code)
			return *((uint16_t *)(ps + 3));
		else if(code < c)
			low = mid + 1;
		else
			high = mid - 1;
	}

	return 0;
}

/** Unicode -> GID
 *
 * ない場合は 0 */

uint16_t font_get_unicode_gid(Font *p,uint32_t c)
{
	uint8_t *ps;
	uint32_t num,start;

	if(_get_data_ptr(p, FONT_DATA_UNIMAP, &ps))
		return 0;

	num = *((uint32_t *)ps);
	start = *((uint32_t *)(ps + 4));
	ps += 8;

	if(c < start)
		return 0;
	else if(c < start + num)
	{
		//配列マップ

		return *((uint16_t *)ps + (c - start));
	}
	else
	{
		//start + num 以降

		ps += num * 2;

		num = *((uint32_t *)ps);
		if(!num) return 0;

		ps += 4;

		start = _get_val24(ps);
		if(c < start) return 0;

		return _search_unimap(c, ps, num);
	}
}

/** UVS -> GID
 *
 * UVS 内にない場合は、c を Unicode として検索
 *
 * sc: セレクタ */

uint16_t font_get_uvs_gid(Font *p,uint32_t sc,uint32_t c)
{
	uint8_t *ps;
	uint32_t selector,num;
	uint16_t gid;

	if(_get_data_ptr(p, FONT_DATA_UVS, &ps))
		return font_get_unicode_gid(p, c);

	while(1)
	{
		selector = _get_val24(ps);
		if(!selector) break;

		num = *((uint32_t *)(ps + 3));
		ps += 7;

		if(sc < selector)
			break;
		else if(selector != sc)
			ps += num * 5;
		else
		{
			gid = _search_uvs(c, ps, num);
			if(!gid) break;

			return gid;
		}
	}

	return font_get_unicode_gid(p, c);
}


//==========================
// ほか
//==========================


/** GID から送り幅取得
 *
 * 縦書きグリフはあっても vmtx はない場合がある。
 * その場合、すべて emsize。
 *
 * vert: 0 以外で、垂直レイアウト */

int16_t font_get_advance_width(Font *p,int vert,uint16_t gid)
{
	uint16_t *buf,*ps;
	uint16_t start,end,pos;
	int low,high,mid,num;

	if(_get_data_ptr(p, FONT_DATA_HWIDTH + (vert != 0), (uint8_t **)&buf))
		return p->emsize;

	num = *buf;
	buf++;

	//範囲外は emsize

	ps = buf;
	low = 0;
	high = num - 1;

	while(low <= high)
	{
		mid = (low + high) >> 1;
		ps = buf + mid * 3;

		start = ps[0];
		end = ps[1];
		pos = ps[2];
		
		if(start <= gid && gid <= end)
			return *((int16_t *)(buf + num * 3 + pos + (gid - start)));
		else if(end < gid)
			low = mid + 1;
		else
			high = mid - 1;
	}

	return p->emsize;
}

/** GID から Y 原点取得
 *
 * VORG がない場合、ascent */

int16_t font_get_origin_y(Font *p,uint16_t gid)
{
	uint16_t *buf,*ps;
	uint16_t n;
	int16_t defy;
	int low,high,mid,num;

	if(_get_data_ptr(p, FONT_DATA_VORG, (uint8_t **)&buf))
		return p->ascent;

	defy = (int16_t)*buf;
	num = buf[1];
	buf += 2;

	//先頭と終端

	if(gid < *buf || gid > buf[(num - 1) * 2])
		return defy;

	//

	ps = buf;
	low = 0;
	high = num - 1;

	while(low <= high)
	{
		mid = (low + high) >> 1;
		ps = buf + mid * 2;

		n = *ps;

		if(gid == n)
			return (int16_t)ps[1];
		else if(n < gid)
			low = mid + 1;
		else
			high = mid - 1;
	}

	return defy;
}

/** GID から GSUB 置換取得
 *
 * ない場合、gid */

uint16_t font_get_gsub(Font *p,int datano,uint16_t gid)
{
	uint16_t *buf,*ps;
	uint16_t n;
	int low,high,mid,num;

	if(_get_data_ptr(p, datano, (uint8_t **)&buf))
		return gid;

	num = *buf;
	buf++;

	//先頭と終端

	if(gid < *buf || gid > buf[(num - 1) * 2])
		return gid;

	//

	ps = buf;
	low = 0;
	high = num - 1;

	while(low <= high)
	{
		mid = (low + high) >> 1;
		ps = buf + mid * 2;

		n = *ps;

		if(gid == n)
			return ps[1];
		else if(n < gid)
			low = mid + 1;
		else
			high = mid - 1;
	}

	return gid;
}

/** GID がリストにあるかどうか
 *
 * return: リストにあるか */

int font_get_gid_list(Font *p,int datano,uint16_t gid)
{
	uint16_t *buf,*ps;
	uint16_t n;
	int low,high,mid,num;

	if(_get_data_ptr(p, datano, (uint8_t **)&buf))
		return FALSE;

	num = *buf;
	buf++;

	//先頭と終端

	if(gid < *buf || gid > buf[num - 1])
		return gid;

	//

	ps = buf;
	low = 0;
	high = num - 1;

	while(low <= high)
	{
		mid = (low + high) >> 1;
		ps = buf + mid;

		n = *ps;

		if(gid == n)
			return TRUE;
		else if(n < gid)
			low = mid + 1;
		else
			high = mid - 1;
	}

	return FALSE;
}

/** CID -> GID
 *
 * ない場合は、cid。CID = GID の場合もデータはない。 */

uint16_t font_get_cid_to_gid(Font *p,uint16_t cid)
{
	uint16_t *buf,*ps;
	uint16_t start,end;
	int low,high,mid,num;

	if(_get_data_ptr(p, FONT_DATA_CIDMAP, (uint8_t **)&buf))
	{
		return (cid >= p->glyph_num)? 0: cid;
	}

	num = *buf;
	buf++;

	//

	ps = buf;
	low = 0;
	high = num - 1;

	while(low <= high)
	{
		mid = (low + high) >> 1;
		ps = buf + mid * 3;

		start = ps[0];
		end = ps[1];

		if(start <= cid && cid <= end)
			return ps[2] + (cid - start);
		else if(end < cid)
			low = mid + 1;
		else
			high = mid - 1;
	}

	//範囲外は GID 0
	return 0;
}

