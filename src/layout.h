/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

#include "bodydata.h"
#include "layout_dat.h"

#define DEBUG_PUT_LAYOUT  0

typedef struct _CaptionLayout CaptionLayout;
typedef struct _IndexItem IndexItem;


/* 現在のページのデータ */

typedef struct
{
	uint8_t	number_type,	//ノンブル表示
		pdfsep,			//PDF 分割 (1=現在ページのみ、2=次のページも)
		has_index,		//目次のタイトル行を含むか
		endtype,		//次のページの最初の終端タイプ (折り返し、見出しの送り時)
		body_align;		//本文の揃え
	int linenum,		//取得されたデータ分の行数
		linenum_page,	//ページで実際に処理する行数 (最大で、レイアウトの行数)
		lineno;			//現在の行位置
	StrData *pstr_top;	//現在の処理行の先頭 (NULL で、ページ先頭か、1文字もない)
	RubyData *pr_top;	//現在の処理行のルビ先頭 (NULL でなし)
}LayoutPageDat;

/* 現在の行のデータ */

typedef struct
{
	uint8_t endtype,		//終端のタイプ
		indent_head_first,	//現在行の字下げ/字上げ
		indent_head_wrap,
		indent_foot,
		line_align;
	layoutpos indent_foot_width,	//字上げ幅
		line_length_max;			//字上げ幅を除いた行長(字下げ幅は行頭の文字に追加される)

	StrData *pstr_top,		//現在の行頭位置
		*pstr_bottom,		//現在の行末位置
		*pstr_next;			//次の行頭位置 (NULL で終端)
	CaptionLayout *caption;	//見出し (NULL でなし)
	IndexItem *index;		//目次の各タイトル (NULL でなし)
}LayoutLineDat;

/* 画像挿入アイテム */

typedef struct
{
	mListItem i;

	uint8_t	page,	//ページ位置
		pagenum,	//ノンブルの表示
		flags;		//フラグ
	ImageInfo info;
}LayoutImageItem;

/* Layout */

typedef struct
{
	uint8_t is_horz,		//現在ページが横組か
		is_column,			//2段組か
		cur_column,			//現在の段位置 (0,1)
		indent_head_first,	//字下げ (先頭)
		indent_head_wrap,	//字下げ (折返し)
		indent_foot,		//字上げ
		align,				//文字送り方向の配置
		body_align,			//行送り方向の配置
		bousen;				//現在の傍線タイプ (0 でなし)

	int32_t pageno,			//ページ番号
		write_pagenum,		//出力したページ数
		last_write_pageno;	//最後に出力したページ位置 (0 でなし)

	layoutpos line_length,	//現在の行長(レイアウト全体)
		base_layout_width,	//ベースレイアウトの、基本版面の幅と高さ
		base_layout_height,
		fontsize,			//現在の本文のフォントサイズ
		base_fontsize,		//レイアウトの基本のフォントサイズ
		base_fontheight,	//基本フォントの高さ
		ruby_fontsize;		//ルビのフォントサイズ

	LayoutItem *layout;		//現在のレイアウト
	FontItem *font;			//現在の本文フォント
	Font *rubyfont;			//ルビフォント
	KentenItem *kenten;		//現在の圏点

	uint8_t *buf;	//現在の本文データ位置
	FILE *fpout;	//出力

	BodyChar bchar;
	LayoutPageDat pagedat;	//ページのデータ
	LayoutLineDat linedat;	//現在の行のデータ

	mList list_str,
		list_strch,
		list_ruby,
		list_rubych,
		list_str2,
		list_title,	//柱の文字列
		list_image;	//挿入待ちの画像
}LayoutData;

enum
{
	WRITEPAGE_F_EMPTY_BODY = 1<<0,	//本文が空
	WRITEPAGE_F_IMAGE = 1<<1		//画像あり
};

enum
{
	LAYOUT_CMDRET_NORMAL,
	LAYOUT_CMDRET_END_PAGE,		//ページとして終了し、出力せずに次のデータへ
	LAYOUT_CMDRET_OUTPUT_NEXT,	//前の文字をページで出力後、次のデータへ行く
	LAYOUT_CMDRET_OUTPUT_CUR,	//前の文字をページで出力後、同じコマンドから再度処理
};

enum
{
	LAYOUT_SETRUBY_F_DISABLE_AFTER = 1<<0,	//後ろの通常文字/熟語ルビに掛けない (行末のルビ単体に指定)
	LAYOUT_SETRUBY_F_DISABLE_BEFORE_NORMAL = 1<<1	//前の通常の文字に掛けない (熟語全体で指定可)
};

//-----------------

void layout_proc_all(LayoutData *p);
int layout_proc_command(LayoutData *p,int no);

/* sub */

mlkbool layout_is_page_odd(LayoutData *p);
mlkbool layout_has_curline_char(LayoutData *p);
mlkbool layout_is_empty_page(LayoutData *p);
mlkbool layout_has_page_char(LayoutData *p);

void layout_next_page(LayoutData *p);

void layout_change_layout(LayoutData *p);
void layout_set_linedat_cur(LayoutData *p);
void layout_set_pagedat_cur(LayoutData *p);
layoutpos layout_get_lines_width(LayoutData *p,int num);
uint16_t layout_get_char_to_gid(Font *font,BodyChar *ch,uint32_t *pcode);

/* str */

StrChar *layout_str_addchar(LayoutData *p,StrData *pistr,BodyChar *ch);
StrData *layout_str_add(LayoutData *p);
void layout_str_delete(LayoutData *p,mList *list,StrData *pstr);
StrChar *layout_str_getchar_lasttop(LayoutData *p,mListItem *last);
StrData *layout_str_getdat_lasttop(LayoutData *p,mListItem *last);
StrData *layout_str_add_chars_pack(LayoutData *p);
void layout_str_reset_width(LayoutData *p,StrData *pstr);

StrData *layout_str_addset_one(LayoutData *p,BodyChar *ch);
void layout_str_addset_area(LayoutData *p,int type);
StrData *layout_str_addset_subchar(LayoutData *p,StrData *ins,uint32_t code,FontItem *font,int32_t fontsize,int fkenten);

void layout_str_set_normal(LayoutData *p,StrData *pi);
void layout_str_set_sub_yakumotu(LayoutData *p,StrData *pi,BodyChar *ch);
void layout_str_set_vert_normal(LayoutData *p,StrData *pi);
void layout_str_set_horz_normal(LayoutData *p,StrData *pi);
void layout_str_set_vert_tateyoko(LayoutData *p,StrData *pi,int fprop);

/* str2 */

void layout_str_setline_rule_space(LayoutData *p);
StrData *layout_proc_line_top(LayoutData *p,StrData *pstr);
void layout_proc_line_bottom(LayoutData *p);
layoutpos layout_get_line_length(LayoutData *p);
void layout_set_line_length(LayoutData *p);
int layout_setline_caption_index(LayoutData *p);
void layout_setline_drawpos(LayoutData *p);

/* str3 */

void layout_set_nextpage_flag(LayoutData *p);
void layout_proc_page(LayoutData *p);
void layout_delete_curpage(LayoutData *p);

int layout_get_title_number_show(LayoutData *p,int fimage);
NormalChar *layout_add_normalchar(mList *list,FontSpec *font,BodyChar *ch,uint32_t code,int half);
int layout_set_number_char(LayoutData *p,mList *list,int hide);
void layout_set_title_drawpos(LayoutData *p,int num_w);

/* ruby */

RubyChar *layout_ruby_addchar(LayoutData *p,BodyChar *ch);
RubyData *layout_ruby_add(LayoutData *p);
RubyData *layout_ruby_getdat_lasttop(LayoutData *p,mListItem *last);
void layout_ruby_delete(LayoutData *p,RubyData *pr);

void layout_ruby_addset(LayoutData *p,int ruby_type);
void layout_ruby_set_line(LayoutData *p);
void layout_ruby_adjust_top(LayoutData *p,RubyData *pr);
int layout_ruby_reset_before(LayoutData *p,RubyData *pr);
void layout_ruby_reset_normal(LayoutData *p,RubyData *pr,int flags);
void layout_ruby_adjust_bottom(RubyData *pr);

int layout_ruby_sub_over(int charw,int maxw,int *remain);
int layout_ruby_after_over_normal(LayoutData *p,RubyData *pr,int *remain);
int layout_ruby_before_over_normal(LayoutData *p,RubyData *pr,int *remain);
void layout_ruby_add_space_parent(StrData *pstr,int remain);

/* ruby2 */

void layout_setruby_jyukugo_top(LayoutData *p,RubyData *pr,int flags);
void layout_setruby_jyukugo_center(LayoutData *p,RubyData *pr,int flags);
int layout_ruby_jyukugo_center_reset(LayoutData *p,RubyData *top,RubyData *end);

/* class */

int layout_get_char_class(uint32_t c,int rule_book);
uint8_t layout_get_class_flags(int cno);
uint32_t layout_ruby_kana_to_big(uint32_t c);

/* write */

void layout_write_page(LayoutData *p,uint32_t wflags);
void layout_write_change_layout(LayoutData *p);
void layout_write_image_page(LayoutData *p);

void layout_debug_put_layout(LayoutData *p);

