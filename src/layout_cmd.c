/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * レイアウト: コマンド処理
 *********************************/

#include <stdio.h>

#include <mlk.h>
#include <mlk_list.h>

#include "app.h"
#include "setting.h"
#include "layout.h"


/* 濁点合成: 文字追加 */

static void _func_dakuten(BodyChar *ch,void *param)
{
	StrData *pstr;

	pstr = layout_str_addset_one((LayoutData *)param, ch);

	pstr->chtop->flags |= STRCHAR_FLAG_DAKUTEN;
}

/* 縦中横 */

static void _proc_tateyoko(LayoutData *p,int fprop)
{
	StrData *pstr;

	//縦/横組、ともに一体として扱う

	pstr = layout_str_add_chars_pack(p);

	if(p->is_horz)
	{
		//横組: 欧文, 横書きのプロポーショナル幅

		pstr->classno = STRCLASS_EURO;
		
		layout_str_set_horz_normal(p, pstr);
	}
	else
		//縦組
		layout_str_set_vert_tateyoko(p, pstr, fprop);
}

/* フォント */

static void _proc_font(LayoutData *p)
{
	FontSpec dat;

	body_read_size(&p->buf, &dat, sizeof(FontSpec));

	if(!dat.item && dat.unit == UNIT_TYPE_DEFAULT)
	{
		//すべて省略で、レイアウトの基本フォントに戻す

		dat = p->layout->basefont;
	}
	else
	{
		//フォント省略時、現在のフォント
		
		if(!dat.item)
			dat.item = p->font;

		//フォントサイズ

		if(dat.unit == UNIT_TYPE_DEFAULT)
			//サイズ省略時、現在のサイズ
			dat.size = p->fontsize;
		else
			//単位 x を適用
			setting_set_fontspec(&dat, &p->layout->basefont, 1);
	}

	p->font = dat.item;
	p->fontsize = dat.size;
}

/* ページ番号 */

static void _proc_pagecnt(LayoutData *p)
{
	uint32_t no;

	no = *((uint32_t *)p->buf);
	p->buf += 4;

	if(no < p->pageno)
		app_enderr("[!] @pagecnt: 現在のページ位置より小さい値が指定されています");

	//同じ位置なら何もしない

	if(no == p->pageno) return;

	//最初のページを除き、見開き + 現在のページの次が左右で合わない場合、空ページを追加

	if(p->write_pagenum)
	{
		if(g_set->page_type != PAGETYPE_ONE
			&& ((p->pageno + 1) & 1) == (no & 1))
		{
			layout_write_page(p, WRITEPAGE_F_EMPTY_BODY);
		}
	}

	//

	p->pageno = no;
}

/* 柱の文字列セット */

static void _proc_title(LayoutData *p)
{
	BodyChar ch;

	mListDeleteAll(&p->list_title);

	ch.next = 1;

	while(1)
	{
		if(body_getchar_finish(&p->buf, &ch)) break;

		layout_add_normalchar(&p->list_title, &g_set->title.font, &ch, 0, FALSE);
	}
}

/* @index-page */

static void _proc_index_page(LayoutData *p)
{
	IndexItem *pi;

	pi = body_read_ptr(&p->buf);

	pi->save_pageno = p->pageno;
}

/* 画像
 *
 * ※文中の位置にかかわらず、行頭にコマンドがあるものとして扱う。 */

static int _proc_image(LayoutData *p)
{
	LayoutImageItem *pi;
	uint8_t *ps = p->buf;
	ImageInfo *info;
	uint8_t page,pagenum,flags;

	//読み込み

	info = (ImageInfo *)ps;
	ps += sizeof(ImageInfo);

	page = *(ps++);
	pagenum = *(ps++);
	flags = *(ps++);

	p->buf = ps;

	//挿入待ちアイテム

	pi = (LayoutImageItem *)mListAppendNew(&p->list_image, sizeof(LayoutImageItem));
	if(!pi) app_enderr_alloc();

	pi->info = *info;
	pi->page = page;
	pi->pagenum = pagenum;
	pi->flags = flags;

	//cur
	// :前に文字がない場合、現在ページに出力して、ページ終了とする。
	// :前に文字がある場合、先に出力 (コマンドの次から続ける)

	if(page == BODY_IMG_PAGE_CUR)
	{
		if(layout_is_empty_page(p))
		{
			layout_write_image_page(p);
			
			return LAYOUT_CMDRET_END_PAGE;
		}
		else
			return LAYOUT_CMDRET_OUTPUT_NEXT;
	}

	return 0;
}

/* @align */

static void _proc_align(LayoutData *p)
{
	uint8_t dat;

	dat = *(p->buf++);

	if(dat & 0x80)
		//現在行のみ
		p->linedat.line_align = dat & 0x7f;
	else
		p->align = p->linedat.line_align = dat;
}

/* @body-align */

static void _proc_bodyalign(LayoutData *p)
{
	uint8_t dat;

	dat = *(p->buf++);

	if(dat & 0x80)
		//現在ページのみ
		p->pagedat.body_align = dat & 0x7f;
	else
		p->body_align = p->pagedat.body_align = dat;
}

/* @ihead */

static void _proc_ihead(LayoutData *p)
{
	uint8_t dat[2];

	dat[0] = *(p->buf++);
	dat[1] = *(p->buf++);

	if(dat[0] & 0x80)
	{
		//現在行のみ
		p->linedat.indent_head_first = dat[0] & 0x7f;
		p->linedat.indent_head_wrap = dat[1];
	}
	else
	{
		p->indent_head_first = p->linedat.indent_head_first = dat[0];
		p->indent_head_wrap = p->linedat.indent_head_wrap = dat[1];
	}
}

/* @ifoot */

static void _proc_ifoot(LayoutData *p)
{
	uint8_t dat;

	dat = *(p->buf++);

	if(dat & 0x80)
		//現在行のみ
		p->linedat.indent_foot = dat & 0x7f;
	else
		p->indent_foot = p->linedat.indent_foot = dat;
}


/** コマンドを処理
 *
 * return: 0 以外で、段落を終了 */

int layout_proc_command(LayoutData *p,int no)
{
	int n;

	switch(no)
	{
		//熟語ルビ
		case BODYCMD_JRUBY:
			layout_ruby_addset(p, RUBY_TYPE_SET_JYUKUGO);
			break;
		//中付きの熟語ルビ
		case BODYCMD_CRUBY:
			layout_ruby_addset(p, RUBY_TYPE_JYUKUGO_CENTER);
			break;
		//モノルビ
		case BODYCMD_MRUBY:
			layout_ruby_addset(p, RUBY_TYPE_SET_MONO);
			break;
		//グループルビ
		case BODYCMD_GRUBY:
			layout_ruby_addset(p, RUBY_TYPE_GROUP);
			break;
		//グループルビ2
		case BODYCMD_GRUBY2:
			layout_ruby_addset(p, RUBY_TYPE_GROUP2);
			break;
		//@lr
		case BODYCMD_LRUBY:
			layout_ruby_addset(p, RUBY_TYPE_LONG);
			break;
		//濁点合成
		case BODYCMD_DAKUTEN:
			body_readchars(&p->buf, _func_dakuten, p);
			break;
		//縦中横
		case BODYCMD_TY:
		case BODYCMD_TY_PROP:
			_proc_tateyoko(p, (no == BODYCMD_TY_PROP));
			break;
		//圏点
		case BODYCMD_KENTEN:
			n = *(p->buf++);
			if(!n)
				p->kenten = 0;
			else
				p->kenten = (KentenItem *)mListGetItemAtIndex(&g_set->list_kenten, n - 1);
			break;
		//傍線
		case BODYCMD_BOUSEN:
			p->bousen = *(p->buf++);
			break;
		//フォント
		case BODYCMD_FONT:
			_proc_font(p);
			break;
		//見出し
		case BODYCMD_CAPTION1:
		case BODYCMD_CAPTION2:
		case BODYCMD_CAPTION3:
			if(layout_has_curline_char(p))
				app_enderr("[!] 見出しは、行に文字がない状態で指定してください");
				
			p->linedat.caption = p->layout->caption + (no - BODYCMD_CAPTION1);
			break;
		//ノンブル表示
		case BODYCMD_PAGE_NUMBER:
			p->pagedat.number_type = *(p->buf++);
			break;
		//文字送り方向の揃え
		case BODYCMD_ALIGN:
			_proc_align(p);
			break;
		//行送り方向の揃え
		case BODYCMD_BODYALIGN:
			_proc_bodyalign(p);
			break;
		//字下げ
		case BODYCMD_INDENT_HEAD:
			_proc_ihead(p);
			break;
		//字上げ
		case BODYCMD_INDENT_FOOT:
			_proc_ifoot(p);
			break;
		//柱の文字列
		case BODYCMD_TITLE:
			_proc_title(p);
			break;
		//画像
		case BODYCMD_IMAGE:
			return _proc_image(p);
		//レイアウト
		case BODYCMD_LAYOUT:
			//現在ページが空なら、そのまま変更。
			//内容があれば、前のデータを先に出力して、再度コマンドを実行。
			
			if(!layout_is_empty_page(p))
				return LAYOUT_CMDRET_OUTPUT_CUR;
			else
			{
				p->layout = (LayoutItem *)body_read_ptr(&p->buf);

				layout_change_layout(p);

				//ページ・行データに適用
				layout_set_linedat_cur(p);
				layout_set_pagedat_cur(p);
				
				layout_write_change_layout(p);
			}
			break;
		//ページ番号
		case BODYCMD_PAGE_COUNT:
			_proc_pagecnt(p);
			break;
		//PDF分割
		case BODYCMD_PDFSEP:
			p->pagedat.pdfsep = 1;
			break;
		//@index
		case BODYCMD_INDEX:
			p->linedat.index = body_read_ptr(&p->buf);
			break;
		//@index-page
		case BODYCMD_INDEX_PAGE:
			_proc_index_page(p);
			break;
	}

	return 0;
}

