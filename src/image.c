/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/********************************
 * 画像
 ********************************/

#include <string.h>
#include <math.h>

#include <mlk.h>
#include <mlk_loadimage.h>
#include <mlk_saveimage.h>
#include <mlk_imageconv.h>
#include <mlk_util.h>
#include <mlk_simd.h>

#include "image.h"
#include "app.h"


//-----------------

static mFuncLoadImageCheck g_loadimg_funcs[] = {
	mLoadImage_checkPNG, mLoadImage_checkJPEG, mLoadImage_checkBMP, 0
};

//-----------------


/* 画像を 32bit で読み込み */

static Image *_load_image(const char *filename)
{
	Image *p = NULL;
	mLoadImage li;
	mLoadImageType type;
	int ret;

	mLoadImage_init(&li);

	li.open.type = MLOADIMAGE_OPEN_FILENAME;
	li.open.filename = filename;
	li.convert_type = MLOADIMAGE_CONVERT_TYPE_RGBA;
	li.flags = MLOADIMAGE_FLAGS_TRANSPARENT_TO_ALPHA;

	//ヘッダからタイプ判定

	ret = mLoadImage_checkFormat(&type, &li.open, g_loadimg_funcs, 0);
	if(ret)
	{
		if(ret == MLKERR_UNSUPPORTED)
			app_enderr2("対応していない画像フォーマットです", filename);
		else
			app_enderrno(ret, filename);
	}

	//開く

	ret = (type.open)(&li);
	if(ret) goto ERR;

	//CMYK カラーは読み込まない

	if(li.src_coltype == MLOADIMAGE_COLTYPE_CMYK)
	{
		ret = MLKERR_UNSUPPORTED;
		goto ERR;
	}

	//作成

	p = image_new(li.width, li.height);

	//元の画像の情報

	p->filetype = (type.format_tag == MLOADIMAGE_FORMAT_TAG_JPEG)
		? IMAGE_FILETYPE_JPEG: IMAGE_FILETYPE_RAW;

	switch(li.src_coltype)
	{
		case MLOADIMAGE_COLTYPE_GRAY:
			p->coltype = IMAGE_COLTYPE_GRAY;
			break;
		default:
			p->coltype = IMAGE_COLTYPE_RGB;
	}

	//イメージ読み込み

	li.imgbuf = p->ppbuf;

	ret = (type.getimage)(&li);
	if(ret) goto ERR;

	//

	(type.close)(&li);

	return p;

	//エラー
ERR:
	(type.close)(&li);
	image_free(p);
	app_enderrno(ret, filename);
	return NULL;
}

/* アルファ合成 & ビット数取得 */

static int _get_image_bits(Image *p,int bits)
{
	uint8_t **ppbuf,*ps;
	int ix,iy,a,flags;
	uint8_t r,g,b;

	ppbuf = p->ppbuf;
	flags = 0; //bit0:color, bit1:gray (白/黒以外)

	for(iy = p->height; iy; iy--)
	{
		ps = *(ppbuf++);
		
		for(ix = p->width; ix; ix--, ps += 4)
		{
			//アルファ合成

			r = ps[0];
			g = ps[1];
			b = ps[2];
			a = ps[3];

			if(a != 255)
			{
				if(!a)
					r = g = b = 255;
				else
				{
					r = (r - 255) * a / 255 + 255;
					g = (g - 255) * a / 255 + 255;
					b = (b - 255) * a / 255 + 255;
				}

			}

			//色変換

			if(bits)
			{
				if(bits == 8)
					r = g = b = (r * 77 + g * 150 + b * 29) >> 8;
				else
				{
					//mono: 白以外は黒

					if(r + g + b != 255 * 3)
						r = g = b = 0;
				}
			}

			//

			ps[0] = r;
			ps[1] = g;
			ps[2] = b;

			//色判定

			if(r != g || g != b)
				//color
				flags |= 1;
			else
			{
				//r = g = b
				
				a = r + g + b;

				if(a != 0 && a != 255 * 3)
					flags |= 2;
			}
		}
	}

	//ビット数

	if(flags & 1)
		return 24;
	else if(flags & 2)
		return 8;
	else
		return 1;
}


//=======================


/** 画像の解放 */

void image_free(Image *p)
{
	if(p)
	{
		mFreeArrayBuf((void **)p->ppbuf, p->height);

		mFree(p);
	}
}

/** 画像の作成 */

Image *image_new(int width,int height)
{
	Image *p;

	p = (Image *)mMalloc0(sizeof(Image));
	if(!p) app_enderr_alloc();

	p->width = width;
	p->height = height;

	//16byte アラインメント

	p->ppbuf = (uint8_t **)mAllocArrayBuf_align(width * 4, 16, height);
	if(!p->ppbuf)
	{
		mFree(p);
		app_enderr_alloc();
	}

	return p;
}

/** 画像ファイルを読み込む
 *
 * エラーは処理済み。
 *
 * bits: 色を強制的に変換 (0=変換なし,1,8) */

Image *image_load(const char *filename,int bits)
{
	Image *p;

	//読み込み

	p = _load_image(filename);

	//アルファ合成、色数計算

	p->imgbits = _get_image_bits(p, bits);

	//JPEG 以外のカラータイプ

	if(p->filetype == IMAGE_FILETYPE_RAW)
		p->coltype = (p->imgbits == 24)? IMAGE_COLTYPE_RGB: IMAGE_COLTYPE_GRAY;

	return p;
}


//========================
// 描画
//========================
/* 出力時のビット数を自動で判断させるため、
 * 描画時にビット数が変化するなら、変更する。 */


/** 指定色でクリア */

void image_clear(Image *p,uint32_t col)
{
	uint32_t **ppbuf;
	int ix,iy;

	col = mRGBAtoHostOrder(col);

	ppbuf = (uint32_t **)p->ppbuf;

#if MLK_ENABLE_SSE2

	__m128i a,*pd;
	int w;

	w = (p->width * 4 + 15) / 16; //16byte 単位

	a = _mm_set1_epi32(col);

	for(iy = p->height; iy; iy--)
	{
		pd = (__m128i *)*(ppbuf++);

		for(ix = w; ix; ix--, pd++)
			_mm_store_si128(pd, a);
	}

#else

	uint32_t *pd;

	for(iy = p->height; iy; iy--)
	{
		pd = *(ppbuf++);

		for(ix = p->width; ix; ix--)
			*(pd++) = col;
	}

#endif
}

/** 点をセット */

void image_setpixel(Image *p,int x,int y,uint32_t col)
{
	uint8_t *pd;

	if(x >= 0 && x < p->width && y >= 0 && y < p->height)
	{
		pd = p->ppbuf[y] + x * 4;

		pd[0] = MLK_RGB_R(col);
		pd[1] = MLK_RGB_G(col);
		pd[2] = MLK_RGB_B(col);
	}
}

/** 点をセット (アルファ付き) */

void image_setpixel_alpha(Image *p,int x,int y,uint32_t col,int a)
{
	uint8_t *pd;
	int r,g,b;

	if(x >= 0 && x < p->width && y >= 0 && y < p->height)
	{
		pd = p->ppbuf[y] + x * 4;

		r = pd[0];
		g = pd[1];
		b = pd[2];

		r = (MLK_RGB_R(col) - r) * a / 255 + r;
		g = (MLK_RGB_G(col) - g) * a / 255 + g;
		b = (MLK_RGB_B(col) - b) * a / 255 + b;

		pd[0] = r;
		pd[1] = g;
		pd[2] = b;
	}
}

/** 水平線の描画 */

void image_drawline_horz(Image *p,int x,int y,int w,uint32_t col)
{
	uint32_t *pd;

	if(y < 0 || y >= p->height) return;

	if(x < 0) w += x, x = 0;
	if(x + w > p->width) w = p->width - x;

	if(w <= 0) return;

	//

	col = mRGBAtoHostOrder(col);

	pd = (uint32_t *)p->ppbuf[y] + x;

	for(; w; w--)
		*(pd++) = col;
}

/** 垂直線の描画 */

void image_drawline_vert(Image *p,int x,int y,int h,uint32_t col)
{
	uint32_t **ppbuf;

	if(x < 0 || x >= p->width) return;

	if(y < 0) h += y, y = 0;
	if(y + h > p->height) h = p->height - y;

	if(h <= 0) return;

	//

	col = mRGBAtoHostOrder(col);

	ppbuf = (uint32_t **)p->ppbuf + y;

	for(; h; h--, ppbuf++)
		*(*ppbuf + x) = col;
}

/** 水平線(AA)の描画 */

void image_drawline_horz_aa(Image *p,int x,double y,int w,double h,uint32_t col)
{
	int i,sy,ey,sa,ea;

	sy = floor(y * 256);
	ey = floor((y + h) * 256);
	if(ey > sy) ey--;

	if((sy >> 8) == (ey >> 8))
		sa = ey - sy;
	else
	{
		sa = 255 - (sy & 255);
		ea = ey & 255;
	}

	sy >>= 8;
	ey >>= 8;

	//

	for(; w > 0; w--, x++)
	{
		if(sy == ey)
			image_setpixel_alpha(p, x, sy, col, sa);
		else
		{
			image_setpixel_alpha(p, x, sy, col, sa);

			for(i = sy + 1; i < ey; i++)
				image_setpixel(p, x, i, col);

			image_setpixel_alpha(p, x, ey, col, ea);
		}
	}
}

/** 垂直線(AA)の描画 */

void image_drawline_vert_aa(Image *p,double x,int y,double w,int h,uint32_t col)
{
	int i,sx,ex,sa,ea;

	sx = floor(x * 256);
	ex = floor((x + w) * 256);
	if(ex > sx) ex--;

	if((sx >> 8) == (ex >> 8))
		sa = ex - sx;
	else
	{
		sa = 255 - (sx & 255);
		ea = ex & 255;
	}

	sx >>= 8;
	ex >>= 8;

	//

	for(; h > 0; h--, y++)
	{
		if(sx == ex)
			image_setpixel_alpha(p, sx, y, col, sa);
		else
		{
			image_setpixel_alpha(p, sx, y, col, sa);

			for(i = sx + 1; i < ex; i++)
				image_setpixel(p, i, y, col);

			image_setpixel_alpha(p, ex, y, col, ea);
		}
	}
}

/** 矩形塗りつぶし */

void image_fillbox(Image *p,int x,int y,int w,int h,uint32_t col)
{
	uint32_t **ppbuf,*pd;
	int ix,iy;

	if(x < 0) w += x, x = 0;
	if(y < 0) h += y, y = 0;
	if(x + w >= p->width) w = p->width - x;
	if(y + h >= p->height) h = p->height - y;

	if(w <= 0 || h <= 0) return;

	//

	col = mRGBAtoHostOrder(col);
	ppbuf = (uint32_t **)p->ppbuf + y;

	for(iy = h; iy; iy--, ppbuf++)
	{
		pd = *ppbuf + x;
		
		for(ix = w; ix; ix--)
			*(pd++) = col;
	}
}

/** 全体にタイル状に敷き詰める */

void image_fill_tile(Image *p,Image *src)
{
	uint32_t **ppdst,**ppsrc,*pd,*psY;
	int ix,iy,sw,sh,dw,dh;

	if(p->imgbits < src->imgbits)
		p->imgbits = src->imgbits;

	//

	ppsrc = (uint32_t **)src->ppbuf;
	sw = src->width;
	sh = src->height;

	ppdst = (uint32_t **)p->ppbuf;
	dw = p->width;
	dh = p->height;

	for(iy = 0; iy < dh; iy++)
	{
		pd = *(ppdst++);
		psY = ppsrc[iy % sh];

		for(ix = 0; ix < dw; ix++, pd++)
		{
			*pd = *(psY + ix % sw);
		}
	}
}

/** 全体を転送
 *
 * はみ出す部分は描画しない */

void image_blt(Image *p,int x,int y,Image *src)
{
	uint8_t **ppdst,**ppsrc;
	int w,h,sx,sy;

	if(p->imgbits < src->imgbits)
		p->imgbits = src->imgbits;

	//

	sx = sy = 0;
	w = src->width;
	h = src->height;

	//クリッピング

	if(x < 0) w += x, sx = -x, x = 0;
	if(y < 0) h += y, sy = -y, y = 0;

	if(x + w >= p->width) w = p->width - x;
	if(y + h >= p->height) h = p->height - y;

	if(w <= 0 || h <= 0) return;

	//

	ppdst = p->ppbuf + y;
	ppsrc = src->ppbuf + sy;
	w *= 4;
	x *= 4;
	sx *= 4;

	for(; h; h--)
	{
		memcpy(*ppdst + x, *ppsrc + sx, w);

		ppdst++;
		ppsrc++;
	}
}

/** 文字の枠を描画
 *
 * 用紙位置を元にして座標を決定 */

void image_draw_grid(Image *p,int x,int y,int w,int h,int len,int feed,int num,
	int fhorz,double mulw,double mulh,int pagex,uint32_t col)
{
	int ix,iy,n1,n2,top,nlen;

	p->imgbits = 24;

	if(fhorz)
	{
		//---- 横組

		top = floor(x * mulw);
		nlen = floor((x + len) * mulw) - top;
		top += pagex;

		for(iy = y; num; num--, iy += feed)
		{
			//横線

			n1 = floor(iy * mulh);
			n2 = floor((iy + h - 1) * mulh);

			image_drawline_horz(p, top, n1, nlen, col);
			image_drawline_horz(p, top, n2, nlen, col);

			//縦線

			n1++;
			n2--;
			n2 = n2 - n1 + 1;

			image_drawline_vert(p, top, n1, n2, col);

			for(ix = w; ix <= len; ix += w)
			{
				image_drawline_vert(p, (int)floor((x + ix) * mulw) + pagex - 1, n1, n2, col);
			}
		}
	}
	else
	{
		//---- 縦組

		top = floor(y * mulh);
		nlen = floor((y + len) * mulh) - top;

		for(ix = x; num; num--, ix -= feed)
		{
			//縦線

			n1 = (int)floor((ix + 1 - w) * mulw) + pagex;
			n2 = (int)floor(ix * mulw) + pagex;

			image_drawline_vert(p, n1, top, nlen, col);
			image_drawline_vert(p, n2, top, nlen, col);

			//横線

			n1++;
			n2--;
			n2 = n2 - n1 + 1;

			image_drawline_horz(p, n1, top, n2, col);

			for(iy = h; iy <= len; iy += h)
			{
				image_drawline_horz(p, n1, (int)floor((y + iy) * mulh) - 1, n2, col);
			}
		}
	}
}


//==========================
// 画像ファイル書き込み
//==========================


/* Y1行イメージセット */

static mlkerr _save_setrow(mSaveImage *p,int y,uint8_t *buf,int line_bytes)
{
	uint8_t *ps;

	ps = *((uint8_t **)p->param1 + y);

	switch(p->coltype)
	{
		//RGB
		case MSAVEIMAGE_COLTYPE_RGB:
			mImageConv_rgbx8_to_rgb8(buf, ps, p->width);
			break;

		//GRAYSCALE (1/8bit)
		case MSAVEIMAGE_COLTYPE_GRAY:
			if(p->bits_per_sample == 8)
				mImageConv_rgbx8_to_gray8(buf, ps, p->width);
			else
				mImageConv_rgbx8_to_gray1(buf, ps, p->width);
			break;
	}

	return MLKERR_OK;
}

/* Y1行イメージセット (PSD) */

static mlkerr _save_setrow_ch(mSaveImage *p,int y,int ch,uint8_t *buf,int line_bytes)
{
	uint8_t *ps;
	int i;

	ps = *((uint8_t **)p->param1 + y) + ch;

	if(p->coltype == MSAVEIMAGE_COLTYPE_GRAY
		&& p->bits_per_sample == 1)
	{
		//GRAY 1bit

		mImageConv_rgbx8_to_gray1(buf, ps, p->width);
	}
	else
	{
		//GRAY 8bit or RGB
	
		for(i = p->width; i; i--, ps += 4)
			*(buf++) = *ps;
	}

	return MLKERR_OK;
}

/** 画像ファイル書き込み
 *
 * bits: 1,8,24
 * dpi: 0 でなし */

void image_savefile(Image *p,const char *filename,int format,int bits,int dpi)
{
	mSaveImage si;
	mFuncSaveImage func;
	mSaveImageOpt opt;
	int ret;

	mSaveImage_init(&si);

	si.open.type = MSAVEIMAGE_OPEN_FILENAME;
	si.open.filename = filename;
	si.width = p->width;
	si.height = p->height;
	si.bits_per_sample = 8;
	si.setrow = _save_setrow;
	si.setrow_ch = _save_setrow_ch;
	si.param1 = p->ppbuf;

	//解像度

	if(dpi)
	{
		si.reso_unit = MSAVEIMAGE_RESOUNIT_DPI;
		si.reso_horz = dpi;
		si.reso_vert = dpi;
	}

	//カラータイプ

	if(bits == 24 || format == OUT_FORMAT_BMP)
	{
		//RGB

		si.coltype = MSAVEIMAGE_COLTYPE_RGB;
		si.samples_per_pixel = 3;
	}
	else if(bits == 1 && format != OUT_FORMAT_JPEG)
	{
		//GRAY 1bit (PNG/PSD)

		si.coltype = MSAVEIMAGE_COLTYPE_GRAY;
		si.bits_per_sample = 1;
		si.samples_per_pixel = 1;
	}
	else
	{
		//GRAY 8bit (PNG/JPEG/PSD)

		si.coltype = MSAVEIMAGE_COLTYPE_GRAY;
		si.samples_per_pixel = 1;
	}

	//フォーマット

	opt.mask = 0;

	switch(format)
	{
		case OUT_FORMAT_BMP:
			func = mSaveImageBMP;
			break;
		//JPEG
		case OUT_FORMAT_JPEG:
			func = mSaveImageJPEG;

			opt.mask = MSAVEOPT_JPEG_MASK_QUALITY | MSAVEOPT_JPEG_MASK_SAMPLING_FACTOR;
			opt.jpg.quality = g_app->jpeg_quality;
			opt.jpg.sampling_factor = g_app->jpeg_sampling;
			break;
		case OUT_FORMAT_PSD:
			func = mSaveImagePSD;
			break;
		default:
			func = mSaveImagePNG;
			break;
	}

	//保存

	ret = (func)(&si, &opt);

	if(ret)
		app_enderrno(ret, filename);
}
