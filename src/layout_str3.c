/*$
 Copyright (c) 2022-2023 Azel

 This file is part of txtnovel.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/*********************************
 * レイアウト処理: ページの確定後
 *********************************/

#include <stdio.h>
#include <math.h>

#include <mlk.h>
#include <mlk_list.h>

#include "app.h"
#include "setting.h"
#include "layout.h"
#include "font/font.h"


/** 行位置から、次のページの文字のフラグを ON */

void layout_set_nextpage_flag(LayoutData *p)
{
	StrData *pstr;
	int maxline;

	if(p->pagedat.linenum_page == p->pagedat.linenum) return;

	maxline = p->pagedat.linenum_page;

	MLK_LIST_FOR(p->list_str, pstr, StrData)
	{
		if(pstr->lineno >= maxline)
			pstr->flags |= STR_FLAG_NEXT_PAGE;
	}
}


//================================
// 描画位置をセット
//================================


/* 行送り方向の配置 */

static void _set_drawpos_page_align(LayoutData *p)
{
	StrData *pstr;
	layoutpos w;

	if(p->pagedat.body_align == LAYOUT_ALIGN_NORMAL
		|| p->pagedat.linenum_page == p->layout->line_num)
		return;

	//ずらす幅
	
	w = layout_get_lines_width(p, 0) - layout_get_lines_width(p, p->pagedat.linenum_page);

	if(p->pagedat.body_align == LAYOUT_ALIGN_CENTER)
		w /= 2;

	//

	MLK_LIST_FOR(p->list_str, pstr, StrData)
	{
		pstr->y += w;
	}
}

/* StrChar の位置をセット
 *
 * 次のページの文字は除く。 */

static void _set_drawpos_strchar(LayoutData *p)
{
	StrData *pstr;
	StrChar *pch;
	int i;
	layoutpos x,y,w;

	for(pstr = (StrData *)p->list_str.top; pstr; pstr = (StrData *)pstr->i.next)
	{
		if(pstr->flags & STR_FLAG_NEXT_PAGE) break;

		//位置のセット

		pch = pstr->chtop;
		x = pstr->x;
		y = pstr->y;

		if(pstr->classno == STRCLASS_TATEYOKO)
		{
			//---- 縦中横
			
			//y 位置を先頭文字の位置へ
			// :フォントサイズの中央揃えはすでに行われている。

			w = 0;
			for(i = pstr->len; i; i--, pch = (StrChar *)pch->i.next)
				w += pch->width;
			
			y += pstr->fontsize - (pstr->fontsize - w) / 2;

			//位置セット (グリフの左上の位置)

			pch = pstr->chtop;

			for(i = pstr->len; i; i--, pch = (StrChar *)pch->i.next)
			{
				pch->x = x;
				pch->y = y;

				y -= pch->width;
			}
		}
		else
		{
			//---- 通常文字
		
			for(i = pstr->len; i; i--, pch = (StrChar *)pch->i.next)
			{
				x += pch->sp_before;

				pch->x = x;
				pch->y = y;

				x += pch->width + pch->sp_after;
			}
		}
	}
}

/* RubyChar の位置をセット
 *
 * 次のページの文字は除く。
 * 空白文字はそのまま含む。 */

static void _set_drawpos_rubychar(LayoutData *p)
{
	StrData *pstr;
	RubyData *pr;
	RubyChar *pch;
	int i,size;
	layoutpos x,y;

	size = p->ruby_fontsize;

	MLK_LIST_FOR(p->list_ruby, pr, RubyData)
	{
		pstr = pr->parent;

		if(pstr->flags & STR_FLAG_NEXT_PAGE) break;

		//

		pch = pr->chtop;
		x = pstr->x + pr->ruby_shift;
		y = pstr->y;

		for(i = pr->len; i; i--, pch = (RubyChar *)pch->i.next)
		{
			x += pch->sp_before;

			pch->x = x;
			pch->y = y - size;

			x += pch->width + pch->sp_after;
		}
	}
}


//=================================
// ほか
//=================================


/* 濁点合成と圏点を文字として追加
 *
 * ページの描画位置セット後に行う。
 * 次ページの分は除く。 */

static void _add_subchar(LayoutData *p)
{
	StrData *pstr,*pnewstr,*pins;
	StrChar *pch,*pnewch;
	KentenItem *pkt;
	int i,xx,yy;
	double d;

	pstr = pins = (StrData *)p->list_str.top;

	//次ページの分の後に追加されないように、先頭文字の前に追加していく

	for( ; pstr; pstr = (StrData *)pstr->i.next)
	{
		if(pstr->flags & STR_FLAG_NEXT_PAGE) break;

		pch = pstr->chtop;

		for(i = pstr->len; i; i--, pch = (StrChar *)pch->i.next)
		{
			//濁点合成
			// :本文文字と同じフォント

			if(pch->flags & STRCHAR_FLAG_DAKUTEN)
			{
				pnewstr = layout_str_addset_subchar(p, pins, 0x3099, pstr->font, pstr->fontsize, FALSE);

				pnewch = pnewstr->chtop;

				d = pstr->fontsize / 1000.0;

				if(p->is_horz)
				{
					xx = lround(pstr->font->dakuten_horz_x * d);
					yy = lround(pstr->font->dakuten_horz_y * d);

					pnewch->x = pch->x + xx;
					pnewch->y = pch->y + yy;
				}
				else
				{
					//縦組: 右端

					xx = lround(pstr->font->dakuten_vert_x * d);
					yy = lround(pstr->font->dakuten_vert_y * d);

					pnewch->x = pch->x + yy;
					pnewch->y = pch->y - xx;
				}
			}

			//圏点

			if(pch->kenten)
			{
				pkt = pch->kenten;
				
				pnewstr = layout_str_addset_subchar(p, pins, pkt->code,
					pkt->font.item, pkt->font.size, TRUE);

				pnewch = pnewstr->chtop;

				if(pkt->pos)
					yy = lround(pkt->pos / 1000.0 * pkt->font.size);
				else
					yy = 0;

				pnewch->x = pch->x + (pch->width - pnewch->width) / 2;
				pnewch->y = pch->y - pnewch->width - yy;
			}
		}
	}
}

/* list_str2 ソート関数 */

static int _sortfunc_font(mListItem *item1,mListItem *item2,void *param)
{
	StrData *p1,*p2;

	p1 = (StrData *)item1;
	p2 = (StrData *)item2;

	if(p1->font < p2->font)
		return -1;
	else if(p1->font > p2->font)
		return 1;
	else if(p1->fontsize < p2->fontsize)
		return -1;
	else if(p1->fontsize > p2->fontsize)
		return 1;
	else if(p1->y < p2->y)
		return -1;
	else if(p1->y > p2->y)
		return 1;
	else if(p1->x < p2->x)
		return -1;
	else if(p1->x > p2->x)
		return 1;
	else
		return 0;
}

/* レイアウトの基本フォントと異なる文字を、list_str2 に移動
 *
 * 次ページの分は対象外 */

static void _move_not_basefont(LayoutData *p)
{
	mList *list,*list2;
	StrData *pstr,*next;
	FontItem *basefont;
	int32_t basesize;

	list = &p->list_str;
	list2 = &p->list_str2;

	basefont = p->layout->basefont.item;
	basesize = p->base_fontsize;

	for(pstr = (StrData *)list->top; pstr; pstr = next)
	{
		next = (StrData *)pstr->i.next;
	
		if(pstr->flags & STR_FLAG_NEXT_PAGE) break;

		if(pstr->font != basefont || pstr->fontsize != basesize)
		{
			mListRemoveItem(list, MLISTITEM(pstr));

			mListAppendItem(list2, MLISTITEM(pstr));
		}
	}

	//ソート

	mListSort(list2, _sortfunc_font, 0);
}

/** ページ確定後の処理
 *
 * ※次ページの分は対象外。 */

void layout_proc_page(LayoutData *p)
{
	if(!p->list_str.top) return;

	//行送り方向の配置

	_set_drawpos_page_align(p);

	//StrChar の位置をセット

	_set_drawpos_strchar(p);

	//RubyChar の位置をセット

	_set_drawpos_rubychar(p);

	//濁点合成、圏点の文字を追加

	_add_subchar(p);

	//基本フォントと異なるものを別リストに移動

	_move_not_basefont(p);
}

/** 現在のページのデータを削除する
 *
 * 次ページの分は残す */

void layout_delete_curpage(LayoutData *p)
{
	StrData *pstr,*next;
	layoutpos width;
	int maxline;

	//残っているデータがなければ、すべて削除

	if(p->pagedat.linenum == p->pagedat.linenum_page)
	{
		mListDeleteAll(&p->list_str);
		mListDeleteAll(&p->list_str2);
		mListDeleteAll(&p->list_strch);
		mListDeleteAll(&p->list_ruby);
		mListDeleteAll(&p->list_rubych);

		return;
	}

	//別リストに移動した分の削除

	for(pstr = (StrData *)p->list_str2.top; pstr; pstr = next)
	{
		next = (StrData *)pstr->i.next;

		layout_str_delete(p, &p->list_str2, pstr);
	}

	//list_str

	maxline = p->pagedat.linenum_page;
	width = p->layout->line_feed.val * maxline;

	for(pstr = (StrData *)p->list_str.top; pstr; pstr = next)
	{
		next = (StrData *)pstr->i.next;
	
		if(pstr->flags & STR_FLAG_NEXT_PAGE)
		{
			//次のページ
			
			pstr->y -= width;
			pstr->lineno -= maxline;
			pstr->flags &= ~STR_FLAG_NEXT_PAGE;
		}
		else
		{
			//削除

			layout_str_delete(p, NULL, pstr);
		}
	}
}


//===========================
// 柱・ノンブル
//===========================


/* フラグによる判定
 *
 * return: 対象なら 1 */

static int _judge_title_number_flags(LayoutData *p,int flags,int fimage)
{
	if(flags & TITLENUMBER_PAGE_ALL)
		//すべてのページ
		return 1;
	else if((flags & TITLENUMBER_PAGE_EVEN) && !layout_is_page_odd(p))
		//偶数ページ
		return 1;
	else if((flags & TITLENUMBER_PAGE_BLANK) && !layout_has_page_char(p))
		//空白ページ
		return 1;
	else if(fimage && (flags & TITLENUMBER_PAGE_IMAGE))
		//画像ページ
		return 1;

	return 0;
}

/** (出力時) ノンブルと柱を表示するか
 *
 * ページ位置や内容による判定。
 *
 * fimage: 画像ページか
 * return: 0bit=ノンブル、1bit=柱、2bit=隠しノンブル */

int layout_get_title_number_show(LayoutData *p,int fimage)
{
	int ret = 3;

	//2段組で下段の場合は、常になし
	// :ノンブル・柱は、上段のみにセットする。
	// :上段が出力されずに下段だけ出力されるということはない。

	if(p->is_column && p->cur_column)
		return 0;

	//ノンブル非表示

	if(_judge_title_number_flags(p, g_set->number.no_page, fimage))
		ret &= ~1;

	//柱非表示

	if(_judge_title_number_flags(p, g_set->title.no_page, fimage))
		ret &= ~2;

	//隠しノンブルにするページ

	if(_judge_title_number_flags(p, g_set->number_hide_page, fimage))
		ret |= 5;

	//ノンブル、ページごとの強制
	
	switch(p->pagedat.number_type)
	{
		case BODY_PAGENUM_YES:
			ret |= 1;
			ret &= ~4;
			break;
		case BODY_PAGENUM_NO:
			ret &= ~1;
			break;
		case BODY_PAGENUM_HIDE:
			ret |= 5;
			break;
	}

	return ret;
}

/** 横書きの通常文字を追加 (ノンブル/柱)
 *
 * ch: NULL で code の Unicode を使う
 * half: グリフを半角幅に置き換え */

NormalChar *layout_add_normalchar(mList *list,FontSpec *font,BodyChar *ch,uint32_t code,int half)
{
	NormalChar *pi;
	Font *pf;
	BodyChar bc;

	pi = (NormalChar *)mListAppendNew(list, sizeof(NormalChar));
	if(!pi) app_enderr_alloc();

	if(ch)
		bc = *ch;
	else
	{
		bc.type = BODYCHAR_TYPE_UNICODE;
		bc.code = code;
	}

	pf = font->item->font;

	//GID

	pi->gid = layout_get_char_to_gid(pf, &bc, &code);

	if(half)
		pi->gid = font_get_gsub(pf, FONT_DATA_GSUB_HWID, pi->gid);

	//幅

	pi->width = font_regist_glyph(pf, pi->gid, code, font->size, FALSE);

	return pi;
}

/** ノンブルの文字リストを作成
 *
 * hide: 隠しノンブルか
 * return: 横書き時のノンブルの幅 (隠しノンブルでは 0) */

int layout_set_number_char(LayoutData *p,mList *list,int hide)
{
	TitleNumberSet *dat;
	FontSpec *fs;
	NormalChar *pch;
	char m[16],*pc;
	layoutpos x,y,w,max;
	int is_odd;

	dat = &g_set->number;
	fs = &dat->font;

	is_odd = layout_is_page_odd(p);

	snprintf(m, 16, "%d", p->pageno);

	//位置

	if(!hide)
	{
		//通常:横書き

		for(pc = m, w = 0; *pc; pc++)
		{
			pch = layout_add_normalchar(list, fs, NULL, *pc, FALSE);

			w += pch->width;
		}

		//
		
		x = (is_odd)? dat->hspace.val: p->base_layout_width - dat->hspace.val - w;

		y = (dat->pos == TITLENUMBER_POS_HEAD)?
			-(dat->vspace.val) - fs->height: p->base_layout_height + dat->vspace.val;

		for(pch = (NormalChar *)list->top; pch; pch = (NormalChar *)pch->i.next)
		{
			pch->x = x;
			pch->y = y;
			x += pch->width;
		}

		return w;
	}
	else
	{
		//隠しノンブル
		// :縦に横書き。用紙ののど側・下端から指定 mm 離した位置
		// :裁ち切り含む、用紙上の絶対位置でセット。

		for(pc = m, max = 0, w = 0; *pc; pc++)
		{
			pch = layout_add_normalchar(list, fs, NULL, *pc, TRUE);

			if(max < pch->width) max = pch->width;
			w += fs->height;
		}

		//位置

		if(is_odd)
			x = convert_mm_to_pt(g_set->bleed + g_set->paper_w - g_set->number_hide_x) - max;
		else
			x = convert_mm_to_pt(g_set->bleed + g_set->number_hide_x);

		y = convert_mm_to_pt(g_set->bleed + g_set->paper_h - g_set->number_hide_y) - w;

		//

		for(pch = (NormalChar *)list->top; pch; pch = (NormalChar *)pch->i.next)
		{
			pch->x = x;
			pch->y = y;
			
			y += fs->height;
		}

		return 0;
	}
}

/** 柱の文字位置をセット
 *
 * num_w: ノンブルの全体の幅 (隠しノンブルでは 0) */

void layout_set_title_drawpos(LayoutData *p,int num_w)
{
	TitleNumberSet *dat;
	NormalChar *pch;
	layoutpos x,y,w,xx;

	dat = &g_set->title;

	//端の余白

	if(num_w && g_set->title.pos == g_set->number.pos)
		xx = g_set->number.hspace.val + num_w + g_set->title_number_space.val;
	else
		xx = dat->hspace.val;

	//x 位置

	if(layout_is_page_odd(p))
		x = xx;
	else
	{
		w = 0;
		MLK_LIST_FOR(p->list_title, pch, NormalChar)
		{
			w += pch->width;
		}
		
		x = p->base_layout_width - xx - w;
	}

	//y 位置

	y = (dat->pos == TITLENUMBER_POS_HEAD)?
		-(dat->vspace.val) - g_set->title.font.height: p->base_layout_height + dat->vspace.val;

	//セット

	MLK_LIST_FOR(p->list_title, pch, NormalChar)
	{
		pch->x = x;
		pch->y = y;
		x += pch->width;
	}
}

