function add_div(name) {
	o = document.createElement('div');
	o.id = name;
	document.body.appendChild(o);
	return o
}

function menu_init() {
	var linkstr = ['cmdopt;コマンドラインオプション',
	'output;出力ファイルについて', 'text;テキストファイルの書き方', 'text_set;設定部分の書き方',
	'text_body;本文部分の書き方', 'cmd_set;設定コマンド (レイアウト以外)', 'cmd_layout;設定コマンド (レイアウト)',
	 'cmd_body;本文コマンド', 'faq;FAQ', 'calc;計算機'];

	o = add_div('jmenu_icon');
	o.onclick = menu_show;
	o.innerHTML = '▼ MENU';

	o = add_div('jmenu');
	o.style.display = 'none';

	t = '<a href="#" onclick="return menu_hide()" style="cursor:pointer">▲ MENU<br><br>'
	len = linkstr.length;

	for(i = 0; i < len; i++) {
		s = linkstr[i].split(';');
		t += '<a href="' + s[0] + '.html">' + s[1] + '</a><br>';
	}

	o.innerHTML = t;
}

function menu_show() {
	document.getElementById('jmenu_icon').style.display = 'none';
	document.getElementById('jmenu').style.display = 'block';
}

function menu_hide() {
	document.getElementById('jmenu_icon').style.display = 'block';
	document.getElementById('jmenu').style.display = 'none';
	return false;
}
