# txtnovel

http://azsky2.html.xdomain.jp/

TeX と同じような形で、独自のフォーマットで記述されたテキストファイルの内容から、主に小説を組版して、PDF または画像に出力します。

- 縦書きがメインなので、右綴じのみです。
- PDF/BMP/PNG/JPEG/PSD で出力できます。
- PDF 出力時は、フォントをサブセットで埋め込みます。
- 複数のレイアウトを定義して、ページごとに切り替えられます。<br>
これにより、一部のページを横書きにすることもできます。
- 2段組、濁点合成、全角ダッシュを線で描画、複数のフォント使用、画像ページの挿入、PDF 分割など、小説に必要な機能はほとんど用意しています。
- TeX と比べると、高速に処理できます。<br>
A6 400P 程度でも、１〜数秒で PDF を出力できます。（画像出力の場合は少し遅くなります）

詳しい使い方は、manual/index.html をご覧ください。

## サンプル

[sample.pdf (機能説明用サンプルPDF)](http://azsky2.html.xdomain.jp/soft/txtnovel/sample.pdf)<br>
[ichiya.pdf (小説サンプルPDF)](http://azsky2.html.xdomain.jp/soft/txtnovel/ichiya.pdf)

## 動作環境

Linux、macOS、ほか Unix 系 OS

## コンパイル/インストール

ninja, pkg-config コマンドが必要です。<br>
ほか、各開発用ファイルのパッケージも必要になるので、詳しくは ReadMe をご覧ください。

~~~
$ ./configure
$ cd build
$ ninja
# ninja install
~~~
